# -*- coding: utf-8 -*-
"""
Created on Mon Aug 28 10:40:19 2017

@author: Satish
"""

import gettext
#t = gettext.translation('AVKIS-Web','locale')
#t=gettext.translation('AVKIS-Web', localedir='locale')
t=gettext.translation('AVKIS-Web', localedir='locale', languages=['de_DE'])
_ = t.gettext

# ...
print(_('This is a translatable string.'))