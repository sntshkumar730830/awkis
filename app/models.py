# -*- coding: utf-8 -*-
#from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
from . import login_manager


class tfidf(db.Model):
    __tablename__ = 'TFIDF'
    AID = db.Column(db.Text, index=True, primary_key=True)
    Name = db.Column(db.Text)
    Business_Activity = db.Column(db.Text)
    Branch_Code_Main = db.Column(db.Text)
    Branch_Codes_Secondaries = db.Column(db.Text)
    Internet = db.Column(db.Text)
    Last_Change = db.Column(db.Text)

class tfidf_stemmer(db.Model):
    __tablename__ = 'TFIDF_stemmer'
    AID = db.Column(db.Text, index=True, primary_key=True)
    Name = db.Column(db.Text)
    Business_Activity = db.Column(db.Text)
    Branch_Code_Main = db.Column(db.Text)
    Branch_Codes_Secondaries = db.Column(db.Text)
    Internet = db.Column(db.Text)

class Address(db.Model):
    __tablename__ = 'Address'
    AID = db.Column(db.Text, index=True, primary_key=True)
    AD_Address_ID = db.Column(db.Text)
    Address_Addendum = db.Column(db.Text)
    Street_Nr = db.Column(db.Text)
    PLZ = db.Column(db.Text)
    City = db.Column(db.Text)
    County = db.Column(db.Text)
    State = db.Column(db.Text)
    Country = db.Column(db.Text)
    GKZ_X = db.Column(db.Text)
    GKZ_Y = db.Column(db.Text)
    GKZ_Street = db.Column(db.Text)
    Last_Change = db.Column(db.Text)

class Contact(db.Model):
    __tablename__ = 'Contact'
    id = db.Column(db.Integer, primary_key=True)
    AID = db.Column(db.Text)
    Contact_Type = db.Column(db.Text)
    Contact_Value = db.Column(db.Text)

class User(UserMixin, db.Model):
    __tablename__ = "User"
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(64), unique = True, index = True)
    username = db.Column(db.String(64), unique = True, index = True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Integer)
    time_out=db.Column(db.String(64))
    smart_buyer_matching=db.Column(db.String(64))
    buyer_portal_search=db.Column(db.String(64))
    manual_company_search=db.Column(db.String(64))

    @property
    def password(self):
        raise AttributeError("Passwort nicht lesbar")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

class financials(db.Model):
    __tablename__ = 'Financials'

    AID = db.Column(db.Text)
    Year = db.Column(db.Text)
    Fin_Value_Type = db.Column(db.Text)
    id = db.Column(db.Integer, primary_key=True)
    Fin_Value = db.Column()

class Branch_Codes(db.Model):
    __tablename__ = 'Branch_Codes'


    AID = db.Column(db.Text, index = True)
    Branch_Code = db.Column(db.Text,primary_key=True)
    Branch_Rank_Main_Sec = db.Column(db.Text)
    Source = db.Column(db.Text)
    Last_Change = db.Column(db.Text)


class Entity(db.Model):
    __tablename__ = 'Entity'

    ID = db.Column(db.Integer, primary_key=True)
    AID = db.Column(db.Text)
    Entity_Last_Name = db.Column(db.Text)
    Entity_First_Name = db.Column(db.Text)
    Address_Ownership = db.Column(db.Text)
    Entity_Type = db.Column(db.Text)
    Legal_Form = db.Column(db.Text)
    Salutation = db.Column(db.Text)
    Titel = db.Column(db.Text)
    Education = db.Column(db.Text)
    Gender = db.Column(db.Text)
    Birth_Year = db.Column(db.Text)
    Comparison_Group_Code = db.Column(db.Text)
    Comparison_Group_Size = db.Column(db.Text)
    Count_Shareholders = db.Column(db.Text)
    Count_Group_Companies = db.Column(db.Text)
    Count_Subsidiaries = db.Column(db.Text)
    ROC_Register_ID = db.Column(db.Text)
    UID = db.Column(db.Text)
    Last_Change = db.Column(db.Text)
    Reserved = db.Column(db.Text)
    Expiry_Date = db.Column(db.Text)


class role(db.Model):
    __tablename__ = 'Role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)

class Business_Activity(db.Model):
    __tablename__ = 'Business_Activity'

    AID = db.Column(db.Text)
    Activity_Description_ROC = db.Column(db.Text, primary_key=True)
    Last_Change = db.Column(db.Text)

class Branch_Text(db.Model):
    __tablename__ = 'Branch_Text'
    Branch_Code = db.Column(db.Text, primary_key=True)
    Branch_Text = db.Column(db.Text)


class websites(db.Model):
    __tablename__ = 'Websites'

    AID = db.Column(db.Text, primary_key=True)
    Content = db.Column(db.Text)
    Weblink = db.Column(db.Text)

class web_search_result(db.Model):

    __tablename__ = 'web_search_result'

    Id = db.Column(db.Integer, primary_key=True)
    First_Crawl_Date = db.Column(db.Text)
    Chiffre = db.Column(db.Text)
    Description = db.Column(db.Text)
    Count_Employees = db.Column(db.Text)
    Last_Turnover = db.Column(db.Text)
    Asking_Price = db.Column(db.Text)
    Activity = db.Column(db.Text)
    Last_Crawl_Date = db.Column(db.Text)
    Reference_URL = db.Column(db.Text)
    email = db.Column(db.Text)
    Latest_Crawl=db.Column(db.Text)
    Active_Status=db.Column(db.Text)

    def __init__(self,daturn,Chiffre,Branches,Description,Count_Employees,Last_Turnover, \
                 Asking_Price,Activity):
        self.daturn = daturn
        self.Chiffre = Chiffre
        self.Branches = Branches
        self.Description = Description
        self.Count_Employees = Count_Employees
        self.Last_Turnover = Last_Turnover
        self.Asking_Price = Asking_Price
        self.tatigteit = Activity

class Web_Branch_Data(db.Model):

    __tablename__ = 'Web_Branch_Data'

    Id = db.Column(db.Integer, primary_key=True)
    Chiffre = db.Column(db.Text)
    Branch_Level1 = db.Column(db.Text)
    Branch_Level2 = db.Column(db.Text)
    Branch_Level3 = db.Column(db.Text)

class web_location_data(db.Model):

    __tablename__ = 'web_location_data'

    Id = db.Column(db.Integer, primary_key=True)
    Chiffre = db.Column(db.Text)
    State = db.Column(db.Text)
    Region = db.Column(db.Text)
    District = db.Column(db.Text)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class web_data_storage(db.Model):

    __tablename__ = 'web_data_storage'

    Id = db.Column(db.Integer, primary_key=True)
    branchcode = db.Column(db.Text)
    langtitel = db.Column(db.Text)
    einschlusse = db.Column(db.Text)
    ausschlusse = db.Column(db.Text)

class Shareholder(db.Model):

    __tablename__ = 'Shareholder'

    Id = db.Column(db.Integer, primary_key=True)
    AID_Company = db.Column(db.Text)
    AID_SHH = db.Column(db.Text)
    SHH_Function = db.Column(db.Text)
    SHH_Shares_Owned = db.Column(db.String)
    SHH_Position = db.Column(db.Text)
    SHH_Function_Mailings = db.Column(db.Text)
    Last_Change = db.Column(db.Text)

class Manager(db.Model):

    __tablename__ = 'Manager'

    Id = db.Column(db.Integer, primary_key=True)
    AID_Company = db.Column(db.Text)
    AID_MAN = db.Column(db.Text)
    MAN_Function = db.Column(db.Text)
    MAN_Position = db.Column(db.Text)
    MAN_Function_Mailings = db.Column(db.Text)
    Last_Change = db.Column(db.Text)

class export_log(db.Model):

    __tablename__ = "Export_log"

    Id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Text)
    user_email = db.Column(db.Text)
    AID = db.Column(db.Text)
    keywords = db.Column(db.Text)
    date = db.Column(db.Text)
    time = db.Column(db.Text)

class preferences(db.Model):

    __tablename__ = "preferences"

    Id = db.Column(db.Integer, primary_key=True)
    column_name = db.Column(db.Text)
    value = db.Column(db.Text)

class Portal_DUB_web_crawl(db.Model):

    __tablename__ = "Portal_DUB_web_crawl"

    Id = db.Column(db.Integer, primary_key=True)
    Description = db.Column(db.Text)
    Branches = db.Column(db.Text)
    Country = db.Column(db.Text)
    PLZ = db.Column(db.Text)
    Turnover = db.Column(db.Text)
    Employees = db.Column(db.Text)
    Investment_volume = db.Column(db.Text)
    Kind_of_participation = db.Column(db.Text)
    Share_of_participation = db.Column(db.Text)
    Reason_for_sale = db.Column(db.Text)
    Real_Estate = db.Column(db.Text)
    Brief_introduction = db.Column(db.Text)
    URL = db.Column(db.Text)

class dub_branch_data(db.Model):

    __tablename__ = 'dub_branch_data'

    Id = db.Column(db.Integer, primary_key=True)
    dub_id = db.Column(db.Text)
    branch1 = db.Column(db.Text)
    branch2 = db.Column(db.Text)
    branch3 = db.Column(db.Text)

class biz_trade_suche_crawl(db.Model):

    __tablename__ = "biz_trade_suche_crawl"

    Id = db.Column(db.Integer, primary_key=True)
    Business_Type = db.Column(db.Text)
    Description = db.Column(db.Text)
    Branches = db.Column(db.Text)
    Region = db.Column(db.Text)
    Entry = db.Column(db.Text)
    Employees = db.Column(db.Text)
    Turnover = db.Column(db.Text)
    Real_Estate_Property = db.Column(db.Text)
    Asking_Price = db.Column(db.Text)
    State = db.Column(db.Text)
    Surrounding_Area = db.Column(db.Text)
    URL = db.Column(db.Text)

class biz_trade_Sell_Offer_crawl(db.Model):

    __tablename__ = "biz_trade_Sell_Offer_crawl"

    Id = db.Column(db.Integer, primary_key=True)
    Business_Type = db.Column(db.Text)
    Description = db.Column(db.Text)
    Branches = db.Column(db.Text)
    Region = db.Column(db.Text)
    Entry = db.Column(db.Text)
    Employees = db.Column(db.Text)
    Turnover = db.Column(db.Text)
    Real_Estate_Property = db.Column(db.Text)
    Asking_Price = db.Column(db.Text)
    State = db.Column(db.Text)
    Surrounding_Area = db.Column(db.Text)
    URL = db.Column(db.Text)

class avad_upload_logs(db.Model):

    __tablename__ = "avad_upload_logs"

    Id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.Text)
    user = db.Column(db.Text)
    time = db.Column(db.Text)

class crawler_details(db.Model):
    __tablename__ = 'crawler_details'
    id = db.Column(db.Integer, index=True, primary_key=True)
    c_user = db.Column(db.Text)
    time_date = db.Column(db.Text)
    crawled_aids = db.Column(db.Text)
    crawled_websites = db.Column(db.Text)
    crawled_links = db.Column(db.Text)

class Keyword_Analysis(db.Model):
    __tablename__ = 'Keyword_Analysis'
    id = db.Column(db.Integer, index=True, primary_key=True)
    AID = db.Column(db.Text)
    website = db.Column(db.Text)
    analysis_result = db.Column(db.Text)
    date_of_data_insert= db.Column(db.Text)

class entitynotperson(db.Model):
    __tablename__ = 'entitynotperson'
    ID = db.Column(db.Integer, primary_key=True)
    AID = db.Column(db.Text)
    Entity_Last_Name = db.Column(db.Text)
    Entity_First_Name = db.Column(db.Text)
    Address_Ownership = db.Column(db.Text)
    Entity_Type = db.Column(db.Text)
    Legal_Form = db.Column(db.Text)
    Salutation = db.Column(db.Text)
    Titel = db.Column(db.Text)
    Education = db.Column(db.Text)
    Gender = db.Column(db.Text)
    Birth_Year = db.Column(db.Text)
    Comparison_Group_Code = db.Column(db.Text)
    Comparison_Group_Size = db.Column(db.Text)
    Count_Shareholders = db.Column(db.Text)
    Count_Group_Companies = db.Column(db.Text)
    Count_Subsidiaries = db.Column(db.Text)
    UID = db.Column(db.Text)
    Last_Change = db.Column(db.Text)
    Reserved = db.Column(db.Text)
    Expiry_Date = db.Column(db.Text)

class Word_Mesh(db.Model):

    __tablename__ = "Word_Mesh"

    word_stem = db.Column(db.Text, primary_key=True)
    word = db.Column(db.Text)
    AID = db.Column(db.Text)

class Fiche_Details(db.Model):

    __tablename__ = "Fiche_Details"

    Id = db.Column(db.Integer, primary_key=True)
    Fiche_Name = db.Column(db.Text)
    Fiche_Desc_Biz_Purpose = db.Column(db.Text)
    Fiche_Desc_Location = db.Column(db.Text)
    Fiche_Number = db.Column(db.Text)
    Project_Manager = db.Column(db.Text)
    Opp_Creation_Date = db.Column(db.Text)
    opp_aid = db.Column(db.Text)
    Account_AID = db.Column(db.Text)
    Account_Name = db.Column(db.Text)

class nexxt_change_intimation_logs(db.Model):

    __tablename__ = "nexxt_change_intimation_logs"

    Id = db.Column(db.Integer, primary_key=True)
    Chiffre = db.Column(db.Text)
    Fiche_Number = db.Column(db.Text)
    intimation_date = db.Column(db.Text)
    user = db.Column(db.Text)
    opp_aid = db.Column(db.Text)
    Project_Manager = db.Column(db.Text)

class Meaningless_Words(db.Model):

    __tablename__ = "Meaningless_Words"

    word = db.Column(db.Text, primary_key=True )

class Wordcount(db.Model):

    __tablename__="Wordcount"

    index = db.Column(db.Integer, primary_key=True)
    Count = db.Column(db.Integer)
    word = db.Column(db.Text)
    Stem = db.Column(db.Text)