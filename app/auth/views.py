# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 18:34:01 2017

@author: Holger
"""

from flask import render_template, redirect, request, url_for, flash, abort, current_app, session
from flask_login import login_user, logout_user, login_required, current_user
from urllib.parse import urlparse, urljoin
from . import auth
from .. import db
from ..models import User
from .forms import RegistrationForm , UserForm
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import app
from ..main.views import set_button_color
from werkzeug.security import generate_password_hash, check_password_hash

import pdb, json
import pdb,datetime

#Debug: pdb.set_trace()

"""--------
Function: is_safe_url
--------"""
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

"""--------
Function: auth.route
--------"""
@auth.route('/login', methods=['GET', 'POST'])
def login():
    error = ""
    print("hi")
    btn_background, btn_text = set_button_color()
    language = session.get('lang','german')
    usrnme , pwd = "",""
    if session.get('usrnme',''):
        usrnme = session['usrnme']
        pwd = session['pwd']
    if request.method == "POST":
        if request.values.has_key("remember_me"):
            session['usrnme'] = str(request.values.get("email","")).lower()
            session['pwd'] = str(request.values.get("password",""))
        else:
            if session.get('usrnme',''):
                del session['usrnme']
                del session['pwd']
        try:
            user = User.query.filter_by(email=str(request.values.get("email","")).lower()).first()
        except Exception as e:
            print("Error is "+str(e))
            error = "Please check whether the database is configured properly."
            return render_template("auth/login.html", language=language,error=error)
        print("user-------------------------------")
        print(user)
        print("Admin: ", user.is_admin)
        if user.is_admin == 1:
                current_app.config["IS_ADMIN"] = True
                print("IS_ADMIN: ", current_app.config["IS_ADMIN"])
                cr = User.query.filter_by(email=str(request.values.get("email", "")).lower()).first()
                try:
                    time_out = cr.time_out
                    if (str(time_out)=='None'):
                        user.time_out = "60"
                        time_out = 60
                        db.session.commit()
                    time_out=int(time_out)
                except Exception as e:
                    print(str(e))
                    time_out = 60
                print("timeout time= " + str(time_out))
                session.permanent = True
                app.current_app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(minutes=time_out)

        if user is not None and user.verify_password(request.values.get("password","")):
            login_user(user, request.values.get("remember_me",""))
            error = ""
            next = request.args.get("next")
            if not is_safe_url(next):
                return abort(404)
            session['lang'] = language
            return redirect(request.args.get(next) or url_for("main.index", is_admin=current_app.config["IS_ADMIN"],\
                                                              language=language, error=error))
        error ="Ungültiger Benutzer / Falsches Passwort"
    session['lang'] = language
    return render_template("auth/login.html", language=language, usrnme=usrnme, pwd=pwd, error=error,\
                           btn_background=btn_background, btn_text=btn_text)

@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    current_app.config["IS_ADMIN"] = True
    logout_user()
    #flash("Sie sind abgemeldet")
    return redirect(url_for("main.index", is_admin=False))

 
@auth.route('/register', methods=['GET', 'POST'])
@login_required
def register():
    form = RegistrationForm()
    language = session.get('lang', 'german')
    if form.validate_on_submit():
        if request.values.get('smart_buyer_matching', 'n') == 'y':
            smart_buyer_matching = 1
        else:
            smart_buyer_matching = 0
        if request.values.get('buyer_portal_search', 'n') == 'y':
            buyer_portal_search = 1
        else:
            buyer_portal_search = 0
        if request.values.get('manual_company_search', 'n') == 'y':
            manual_company_search = 1
        else:
            manual_company_search = 0

        user = User(email=str(form.email.data).lower(), username=form.username.data, password=form.password.data, is_admin=form.is_admin.data,\
                    smart_buyer_matching=smart_buyer_matching, buyer_portal_search= buyer_portal_search,manual_company_search=manual_company_search)
        db.session.add(user)
        error = "Nutzer wurde registriert"
        print("redurect to user_list")
        form = UserForm()
        all_data = request.args.get("all_data", "yes")
        return render_template("user_list.html",all_data=all_data,form=form,error=error,user=user)
        #redirect(url_for("auth.user_list"))
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("./auth/register.html", form=form, language=language, user=user.username)

@auth.route('/change_password/',  methods = ["GET", "POST"])
@login_required
def change_password() :
    passw = request.args.get('pass','')
    form = RegistrationForm()
    change_pass = request.args.get('change','')
    if change_pass:
        return render_template('change_password.html', form=form, type="change_pwd")
    elif passw:
        user = User.query.filter_by(email=current_user.email).first()
        user.password = passw
        db.session.commit()
        error = "Password changed successfully"
        return render_template('change_password.html', form=form, error=error)
    else:
        to = str(current_user.email)
        froms = "projekte@avandil.com"
        smtpserver = smtplib.SMTP(host='mailout.netzbest.de', port=465)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Link to change password"
        msg['From'] = froms
        msg['To'] = to
        html = "http://localhost:5000/auth/change_password?change=yes"
        part2 = MIMEText(html, 'html')
        msg.attach(part2)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login("projekte@avandil.com", 'v37&29sZS')
        smtpserver.sendmail(froms,to,msg.as_string())
        smtpserver.close()
        return render_template('change_password.html',form=form, type="change_pwd_link")

@auth.route('/change_database/',  methods = ["GET", "POST"])
@login_required
def change_database():
    return render_template('database_select.html')

@auth.route('/change_database_name/',  methods = ["GET", "POST"])
@login_required
def change_database_name():
    file_path = request.values.get("db","")
    current_app.config["DATABASE"] = file_path
    current_app.config["DBDIR"] = file_path
    current_app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+file_path
    import shelve
    SHELF_FILE = shelve.open(current_app.config["SHELF_FILE_NAME"])
    SHELF_FILE['DBDIR'] = current_app.config["DATABASE"]
    SHELF_FILE.sync()
    SHELF_FILE.close()
    print("Database is changed-----------------------------------------")

@auth.route('/user_list/',  methods = ["GET", "POST"])
@login_required
def user_list():
    error = ""
    btn_background, btn_text = set_button_color()
    all_data = request.args.get("all_data", "yes")
    form = UserForm()
    issubmit = request.values.get('sub', 'no')
    user = User.query.filter_by(email=current_user.email).first()
    if all_data == "no" and issubmit != 'issubmit':
        current_data = request.args.get("current_data", "no")
        user_browse = "no"
        if current_data == "yes":
            user_browse = User.query.filter_by(id=request.args.get('Id', 0)).first()
        return render_template("user_list.html", all_data=all_data, form=form, user_browse=user_browse, \
                               current_data=current_data,btn_background=btn_background,btn_text=btn_text,user=user.username)
    if form.validate_on_submit():
        user_browse = User.query.filter_by(id=request.values.get('Id', '')).first()
        if user_browse:
            user_browse.username = request.values.get('User_name', '')
            user_browse.email = request.values.get('email', '')
            user_browse.time_out = request.values.get('timeout', '')
            pswd_1 = request.values.get('password', '')
            pswd_2 = request.values.get('password2', '')
            user_browse.is_admin = request.values.get('is_admin', '')
            if pswd_1 and pswd_2 and pswd_2 == pswd_1:
                user_browse.password_hash=generate_password_hash(pswd_1)
            if request.values.get('smart_buyer_matching','n') == 'y':
                user_browse.smart_buyer_matching = 1
            else:
                user_browse.smart_buyer_matching = 0
            if request.values.get('buyer_portal_search','n') == 'y':
                user_browse.buyer_portal_search = 1
            else:
                user_browse.buyer_portal_search = 0
            if request.values.get('manual_company_search','n') == 'y':
                user_browse.manual_company_search = 1
            else:
                user_browse.manual_company_search = 0
            if request.values.get('is_admin','n') == 'y':
                user_browse.is_admin = True
            else:
                user_browse.is_admin = False
            error = "User updated successfully"
        # else:
        #     preference_detail = User(column_name=request.values.get('column_name', ''),
        #                                     value=request.values.get('value', ''))
        #     db.session.add(preference_detail)
        #     flash_msg = "Preference added successfully"
        db.session.commit()
        all_data = "yes"
    return render_template("user_list.html",all_data=all_data,form=form,error=error,btn_background=btn_background,\
                           btn_text=btn_text,user=user.username)

@auth.route('/user_list_data/',  methods = ["GET", "POST"])
@login_required
def user_list_data():
    user_brw = User.query.filter_by().all()
    user_data = {'total': 0, 'rows': []}
    i = 0
    for each_user in user_brw:
        user_data['rows'].append({'id': each_user.id, 'username': each_user.username, 'email': each_user.email,\
                                  'is_admin':each_user.is_admin,'session_logout_time': each_user.time_out,'sid': each_user.id})
        i += 1
    user_data['total'] = i
    return json.dumps(user_data)

@auth.route('/deleteuser',  methods = ["POST"])
@login_required
def deleteuser():
    sql_query = "delete from user where id='"+str(request.values['id'])+"'"
    db.engine.execute(sql_query)
    return json.dumps({})
