# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 18:55:14 2017

@author: Holger
"""

from flask_wtf import FlaskForm as Form
from wtforms import StringField, SubmitField, PasswordField, BooleanField
from wtforms.validators import Required, Email, Length, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User
from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, IntegerField, PasswordField, HiddenField, BooleanField
from wtforms.validators import Required
from wtforms.validators import DataRequired, ValidationError, EqualTo, Email, Length, \
	optional, NumberRange, InputRequired


class UserForm(Form):
    Id = HiddenField('Id')
    User_name = StringField('User Name', validators=[Required()])
    email = StringField('Email', validators=[Required()])
    timeout = StringField('timeout', validators=[Required()])
    password = PasswordField("Password", validators=[EqualTo("password2", message="Passwörter müssen übereinstimmen")])
    password2 = PasswordField("Confirm Password")
    is_admin = BooleanField("Administrator")
    smart_buyer_matching = BooleanField('Smart Buyer Matching')
    buyer_portal_search = BooleanField('Buyer Portal Search')
    manual_company_search = BooleanField('Manual Company Search ')
    submit = SubmitField('Submit')
    
class RegistrationForm(Form):
    email = StringField("Email", validators=[Required(), Length(1,64), Email()])
    username = StringField("Name", validators=[Required(), Length(1, 64), Regexp("^[A-Za-z0-9_.]*$", 0, 
                                               "Namen dürfen nur Buchstaben, Ziffern, Punkt oder Unterstrich enthalten")])
    password = PasswordField("Password", validators=[Required(), EqualTo("password2", message="Passwörter müssen übereinstimmen")])
    password2 = PasswordField("Confirm Password", validators=[Required()])
    is_admin = BooleanField("Administrator")
    smart_buyer_matching = BooleanField('Smart Buyer Matching')
    buyer_portal_search = BooleanField('Buyer Portal Search')
    manual_company_search = BooleanField('Manual Company Search ')
    submit = SubmitField("Submit")
    
    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email bereits vorhanden")
            
    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("Name bereits vorhanden")
        