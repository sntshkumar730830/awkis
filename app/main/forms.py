﻿from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, IntegerField, PasswordField, HiddenField
from wtforms.validators import Required
from wtforms.validators import DataRequired, ValidationError, EqualTo, Email, Length, \
	optional, NumberRange, InputRequired


class AIDForm(Form):
    AID = StringField('') #, validators=[Required()])
    suchworte = TextAreaField('Your previously selected keywords')
    submit = SubmitField('Submit')

class EntityForm(Form):
    AID = StringField('Enter AID (Crefo-Nummer)', validators=[Required()])
    submit = SubmitField('Submit')

class StopwortForm(Form):
    anzahl = IntegerField('Enter Count of stop words', validators=[Required()])
    submit = SubmitField('Absenden')

class PreferenceForm(Form):
    Id = HiddenField('Id')
    column_name = StringField('Column Name', validators=[Required()])
    value = StringField('Value', validators=[Required()])
    submit = SubmitField('Submit')

class FicheForm(Form):
    Id = HiddenField('Id')
    Fiche_Name = StringField('Fiche Name', validators=[Required()])
    fiche_desc = StringField('Fiche Desc', validators=[Required()])
    Fiche_Number = StringField('Fiche Count', validators=[Required()])
    submit = SubmitField('Submit')