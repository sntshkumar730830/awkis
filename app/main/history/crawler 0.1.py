"""# -*- coding: utf-8 -*-"""
# -*- coding: latin1 -*-
"""
Created on Sun Jan 29 15:15:52 2017

Einfache Klasse zum Crawlen von Websites für das AvCrawl-Projekt
Übergeben wird eine URL die zu crawlen ist und die Klasse speichert dann den Text der Website

@author: Holger
"""

from bs4 import BeautifulSoup
from nltk import FreqDist
from nltk.collocations import *
from urllib.error import HTTPError
from urllib.parse import urlparse


from sklearn.feature_extraction.text import CountVectorizer

import collections
import nltk
import pandas as pd
import requests



"""
Funktion: verify_url(url)
Zweck: prüft die übergebene url und gibt eine url zurück, die direkt mit html.open zu öffnen ist
"""
def verify_url(url):
    test = urlparse(url)
    if not test.scheme :
        scheme = "http:"
    else :
        scheme = test.scheme
        if not scheme.endswith(":") :
          scheme = scheme + ":"
    if not test.netloc :
         netloc = test.path
         path = ""
    else :
         netloc = test.netloc
    if test.path != netloc:
         path = test.path
    url = scheme + "//" + netloc + "/" + path
    return scheme, url

"""--------
Funktion: get_object
--------"""
def get_object(url, encoding):
    try :
#        print("URL: " + url)
        try :
            r = requests.get(url)
#            html = r.content.decode('utf-8', 'ignore')
            html = r.content.decode('latin1', 'ignore')
        except :
            ReadError = None        
            return ReadError
    except HTTPError as ReadError:
        ReadError = None        
        return ReadError

    try :
        soup = BeautifulSoup(html, "lxml")
    except AttributeError as ReadError:
        ReadError = None        
        return ReadError
    return soup

"""--------
Funktion: clean_page_text
--------"""
def clean_page_text(textliste):
    pagetext = []
    for zeile in textliste :
        woerter = zeile.split()
        for wort in woerter :
            if not wort :
                continue
            if wort.endswith(".") :
                wort = wort.replace(".","")
            if not wort.isalpha() == True :
                continue
            pagetext.append(wort.lower())
    return(pagetext)


"""--------
Funktion: suchwort_selektion
Zweck: Hier wird versucht mittels ML-Methoden die relevanten Suchworte zu extrahieren, bzw. Worte die keine Suchworte sind zu löschen
--------"""
def suchwort_extractor(text):
    tx = text.split()
    fd = FreqDist(tx)
    anzahl = len(fd)
    unigramme = fd.most_common(anzahl)
    df1 = pd.DataFrame(unigramme, columns=["Wort", "W-Anzahl"])
    pairs = nltk.bigrams(tx)
    fdist = FreqDist(pairs)
    bigramme = fdist.most_common(anzahl)
    df2 = pd.DataFrame(bigramme, columns=["Bigramme", "B-Anzahl"])
    triples = nltk.trigrams(tx)
    fdist = FreqDist(triples)
    trigramme = fdist.most_common(anzahl)
    df3 = pd.DataFrame(trigramme, columns=["Trigramme", "T-Anzahl"])
    df = pd.concat([df1, df2, df3], axis=1)
    return df

"""--------
Klassendefinition: Crawler
--------"""
class Crawler:
    def __init__(self):
        self.url = "http://avandil.com"
        self.pages = set()
        self.log = []
        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
                    , "?pdf=1", ".exe", "bat"]
        self.seiteninhalt = ""
        self.encoding = "latin1"
        self.soup = ""

                    
    def crawl_site(self):
        self.get_links()
        self.read_pages()
#        print("======= ERGEBNIS ========")
        if self.seiteninhalt :
#            print(self.seiteninhalt)
            return True
        else :
            print(self.log)
            return False
    
    def get_links(self):
        pages = set()
        url = self.url
        no_search_sites = ["sitemap", "impressum", "kontakt", "download", "standort", "agb", "datenschutz", "login", "site-map", 
                           "contact", "youtube", "google", "anfahrt", "karriere", "location", "printpdf"]
        scheme, url = verify_url(url)
        try :
            r = requests.get(url)
        except requests.exceptions.Timeout:
            pass
            self.log.append("Timeout")
            return False
        except :
            self.log.append("Fehler bei der Abfrage")
            return
        html = r.content.decode('latin1', 'ignore')
        try :
            url = html.geturl() #Für den Fall redirecting der url
        except: 
            pass

        domain = urlparse(url).netloc
        text = "Website crawlen: " + domain + "\n"
        self.log.append(text)
#        print (text)
        bsObj = BeautifulSoup(html, "lxml")
#        language = bsObj.findAll("html")[0].get("lang")  # herausgenommen: weil es gibt Webseiten deren lang-Tag nicht korrekt ist
#        if language and language.lower().find("de") < 0:
#             continue
#             pass #es gibt Webseiten, bei denen language = en eingestellt ist, obwogl die Seite DE ist
        if len(self.pages) == 0 and len(bsObj.findAll("a")) == 0:
            self.log.append("Keine weiterführenden Links gefunden")
            return False
        for link in bsObj.findAll("a") :
            linktext = link.get('href')
            if not linktext :
                continue
            parse_not = False     
            for site in no_search_sites :
                if linktext.lower().find(site) > 0 :
                    parse_not = True
                    break
            if parse_not :
                continue
            urlpage = urlparse(linktext)
            if urlpage.scheme and not urlpage.scheme.startswith("http") :
                continue
            if urlpage.netloc and urlpage.netloc != domain :
                nl = urlpage.netloc.replace("www.","",1)
                dm = domain.replace("www.","",1)
                if nl != dm :
                    continue
            path = urlpage.path
            parse_not = False
            for filetype in self.no_search_filetypes :
                if path and path.lower().endswith(filetype) :
                    parse_not = True
                    break
            if parse_not :
                continue
            if path and path.startswith("/") :
                path = path.replace("/","",1)
            if path and path.startswith("../") :
                path = path.replace("../","",1)
            if path and path.startswith("./") : #wenn relativer Pfad angegeben wird
                urlpath = urlparse(url).path
                urlpathlist = urlpath.split("/")
                path_location = urlpathlist[:len(urlpathlist)-1]
                pathlist = path.split("/")
                location = pathlist[1:]
                newpath = ""
                for part in path_location :
                    if part :
                        if newpath :
                            newpath = newpath + "/" + part
                        else :
                            newpath = part
                newpath = newpath + "/" + location[0]
                path = newpath

            query = urlpage.query
            if query :
                query = "?" + query
                
            newpage = scheme + "//" + domain + "/" + path + query
            self.log.append("Gefunden: " + newpage)
#            print("Gefunden: ", newpage, "\n")
            pages.add(newpage)
        if len(pages) == 0 :
            self.log.append("Keine weiterführenden Links gefunden")
            return False
        else :
            self.pages = pages
            return True                          
                        
    def get_object(self):
        try :
            try :
                r = requests.get(self.url)
#                html = r.content.decode('utf-8', 'ignore')
                html = r.content.decode(self.encoding, 'ignore')
            except :
                ReadError = None        
                return ReadError
        except HTTPError as ReadError:
            ReadError = None        
            return ReadError

        try :
            self.soup = BeautifulSoup(html, "lxml")
        except AttributeError as ReadError:
            self.soup = ""
            ReadError = None        
            return ReadError
        



    def read_pages(self) :
        page_refs = ["title", "h1", "h2", "h3", "h4", "h5", "p", "li", "div"]
        textliste = []
        pages = self.pages
        for url in pages:
            try :
                req = requests.get(url)
                url2 = req.url #Für den Fall redirecting der url
                if url != url2 :
                    print("Redirect: ", url2)
                    url = url2
            except: 
                pass
            parse_not = False
            for filetype in self.no_search_filetypes :
                if url.lower().endswith(filetype) :
                    parse_not = True
            if parse_not :
                continue
            soup = get_object(url, "latin1")
            if soup == None:
                print("Object not found")
                self.log.append("Object not found" + url)
            else:
                for ref in page_refs :
                    seite = soup.findAll(ref)
                    for zeile in seite :
                        wort = zeile.get_text()
#                        text = ":: " + wort
#                        print(text, "\n")
                        textliste.append(wort)

        pagetext = clean_page_text(textliste)                    
        taetigkeit = ""
        for wort in pagetext :
            taetigkeit = taetigkeit + " " + wort
        self.seiteninhalt = taetigkeit

#    def __init__(self):
#        self.Url = "http://avandil.com"
#        
#        self.pages = set()
#        self.Textliste = pd.DataFrame()
#        self.Stopliste = pd.DataFrame()
#        self.Order = "Anzahl"
#        self.StopWords = set()
#        self.Tabelle = "Stopwords"
#        self.ShowList = "Suchwörter"
#        self.Zaehler = True
#        self.AID = "5210308117" # AVANDIL
#        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
#                    , "?pdf=1", ".exe", "bat"]

#
#    def start_crawling(self,display) :
#        self.get_links(display)
#        if not self.pages :
#            display.insert("end", "Crawlingprozess beendet - Keine zu crawlenden Seiten gefunden\n\n")
#            display.update_idletasks
#            errlog = self.AID + ", " + self.Url[0]
#            ErrlogSchreiben(errlog)                
#            return                    
#        self.read_pages(display)
#
#    def show(self, display) :
#        if self.ShowList == "Suchwörter" :
#            liste = self.Textliste
#        elif self.ShowList == "Stopwörter" :
#            liste = self.Stopliste
#        display.delete(1.0, END)
#        if len(liste) :
#            for index,row in liste.iterrows() :
#                if self.Zaehler :
#                    zeile = row["Wort"] + " : " + str(row["Anzahl"]) + "\n"
#                else :
#                    zeile = row["Wort"] + "\n"
#                display.insert(END,zeile)
#        else :
#                display.insert(END,"Keine Wörter vorhanden\n")


print("main() gestartet")
KiCrawl = Crawler()
KiCrawl.url = "www.bus-taxi-sieben.de"
ok = KiCrawl.crawl_site()
text = suchwort_extractor(KiCrawl.seiteninhalt)    
print("Ende des Programms")
    

