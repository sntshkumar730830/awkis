#﻿from flask import render_template, session, redirect, current_app, flash, request, send_file, Flask
from flask_login import current_user
from bs4 import BeautifulSoup
from .Stemmer_Class import Stemmer
from .. import db
from ..models import tfidf, Address, Contact, User, financials, websites, Entity, preferences, \
    Branch_Codes, Business_Activity, role, web_search_result, Web_Branch_Data, web_location_data, Shareholder, Manager, export_log, \
    Portal_DUB_web_crawl, dub_branch_data, biz_trade_suche_crawl, avad_upload_logs,Keyword_Analysis, Fiche_Details, \
    nexxt_change_intimation_logs, Branch_Text, Word_Mesh, Meaningless_Words, Wordcount
from . import main
from .forms import AIDForm, EntityForm, StopwortForm, PreferenceForm, FicheForm
from .import user_interface as ui
from .crawler import Crawler
from flask_login import login_required, logout_user
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sqlalchemy import func, text, or_, and_, text, distinct
from werkzeug import secure_filename
#from openpyxl.utils import get_column_letter
#from PyQt5 import QtCore, QtGui, QtWebKit, QtNetwork
from flask import render_template, session, redirect, current_app, flash, request, send_file, Flask
import collections, time, requests
import openpyxl
import os, sys#, tkinter
import pandas as pd
import pdb, xlwt
import re, ast, codecs
import shelve
import sqlite3
import json,platform
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from urllib.request import urlopen
from email.mime.text import MIMEText
import logging
from nltk import FreqDist
import nltk
from celery_task import crawl_again
import ast
from cryptography.fernet import Fernet
#from flask_googlecharts import AreaChart, ScatterChart, PieChart, LineChart, ColumnChart, BarChart, ComboChart
#from flask_googlecharts.utils import render_data
from flask import jsonify, url_for
ki_ergebnisdict={}
def set_button_color():
    c = db.engine.execute(text("select value from preferences where column_name='button_background'")).fetchone()
    btn_background = c[0]
    c = db.engine.execute(text("select value from preferences where column_name='button_text'")).fetchone()
    btn_text = c[0]
    return btn_background, btn_text

def replace_case_insensitive(match,input):
    if match and re.findall(str(match), str(input), re.IGNORECASE):
        sourceCaseMatch = re.findall(str(match), str(input), re.IGNORECASE)[0]
        m=str(input).lower().find(str(match).lower())
        n = len(match)+m
        match1 = str(input)[m:n]
        replace = "<b>"+match1+"</b>"
        result = str(input).replace(sourceCaseMatch, replace)
        return str(result)
    else:
        return str(input)


def sort_by_key(c,d):
    new_list1,new_list2 = [],[]
    e=tuple(c)
    f=list(e)
    c.sort()
    for key in c:
        new_list1.append(key)
        new_list2.append(d[f.index(key)])
        f[f.index(key)] = "done"
    return new_list1,new_list2

#Debug:  # #######pdb.set_trace()

# Später evtl. wieder weg
#from tkinter import *
#from tkinter import filedialog

def german_formating(num):
    # # #######pdb.set_trace()
    k = ""
    if not num:
        num = 0
    a = str(num)
    if a.find(".") != -1:
        k = a[a.find(".") + 1:a.find(".") + 4]
        a = a[0:a.find(".")]
    i = len(a)
    b = ""
    c = 0
    while i:
        i -= 1
        if c % 3 == 0 and c != 0:
            b = b + "."
        c = float(c) + 1;
        b = b + a[i]
    final_ans = b[::-1].replace("-.", "-")
    print(final_ans)
    return final_ans

def normal_format(num):
    if not num:
        num = 0
    abc = str(num)
    while (abc.find(".") != -1):
            abc = abc.replace(".", "").replace("-.", "-")
    while (abc.find(",") != -1):
        abc = abc.replace(",", ".").replace("-.", "-")
    return abc

def send_email(to,subject,body,path_list=[]):
    for each_to in to:
        to = each_to
        froms = "projekte@avandil.com"
        smtpserver = smtplib.SMTP(host='smtp.office365.com', port=587)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = str(subject)
        msg['From'] = froms
        msg['To'] = to
        html = body
        for f in path_list:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=os.path.basename(f)
                )
            part['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(f)
            msg.attach(part)
        part2 = MIMEText(html, 'html')
        msg.attach(part2)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login("projekte@avandil.com", 'v37&29sZS')
        smtpserver.sendmail(froms, to, msg.as_string())
        smtpserver.close()

"""--------
Funktion: allowed_file
--------"""
def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['xlsx'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@main.before_app_first_request
def _run_on_start():
    pass
    #    dbfile = current_app.config["DATABASE"]
#    if not current_app.config["B_LAND"] :
#        if not dbfile :
#            print("Keine Datenbank definiert - Statistik kann nicht berechnet werden")
#        print("Datenbank - Statistik berechnen")
#        b_land = {
#                "Ohne" : "0", "Baden-Württemberg" :f "0", "Bayern" : "0", "Berlin" : "0", "Brandenburg" : "0", "Bremen" : "0", "Hamburg" : "0",
#                "Hessen" : "0", "Mecklenburg-Vorpommern" : "0", "Niedersachsen" : "0", "Nordrhein-Westfalen" : "0", "Rheinland-Pfalz" : "0",
#                "Saarland" : "0", "Sachsen" : "0", "Sachsen-Anhalt" : "0", "Schleswig-Holstein" : "0", "Thüringen" : "0"}
#        bundeslaender = db.session.query(Address.State, func.count(tfidf.AID)).filter(tfidf.AID == Address.AID).group_by(Address.State)
#        anzahl_laender = bundeslaender.count()
#        rec_count = 0
#        for nr in range (0, anzahl_laender) :
#            Country, firmen = bundeslaender[nr]
#            rec_count += firmen
#            anzahl = format(firmen,",").replace(",",".")
#            if not Country :
#                b_land["Ohne"] = anzahl
#            else :
#                b_land[Country] = anzahl
#        current_app.config["B_LAND"] = b_land
#        print("Datenbank - Statistik berechnet")

"""--------
Funktion: main.route
--------"""
@main.route('/') #, methods=['GET', 'POST'])
@main.route('/home')
def index():
#    # #######pdb.set_trace()
#    dbfile = globals["dbfile"]
#    records = globals["records"]
    try:
        user = User.query.filter_by(email=current_user.email).first()
    except:
        return redirect(url_for("auth.login"))
    return render_template("all_apps.html",user=user,home="home")

@main.after_request
def add_header(response):
    response.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
    return response

@main.route('/001/099/', methods=['GET', 'POST'])  # For Help Menu
@main.route('/002/099/', methods=['GET', 'POST'])
@main.route('/003/099/', methods=['GET', 'POST'])
def start_1():

    origin = ''
    rule = request.url_rule
    error = request.args.get("error", "")
    dbfile = current_app.config["DATABASE"]
    user = User.query.filter_by(email=current_user.email).first()
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    language = session.get('lang','')
    total_crawl_count = biz_trade_suche_crawl.query.count() + dub_branch_data.query.count() + web_search_result.query.count()
    if current_app.config['DBDIR']:
        dbpresent = "yes"
    else:
        dbpresent = "no"
    if '001' in rule.rule:
        origin='001'
    if '002' in rule.rule:
        origin = '002'
    if '003' in rule.rule:
        origin = '003'
    return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],\
                           language=language,user=user.username,total_crawl_count=german_formating(total_crawl_count),\
                           dbpresent=dbpresent, error=error, origin=origin)

@main.route('/start/<origin>', methods=['GET', 'POST'])
def start(origin):
    origin = str(origin)
    error = request.args.get("error", "")
    dbfile = current_app.config["DATABASE"]
    user = User.query.filter_by(email=current_user.email).first()
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    language = session.get('lang','')
    total_crawl_count = biz_trade_suche_crawl.query.count() + dub_branch_data.query.count() + web_search_result.query.count()
    if current_app.config['DBDIR']:
        dbpresent = "yes"
    else:
        dbpresent = "no"
    return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],\
                           language=language,user=user.username,total_crawl_count=german_formating(total_crawl_count),\
                           dbpresent=dbpresent, error=error, origin=origin)

@main.route('/all_apps/', methods = ["GET", "POST"])
@login_required
def all_apps():
    user = User.query.filter_by(email=current_user.email).first()
    pdb.set_trace()
    return render_template("all_apps.html",user=user)

"""--------
Funktion: Datenbak Öffnen
--------"""
@main.route('/Datenbank_Oeffnen/')
@login_required
def datenbank_oeffnen():
    dbfile = ui.open_file()
    shelf_file_name = current_app.config["SHELF_FILE_NAME"]
    shelf_file = shelve.open(shelf_file_name)
#    shelf_file = current_app.config["SHELF_FILE"]
    shelf_file["DATABASE"] = dbfile
    shelf_file.sync()
    shelf_file.close()
    rec_count = ""
    return render_template('index.html', datenbank=dbfile, records=rec_count, is_admin=current_app.config["IS_ADMIN"])

"""--------
Funktion: Datenbank Zeigen
--------"""
@main.route('/Datenbank_Zeigen/')
@login_required
def datenbank_zeigen():
    liste = db.session.query(tfidf.AID, tfidf.Name, tfidf.Business_Activity)
    language = session.get('lang','german')
    return render_template('show_database.html', items=liste, language=language)

"""--------
Funktion: Datenbank Summary
--------"""
@main.route('/Datenbank_Summary/')
@login_required
def datenbank_summary():
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    if current_app.config["B_LAND"] :
        b_land = current_app.config["B_LAND"]
    else :
        b_land = {
                "Ohne" : "0", "Baden-Württemberg" : "0", "Bayern" : "0", "Berlin" : "0", "Brandenburg" : "0", "Bremen" : "0", "Hamburg" : "0",
                "Hessen" : "0", "Mecklenburg-Vorpommern" : "0", "Niedersachsen" : "0", "Nordrhein-Westfalen" : "0", "Rheinland-Pfalz" : "0",
                "Saarland" : "0", "Sachsen" : "0", "Sachsen-Anhalt" : "0", "Schleswig-Holstein" : "0", "Thüringen" : "0"}
        bundeslaender = db.session.query(Address.State, func.count(tfidf.AID)).filter(tfidf.AID == Address.AID).group_by(Address.State)
        anzahl_laender = bundeslaender.count()
        rec_count = 0
        for nr in range (0, anzahl_laender) :
            Country, firmen = bundeslaender[nr]
            rec_count += firmen
            anzahl = format(firmen,",").replace(",",".")
            if not Country :
                b_land["Ohne"] = anzahl
            else :
                b_land[Country] = anzahl
        records = format(rec_count,",").replace(",",".")
    language = session.get('lang', '')
    return render_template('db_summary.html', datenbank=dbfile, records=records, Country = b_land, language=language)

"""--------
Funktion: KI-Suche
--------"""
@main.route('/001/012/', methods = ["GET", "POST"])
# @main.route('/KI_Suche/', methods = ["GET", "POST"])
@login_required
def ki_suche():

    # autofill_aid = str(int(request.values.get('autofill_aid', '0'),0))
    # autofill_aid = str(int(request.values.get('autofill_aid', '0'),0))
    # pdb.set_trace()
    # autofill_aid_encryp =  request.values.get('autofill_aid', '').encode('utf-8')
    # temp_key=current_app.config['current_key']
    # autofill_aid_=temp_key.decrypt(autofill_aid_encryp)
    # autofill_aid=autofill_aid_.decode('utf-8')

    auto_aid = request.values.get('autofill_aid', '0').split(",")
    autofill_aid,direct = "","NO"
    for temp in auto_aid:
        if temp:
            autofill_aid = autofill_aid + str(int(temp, 0)) + " "

    autofill_aid = autofill_aid.strip()
    if not autofill_aid:
        direct = "YES"

    export_count = preferences.query.filter_by(column_name="ki_match_export_count").first().value
    btn_background, btn_text = set_button_color()
    autofill_val = request.values.get('autofill_val','')
    autofill_val=autofill_val.replace("''","")
    # autofill_aid_ = request.values.get('autofill_aid', '')
    autofill_name=request.values.get('autofill_name', '')
    formsubmit = request.values.get('formsubmit', '')
    print(autofill_val)
    print(autofill_aid)
    print(autofill_name)
    print(formsubmit)
    if autofill_aid == 'undefined' or autofill_aid == '0' :
        autofill_aid=""
    if autofill_aid:
       autofill_name = Entity.query.filter_by(AID=autofill_aid).first().Entity_Last_Name
    method_details = request.values.get('method_details', '')

    current_year = time.strftime("%Y")
    last_5_years, i = [], 5
    while i != 0:
        last_5_years.append(current_year)
        current_year = str(int(current_year)-1)
        i -= 1
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    AID = None
    ALL = []
    form = AIDForm()
    user = User.query.filter_by(email=current_user.email).first()
    is_admin = 0
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    language = session.get('lang', '')
    if   formsubmit or form.validate_on_submit():
        all_any = request.values.get('all_any','all')
        AID = autofill_aid
        suchworte = request.values.get('autofill_val','')
        form.AID.data = ""
        form.suchworte.data = ""
        if AID :
            current_app.config["AID"] = AID
        else :
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language,user=user.username)
        global ki_ergebnisdict
        # pdb.set_trace()
        ki_ergebnis = ui.create_tfidf(AID, ALL, suchworte)
        ki_ergebnisdict[user.username]=ki_ergebnis
        print(type(ki_ergebnis))

        ######pdb.set_trace()
        years, state_list = "", []
        for each in request.values:
            if str(each)[0:5] == "years":
                years = request.values[each]
            if str(each)[-5:] == 'state':
                state_list.append(request.values[each])
        if isinstance(ki_ergebnis, str):
            print("No AID found. Please Enter Proper AID.------------------------------------------")
            flash("No AID found. Please Enter Proper AID.")
            return render_template('aid.html', form=form, AID=AID, is_admin=is_admin,
                                   user=user.username,autofill_name=autofill_name)
        # pdb.set_trace()
        return render_template('ki_ergebnis.html',\
                               language=language,AID=AID,is_admin=is_admin,user=user.username,search_word=suchworte,all=ALL,
                               from_range_umsatz=request.values.get('from_range_umsatz',0),to_range_umsatz=request.values.get('to_range_umsatz',0), \
                               from_range_mitarbeiter=request.values.get('from_range_mitarbeiter', 0),all_any=all_any,\
                               to_range_mitarbeiter=request.values.get('to_range_mitarbeiter', 0),last_5_years=last_5_years,\
                               years=years,state_list=str(state_list),export_count=export_count,btn_background=btn_background,btn_text=btn_text,autofill_name=autofill_name)
    print("render AID")
    print(str(request.method))
    return render_template('aid.html', form=form, AID=AID, is_admumsatzin=is_admin,last_5_years=last_5_years,\
                           user=user.username,autofill_val=autofill_val,autofill_aid=autofill_aid,btn_background=btn_background,\
                           btn_text=btn_text,autofill_name=autofill_name,method_details=method_details)


@main.route('/KI_Suche_stemmer/', methods = ["GET", "POST"])
@login_required
def ki_suche_stemmer():
    autofill_val = request.values.get('autofill_val','')
    autofill_val = request.values.get('autofill_val','')
    autofill_aid = request.values.get('autofill_aid','')
    current_year = time.strftime("%Y")
    last_5_years, i = [], 5
    while i != 0:
        last_5_years.append(current_year)
        current_year = str(int(current_year)-1)
        i -= 1
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    AID = None
    ALL = []
    form = AIDForm()
    user = User.query.filter_by(email=current_user.email).first()
    is_admin = 0
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    language = session.get('lang', '')
    if form.validate_on_submit():
        all_any = request.values.get('all_any','all')
        AID = form.AID.data
        suchworte = form.suchworte.data
        form.AID.data = ""
        form.suchworte.data = ""
        if AID :
            current_app.config["AID"] = AID
        else :
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language,user=user.username)
        global  ki_ergebnisdict
        ki_ergebnis = ui.create_tfidf_stemmer(AID, ALL, suchworte)
        ki_ergebnisdict[user.username] = ki_ergebnis
        years, state_list = "", []
        for each in request.values:
            if str(each)[0:5] == "years":
                years = request.values[each]
            if str(each)[-5:] == 'state':
                state_list.append(request.values[each])
        if isinstance(ki_ergebnis, str):
            flash("No AID found. Please Enter Proper AID.")
            return render_template('aid.html', form=form, AID=AID, is_admin=is_admin,
                                   user=user.username)
        btn_background, btn_text = set_button_color()
        return render_template('ki_ergebnis.html',\
                               language=language,AID=AID,is_admin=is_admin,user=user.username,search_word=suchworte,all=ALL,
                               from_range_umsatz=request.values.get('from_range_umsatz',0),to_range_umsatz=request.values.get('to_range_umsatz',0), \
                               from_range_mitarbeiter=request.values.get('from_range_mitarbeiter', 0),all_any=all_any,\
                               to_range_mitarbeiter=request.values.get('to_range_mitarbeiter', 0),last_5_years=last_5_years,
                               years=years,state_list=str(state_list),btn_background=btn_background,btn_text=btn_text)
    return render_template('aid.html', form=form, AID=AID, is_admumsatzin=is_admin,last_5_years=last_5_years,\
                           user=user.username,autofill_val=autofill_val,autofill_aid=autofill_aid)

@main.route('/client_fast/', methods = ["GET", "POST"])
@login_required
def client_fast():
    print()

@main.route('/KI_Suche_tfidf/', methods = ["GET", "POST"])
@login_required
def ki_suche_tfidf():
    autofill_val = request.values.get('autofill_val','')
    autofill_aid = request.values.get('autofill_aid', '')
    current_year = time.strftime("%Y")
    last_5_years, i = [], 5
    while i != 0:
        last_5_years.append(current_year)
        current_year = str(int(current_year)-1)
        i -= 1
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    AID = None
    ALL = []
    form = AIDForm()
    user = User.query.filter_by(email=current_user.email).first()
    is_admin = 0
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    language = session.get('lang', '')
    if form.validate_on_submit():
        all_any = request.values.get('all_any','all')
        AID = form.AID.data
        suchworte = form.suchworte.data
        form.AID.data = ""
        form.suchworte.data = ""
        if AID :
            current_app.config["AID"] = AID
        else :
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language,user=user.username)
        # global  ki_ergebnis
        ki_ergebnis = ui.create_tfidf(AID, ALL, suchworte,"yes")
        ki_ergebnisdict[user.username] = ki_ergebnis
        years, state_list = "", []
        for each in request.values:
            if str(each)[0:5] == "years":
                years = request.values[each]
            if str(each)[-5:] == 'state':
                state_list.append(request.values[each])
        if isinstance(ki_ergebnis, str):
            flash("No AID found. Please Enter Proper AID.")
            return render_template('aid.html', form=form, AID=AID, is_admin=is_admin,
                                   user=user.username)
        return render_template('ki_ergebnis.html',\
                               language=language,AID=AID,is_admin=is_admin,user=user.username,search_word=suchworte,all=ALL,
                               from_range_umsatz=request.values.get('from_range_umsatz',0),to_range_umsatz=request.values.get('to_range_umsatz',0), \
                               from_range_mitarbeiter=request.values.get('from_range_mitarbeiter', 0),all_any=all_any,\
                               to_range_mitarbeiter=request.values.get('to_range_mitarbeiter', 0),years=years,last_5_years=last_5_years,state_list=str(state_list))
    return render_template('aid.html', form=form, AID=AID, is_admumsatzin=is_admin,last_5_years=last_5_years,\
                           user=user.username,autofill_val=autofill_val,autofill_aid=autofill_aid)

def get_branch_code(super_branch_code,primary_bc):
    Bcc = 0
    # pdb.set_trace()
    # if super_branch_code[0:1] == primary_bc[0:1]:
    #     Bcc = 1
    if super_branch_code[1:3] == primary_bc[1:3]:
        Bcc = 60
    if super_branch_code[1:4] == primary_bc[1:4]:
        Bcc +=15
    if super_branch_code[1:5] == primary_bc[1:5]:
        Bcc +=15
    if super_branch_code[1:6] == primary_bc[1:6]:
        Bcc += 10
    else:
        pass
    return Bcc


@main.route('/get_kisuche_data/', methods = ["GET"])
def get_kisuche_data():
    # pdb.set_trace()
    AID = request.args['AID']
    branch_code_obj=Branch_Codes.query.filter_by(AID=AID).first()
    super_branch_code=branch_code_obj.Branch_Code

    global ki_ergebnisdict
    user = User.query.filter_by(email=current_user.email).first()
    ki_ergebnis=ki_ergebnisdict[user.username]
    ALL = ast.literal_eval(request.args['all'])
    suchworte = request.args['search_word']
    suchworte=suchworte.replace("undefined","")
    state_list, umsatz_range, mitarbeiter_range, year_list = [], [], [], []
    values_dict = {'from_range_umsatz': request.args.get('from_range_umsatzs', '0'),
                   'to_range_umsatz': request.args.get('to_range_umsatzs', '0'), \
                   'from_range_mitarbeiter': request.args.get('from_range_mitarbeiters', '0'), 'to_range_mitarbeiter': \
                       request.args.get('to_range_mitarbeiters', '0'), 'years': request.args.get('years', '0'), \
                   'state': str(request.args.get('states', '').replace('[', '').replace(']', '')).split(','),
                   'all_any': request.args.get('all_anys', 'any'),'to_range_mitarbeiters': request.args.get('to_range_mitarbeiters', 'any')}
    for each in values_dict:
        if str(each)[0:5] == 'years':
            year_list.append(values_dict.get(each, ''))
        if str(each)[-5:] == 'state':
            state_list = values_dict['state']
        if str(each) == 'from_range_umsatz' and values_dict[str(each)] != '':
            umsatz_range = [int(values_dict['from_range_umsatz']), int(values_dict['to_range_umsatz'])]

        if str(each) == 'from_range_mitarbeiter' and values_dict[str(each)] != '':
            mitarbeiter_range = [int(values_dict['from_range_mitarbeiter']),
                                 int(values_dict['to_range_mitarbeiter'])]
    if '' in state_list:
        state_list.pop(state_list.index(''))
    all_any = values_dict['all_any']
    to_range_mitarbeiters = values_dict['to_range_mitarbeiters']
    for each_aid in ki_ergebnis.Company:
        if state_list:
            get_addr = Address.query.filter_by(AID=each_aid).first()
            if get_addr:
                if get_addr.State not in state_list:
                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
            else:
                ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
        if umsatz_range:
            if all_any == "all" and year_list:
                for each_year in year_list:
                    sale_data = financials.query.filter(
                        and_(financials.Fin_Value_Type == 'Turnover', financials.AID == each_aid,
                             financials.Year == each_year)).all()
                    if sale_data:
                        for row1 in sale_data:
                            if row2.Fin_Value and (int(str(row1.Fin_Value).split(',')[0]) < umsatz_range[0] or int(
                                    str(row1.Fin_Value).split(',')[0]) > umsatz_range[1]):
                                ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                            if not row2.Fin_Value:
                                ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                    else:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
            if all_any == "any" and year_list:
                i = 0
                for each_year in year_list:
                    sale_data = financials.query.filter(
                        and_(financials.Fin_Value_Type == 'Turnover', financials.AID == each_aid,
                             financials.Year == each_year)).all()
                    if sale_data:
                        for row1 in sale_data:
                            if row2.Fin_Value and (int(str(row1.Fin_Value).split(',')[0]) < umsatz_range[0] or int(
                                    str(row1.Fin_Value).split(',')[0]) > umsatz_range[1]):
                                pass
                            else:
                                i += 1
                            if not row2.Fin_Value:
                                pass
                            else:
                                i += 1
                    else:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                if not i:
                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] == "nothing"]
        if mitarbeiter_range:
            if to_range_mitarbeiters == "all" and year_list:
                for each_year in year_list:
                    emp_data = financials.query.filter(
                        and_(financials.Fin_Value_Type == 'Mitarbeiter', financials.AID == each_aid,
                             financials.Year == each_year)).all()
                    if emp_data:
                        for row2 in emp_data:
                            if row2.Fin_Value and (int(str(row2.Fin_Value).split(',')[0]) < mitarbeiter_range[0] or int(
                                    str(row2.Fin_Value).split(',')[0]) > mitarbeiter_range[1]):
                                ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                            if not row2.Fin_Value:
                                ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                    else:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
            if to_range_mitarbeiters == "any" and year_list:
                i = 0
                for each_year in year_list:
                    emp_data = financials.query.filter(
                        and_(financials.Fin_Value_Type == 'Mitarbeiter', financials.AID == each_aid,
                             financials.Year == each_year)).all()
                    if emp_data:
                        for row2 in emp_data:
                            if row2.Fin_Value and (int(str(row2.Fin_Value).split(',')[0]) < mitarbeiter_range[0] or int(
                                    str(row2.Fin_Value).split(',')[0]) > mitarbeiter_range[1]):
                                pass
                            else:
                                i += 1
                            if not row2.Fin_Value:
                                pass
                            else:
                                i += 1
                    else:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                if not i:
                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] == "nothing"]
    # flash("Erzeuge KI-Suche.... Bitte warten")
    current_app.config["KI_ERGEBNIS"] = ki_ergebnis
    bootstrap_dict = {'total': 0, 'rows': []}
    data_dict = {}
    for each in ki_ergebnis.to_dict():
        # pdb.set_trace()
        if each == 'Value':
            fi_list=[]
            temp=list(ki_ergebnis.to_dict()[each].values())
            for val in temp:
                abc = 0
                if val >= 1 and val.is_integer():
                    abc = 100
                else:
                    abc = (round(val - int(val), 3)) * 100

                fi_list.append(abc)
            data_dict[each] = fi_list

        else:
            data_dict[each] = list(ki_ergebnis.to_dict()[each].values())
    i, j = 0, 0
    search_filter = request.args.get('search', '')
    search_extra = False
    nor = int(request.args.get('limit', 0))
    nor_added = int(request.args.get('limit', 0))
    skip_records = ((int(request.args.get('offset', 0)) / nor) + 1) * nor
    if request.args.get('from_ranges', 0) == '':
        from_wert = 0
        from_to_search = "no"
    else:
        from_wert = int(request.args.get('from_ranges', 0))
        search_extra = True
        from_to_search = "yes"
    if request.args.get('to_ranges', 0) == '':
        to_wert = 999
    else:
        to_wert = int(request.args.get('to_ranges', 0))
        while len(str(to_wert)) < 3:
            to_wert = int(str(to_wert) + '0')
        search_extra = True
    if request.args.get('nors', 0) != '' and int(request.args.get('nors', 0)) > 0:
        nor_range = int(request.args.get('nors', 0))
        nor_filter = "yes"
        search_extra = True
    else:
        nor_range = 0
        nor_filter = "no"
    if request.args.get('is_zeros', 'false') == 'true':
        zero_two = [0]
    elif request.args.get('is_twos', 'false') == 'true':
        zero_two = [2]
    elif request.args.get('from_ranges', -1) != "" and int(request.args.get('from_ranges', -1)) != -1:
        zero_two = [0, 2]
    else:
        zero_two = 'no'
    wert_aid = {}
    wert_ind = 0
    for each_wert in data_dict['Value']:
        # pdb.set_trace()
        # each_wert_=float(each_wert)
        # if each_wert_ > 0:
        #     each_wert_=each_wert_*1.5
        wert_aid[each_wert] = data_dict['Company'][wert_ind]
        wert_ind += 1
    keys = wert_aid.keys()
    keys = list(keys)
    keys.sort()
    keys.reverse()
    length = len(keys)
    ki = 0
    new_dict = {}
    while ki < length:
        k = keys[ki]
        new_dict[k] = wert_aid[k]
        ki += 1
    aid_list = []
    if nor_range > 0:
        for eachr in new_dict:
            aid_list.append(new_dict[eachr])
            nor_range -= 1
            if nor_range == 0:
                break
    nor_range_num = len(aid_list)
    z=0
    for each_company in data_dict['Company']:

        # pdb.set_trace()
        if skip_records == nor and (
                (nor_added > 0 and not search_filter and not search_extra) or (search_filter or search_extra)):
            if search_filter or search_extra:
               if  search_extra:
                    if search_filter in str(each_company) or search_filter in repr(data_dict['Value'][j]) or \
                            search_filter in str(data_dict['Name'][j]) or search_filter in str(
                        data_dict['Business_Activity'][j]):
                        if (zero_two != 'no' and int(repr(data_dict['Value'][j])[2:5]) >= from_wert and \
                            int(repr(data_dict['Value'][j])[2:5]) <= to_wert and int(repr(data_dict['Value'][j])[0:1]) \
                            in zero_two) or zero_two == 'no':
                            if data_dict['Company'][j] in aid_list or not aid_list:
                                if nor_added > 0:
                                    if data_dict['Business_Activity'][j].find(suchworte)>-1:
                                        data_dict['Business_Activity'][j]=data_dict['Business_Activity'][j].replace(suchworte, "")
                                    if suchworte and z==0:

                                        data_dict['Business_Activity'][j]=data_dict['Business_Activity'][j]+"\n <br> Search Words = "+suchworte

                                    data_row = {'Company': data_dict['Company'][j], 'Value': repr(data_dict['Value'][j])[0:5], \
                                                'Name': data_dict['Name'][j], 'Business_Activity': data_dict['Business_Activity'][j],'bcc':Bcc}
                                    search_branchcode = Branch_Codes.query.filter_by(AID=data_dict['Company'][j]).all()
                                    if search_branchcode:
                                        primary_bc, secondary_bc = "", ""
                                        for each_branch in search_branchcode:
                                            branch_text = Branch_Text.query.filter_by(Branch_Code=each_branch.Branch_Code).first()
                                            branch_text_val = ""
                                            if branch_text:
                                                branch_text_val = str(branch_text.Branch_Text)
                                            if str(each_branch.Branch_Rank_Main_Sec) == "WZ08H":
                                                primary_bc = primary_bc + " , " + each_branch.Branch_Code +" "+branch_text_val+"<br>"
                                            elif str(each_branch.Branch_Rank_Main_Sec) == "WZ08N":
                                                secondary_bc = secondary_bc + " , " + each_branch.Branch_Code+" "+branch_text_val+"<br>"
                                        if primary_bc:
                                            primary_bc = primary_bc[3:]
                                        if secondary_bc:
                                            secondary_bc = secondary_bc[3:]
                                        data_row["primary_bc"] = primary_bc
                                        data_row["secondary_bc"] = secondary_bc
                                        data_row["branch_code"]="<b>"+primary_bc+"</b> <br>"+secondary_bc
                                        data_row["bcc"] = get_branch_code(super_branch_code,primary_bc.split(" ")[0])
                                    bootstrap_dict['rows'].append(data_row)
                                    nor_added -= 1
                                i += 1
                                nor_range_num -= 1
               else:
                  if search_filter in str(each_company) or search_filter in repr(data_dict['Value'][j]) or \
                            search_filter in str(data_dict['Name'][j]) or search_filter in str(
                        data_dict['Business_Activity'][j]):
                        if (zero_two != 'no' and int(repr(data_dict['Value'][j])[2:5]) >= from_wert and \
                            int(repr(data_dict['Value'][j])[2:5]) <= to_wert and int(repr(data_dict['Value'][j])[0:1]) \
                            in zero_two) or zero_two == 'no':
                            if data_dict['Company'][j] in aid_list or not aid_list:
                                if nor_added > 0:
                                    if data_dict['Business_Activity'][j].find(suchworte)>-1:
                                        data_dict['Business_Activity'][j]=data_dict['Business_Activity'][j].replace(suchworte, "")
                                    if suchworte and z==0:

                                        data_dict['Business_Activity'][j]=data_dict['Business_Activity'][j]+"\n <br> Search Words = "+suchworte

                                    data_row = {'Company': data_dict['Company'][j], 'Value': repr(data_dict['Value'][j])[0:5], \
                                                'Name': data_dict['Name'][j], 'Business_Activity': data_dict['Business_Activity'][j],'bcc':BCC}
                                    search_branchcode = Branch_Codes.query.filter_by(AID=data_dict['Company'][j]).all()
                                    if search_branchcode:
                                        primary_bc, secondary_bc = "", ""
                                        for each_branch in search_branchcode:
                                            branch_text = Branch_Text.query.filter_by(Branch_Code=each_branch.Branch_Code).first()
                                            branch_text_val = ""
                                            if branch_text:
                                                branch_text_val = str(branch_text.Branch_Text)
                                            if str(each_branch.Branch_Rank_Main_Sec) == "WZ08H":
                                                primary_bc = primary_bc + " , " + each_branch.Branch_Code+" "+branch_text_val+"<br>"
                                            elif str(each_branch.Branch_Rank_Main_Sec) == "WZ08N":
                                                secondary_bc = secondary_bc + " , " + each_branch.Branch_Code+" "+branch_text_val+"<br>"
                                        if primary_bc:
                                            primary_bc = primary_bc[3:]
                                        if secondary_bc:
                                            secondary_bc = secondary_bc[3:]
                                        data_row["primary_bc"] = primary_bc
                                        data_row["secondary_bc"] = secondary_bc
                                        data_row["branch_code"]="<b>"+primary_bc+"</b> <br>"+secondary_bc
                                        data_row["bcc"] = get_branch_code(super_branch_code,primary_bc.split(" ")[0])
                                    bootstrap_dict['rows'].append(data_row)
                                    nor_added -= 1
                                i += 1
                                nor_range_num -= 1
                  else:
                      pass

            elif str(each_company) in aid_list or not aid_list:
                # pdb.set_trace()
                if data_dict['Business_Activity'][j].find(suchworte) > -1:
                    data_dict['Business_Activity'][j]=data_dict['Business_Activity'][j].replace(suchworte, "")
                if suchworte and z==0:
                    m = 0
                    # val = repr(data_dict['Value'][j])[0:5]
                    # if val > 0:
                    #     val = val * 1.5
                    # print("Last........"+val)

                    data_dict['Business_Activity'][j] = data_dict['Business_Activity'][j] + "\n <br> Search Words = " + suchworte

                data_row = {'Company': str(each_company), 'Value': repr(data_dict['Value'][j])[0:5],
                            'Name': data_dict['Name'][j] \
                    , 'Business_Activity': data_dict['Business_Activity'][j]}
                search_branchcode = Branch_Codes.query.filter_by(AID=data_dict['Company'][j]).all()
                if search_branchcode:
                    primary_bc, secondary_bc = "", ""
                    for each_branch in search_branchcode:
                        branch_text = Branch_Text.query.filter_by(Branch_Code=each_branch.Branch_Code).first()
                        branch_text_val = ""
                        if branch_text:
                            branch_text_val = str(branch_text.Branch_Text)
                        if str(each_branch.Branch_Rank_Main_Sec) == "WZ08H":
                            primary_bc = primary_bc + " , " + each_branch.Branch_Code+" "+branch_text_val+"<br>"
                        elif str(each_branch.Branch_Rank_Main_Sec) == "WZ08N":
                            secondary_bc = secondary_bc + " , " + each_branch.Branch_Code+" "+branch_text_val+"<br>"
                    if primary_bc:
                        primary_bc = primary_bc[3:]
                    if secondary_bc:
                        secondary_bc = secondary_bc[3:]
                    data_row["primary_bc"] = primary_bc
                    data_row["secondary_bc"] = secondary_bc
                    data_row["branch_code"]="<b>"+primary_bc+"</b> <br>"+secondary_bc
                    data_row["bcc"] = get_branch_code(super_branch_code,primary_bc.split(" ")[0])
                bootstrap_dict['rows'].append(data_row)
                nor_added -= 1
                i += 1
                nor_range_num -= 1
        elif nor_added == 0 and not search_filter and not search_extra:
            i = len(data_dict['Value'])
            break
        elif ((nor_filter == "yes" and str(each_company) in aid_list) or (nor_filter == 'no' and search_extra)) \
                or not search_extra:
            if from_to_search == "no":
                skip_records -= 1
            elif from_to_search == "yes" and zero_two != 'no' and int(repr(data_dict['Value'][j])[2:5]) >= from_wert and \
                    int(repr(data_dict['Value'][j])[2:5]) <= to_wert and int(repr(data_dict['Value'][j])[0:1]) \
                    in zero_two:
                skip_records -= 1
                i += 1
        j += 1
        if nor_filter == "yes":
            i = len(aid_list)
            if nor_range_num == 0:
                break
        z=z+1
    bootstrap_dict['total'] = i
    return json.dumps(bootstrap_dict)

def style_check(j):
    xlwt.add_palette_colour("custom_colour1", 0x09)
    xlwt.add_palette_colour("custom_colour2", 0x16)
    style1 = xlwt.easyxf('pattern: pattern solid, fore_colour custom_colour1;' 'alignment: horizontal left;' 'alignment: vertical center')
    style2 = xlwt.easyxf('pattern: pattern solid, fore_colour custom_colour2;' 'alignment: horizontal left;' 'alignment: vertical center' )
    style1.alignment.wrap = 1
    style2.alignment.wrap = 1
    fmts = [
        'general',
        '0',
        '0.00',
        '#,##0',
        '#,##0.00',
        '"$"#,##0_);("$"#,##',
        '"$"#,##0_);[Red]("$"#,##',
        '"$"#,##0.00_);("$"#,##',
        '"$"#,##0.00_);[Red]("$"#,##',
        '0%',
        '0.00%',
        '0.00E+00',
        '# ?/?',
        '# ??/??',
        'M/D/YY',
        'D-MMM-YY',
        'D-MMM',
        'MMM-YY',
        'h:mm AM/PM',
        'h:mm:ss AM/PM',
        'h:mm',
        'h:mm:ss',
        'M/D/YY h:mm',
        '_(#,##0_);(#,##0)',
        '_(#,##0_);[Red](#,##0)',
        '_(#,##0.00_);(#,##0.00)',
        '_(#,##0.00_);[Red](#,##0.00)',
        '_("$"* #,##0_);_("$"* (#,##0);_("$"* "-"_);_(@_)',
        '_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)',
        '_("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_)',
        '_(* #,##0.00_);_(* (#,##0.00);_(* "-"??_);_(@_)',
        'mm:ss',
        '[h]:mm:ss',
        'mm:ss.0',
        '##0.0E+0',
        '@'
    ]
    for fmt in fmts:
        style1.num_format_str = fmt
        style2.num_format_str = fmt
        # style1.num_format_str ="#,###.##"
        # style2.num_format_str = "#,###.##"
    if j % 2 == 1:
        final_style = style1
    else:
        final_style = style2
    return final_style

"""--------
Funktion: KI-Export
--------"""
@main.route('/KI_Export/<AID>/<selected_aid>/<unselected_aid>/<search_word>')
@login_required
def ki_export(AID,selected_aid,unselected_aid,search_word):
    # pdb.set_trace()
    main_aids = []
    remove_aids = []
    if selected_aid != "all":
        main_aids = selected_aid.split(",")
    else:
        if unselected_aid=="Zero":
            remove_aids=[]
        else:
            remove_aids =unselected_aid.split(",")
    df2 = ""
    if search_word and search_word != 'none':
        search_word_list = str(search_word).split(" ")
        search_word_list = list(filter(lambda a: a != '', search_word_list))
        time_list = ['']*len(search_word_list)
        df2 = pd.DataFrame({"Searched Words":search_word_list,time.strftime('%Y-%m-%d'):time_list})
    backslash = "\\"
    filedir = os.path.abspath(".")
    df1 = current_app.config["KI_ERGEBNIS"]
    states, emp_prev, emp_prev_year, sale_prev, sale_prev_year, emp_2_prev, emp_2_prev_year, sale_2_prev, sale_2_prev_year\
        = [], [], [], [], [], [], [], [], []
    #######pdb.set_trace()
    export_count = int(preferences.query.filter_by(column_name="ki_match_export_count").first().value)
    if main_aids:
        df1 = df1.loc[df1['Company'].isin(main_aids)]
    if df1.shape[0]>export_count:
        tempdf=df1
        df1=df1.head(export_count)
    kr = 1
    for each_aid in df1.Company:
        print(each_aid)
        print(logging.warning(kr))
        kr+=1
        if each_aid in remove_aids:
            continue

       # if (str(each_aid)  in main_aids and selected_aid != "all") or selected_aid == "all":

        get_addr = Address.query.filter_by(AID=each_aid).first()
        if get_addr:
            states.append(get_addr.State.encode('utf-8'))
        else:

            states.append(0)
        sql_query = "select Year,Fin_Value from Financials where Fin_Value_Type='Mitarbeiter' and AID='"+each_aid+"' order by Year desc limit 2"
        sql = text(sql_query)
        result1 = db.engine.execute(sql)
        i = 0
        for row1 in result1:
            if i == 0:
                if str(row1[0]).split(",")[0]!="":
                   emp_prev_year.append(str(row1[0]).split(",")[0])
                else:
                    emp_prev_year.append(0)
                if  str(row1[1]).split(",")[0]!="":
                    emp_prev.append(str(row1[1]).split(",")[0])
                else:
                    emp_prev.append(0)
                i += 1
            else:
                if str(row1[0]).split(",")[0] != "":
                    emp_2_prev_year.append(str(row1[0]).split(",")[0])
                else:
                    emp_2_prev_year.append(0)
                if str(row1[1]).split(",")[0] != "":
                    emp_2_prev.append(str(row1[1]).split(",")[0])
                else:
                    emp_2_prev.append(0)
                #emp_2_prev_year.append(str(row1[0]).split(",")[0])
                #emp_2_prev.append(str(row1[1]).split(",")[0])
                i += 1
        if i == 0:
            emp_prev.append(0)
            emp_prev_year.append(0)
            i += 1
        if i == 1:
            emp_2_prev.append(0)
            emp_2_prev_year.append(0)

        sql_query = "select Year,Fin_Value from Financials where Fin_Value_Type='Turnover' and AID='"+each_aid+"' order by Year desc limit 2"
        sql = text(sql_query)
        result2 = db.engine.execute(sql)
        i = 0
        for row2 in result2:
            if i == 0:
                if str(row2[0]).split(",")[0] != "":
                    sale_prev_year.append(str(row2[0]).split(",")[0])
                else:
                    sale_prev_year.append(0)
                if str(row2[1]).split(",")[0] != "":
                    sale_prev.append(str(row2[1]).split(",")[0])
                else:
                    sale_prev.append(0)
                #sale_prev_year.append(str(row2[0]).split(",")[0])
                #sale_prev.append(str(row2[1]).split(",")[0])
                i += 1
            else:
                if str(row2[0]).split(",")[0] != "":
                    sale_2_prev_year.append(str(row2[0]).split(",")[0])
                else:
                    sale_2_prev_year.append(0)
                if str(row2[1]).split(",")[0] != "":
                    sale_2_prev.append(str(row2[1]).split(",")[0])
                else:
                    sale_2_prev.append(0)
                #sale_2_prev_year.append(str(row2[0]).split(",")[0])
                #sale_2_prev.append(str(row2[1]).split(",")[0])
                i += 1
        if i == 0:
            sale_prev.append(0)
            sale_prev_year.append(0)
            i += 1
        if i == 1:
            sale_2_prev.append(0)
            sale_2_prev_year.append(0)
    #pdb.set_trace()

    book = xlwt.Workbook(style_compression=2)
    xlwt.add_palette_colour("custom_colour", 0x16)
    style = xlwt.easyxf('pattern: pattern solid, fore_colour custom_colour;' 'alignment: horizontal left;' 'alignment: vertical center')
    style.alignment.wrap = 1
    font = xlwt.Font()
    font.bold = True
    style.font = font
    sheet1 = book.add_sheet('KI-Suche')
    sheet1.write(0, 0, 'AID', style)
    j = 1
    position_list = []
    custom_fmts = (
        '_(* (#,##0);_(* "-");(@)',
    '_(#0_);(#0);"-"_)', # positive;negative;zero
    '#0;-#0;"-"', # positive;negative;zero
     )
    #xfs = [xlwt.easyxf('', fmt) for fmt in custom_fmts]
    xfs = [ fmt for fmt in custom_fmts]
    fmts = [
        'general',
        '0',
        '0.00',
        '#,##0',
        '#,##0.00',
        '"$"#,##0_);("$"#,##',
        '"$"#,##0_);[Red]("$"#,##',
        '"$"#,##0.00_);("$"#,##',
        '"$"#,##0.00_);[Red]("$"#,##',
        '0%',
        '0.00%',
        '0.00E+00',
        '# ?/?',
        '# ??/??',
        'M/D/YY',
        'D-MMM-YY',
        'D-MMM',
        'MMM-YY',
        'h:mm AM/PM',
        'h:mm:ss AM/PM',
        'h:mm',
        'h:mm:ss',
        'M/D/YY h:mm',
        '_(#,##0_);(#,##0)',
        '_(#,##0_);[Red](#,##0)',
        '_(#,##0.00_);(#,##0.00)',
        '_(#,##0.00_);[Red](#,##0.00)',
        '_("$"* #,##0_);_("$"* (#,##0);_("$"* "-"_);_(@_)',
        '_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)',
        '_("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_)',
        '_(* #,##0.00_);_(* (#,##0.00);_(* "-"??_);_(@_)',
        'mm:ss',
        '[h]:mm:ss',
        'mm:ss.0',
        '##0.0E+0',
        '@'
    ]
    count=0
    for each_company in list(df1['Company']):
      if str(each_company) in remove_aids:
            continue
      count+=1
      if count==export_count:
          count=0
          break
      try:
        if str(each_company) in main_aids or selected_aid == "all":

            final_style = style_check(j)
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 0, each_company, final_style)
            position_list.append(j-1)
        j += 1
      except Exception as e:
            ######pdb.set_trace()
            if str(e).find("row index was 65536, not allowed by .xls format")<-1:
                break;
            print(e)
      print(logging.warning(j))
    sheet1.write(0, 1, 'Match', style)
    j = 1
    for each_wert in list(df1['Value']):
      # pdb.set_trace()

      if each_wert >=1 and each_wert.is_integer():
          abc = 100
      else:

          abc=(round(each_wert-int(each_wert),3))*100

      count += 1
      if count == export_count:
        count = 0
        break
      try:
        if j-1 in position_list:
            final_style = style_check(j)
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 1, abc, final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 2, 'Company', style)
    j = 1
    for each_name in list(df1['Name']):
      count += 1
      if count == export_count:
            count = 0
            break
      try:
          if j - 1 in position_list:
            final_style = style_check(j)
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 2, each_name, final_style)
          j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 3, 'Activity', style)
    j = 1

    for each_tatig in list(df1['Business_Activity']):
       count += 1
       if count == export_count:
            count = 0
            break
       try:
        if j - 1 in position_list:
            each_tatig=each_tatig.replace(search_word, "")
            each_tatig = each_tatig.replace("Search Words =", "")
            each_tatig = each_tatig.replace("\n < br >", "")
            final_style = style_check(j)
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 3, each_tatig.strip(), final_style)
        j += 1
       except Exception as e:
           ######pdb.set_trace()
           if str(e).find("row index was 65536, not allowed by .xls format") < -1:
               break;
           print(e)
    sheet1.write(0, 4, 'State', style)
    j = 1
    for each_state in states:
      count += 1
      if count == export_count:
            count = 0
            break
      try:
        if j - 1 in position_list:
            final_style = style_check(j)
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 4, each_state.decode("utf-8"), final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 10, 'EmployeeYear (LY)', style)
    j = 1
    for each_emp_prev_year in emp_prev_year:
       count += 1
       if count == export_count:
            count = 0
            break
       try:
            if j - 1 in position_list:
                final_style = style_check(j)
            if each_emp_prev_year == 0:
                #for xf in enumerate(xfs):
                for xf in xfs:
                    final_style.num_format_str = xf
                #sheet1.write(j, 10, str(" "), final_style)

                sheet1.write(j, 10, each_emp_prev_year, final_style)
            else:
                for fmt in fmts:
                    final_style.num_format_str = fmt

                sheet1.write(j, 10,abs(int(float( each_emp_prev_year))), final_style)
            j += 1
       except Exception as e:
           ######pdb.set_trace()
           if str(e).find("row index was 65536, not allowed by .xls format") < -1:
               break;
           print(e)
    sheet1.write(0, 9, 'Employees (LY)', style)
    j = 1
    for each_emp_prev in emp_prev:
      count += 1
      if count == export_count:
            count = 0
            break
      try:
        if j - 1 in position_list:
            final_style = style_check(j)
        if each_emp_prev==0:
            #for xf in enumerate(xfs):
            for xf in xfs:
                final_style.num_format_str = xf
            #sheet1.write(j, 9, str(" "), final_style)
            sheet1.write(j, 9, each_emp_prev, final_style)
        else:
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 9, abs(int(float(each_emp_prev))), final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 12, 'EmployeeYear (LY-1)', style)
    j = 1
    for each_emp_2_prev_year in emp_2_prev_year:
      count += 1
      if count == export_count:
            count = 0
            break
      try:
        if j - 1 in position_list:
            final_style = style_check(j)
        if each_emp_2_prev_year==0:
            #for xf in enumerate(xfs):
            for xf in xfs:
                final_style.num_format_str = xf
            #sheet1.write(j, 12, str(" "), final_style)
            sheet1.write(j, 12, each_emp_2_prev_year, final_style)
        else:
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 12,int( abs(float(each_emp_2_prev_year))), final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 11, 'Employees (LY-1))', style)
    j = 1
    for each_emp_2_prev in emp_2_prev:
      count += 1
      if count == export_count:
            count = 0
            break
      try:
        if j - 1 in position_list:
            final_style = style_check(j)
        if each_emp_2_prev==0:
            #for xf in enumerate(xfs):
            for xf in xfs:
                final_style.num_format_str = xf
            sheet1.write(j, 11, each_emp_2_prev, final_style)
        else:
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 11, abs(int(float(each_emp_2_prev))), final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 6, 'SalesYear (LY)', style)
    j = 1
    for each_sale_prev_year in sale_prev_year:
       count += 1
       if count == export_count:
            count = 0
            break
       try:
            if j - 1 in position_list:
                final_style = style_check(j)
            if each_sale_prev_year == 0:
                #for xf in enumerate(xfs):
                for xf in xfs:
                    final_style.num_format_str = xf
                #sheet1.write(j, 6, str(" "), final_style)
                sheet1.write(j, 6, each_sale_prev_year, final_style)
            else:
                for fmt in fmts:
                    final_style.num_format_str = fmt
                sheet1.write(j, 6, abs(int(float(each_sale_prev_year))), final_style)
            j += 1
       except Exception as e:
           ######pdb.set_trace()
           if str(e).find("row index was 65536, not allowed by .xls format") < -1:
               break;
           print(e)
    sheet1.write(0, 5, 'Sales (LY)', style)
    j = 1
    for each_sale_prev in sale_prev:
       count += 1
       if count == export_count:
            count = 0
            break
       try:
            if j - 1 in position_list:
                final_style = style_check(j)
            if each_sale_prev == 0:
                #for xf in enumerate(xfs):
                for xf in xfs:
                    final_style.num_format_str = xf
                sheet1.write(j, 5,each_sale_prev , final_style)
            else:
                for fmt in fmts:
                    final_style.num_format_str = fmt
                sheet1.write(j, 5, abs(int(float(each_sale_prev))), final_style)
            j += 1
       except Exception as e:
           ######pdb.set_trace()
           if str(e).find("row index was 65536, not allowed by .xls format") < -1:
               break;
           print(e)
    sheet1.write(0, 8, 'SalesYear (LY-1)', style)
    j = 1
    for each_sale_2_prev_year in sale_2_prev_year:
      count += 1
      if count == export_count:
            count = 0
            break
      try:
        if j - 1 in position_list:
            final_style = style_check(j)
        if each_sale_2_prev_year == 0:
            #for xf in enumerate(xfs):
            for xf in xfs:
                final_style.num_format_str = xf
            sheet1.write(j, 8, each_sale_2_prev_year, final_style)
        else:
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 8, abs(int(float(each_sale_2_prev_year))), final_style)
        j += 1
      except Exception as e:
          ######pdb.set_trace()
          if str(e).find("row index was 65536, not allowed by .xls format") < -1:
              break;
          print(e)
    sheet1.write(0, 7, 'Sales (LY-1)', style)
    j = 1
    for each_sale_2_prev in sale_2_prev:
     count += 1
     if count == export_count:
            count = 0
            break
     try:
        if j - 1 in position_list:
            final_style = style_check(j)
        if each_sale_2_prev == 0:
            #for xf in enumerate(xfs):
            for xf in xfs:
                final_style.num_format_str = xf
            sheet1.write(j, 7, each_sale_2_prev, final_style)
        else:
            for fmt in fmts:
                final_style.num_format_str = fmt
            sheet1.write(j, 7, abs(int(float(each_sale_2_prev))), final_style)
        j += 1
     except Exception as e:
         ######pdb.set_trace()
         if str(e).find("row index was 65536, not allowed by .xls format") < -1:
             break;
         print(e)
    sheet1.col(0).width = int(12 * 260)
    sheet1.col(1).width = int(6 * 260)
    sheet1.col(2).width = int(62 * 260)
    sheet1.col(3).width = int(50 * 260)
    sheet1.col(4).width = int(18 * 260)
    sheet1.col(5).width = int(10 * 260)
    sheet1.col(6).width = int(10 * 260)
    sheet1.col(7).width = int(10 * 260)
    sheet1.col(8).width = int(10 * 260)
    sheet1.col(9).width = int(10 * 260)
    sheet1.col(10).width = int(10 * 260)
    sheet1.col(11).width = int(10 * 260)
    sheet1.col(12).width = int(10 * 260)
    file_name = 'KI-Export.xls'
    ######pdb.set_trace()
    book.save(file_name)
    #######pdb.set_trace()
    filename=''
    if platform.system() == 'Windows':
           filename = filedir + backslash + "KI-Export.xls"
    if platform.system() == 'Linux':
        filename = filedir + "/KI-Export.xls"
    #writer = pd.ExcelWriter(filename)
    language = session.get('lang','')
    try :
        '''df1.to_excel(writer, sheet_name='KI-Suche', index=False)
        if not isinstance(df2,str):
            df2.to_excel(writer, sheet_name='Searched Words', index=False)
        writer.save()'''
        exportlog = export_log(type="KI_Export", user_email=current_user.email, AID=AID, keywords=search_word,
                               date=time.strftime('%Y-%m-%d'), \
                               time=time.strftime('%H-%M-%S'))
        db.session.add(exportlog)
        db.session.commit()
        return send_file(filename,as_attachment=True)#attachment_filename='KI-Export.xls')

    except :
        dbfile = current_app.config["DATABASE"]
        rec_count = tfidf.query.count()
        records = format(rec_count,",").replace(",",".")
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language)

"""--------
Funktion: KI-Liste Prüfen - Wählt per Template die Excel-Datei aus
--------"""
@main.route('/KI_Check/', methods = ["GET", "POST"])
@login_required
def liste_pruefen() :
    ip_adresse = "http://" + current_app.config["IP"] + ":5000/uploader/"
#    ip_adresse = "http://192.168.2.113:5000/uploader/"
    print(ip_adresse)
    return render_template('upload.html', ip_adresse=ip_adresse)


"""--------
Funktion: uploader - Verarbeitet die hochgeladene KI-Liste
--------"""
@main.route('/uploader/', methods = ["GET", "POST"])
@login_required
def uploader() :
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    backslash = "\\"
    filedir = os.getcwd()
    UPLOAD_FOLDER = filedir + backslash +"files" + backslash

    ExcelFile = ""

    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))

    ExcelFile = UPLOAD_FOLDER + file.filename
    if not ExcelFile :
        flash("Datei nicht gefunden")
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    wb = openpyxl.load_workbook(ExcelFile)
    try:
        sheet = wb.get_sheet_by_name("KI-Suche")
    except :
        Text = 'Die Datei\n\n' + ExcelFile + '\n\nenthält kein Sheet mit dem Namen "KI-Suche" !\nVorgang wird abgebrochen.'
        flash(Text)
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    ueberschrift = sheet["A1"].value
    if ueberschrift != "AID" :
        Text = 'Das Excel-Sheet "KI-Suche" enthält keine Überschrift "AID"" !\nVorgang wird abgebrochen.'
        flash(Text)
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    ki_liste = []
    anzahl = sheet.max_row + 1
    for row in range(2, anzahl):
        AID = sheet["A" + str(row)].value
        ki_liste.append(AID)

    ki_ergebnis = ui.create_tfidf(ki_liste[0],ki_liste,"")
    current_app.config["KI_ERGEBNIS"] = ki_ergebnis
    return render_template('ki_ergebnis.html', items=ki_ergebnis)

"""--------
Funktion: Einzelanzeige
--------"""
@main.route('/001/011/',  methods = ["GET", "POST"])
@login_required
def einzelanzeige() :

    # pdb.set_trace()
    auto_aid=request.values.get('autofill_aid', '0').split(",")
    autofill_aid,direct="","NO"
    for temp in auto_aid:
        if temp:
            autofill_aid=autofill_aid+str(int(temp, 0))+" "
    autofill_aid=autofill_aid.strip()
    if not autofill_aid:
        direct = "YES"
    current_app.config["CRAWL_URL"] = ""
    AID,error = "None",""
    form = EntityForm()
    p_aid=''
    p_name=''
    first_aid=''
    idrecords = dict()
    fill_aids=[]
    language = session.get('lang', '')
    # autofill_aid = request.values.get('autofill_aid', '')
    method_details="accurate"
    autofill_aids = autofill_aid.split(" ")
    btn_background, btn_text = set_button_color()
    if autofill_aids:
        first_aid=autofill_aids[0]
    try:
        for ids in autofill_aids:
            idrecords[str(ids)]=[]
            idrecord = Entity.query.filter_by(AID=ids).first()
            idwebsite = Contact.query.filter(and_(Contact.AID==str(ids),Contact.Contact_Type=="Internet")).first()
            if idrecord:
                idrecords[str(ids)].append(idrecord.Entity_Last_Name)
            if idwebsite:
                idrecords[str(ids)].append(idwebsite.Contact_Value)
            else:
                pass
    except Exception as e:
        print("AID not found")
        print(e)
        pass
   # print(idrecords)
    for each in list(idrecords.keys()):
        if str(each)=="":
            idrecords = dict()
    user = User.query.filter_by(email=current_user.email).first()
    is_admin = False
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
        user_email=user.email
    formsubmit = request.values.get('formsubmit', '')
    if not idrecords and formsubmit and direct == "NO":
        error = "AID not Found. Please enter proper AID."
        return render_template('aid.html', form=form, AID=AID, is_admin=is_admin, screen='analysieren', \
                               user=user.username, autofill_aid=autofill_aid, showid=idrecords, aidcount=len(idrecords), \
                               btn_background=btn_background, btn_text=btn_text, error=error,first_aid=first_aid)
    if form.validate_on_submit() or formsubmit :
        # pdb.set_trace()
        if direct == "NO":
            if not idrecords:
                error = "AID not Found. Please enter proper AID."
           # # #######pdb.set_trace()
            aids = autofill_aid
            if not aids:
                error = "Atleast one AID should be selected."
                return render_template('aid.html', form=form, AID=AID, is_admin=is_admin, screen='analysieren', \
                                       user=user.username, autofill_aid=autofill_aid, showid=idrecords,first_aid=first_aid,
                                       aidcount=len(idrecords), \
                                       btn_background=btn_background, btn_text=btn_text, error=error)
            fill_aids = aids.split(" ")
            fill_aids = ' '.join(fill_aids).split()
            # pdb.set_trace()
            # if fill_aids:
            #     forward_aid = str(int(request.values.get('autofill_aid', '0'), 0))
        enteredwebsite = request.values.get('enteredwebsite', '')
        today = datetime.datetime.now()
        extra_website=enteredwebsite.split(" ")

        form.AID.data = ""
        webinhalt = ""
        websites = dict()
        aidrecords = dict()
        records = []

          # forward_aid_encryp = request.values.get('selectedaid', fill_aids[0]).encode('utf-8')
          # # autofill_aid_encryp = request.values.get('autofill_aid', '').encode('utf-8')
          # temp_key = current_app.config['current_key']
          # forward_aid_ = temp_key.decrypt(forward_aid_encryp)
          # forward_aid = forward_aid_.decode('utf-8')
        print("crawling started" + str(today.strftime("%A %d %B %y %I:%M:%S %p")))
        for AID in fill_aids:
            records = []
            currentaid=AID

            suche1 = db.session.query(tfidf.AID, tfidf.Name, tfidf.Business_Activity, tfidf.Branch_Code_Main, tfidf.Branch_Codes_Secondaries,
                                      tfidf.Internet).filter_by(AID=AID)
            suche2 = db.session.query(Address.Street_Nr, Address.PLZ, Address.City, Address.County,
                                      Address.State,
                                      Address.Country).filter_by(AID=AID)
            suche3 = db.session.query(Contact.AID, Contact.Contact_Type, Contact.Contact_Value).filter_by(AID=AID,
                                                                                                            Contact_Type="Internet")
            # suche4 = db.session.query(tfidf.AID, tfidf.Name, tfidf.Business_Activity, tfidf.Branch_Code_Main, tfidf.Branch_Codes_Secondaries,
            #                           tfidf.Internet).filter_by(AID=AID)
            try:
                p_aid,p_name='',''
                if direct == "NO":
                      p_aid = suche1[0][0]
                      p_name = suche1[0][1]
            except:
                return render_template('aid.html', form=form, AID=AID, is_admin=is_admin, screen='analysieren', \
                          user=user.username, autofill_aid=autofill_aid, showid=idrecords,aidcount=len(idrecords),\
                          btn_background=btn_background,btn_text=btn_text,error =error)



            try:
                AID = suche1[0][0]
                records.append(AID)
            except:
                flash("No AID found. Please Enter Proper AID.")
                AID = currentaid
                records.append(AID)
            try:
                name = suche1[0][1]
                records.append(name)
            except:

                name = "no record found"
                records.append(name)
            try:
                ttg = suche1[0][2]
                records.append(ttg)
            except:

                ttg = "no record found"
                records.append(ttg)
            try:
                bch = suche1[0][3]
                records.append(bch)
            except:

                bch = "no record found"
                records.append(bch)
            try:
                bcn = suche1[0][4]
                records.append(bcn)
            except:

                bcn = "no record found"
                records.append(bcn)
            try:
                internet = suche1[0][5]
                records.append(internet)
            except:

                internet = "no record found"
                records.append(internet)
            try:
                strasse = suche2[0][0]
                records.append(strasse)
            except:

                strasse = "no record found"
                records.append(strasse)
            try:
                PLZ = suche2[0][1]
                records.append(PLZ)
            except:

                PLZ = "no record found"
                records.append(PLZ)
            try:
                ort = suche2[0][2]
                records.append(ort)
            except:

                ort = "no record found"
                records.append(ort)
            try:
                landkreis = suche2[0][3]
                records.append(landkreis)
            except:

                landkreis = "no record found"
                records.append(landkreis)
            try:
                State = suche2[0][4]
                records.append(State)
            except:

                State = "no record found"
                records.append(State)
            try:
                Country = suche2[0][5]
                records.append(Country)
            except:

                Country = "no record found"
                records.append(Country)
                # return render_template('aid.html', form=form, AID=AID, is_admin=is_admin, screen='analysieren',user=user.username)
            keywords = 0
            try:
                websites[AID]=(suche3[0][2])
                maxlen = len(websites)
                records.append(websites[AID])
            except:
                websites[AID]="website not available"
               # print("website not available")
                records.append("website not available")
            if websites:
                KiCrawl = Crawler()
                # pdb.set_trace()
                KiCrawl.stopwords = current_app.config["STOPWORDS"]
              #  print(current_app.config["STOPWORDS"])
                aidrecords[AID] = records
        print(aidrecords)
        for e in extra_website:
            if e =="":
               continue
            records = []
            websites[str(e)] = str(e)
            fill_aids.append(str(e))
            AID = "no record found "
            records.append(AID)
            name = "no record found"
            records.append(name)
            ttg = "no record found"
            records.append(ttg)
            bch = "no record found"
            records.append(bch)
            bcn = "no record found"
            records.append(bcn)
            internet = "no record found"
            records.append(internet)
            strasse = "no record found"
            records.append(strasse)
            PLZ = "no record found"
            records.append(PLZ)
            ort = "no record found"
            records.append(ort)
            landkreis = "no record found"
            records.append(landkreis)
            State = "no record found"
            records.append(State)
            Country = "no record found"
            records.append(Country)
            records.append(str(e))
            aidrecords[str(e)] = records
        print(websites)
        pages=[]
        sites_crawl=""
        try:
            # pdb.set_trace()

            for AID in fill_aids:

                if websites[AID]=="website not available":
                    continue
                time.sleep(2)
                current_app.config["CRAWL_URL"] = websites[AID]+" Crawling Started"
                print(current_app.config["CRAWL_URL"])

                stored_result =Keyword_Analysis.query.filter_by(AID=AID).first()
                time.sleep(2)
                current_app.config["CRAWL_URL"] = websites[AID] + "<i class='fa fa-spinner fa-spin'></i> Crawling"
                print(current_app.config["CRAWL_URL"])
                if not stored_result:

                    KiCrawl = Crawler()
                    KiCrawl.stopwords = current_app.config["STOPWORDS"]
                   # print(current_app.config["STOPWORDS"])
                    KiCrawl.URL = websites[AID]
                    ok = KiCrawl.crawl_site()
                    if ok:
                        sites_crawl =KiCrawl.all_crawl_urls
                        pages.extend(KiCrawl.ngrams)
                        today2 = datetime.datetime.now()
                        print(str(KiCrawl.ngrams).replace("]", "").replace(" ", "").replace("[", ""))

                        sql_keyword_analysis_insert = 'insert into Keyword_Analysis("AID","website","analysis_result","date_of_data_insert") values(' + '"' + str(
                            AID) + '","' +str(websites[AID])+ '","' + str(KiCrawl.ngrams).replace("]", "").replace(" ","").replace("[", "")+ '","' + str(today2.strftime("%A %d %B %y %I:%M:%S %p")) + '");'
                        sql_ka_insert = text(sql_keyword_analysis_insert)
                        db.engine.execute(sql_ka_insert)
                        time.sleep(2)
                        current_app.config["CRAWL_URL"] = websites[AID] + " Done"
                        print(current_app.config["CRAWL_URL"])
                else:

                    pages.extend(stored_result.analysis_result.replace("'","").replace(" ","").replace("]","").replace("[","").split(","))
                    sites_crawl = "output shown from earlier data"
                    time.sleep(2)
                    current_app.config["CRAWL_URL"] = websites[AID] + " Done"
                    print(current_app.config["CRAWL_URL"])
                   # crawl_again.delay(AID,websites[AID],stored_result.analysis_result.replace("'","").replace("]","").replace("[","").replace(" ","").split(","),user_email,current_app.config["STOPWORDS"])
        except Exception as e:
            print(e)
            if str(e).find('no such table') >= 0:
                sql_Keyword_Analysis = 'create table Keyword_Analysis(Id INTEGER  PRIMARY KEY AUTOINCREMENT,AID text ,website text,analysis_result text,date_of_data_insert text)'
                sql2 = text(sql_Keyword_Analysis)
                db.engine.execute(sql2)
                print("Keyword_Analysis table created")
                today2 = datetime.datetime.now()
                sql_keyword_analysis_insert = 'insert into Keyword_Analysis("AID","website","analysis_result","date_of_data_insert") values(' + '"' + str(
                    AID) + '","' + str(websites[AID]) + '","' +str(KiCrawl.ngrams).replace(" ","").replace("]", "").replace("[", "")+ '","' + str(today2.strftime("%A %d %B %y %I:%M:%S %p")) + '");'
                sql_ka_insert = text(sql_keyword_analysis_insert)
                db.engine.execute(sql_ka_insert)
        # pdb.set_trace()
        stopword_list=[]
        normal_word=[]
        stoplist_obj=Word_Mesh.query.filter().limit(2000)
        for obj in stoplist_obj:
            stopword_list.append(obj.word_stem)

        for word_ in pages:
            word=Stemmer(word_).stem
            if word in stopword_list:
                pages.remove(word_)


# --------------------------Data Filtering Coding-------------------------
        filter_count=preferences.query.filter_by(column_name='filter_word_count').first()
        wrd_mesh = Word_Mesh.query.filter_by().limit(int(filter_count.value))
        words_stem = []
        for each_word in wrd_mesh:
            temp=each_word.word.split(',')
            for temp_ in temp:
                normal_word.append(temp_.strip())
            words_stem.append(each_word.word_stem)

        meaningless_word = []
        for wd in pages:
            if wd not in normal_word:
                meaningless_word.append(wd)

        for word_ in meaningless_word:
            word = Stemmer(word_).stem
            if word not in words_stem:
                meningless_logs = Meaningless_Words(word=word)
                pages.remove(word_)
                try:
                    db.session.add(meningless_logs)
                    db.session.commit()
                except:
                    db.session.rollback()

# ----------------------------------Data filtering End------------------------


        fd = FreqDist(pages)
        anzahl = len(fd)
        unigramme = fd.most_common(anzahl)
        df1 = pd.DataFrame(unigramme, columns=["Wort", "Anzahl "])
        pairs = nltk.bigrams(pages)
        fdist = FreqDist(pairs)
        # bigramme = fdist.most_common(anzahl)
        # df2 = pd.DataFrame(bigramme, columns=["Bigramme", "Anzahl  "])
        # triples = nltk.trigrams(pages)
        # fdist = FreqDist(triples)
        # trigramme = fdist.most_common(anzahl)
        # df3 = pd.DataFrame(trigramme, columns=["Trigramme", " Anzahl  "])
        webinhalt = pd.concat([df1], axis=1)
        keywords = len(webinhalt)
        new_webinhalt=dict()
        for each in webinhalt.index:
            new_webinhalt[str(int(each)+1)]=[webinhalt.iloc[each,0],webinhalt.iloc[each,1]]
        c_user= current_user.email
        c_time_date=str(today.strftime("%A %d %B %y %I:%M:%S %p"))
        c_crawled_aids= str(fill_aids).replace("[","").replace("]","")
        c_crawled_websites=str(websites).replace("[","").replace("]","")
        c_crawled_info=str(sites_crawl).replace("[","").replace("]","")
        sql_crawl_info_insert='insert into crawler_details("c_user","time_date","crawled_aids","crawled_websites","crawled_links") values('+'"'+str(c_user)+'","'+c_time_date+'","'+c_crawled_aids+'","'+c_crawled_websites+'","'+c_crawled_info+'");'
        sql = text(sql_crawl_info_insert)
        try:
            db.engine.execute(sql)
        except Exception as e:
            print(e)
            if str(e).find('no such table')>=0:
                sql_crawl_table = 'create table crawler_details(Id INTEGER  PRIMARY KEY AUTOINCREMENT,c_user text ,time_date text,crawled_aids text,crawled_websites text,crawled_links text)'
                sql2 = text(sql_crawl_table)
                db.engine.execute(sql2)
                print("crawler_details table created")
                db.engine.execute(sql)


        # return render_template('show_entity.html', AID=AID, name=name, ttg=ttg, bch=bch, bcn=bcn,internet=internet, strasse=strasse,
        #                       PLZ=PLZ, ort=ort, landkreis=landkreis, State=State, Country=Country, website=websites, keywords=keywords,
        #                       webinhalt=webinhalt,language=language,is_admin=is_admin,screen='analysieren',user=user.username)
        print(aidrecords)
        today2 = datetime.datetime.now()
        #####pdb.set_trace()
        print(logging.warning("crawling completed"+ str(today2.strftime("%A %d %B %y %I:%M:%S %p"))))
        print(current_app.config["CRAWL_URL"])
        # pdb.set_trace()
        print(keywords)

        return render_template('show_entity.html', is_admin=is_admin, screen='analysieren', user=user.username,AID=p_aid, name=p_name,
                               webinhalt=webinhalt,new_webinhalt=new_webinhalt, language=language, keywords=keywords, allrecords=aidrecords, \
                               btn_background=btn_background, btn_text=btn_text,startvalue=0,endvalue=100,method_details=method_details)

    if len(list(idrecords.keys())) == 1 and list(idrecords.keys())[0] == "0":
        idrecords={}
    print(idrecords)
    return render_template('aid.html', form=form, AID=AID, is_admin=is_admin, screen='analysieren', \
                           user=user.username, autofill_aid=autofill_aid, showid=idrecords,aidcount=len(idrecords),\
                           btn_background=btn_background,btn_text=btn_text,first_aid=first_aid,direct=direct)



@main.route('/encrypt_aid/',  methods = ["GET"])
@login_required
def encrypt_aid():
    # pdb.set_trace()
    aid=""
    aid_list=(request.args["aid"]).split(",")
    for each in aid_list:
        if each:
            temp = hex(int(each))
            aid=aid+temp+","
    return json.dumps({'cipher_text':aid[:-1]})

@main.route('/urlprint/',  methods = ["GET", "POST"])
@login_required
def urlprint():
    curl=current_app.config["CRAWL_URL"]
    while (curl.find(".") > -1 ) :
           curl=curl.replace("http://",'').replace("/",'').replace(".",'').replace("-",'')

    return json.dumps({'currenturl': curl})


@main.route('/taskreport/',  methods = ["GET", "POST"])
@login_required
def AVAD_DATA_UPLOAD_OUTPUT():
    opshow=current_app.config["AVAD_DATA_UPLOAD_OUTPUT"]
    return json.dumps({'outputshow': opshow})



"""--------
Funktion: Create_Stopwords
--------"""
@main.route('/Create_Stopwords/',  methods = ["GET", "POST"])
@login_required
def create_stopwords() :
    dbfile = current_app.config["DBDIR"]
    shelf_file_name = current_app.config["SHELF_FILE_NAME"]
    user = User.query.filter_by(email=current_user.email).first()

    anzahl_stopwoerter = len(current_app.config["STOPWORDS"])
    form = StopwortForm()
#    form.anzahl.data = 500
    if form.validate_on_submit():
        anzahl_stopwoerter = form.anzahl.data
        print("Ermittle Stopwörter")
        d = collections.Counter()
        connection = sqlite3.connect(dbfile)
        cursor = connection.cursor()
        cursor.execute("SELECT Business_Activity FROM TFIDF ")
        row = cursor.fetchone
        for row in cursor:
            taetigkeit = row[0]
            wortliste = re.split(r"(\W)", taetigkeit)
            for wort in wortliste :
                if wort.isalnum() :
                    d[wort.lower()] +=1
                    print("Anzahl Wörter: ", len(d))
        connection.close()
        mcm = d.most_common(anzahl_stopwoerter)
        liste = []
        anzeige = []
        nr = 1
        for wort in mcm :
            stopwort = wort[0]
            vorkommen = format(wort[1],",").replace(",",".")
            liste.append(stopwort)
            wert = (nr, stopwort, vorkommen)
            anzeige.append(wert)
            nr +=1
        current_app.config["STOPWORDS"] = liste
        print("******* Liste der Stopwörter ********")
        print(liste)
        print("******** Ende der Liste ********")
        shelf_file = shelve.open(shelf_file_name)
        shelf_file["STOPWORDS"] = liste
        shelf_file.sync()
        shelf_file.close()
        return render_template('show_stopwords.html', items=anzeige)
    language = session.get('lang','german')
    return render_template('get_stopwords.html', form=form, anzahl=anzahl_stopwoerter, language=language,user=user.username)
"""--------
Funktion: Show_Stopwords
--------"""
@main.route('/Show_Stopwords/',  methods = ["GET", "POST"])
@login_required
def Show_Stopwords() :
    dbfile = current_app.config["DBDIR"]
    shelf_file_name = current_app.config["SHELF_FILE_NAME"]
    user = User.query.filter_by(email=current_user.email).first()

    anzahl_stopwoerter = len(current_app.config["STOPWORDS"])
    # form = StopwortForm()
#    form.anzahl.data = 500
#     if form.validate_on_submit():
#         anzahl_stopwoerter = form.anzahl.data
    print("Ermittle Stopwörter")
    d = collections.Counter()
    connection = sqlite3.connect(dbfile)
    cursor = connection.cursor()
    cursor.execute("SELECT Business_Activity FROM TFIDF ")
    row = cursor.fetchone
    for row in cursor:
        taetigkeit = row[0]
        wortliste = re.split(r"(\W)", taetigkeit)
        for wort in wortliste :
            if wort.isalnum() :
                d[wort.lower()] +=1
                print("Anzahl Wörter: ", len(d))
    connection.close()
    mcm = d.most_common(anzahl_stopwoerter)
    liste = []
    anzeige = []
    nr = 1
    for wort in mcm :
        stopwort = wort[0]
        vorkommen = format(wort[1],",").replace(",",".")
        liste.append(stopwort)
        wert = (nr, stopwort, vorkommen)
        anzeige.append(wert)
        nr +=1
    current_app.config["STOPWORDS"] = liste
    print("******* Liste der Stopwörter ********")
    print(liste)
    print("******** Ende der Liste ********")
    shelf_file = shelve.open(shelf_file_name)
    shelf_file["STOPWORDS"] = liste
    shelf_file.sync()
    shelf_file.close()
    return render_template('show_stopwords.html', items=anzeige,user=user.username)
    # language = session.get('lang','german')
    # return render_template('get_stopwords.html', form=form, anzahl=anzahl_stopwoerter, language=language)

"""--------
Funktion: Nexxt_Change
--------"""
@main.route('/Nexxt_Change/',  methods = ["GET", "POST"])
@login_required
def nexxt_change() :
    AID = "None"
    form = EntityForm()
    user = User.query.filter_by(email=current_user.email).first()
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    if form.validate_on_submit():
        AID = form.AID.data
        form.AID.data = ""
        suche1 = db.session.query(tfidf.AID, tfidf.Name, tfidf.Business_Activity).filter_by(AID=AID)
        suche2 = db.session.query(Address.ZIP, Address.County, Address.State, Address.Country).filter_by(AID=AID)
        AID=suche1[0][0]
        name=suche1[0][1]
        ttg=suche1[0][2]
        PLZ=suche2[0][0]
        landkreis=suche2[0][1]
        State=suche2[0][2]

        file_name = os.path.abspath(".") + "\\files\\nexxt-change.xlsx"
        if not os.path.exists(file_name):
            dbfile = current_app.config["DATABASE"]
            rec_count = tfidf.query.count()
            records = format(rec_count,",").replace(",",".")
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
        nx_raw_data = pd.read_excel(open(file_name, "rb"), sheetname=0, encoding='latin1')
        nx_daten = nx_raw_data[nx_raw_data.State == State]
        nx_daten.insert(len(nx_daten.columns), "Inseratstext" , "")
        nx_daten.reset_index(drop=True, inplace=True)
        nx_daten.is_copy = False
        for index, row in nx_daten.iterrows() :
            titel = row["Titel"]
            bl1 = row["Branchenlevel 1"]
            bl2 = row["Branchenlevel 2"]
            bl3 = row["Branchenlevel 3"]
            Description = row["Description"]
            if not pd.isnull(titel) :
                text = titel
            if not pd.isnull(bl1) :
                text = text + " " + bl1
            if not pd.isnull(bl2) :
                text = text + " " + bl2
            if not pd.isnull(bl3) :
                text = text + " " + bl3
            if not pd.isnull(Description) :
                text = text + " " + Description
            inseratstext = ""
            wortliste = re.split(r"(\W)", text)
            for wort in wortliste :
                if wort.isalnum() :
                    inseratstext = inseratstext + wort.lower() + " "
#            print(index)
            nx_daten.loc[index, "Inseratstext"] = inseratstext
        inseratstext = ""
        wortliste =  re.split(r"(\W)", ttg)
        for wort in wortliste :
            if wort.isalnum() :
                inseratstext = inseratstext + wort.lower() + " "
        nummer = len(nx_daten)
        nx_daten.loc[nummer] = ["", name, State, PLZ, landkreis, "", "", "", "" , "", "", "", "AID", "Mandat", ttg, inseratstext]
#        nx_daten.loc[nummer] = ["", name, State, PLZ, landkreis, "", "", "", "" , "", "", "", AID, "Mandat", inseratstext]
        tt_liste = nx_daten["Inseratstext"]
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_nx = tfidf_vectorizer.fit_transform(tt_liste)
        ar = cosine_similarity(tfidf_nx[nummer:nummer+1], tfidf_nx)
        nx_daten.insert(1, "Value" , "")
        ergebnisliste = ar[0]
        se = pd.Series(ergebnisliste)
        nx_daten["Value"] = se.values
        nx_daten = nx_daten.drop("ID", 1)
        nx_daten = nx_daten.drop("Inseratstext", 1)
        result_frame = nx_daten.sort_values(by="Value", ascending = False)
        result_frame.reset_index(drop=True, inplace=True)
        return render_template('nx_matching.html', items=result_frame)
    return render_template('aid.html', form=form, AID=AID,is_admin=is_admin)

@main.route('/set_language/',  methods = ["GET"])
def set_language() :
    session['lang'] = request.args['lang']
    return json.dumps({})

@main.route('/auto_logout/',  methods = ["GET"])
def auto_logout() :
    logout_user()
    return json.dumps({})

@main.route('/set_database/',  methods = ["GET","POST"])
def set_database() :
    '''import pdb
    # #######pdb.set_trace()
    #RESET CODE
    current_app.config["DATABASE"] = "C:\\Users\\Sandeep Agrawal\\Dropbox\\AVKIS-Web\\database\\Firmen2.db"
    import shelve
    SHELF_FILE = shelve.open(current_app.config["SHELF_FILE_NAME"])
    SHELF_FILE['DBDIR'] = current_app.config["DATABASE"]
    SHELF_FILE.sync()
    SHELF_FILE.close()'''



    print("Test-----------------------")
    file_path = request.form.get("database_path","")
    current_app.config["DATABASE"] = file_path
    import shelve
    SHELF_FILE = shelve.open(current_app.config["SHELF_FILE_NAME"])
    SHELF_FILE['DBDIR'] = current_app.config["DATABASE"]
    SHELF_FILE.sync()
    SHELF_FILE.close()
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    records = format(rec_count, ",").replace(",", ".")
    language = session.get('lang', '')
    return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],
                           language=language)


@main.route('/crawl_web/',  methods = ["GET","POST"])
def crawl_web() :
    try:
        crawl_broke = request.args.get('crawl_broke','no')
        if crawl_broke == 'yes':
            URL = request.args.get('new_url')
            crawled_link = ast.literal_eval(request.args.get('crawled_link',"[]"))
        else:
            crawled_link = []
            URL = "https://www.nexxt-change.org/SiteGlobals/Forms/Kaufgesuch_Suche/Kaufgesuche_Formular.html"
        req = urlopen(URL).read()
        soup = BeautifulSoup(req, "lxml")
        i,j=1,0
        sql_query = "select Chiffre from  web_search_result"
        sql = text(sql_query)
        allchiffre=db.engine.execute(sql)
        chiffrelist=[i[0] for i in allchiffre.fetchall()]

        while i:
            for link in soup.findAll("a"):
                if str(link.get('href'))[-6:].isdigit() and str(link.get('href')[:27]) == '/DE/Kaufgesuch/Detailseite/' \
                        and str(link.get('href')) not in crawled_link:
                    crawled_link.append(str(link.get('href')))
                    description, no_of_emp, date, branch2, serial_no, seeker_type, sales, asking_price, activity, email_data = \
                        "''","''","''","''","''","''","''","''","''","''"
                    new_url = "https://www.nexxt-change.org"+str(link.get('href'))
                    req_each = urlopen(new_url).read()
                    soup_each = BeautifulSoup(req_each, "lxml")
                    for desc in soup_each.findAll("h3"):
                        if str(desc.text) == 'Description':
                            if desc.findNext('p').text:
                                description = "'"+str(desc.findNext('p').text)+"'"
                    for email_search in soup_each.findAll("div"):
                        if email_search.get('class') and str(email_search.get('class')[0]) == 'box-wrapper':
                            if email_search.findChildren("a") and email_search.findChildren("a")[0].get('href')\
                                and email_search.findChildren("a")[0].get('href')[:36] == '/DE/Regionalpartnersuche/Detailseite':
                                email_url = "https://www.nexxt-change.org" + str(email_search.findChildren("a")[0].get('href'))
                                req_email = urlopen(email_url).read()
                                soup_email = BeautifulSoup(req_email, "lxml")
                                for each_email in soup_email.findAll("a"):
                                    if each_email.get('class') and each_email.get('class')[0] == 'link-email':
                                        email_data = "'"+each_email.text+"'"

                    for each_dt in soup_each.findAll("dt"):
                        if str(each_dt.text) == 'Count Employees:' and each_dt.findNext('dd').text:
                            no_of_emp = "'"+str(each_dt.findNext('dd').text)+"'"
                        if str(each_dt.text) == 'Datum:' and each_dt.findNext('dd').text:
                            date = "'"+str(each_dt.findNext('dd').text)+"'"
                        if str(each_dt.text)[:9] == 'Branches:':
                            branch1 = each_dt.findNext('dd').text.strip().replace('\n\n','*').replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')
                            branch2 = branch1.strip().replace('\n','*').replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')
                        search_serial = ""
                        if str(each_dt.text) == 'Chiffre:' and each_dt.findNext('dd').text:
                            serial_no = "'"+str(each_dt.findNext('dd').text)+"'"
                            print(serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö'))
                           # #pdb.set_trace()
                            search_serial = web_search_result.query.filter_by(Chiffre=serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')).first()
                        if search_serial:
                            try:
                                chiffrelist.remove(serial_no.strip().replace("'","").replace("\r\n",'').replace("   "," "))
                                sql_query = "update  web_search_result set Latest_Crawl='"+datetime.datetime.today().strftime('%d.%m.%Y')+"',Description='"+ description.strip().replace("'","").replace("\r\n", '').replace("   ", " ")+"',Active_Status='True' where Chiffre='"+serial_no.strip().replace("'","").replace("\r\n",'').replace("   "," ")+"'"
                                sql = text(sql_query)
                                db.engine.execute(sql)
                            except Exception as e:
                           #     #pdb.set_trace()
                                sql_query="alter table web_search_result RENAME column daturn to First_Crawl_Date"
                                sql_query1="alter table web_search_result add column Latest_Crawl text"
                                sql_query2="update web_search_result set Latest_Crawl  = First_Crawl_Date;"
                                sql_query3 = "alter table web_search_result add column Active_Status text"
                                sql = text(sql_query)
                                db.engine.execute(sql)
                                sql1 = text(sql_query1)
                                db.engine.execute(sql1)
                                sql2 = text(sql_query2)
                                db.engine.execute(sql2)
                                sql3 = text(sql_query3)
                                db.engine.execute(sql3)
                                print(date.strip().replace("'", "").replace("\r\n", '').replace("  ",""))
                                sql_query = "update table web_search_result set Latest_Crawl='" + date.strip().replace("'", "").replace("\r\n", '').replace("  ","") + ",Description=" + description.strip().replace(
                                    "'", "").replace("\r\n", '').replace("   "," ") + ",Active_Status=True where Chiffre=" + serial_no.strip().replace("'", "").replace("\r\n", '').replace("   ", " ")
                                sql = text(sql_query)
                                db.engine.execute(sql)
                                str(e)
                            break
                        if str(each_dt.text) == 'Inseratstyp:'and each_dt.findNext('dd').text:
                            seeker_type = str(each_dt.findNext('dd').text)
                        if str(each_dt.text) == 'Letzter Jahresumsatz in TEUR:' and each_dt.findNext('dd').text:
                            sales = "'"+str(each_dt.findNext('dd').text)+"'"
                        if str(each_dt.text) == 'Asking_Price:' and each_dt.findNext('dd').text:
                            asking_price = "'"+str(each_dt.findNext('dd').text)+"'"
                        if str(each_dt.text) == 'Internationale Business_Activity:' and each_dt.findNext('dd').text:
                            activity = "'"+str(each_dt.findNext('dd').text)+"'"
                        if str(each_dt.text)[:10] == 'Standorte:' and each_dt.findNext('dd').text:
                            a = str(each_dt.findNext('dd').text).strip().replace('\n\n','').replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')
                            a = a.split('\n')
                            for each_standorte in a:
                                k = each_standorte.split('>')
                                if len(k) == 1:
                                    State = "'"+str(k[0])+"'"
                                    State = State.replace('ü', 'u')
                                    print("dskksmkdskdfmdfsds")
                                    sql_query = "insert into web_location_data (Chiffre,State) values ('"+\
                                                serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                                    replace('Ÿ','ß').replace('¶','ö') + "','" + State.strip().replace("'","").\
                                                    replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').\
                                                    replace('¶','ö')+"')"
                                    print(sql_query)
                                if len(k) == 2:
                                    State, Region = "'"+str(k[0])+"'", "'"+str(k[1])+"'"
                                    State = State.replace('ü', 'u')
                                    Region = Region.replace('ü', 'u')
                                    sql_query = "insert into web_location_data (Chiffre,State,Region) values ('" + \
                                            serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                                replace('Ÿ','ß').replace('¶','ö') + "','" + State.strip().replace("'","").replace('ÃƒÂ','').\
                                                    replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')+"','"+ \
                                                Region.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                                    replace('Ÿ','ß').replace('¶','ö') + "')"
                                if len(k) == 3:
                                    State, Region, kreis = "'"+str(k[0])+"'", "'"+str(k[1])+"'", "'"+str(k[2])+"'"
                                    State = State.replace('ü', 'u')
                                    Region = Region.replace('ü', 'u')
                                    kreis = kreis.replace('ü', 'u')
                                    sql_query = "insert into web_location_data (Chiffre,State,Region,kreis) values ('" + \
                                    serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').\
                                    replace('¶','ö') + "','" + State.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').\
                                    replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')+"','"+ Region.replace("'","").\
                                    replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').\
                                    replace('¶','ö')+"','"+ kreis.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').\
                                    replace('¼','ü').replace('Ÿ','ß').replace('¶','ö') + "')"
                                sql = text(sql_query)
                                ## #######pdb.set_trace()
                                db.engine.execute(sql)
                        if str(each_dt.text)[:9] == 'Branches:' and each_dt.findNext('dd').text:
                            a = str(each_dt.findNext('dd').text).replace('\n\n','')
                            a = a.split('\n')
                            for each_standorte in a:
                                k = each_standorte.split('>')
                                if len(k) == 1:
                                    Branch_Level1 = "'"+str(k[0])+"'"
                                    sql_query = "insert into Web_Branch_Data (Chiffre,Branch_Level1) values ('"+serial_no.strip().replace("'","").\
                                        replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')\
                                            + "','" + Branch_Level1.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                                    replace('Ÿ','ß').replace('¶','ö')+"')"
                                if len(k) == 2:
                                    Branch_Level1, Branch_Level2 = "'"+str(k[0])+"'", "'"+str(k[1])+"'"
                                    sql_query = "insert into Web_Branch_Data (Chiffre,Branch_Level1,Branch_Level2) values ('" + \
                                    serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                    replace('Ÿ','ß').replace('¶','ö') + "','" + Branch_Level1.strip().replace("'","").replace('ÃƒÂ','').\
                                    replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')+"','"+ Branch_Level2.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö') + "')"
                                if len(k) == 3:
                                    Branch_Level1, Branch_Level2, Branch_Level3 = "'"+str(k[0])+"'", "'"+str(k[1])+"'", "'"+str(k[2])+"'"
                                    sql_query = "insert into Web_Branch_Data (Chiffre,Branch_Level1,Branch_Level2,Branch_Level3) values ('" + \
                                    serial_no.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').\
                                    replace('¶','ö') + "','" + Branch_Level1.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').\
                                    replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')+"','"+Branch_Level2.strip().replace("'","").replace('ÃƒÂ','')\
                                    .replace('¤','ä').replace('¼','ü').replace('Ÿ','ß').replace('¶','ö')+"','"+\
                                    Branch_Level3.strip().replace("'","").replace('ÃƒÂ','').replace('¤','ä').replace('¼','ü').\
                                                    replace('Ÿ','ß').replace('¶','ö') + "')"
                                sql = text(sql_query)
                                db.engine.execute(sql)
                    date_current = "'"+time.strftime('%Y-%m-%d')+"'"
                    Reference_URL = "'"+new_url+"'"

                    if search_serial:
                   #     # #######pdb.set_trace()
                        print("Crawl broke because it got old Chiffre")
                        break
                    try:
                        sql_query = "insert into web_search_result (First_Crawl_Date,Latest_Crawl,Chiffre,Description,Count_Employees,Last_Turnover,\
                        Asking_Price,Activity,Last_Crawl_Date,Reference_URL,email,Active_Status) values ('"+date.strip().replace("'","").replace("\r\n",'').replace("  ","")+"','"+date.strip().replace("'","").replace("\r\n",'').replace("  ","")+"','"+serial_no.strip().replace("'","").replace("\r\n",'').replace("   "," ")+"','"+description.strip().replace("'","").replace("\r\n",'').replace("   "," ")+ "','"+no_of_emp.strip().replace("\r\n",'').replace("   "," ").replace("'","")+\
                                    "','"+sales.replace("\r\n",'').replace("   "," ").strip().replace("'","")+"','"+asking_price.replace("\r\n",'').replace("   "," ").strip().replace("'","")+"','"+activity.replace("\r\n",'').replace("   "," ").strip().replace("'","")\
                                    +"','"+date_current.replace("\r\n",'').replace("   "," ").strip().replace("'","")+"','"+Reference_URL.replace("\r\n",'').replace("   "," ").strip().replace("'","")+"','"+email_data.replace("\r\n",'').replace("   "," ").strip().replace("'","")+"','True')"
                        sql = text(sql_query)
                        db.engine.execute(sql)
                    except Exception as e:
                       # #pdb.set_trace()
                        sql_query = "alter table web_search_result RENAME column daturn to First_Crawl_Date"
                        sql_query1 = "alter table web_search_result add column Latest_Crawl text"
                        sql_query2 = "update web_search_result set Latest_Crawl  = First_Crawl_Date;"
                        sql_query3 = "alter table web_search_result add column Active_Status text"
                        sql = text(sql_query)
                        db.engine.execute(sql)
                        sql1 = text(sql_query1)
                        db.engine.execute(sql1)
                        sql2 = text(sql_query2)
                        db.engine.execute(sql2)
                        sql3 = text(sql_query3)
                        db.engine.execute(sql3)
                        sql_query = "insert into web_search_result (First_Crawl_Date,Latest_Crawl,Chiffre,Description,Count_Employees,Last_Turnover,\
                                                Asking_Price,Activity,Last_Crawl_Date,Reference_URL,email,Active_Status) values ('" + date.strip().replace("'", "").replace("\r\n", '').replace("  ", "") + "','" + date.strip().replace("'","").replace(
                            "\r\n", '').replace("  ", "") + "','" + serial_no.strip().replace("'", "").replace("\r\n",'').replace( "   ", " ") + "','" + description.strip().replace("'", "").replace("\r\n", '').replace(
                            "   ", " ") + "','" + no_of_emp.strip().replace("\r\n", '').replace("   ", " ").replace("'","") +  "','" + sales.replace("\r\n", '').replace("   ", " ").strip().replace("'","") + "','" + asking_price.replace(
                            "\r\n", '').replace("   ", " ").strip().replace("'", "") + "','" + activity.replace("\r\n",'').replace("   ", " ").strip().replace("'", "")+ "','" + date_current.replace("\r\n", '').replace("   ", " ").strip().replace("'","") + "','" + Reference_URL.replace(
                            "\r\n", '').replace("   ", " ").strip().replace("'", "") + "','" + email_data.replace("\r\n", '').replace("   ", " ").strip().replace("'", "") + "','True')"
                        sql = text(sql_query)
                        db.engine.execute(sql)
                        str(e)
                        ##pdb.set_trace()
            i=0
            print("completed current page---------i------------------------------")
            j+=1
            print(j)
            for each_li in soup.findAll("li"):
                if each_li.get('class') and str(each_li.get('class')[0]) == 'forward':
                    print("test")
                    new_url = "https://www.nexxt-change.org"+str(each_li.a.get('href'))
                    req = urlopen(new_url).read()
                    soup = BeautifulSoup(req, "lxml")
                    i+=1
        prinf(chiffrelist)
        for each in chiffrelist:
            sql_query = "update web_search_result set Latest_Crawl='"+datetime.datetime.today().strftime('%d.%m.%Y')+"', Active_Status='False'  where Chiffre='"+each+"'"
            sql = text(sql_query)
            db.engine.execute(sql)
        sql_query = "update web_search_result set Latest_Crawl='"+datetime.datetime.today().strftime('%d.%m.%Y')+"', Active_Status='False' where Active_Status='' or Active_Status is NULL"
        sql = text(sql_query)
        db.engine.execute(sql)
        return "CRAWLING IS COMPLETED"
    except Exception as e:
        ##pdb.set_trace()

        headers = {'content-type': 'application/json'}
        urlopen("http://localhost:5000/crawl_web/?crawl_broke=yes&new_url="+new_url+"&crawled_link="+str(crawled_link), headers=headers, verify=False)

@main.route('/set_web_data/',  methods = ["GET","POST"])
def set_web_data() :
 #   # #######pdb.set_trace()
    # name = QtGui.QFileDialog.getOpenFileName('Open file')
    # file = open(name)
    # root = tkinter.Tk()
    # root.withdraw()
    # currdir = os.getcwd()
    # tempdir = tkinter.filedialog.askopenfile(parent=root, initialdir=currdir, title='Please select a File')
    # print("")
    excel_file = pd.read_excel(open('C:\\Users\\Sandeep Agrawal\\Documents\\web_data.xlsx','rb'))
    headers = excel_file.columns
    settlement_data = excel_file.as_matrix(columns=headers)
    for each_record in settlement_data:
        branch_code = str(each_record[0]) + str(each_record[1]).replace(".","")
        branch_code = "'"+'{:<05s}'.format(branch_code)+"'"
        business = "'"+str(each_record[2])+"'"
        business_detail = "'"+str(each_record[3])+"'"
        business_exclude = "'"+str(each_record[4])+"'"
        sql_query = "insert into web_data_storage (branchcode,langtitel,einschlusse,ausschlusse) values (" + \
                    branch_code + "," + business + "," + business_detail + "," + business_exclude + ")"
        sql = text(sql_query)
        result1 = db.engine.execute(sql)
        ## #######pdb.set_trace()

@main.route('/test_web_data/',  methods = ["GET","POST"])
def test_web_data() :
    print(current_app.config['SHELF_FILE'])

def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.max()
    return 'background-color: yellow'

@main.route('/KI_Export_salesforce/<aid_firma_list>/<AID>/<search_word>',  methods = ["GET","POST"])
@login_required
def ki_export_salesforce(aid_firma_list,AID,search_word) :
    aid_firma_list = aid_firma_list.split(',')
    #aid_firma_list = ['2050828681']
    data_dict = {'AID':[],'ATE':[]}
    for each_aid in aid_firma_list:
        aid_man, aid_ate = [], []
        man_data = Manager.query.filter_by(MAN_Function='Geschäftsführer',AID_Company=each_aid).all()
        for man_aid in man_data:
            aid_man.append(man_aid.AID_MAN)
        ate_data = Shareholder.query.filter_by(AID_Company=each_aid).all()
        for ate_aid in ate_data:
            aid_ate.append(ate_aid.AID_SHH)
        for each_aid_man in aid_man:
            if each_aid_man in aid_ate:
                data_dict['AID'].append(each_aid)
                data_dict['ATE'].append(each_aid_man)
                break
    exportlog = export_log(type="Salesforce",user_email=current_user.email,AID=AID,keywords=search_word,\
                           date=time.strftime('%Y-%m-%d'),time=time.strftime('%H-%M-%S'))
    db.session.add(exportlog)
    db.session.commit()
    df = pd.DataFrame(data_dict)
    df.style.applymap(highlight_max)
    df.to_csv("Salesforce_Export.csv",index=False)
    filedir = os.path.abspath("")
    filename = filedir + "\Salesforce_Export.csv"
    return send_file(filename,as_attachment=True)

@main.route('/bootstrap_test/', methods = ["GET"])
def bootstrap_test():
    return render_template("bootstrap_test.html",a="hello")
@main.route('/2/3/', methods = ["GET"])
@main.route('/002/', methods = ["GET"])
@login_required
def nexxt_change_filter():
    fiche_det = Fiche_Details.query.filter_by().all()
    email_sub = preferences.query.filter_by(column_name="email_sub").first().value
    email_sub = email_sub.replace("website","nexxt-change.org")
    email_body = preferences.query.filter_by(column_name="email_body").first().value
    user = User.query.filter_by(email=current_user.email).first()
    search_extra = str(request.args.get('search_extra','')).split(",")
    if len(search_extra) < 3:
        search_extra = ['','','']
    branch_browse = Web_Branch_Data.query.filter_by().all()
    Branch_Level1 = []


    if branch_browse:
        for each_branch in branch_browse:
         try:
              if each_branch:
                if each_branch.Branch_Level1 not in Branch_Level1:
                    Branch_Level1.append(each_branch.Branch_Level1)
         except Exception as e:
             print(str(e))
             ##pdb.set_trace()
             print(branch_browse)

    btn_background, btn_text = set_button_color()
    return render_template("next_change_filter.html",Branch_Level1=Branch_Level1,search_extra=search_extra,sektordata=search_extra[0].replace(";",","),\
                           rubrikdata=search_extra[1].replace(";",","),einzelbranchedata=search_extra[2].replace(";",","),email_sub=email_sub,
                           email_body=email_body,btn_background=btn_background,btn_text=btn_text,user=user.username,\
                           fiche_det=fiche_det)

@main.route('/set_next_change_filter/', methods = ["POST"])
@login_required
def set_next_change_filter():
    data_list = []
    if request.values.get('data','') == 'sek':
        branch_browse = Web_Branch_Data.query.filter_by(Branch_Level1=request.values['Branch_Level1']).all()
    if request.values.get('data', '') == 'rub':
        branch_browse = Web_Branch_Data.query.filter_by(Branch_Level2=request.values['Branch_Level2']).all()
    for each_branch in branch_browse:
        if request.values.get('data','') == 'sek' and each_branch.Branch_Level2 and each_branch.Branch_Level2 not in data_list:
            data_list.append(each_branch.Branch_Level2)
        if request.values.get('data','') == 'rub' and each_branch.Branch_Level3 and each_branch.Branch_Level3 not in data_list:
            data_list.append(each_branch.Branch_Level3)
    return json.dumps({'Data':list(set(data_list))})

@main.route('/get_nextchange_data/', methods = ["GET"])
@login_required
def get_nextchange_data():
    data_sort = []
    if "sort" in request.args:
        element = None
        if request.args['sort'] == 'Id':
            element = web_search_result.Id
        elif request.args['sort'] == 'daturn':
            element = web_search_result.Latest_Crawl
        elif request.args['sort'] == 'Chiffre':
            element = web_search_result.Chiffre
        elif request.args['sort'] == 'Description':
            element = web_search_result.Description
        elif request.args['sort'] == 'Count_Employees':
            element = web_search_result.Count_Employees
        elif request.args['sort'] == 'Last_Turnover':
            element = web_search_result.Last_Turnover
        elif request.args['sort'] == 'Asking_Price':
            element = web_search_result.Asking_Price

        if "order" in request.args and element is not None:
            if request.args['order'] == 'desc':
                data_sort.append(element.desc())
            else:
                data_sort.append(element.asc())
    else:
        data_sort.append(web_search_result.Id.asc())
    search = request.args.get('search', '')
    search_extra = request.args.get('search_extra', '').split(",")
    is_search = True
    if search_extra[0] == 'select':
       is_search = False
    else:
        search_sek, search_rub, search_enz = (search_extra[0]), (search_extra[1]), (search_extra[2])
    next_data = web_search_result.query.filter(or_(web_search_result.Id.like('%' + search + '%'),
                                             web_search_result.Latest_Crawl.like('%' + search + '%'),
                                             web_search_result.Chiffre.like('%' + search + '%'),
                                             web_search_result.Description.like('%' + search + '%'),
                                             web_search_result.Count_Employees.like('%' + search + '%'),
                                             web_search_result.Last_Turnover.like('%' + search + '%'),
                                             web_search_result.Asking_Price.like('%' + search + '%'),
                                             )).order_by(*data_sort)
    total_ids = []
    if next_data:
      try:
          for each_next in next_data:
             total_ids.append(each_next.Id)
      except Exception as e:
          ##pdb.set_trace()
          print(str(e))

    output = []
    if not is_search:
        if len(total_ids) == 1:
            final_total_ids = str(tuple(total_ids)).replace(",","")
        else:
            final_total_ids = str(tuple(total_ids))
        sql_query = "select * from web_search_result where Id in "+final_total_ids + " limit " + request.args['limit'] +\
        " offset " + request.args['offset']
        sql_query_count = "select count(*) from web_search_result where Id in " + final_total_ids
    else:
        if len(total_ids) == 1:
            total_ids = str(tuple(total_ids)).replace(",","")
        else:
            total_ids = str(tuple(total_ids))
        if search_sek != 'select' and search_rub == 'select' and search_enz == 'select':
            sql_query = "select * from web_search_result where Id in " + total_ids + " and Chiffre in\
             ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                        + "') limit " + request.args['limit'] + " offset " + request.args['offset']
            sql_query_count = "select count(*) from web_search_result where Id in " + total_ids + " and Chiffre in\
                         ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                        + "') limit " + request.args['limit'] + " offset " + request.args['offset']
        if search_sek != 'select' and search_rub != 'select' and search_enz == 'select':
            sql_query = "select * from web_search_result where Id in " + total_ids + " and Chiffre in\
             ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                        + "' and Branch_Level2='"+str(search_rub)+"') limit " + request.args['limit'] + " offset " + request.args['offset']
            sql_query_count = "select count(*) from web_search_result where Id in " + total_ids + " and Chiffre in\
                         ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                        + "' and Branch_Level2='" + str(search_rub) + "') limit " + request.args['limit'] + " offset " + \
                        request.args['offset']
        if search_sek != 'select' and search_rub != 'select' and search_enz != 'select':
            sql_query = "select * from web_search_result where Id in " + total_ids + " and Chiffre in\
                         ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                        + "' and Branch_Level2='" + str(search_rub) + "' and Branch_Level3='"+str(search_enz) +"')  \
                        limit " + request.args['limit'] + " offset " + \
                        request.args['offset']
            sql_query_count = "select count(*) from web_search_result where Id in " + total_ids + " and Chiffre in\
                                     ( select Chiffre from Web_Branch_Data where Branch_Level1='" + str(search_sek) \
                              + "' and Branch_Level2='" + str(search_rub) + "' and Branch_Level3='"+str(search_enz) + "') limit " +\
                              request.args['limit'] + " offset " + request.args['offset']
    sqlexec = text(sql_query)
    sqlcountexec = text(sql_query_count)
    result1 = db.engine.execute(sqlexec)
    result2 = db.engine.execute(sqlcountexec)
    count_data = result2.fetchone()[0]
    for k in result1:
        dic = {
            "Id":k.Id,
            "daturn": k.Latest_Crawl,
            "Chiffre": k.Chiffre,
            "Description": k.Description,
            "Count_Employees": k.Count_Employees,
            "Last_Turnover": k.Last_Turnover,
            "Asking_Price": k.Asking_Price,
            "Email": k.email
        }
        output.append(dic)
    final_data = {'total': count_data, 'rows': output}
    return json.dumps(final_data)

@main.route('/bootstrap_test_data/', methods = ["GET"])
@login_required
def bootstrap_test_data():
    #a = {'total': 23 , 'rows':[{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'tandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii1','state':'sandstate'}]}
    #return json.dumps(a)
    return send_file("C:\\Users\\Acer\\excel_test.py",as_attachment=True)

@main.route('/send_intimation/<web>/<email_sub>/<Fiche_Number>', methods = ["POST"])
@login_required
def send_intimation(web,email_sub,Fiche_Number):
    # pdb.set_trace()
    web="nexxt-change.org"
    email_ids = request.values.get('to_email','')
    chiffres = request.values.get('chiffres', '')
    chiffres = chiffres.split(",")
    if email_ids:
        email_ids = email_ids.split(",")
        pref_body = str(preferences.query.filter_by(column_name='email_body').first().value) % (web)
        path_list = []
        for each_file in request.files:
            file = request.files[each_file]
            if file.filename:
                file_name = current_app.config["ATTACHMENT"] + str(file.filename).split(".")[0] + time.strftime("_%Y_%M_%S_%H_%M_%S") \
                            + "." + str(file.filename).split(".")[1]
                file.save(file_name)
                path_list.append(file_name)
        k = 0
        for each_email in email_ids:
            pref_body = pref_body.replace("(Variable.Link to a URL ofourwebsite)","")
            email_sub_final = email_sub.replace("Chiffre",chiffres[k])
            send_email([each_email],str(email_sub_final),pref_body,path_list)
            nexxt_change_logs=nexxt_change_intimation_logs(Chiffre="Chiffre",Fiche_Number=str(Fiche_Number),intimation_date=\
                time.strftime('%Y-%m-%d'),user=current_user.username)
            db.session.add(nexxt_change_logs)
            db.session.commit()
            k += 1
    return json.dumps({'Message':'Email Sent Successfully'})

@main.route('/preference/<origin>', methods = ["GET","POST"])
@main.route('/preference/', methods = ["GET","POST"])
@login_required
def preference(origin=None):
    # pdb.set_trace()
    all_data = request.args.get("all_data","yes")
    form = PreferenceForm()
    issubmit = request.values.get('sub','no')
    btn_background, btn_text = set_button_color()
    user = User.query.filter_by(email=current_user.email).first()
    if all_data == "no" and issubmit != 'issubmit':
        print("@#$%^&*@#$%^&*@#$%^&*@#$%^&*@#$%^&*@#$%^&*@#$%^&*@#$%^&*@#$%^&*")
        # pdb.set_trace()
        current_data = request.args.get("current_data","no")
        preference_browse = "no"
        if current_data == "yes":
            preference_browse = preferences.query.filter_by(Id=request.args.get('Id',0)).first()
        return render_template("preference.html", all_data=all_data, form=form, preference_browse=preference_browse,\
                               current_data=current_data,btn_background=btn_background,btn_text=btn_text,user=user.username,origin=origin )
    if form.validate_on_submit():
        preference_browse = preferences.query.filter_by(Id=request.values.get('Id','')).first()
        if preference_browse:
            preference_browse.column_name = request.values.get('column_name','')
            preference_browse.value = request.values.get('value','')
            flash_msg = "Preference updated successfully"
        else:
            preference_detail = preferences(column_name=request.values.get('column_name',''), value=request.values.get('value',''))
            db.session.add(preference_detail)
            flash_msg = "Preference added successfully"
        db.session.commit()
        flash(flash_msg)
        all_data="yes"
        btn_background, btn_text = set_button_color()
    return render_template("preference.html",all_data=all_data,form=form,btn_background=btn_background,btn_text=btn_text,user=user.username, origin=origin)

@main.route('/preference_data/', methods = ["GET"])
@login_required
def preference_data():
    preference_browse = preferences.query.filter_by().all()
    pref_data = {'total':0,'rows':[]}
    i = 0
    for each_pref in preference_browse:
        if each_pref.column_name=="password":
            passw="******"
            pref_data['rows'].append(
                {'Id': each_pref.Id, 'column_name': each_pref.column_name, 'value': passw})
        else:
            pref_data['rows'].append({'Id':each_pref.Id,'column_name':each_pref.column_name,'value':each_pref.value})
        i += 1
    pref_data['total'] = i
    return json.dumps(pref_data)

@main.route('/bundesland_stat_data/', methods = ["GET"])
@login_required
def bundesland_stat_data():
    bundesland_stat_data = {'total': 0, 'rows': []}
    sql_query = "select State,Count,Country from Statistics_State"
    sql = text(sql_query)
    result1 = db.engine.execute(sql)
    i = 0
    for each_result in result1:
        if each_result[1]:
            check_land = 0
            for each_land in bundesland_stat_data['rows']:
                if str(each_land['Country']).split(" -")[0] == each_result[2]:
                    total_state_country = int(str(each_land['Country']).split(" -")[1]) + int(each_result[1])
                    each_land['Country'] = str(each_land['Country']).split(" -")[0] + " - " + str(total_state_country)
                    each_land['data'].append({'state':each_result[0],'Count':each_result[1]})
                    check_land += 1
            if not check_land:
                total_state_country = 0
                total_state_country += int(each_result[1])
                Country = str(each_result[2]) + " - " + str(total_state_country)
                bundesland_stat_data['rows'].append({'Country':Country,'data':[{'state':each_result[0],'Count':each_result[1]}]})
            i += 1
    bundesland_stat_data['total'] = i
    return json.dumps(bundesland_stat_data)


@main.route('/001/010/', methods = ["GET","POST"])
@login_required
def company_data_1_3():
    aidlist = str(request.args.get('aids', ''))
    aidlist = aidlist.split(" ")
    selectedaids=dict()
    if aidlist:
        for each in aidlist:
            idrecord = Entity.query.filter(and_(Entity.AID==str(each),Entity.Entity_Type!='Person' )).first()
            if idrecord:
                selectedaids[str(each)]=str(idrecord.Entity_Last_Name)
    btn_background, btn_text = set_button_color()
    print(selectedaids)
    user = User.query.filter_by(email=current_user.email).first()
    # if origin == 'manualcompany':
    #     return render_template("company_data_single_view.html", btn_background=btn_background, btn_text=btn_text,
    #                            earlier_selected=selectedaids, \
    #                            user=user.username, origin=origin)

    return render_template("company_data_1.html",btn_background=btn_background,btn_text=btn_text,earlier_selected=selectedaids,\
                           user=user.username)

@main.route('/003/<origin>/', methods = ["GET","POST"])
@main.route('/001/<origin>/', methods = ["GET","POST"])
@login_required
def company_data_1_(origin):
    origin=str(origin)
    rule = request.url_rule

    if '001' in rule.rule:
        origin='001'
    if '003' in rule.rule:
        origin = '003'

    aidlist = str(request.args.get('aids', ''))
    aidlist = aidlist.split(" ")
    selectedaids=dict()
    if aidlist:
        for each in aidlist:
            idrecord = Entity.query.filter(and_(Entity.AID==str(each),Entity.Entity_Type!='Person' )).first()
            if idrecord:
                selectedaids[str(each)]=str(idrecord.Entity_Last_Name)
    btn_background, btn_text = set_button_color()
    print(selectedaids)
    user = User.query.filter_by(email=current_user.email).first()
    # if origin == 'manualcompany':
    #     return render_template("company_data_single_view.html", btn_background=btn_background, btn_text=btn_text,
    #                            earlier_selected=selectedaids, \
    #                            user=user.username, origin=origin)

    return render_template("company_data_1.html",btn_background=btn_background,btn_text=btn_text,earlier_selected=selectedaids,\
                           user=user.username,origin=origin)

@main.route('/001/', methods = ["GET","POST"])
@login_required
def company_data_1():
    aidlist = str(request.args.get('aids', ''))
    aidlist = aidlist.split(" ")
    selectedaids=dict()
    if aidlist:
        for each in aidlist:
            idrecord = Entity.query.filter(and_(Entity.AID==str(each),Entity.Entity_Type!='Person' )).first()
            if idrecord:
                selectedaids[str(each)]=str(idrecord.Entity_Last_Name)
    btn_background, btn_text = set_button_color()
    print(selectedaids)
    user = User.query.filter_by(email=current_user.email).first()
    # if origin == 'manualcompany':
    #     return render_template("company_data_single_view.html", btn_background=btn_background, btn_text=btn_text,
    #                            earlier_selected=selectedaids, \
    #                            user=user.username, origin=origin)

    return render_template("company_data_1.html",btn_background=btn_background,btn_text=btn_text,earlier_selected=selectedaids,\
                           user=user.username)

@main.route('/003/010/', methods = ["GET","POST"])
@login_required
def company_data_3():
    aidlist = str(request.args.get('aids', ''))
    aidlist = aidlist.split(" ")
    selectedaids=dict()
    if aidlist:
        for each in aidlist:
            idrecord = Entity.query.filter(and_(Entity.AID==str(each),Entity.Entity_Type!='Person' )).first()
            if idrecord:
                selectedaids[str(each)]=str(idrecord.Entity_Last_Name)
    btn_background, btn_text = set_button_color()
    print(selectedaids)
    user = User.query.filter_by(email=current_user.email).first()

    return render_template("company_data_single_view.html", btn_background=btn_background, btn_text=btn_text,
                           earlier_selected=selectedaids, \
                           user=user.username)


@main.route('/003/010/<origin1>',methods = ["GET","POST"])
@login_required
def company_report(origin1):
    # pdb.set_trace()
    auto_aid = origin1.split(",")
    autofill_aid = ""
    for temp in auto_aid:
        autofill_aid = autofill_aid + str(int(temp, 0)) + " "
    origin = autofill_aid.strip()
    # origin = int(origin1, 0)
    adress=Address.query.filter_by(AID = origin).first()
    web_test=Contact.query.filter_by(AID=origin).all()
    # sql="select Contact_Type,Contact_Value from Contact where AID='"+origin+"'"
    # cd = db.engine.execute(text(sql)).fetchall()
    web_contact, email, tel='','',''
    # pdb.set_trace()
    for each in web_test:
        if each.Contact_Type == 'Telefon':
            tel=each.Contact_Value
        elif each.Contact_Type == 'Internet':
            web_contact=each.Contact_Value
        elif each.Contact_Type == 'eMail':
            email=each.Contact_Value
        else:
            pass
    entity=Entity.query.filter_by(AID = origin).first()
    branchcode = Branch_Codes.query.filter_by(AID=origin).all()
    # branch_text_o = Branch_Text.query.filter_by(Branch_Code=branchcode.Branch_Code).first()
    wz_primary,wz_secondary,avandil_primary,avandil_secondary='',[],'',[]
    for each in branchcode:
        branch_text=""
        branch_text_o = Branch_Text.query.filter_by(Branch_Code=each.Branch_Code).first()
        # if each.WZ08_Branch_Text_DE:
        branch_text = branch_text_o.Branch_Text
        if each.Branch_Rank_Main_Sec[0:2] == 'WZ' and each.Branch_Rank_Main_Sec[-1] == 'H':
            wz_primary=each.Branch_Code + " " + branch_text
        elif each.Branch_Rank_Main_Sec[0:2] == 'WZ' and each.Branch_Rank_Main_Sec[-1] == 'N':
            wz_secondary.append(each.Branch_Code + " " + branch_text)
        elif each.Branch_Rank_Main_Sec[0:2] == 'BD' and each.Branch_Rank_Main_Sec[-1] == 'H':
            avandil_primary=each.Branch_Code + " " + branch_text
        elif each.Branch_Rank_Main_Sec[0:2] == 'BD' and each.Branch_Rank_Main_Sec[-1] == 'N':
            avandil_secondary.append(each.Branch_Code + " " + branch_text )
        else:
            pass
    currentYear = int(datetime.datetime.today().year)

    print(type(currentYear))
    years=[]
    fin_value=['','','','','','','']
    for i in range(7):
        years.append(str(currentYear))
        currentYear-=1
    financial = financials.query.filter_by(AID=origin).all()
    employee=['','','','','','','']
    # pdb.set_trace()
    for temp in financial:

        if temp.Year == years[0] and temp.Fin_Value_Type == 'Mitarbeiter':

            employee[0]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[1] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[1]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[2] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[2]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[3] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[3]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[4] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[4]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[5] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[5]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[6] and temp.Fin_Value_Type == 'Mitarbeiter':
            employee[6]=german_formating(int(temp.Fin_Value))
        else:
            pass


        if temp.Year == years[0] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[0]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[1] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[1]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[2] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[2]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[3] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[3]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[4] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[4]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[5] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[5]=german_formating(int(temp.Fin_Value))
        elif temp.Year == years[6] and temp.Fin_Value_Type == 'Umsatz':
            fin_value[6]=german_formating(int(temp.Fin_Value))
        else:
            pass
    business_activity=Business_Activity.query.filter_by(AID = origin).first()
    shareholder=Shareholder.query.filter_by(AID_Company = origin).all()
    # pdb.set_trace()
    shared_data=[]
    if shareholder:
        for share_obj in shareholder:
            shar_holder_data = ['','','','','','','']
            Shh_entity = Entity.query.filter_by(AID=share_obj.AID_SHH).first()
            shh_address= Address.query.filter_by(AID=share_obj.AID_SHH).first()
            shar_holder_data[0]=Shh_entity.Entity_First_Name+" "+Shh_entity.Entity_Last_Name
            shar_holder_data[1]=shh_address.Country
            shar_holder_data[2]=Shh_entity.Entity_Type
            shar_holder_data[3]=share_obj.SHH_Shares_Owned
            sort_by = []
            element = financials.Year
            # element1 = financials.Fin_Value
            sort_by.append(element.desc())
            # sort_by.append(element1.desc())
            fin_search_u = financials.query.filter(
                and_(financials.AID == share_obj.AID_Company, financials.Fin_Value_Type == "Umsatz")) \
                .order_by(*sort_by).first()
            if fin_search_u:

                temp_fin_val = german_formating(int((fin_search_u.Fin_Value)))
                temp_fin_yr = fin_search_u.Year
                final_text = temp_fin_val + " (" + temp_fin_yr + ")"
                shar_holder_data[4] = final_text
            else:
                shar_holder_data[4]=''
            fin_search_m = financials.query.filter(and_(financials.AID == share_obj.AID_Company, financials.Fin_Value_Type == "Mitarbeiter")) \
                .order_by(*sort_by).first()
            if fin_search_m:
                temp_fin_val = german_formating(int((fin_search_m.Fin_Value)))
                temp_fin_yr = fin_search_m.Year
                final_text = temp_fin_val + " (" + temp_fin_yr + ")"
                shar_holder_data[5]=final_text
            else:
                shar_holder_data[5]=''
            web_shar = Contact.query.filter(and_(Contact.AID == share_obj.AID_SHH, Contact.Contact_Type == 'Internet')).first()
            if web_shar:
                shar_holder_data[6]=web_shar.Contact_Value
            else:
                shar_holder_data[6] = 'n/a'


            shared_data.append(shar_holder_data)



    manager = Manager.query.filter_by(AID_Company=origin).all()
    man_data=[]
    for man_obj in manager:
        man_info=[]
        man_entity=Entity.query.filter_by(AID=man_obj.AID_MAN).first()
        shareholder = Shareholder.query.filter_by(AID_SHH=man_obj.AID_Company).first()
        man_info.append(man_entity.Entity_First_Name+" "+man_entity.Entity_Last_Name)
        man_info.append(man_entity.Birth_Year)
        man_info.append(man_obj.MAN_Position)
        man_info.append(man_obj.MAN_Function)
        if shareholder:
            man_info.append(shareholder.SHH_Function)
        else:
            man_info.append('n/a')
        man_data.append(man_info)

    subcidiry = Shareholder.query.filter_by(AID_SHH=origin).all()
    subsidry = []
    if subcidiry:
        # pdb.set_trace()
        for shr_obj in subcidiry:
            temp_sub=[]
            Shh_entity = Entity.query.filter_by(AID=shr_obj.AID_Company).first()
            shh_address = Address.query.filter_by(AID=shr_obj.AID_Company).first()
            temp_sub.append(Shh_entity.Entity_Last_Name)
            temp_sub.append(shh_address.Country)
            temp_sub.append(Shh_entity.Entity_Type)
            temp_sub.append(shr_obj.SHH_Shares_Owned)
            fin_search_u = financials.query.filter(
                and_(financials.AID == shr_obj.AID_Company, financials.Fin_Value_Type == "Umsatz")) \
                .order_by(*sort_by).first()
            if fin_search_u:
                # temp_sub.append(german_formating(int((fin_search_u.Fin_Value))))
                temp_fin_val=german_formating(int((fin_search_u.Fin_Value)))
                temp_fin_yr=fin_search_u.Year
                final_text=temp_fin_val+" ("+temp_fin_yr+")"
                temp_sub.append(final_text)
            else:
                temp_sub.append('n/a')

            fin_search_m = financials.query.filter(
                and_(financials.AID == shr_obj.AID_Company, financials.Fin_Value_Type == "Mitarbeiter")) \
                .order_by(*sort_by).first()
            if fin_search_m:
                if str(fin_search_m.Fin_Value).find(',') != -1:
                    final_search_val=int(float(str(fin_search_m.Fin_Value).replace(',','.')))
                else:
                    final_search_val=int((fin_search_m.Fin_Value))
                temp_fin_val = german_formating(final_search_val)
                temp_fin_yr = fin_search_m.Year
                final_text = temp_fin_val + " (" + temp_fin_yr + ")"
                temp_sub.append(final_text)
                # temp_sub.append(german_formating(int((fin_search_m.Fin_Value))))
            else:
                temp_sub.append('n/a')
            web = Contact.query.filter(and_(Contact.AID==shr_obj.AID_Company,Contact.Contact_Type == 'Internet')).first()
            if web:
                temp_sub.append(web.Contact_Value)
            subsidry.append(temp_sub)
    legal_info=[]
    legal_info.append("")
    legal_info.append(entity.Legal_Form)
    legal_info.append(entity.Legal_Form)
    legal_info.append(entity.Birth_Year)

    finan_group=[]
    finan_group.append(entity.Count_Group_Companies)
    finan_group.append(entity.Count_Shareholders)
    finan_group.append(entity.Count_Subsidiaries)
    emp_ind=0
    for emp in employee:
        if emp:
            emp_ind=employee.index(emp)
            break
    latest_year_emp = years[emp_ind]
    latest_emp = employee[emp_ind]
    fin_ind=0
    for fin in fin_value:
        if fin:
            fin_ind=fin_value.index(fin)
            break
    latest_year_fin = years[fin_ind]

    latest_fin=fin_value[fin_ind]
    tm=time.strftime("%d.%m.%y|%H.%M.%S")


    return render_template("company_report.html",address=adress,web_contact=web_contact,email=email,tel=tel, Entity=entity,wz_primary=wz_primary,\
                           wz_secondary=wz_secondary,avandil_primary=avandil_primary,avandil_secondary=avandil_secondary,years=years,\
                           employee=employee,fin_value=fin_value,business_activity=business_activity,\
                           shared_data=shared_data,man_data=man_data,subsidry=subsidry,legal_info=legal_info,finan_group=finan_group,\
                          latest_emp=latest_emp,latest_year_emp=latest_year_emp,latest_fin=latest_fin,latest_year_fin=latest_year_fin,tm=tm)

@main.route('/company_data_detail/', methods = ["GET","POST"])
@login_required
def company_data_detail():
    # pdb.set_trace()
    company_data = {'total': 0, 'rows': []}
    if 'search' not in request.args:
        search = ""
    else:
        search = request.args['search']
    if search:
        sort_by = []

        if "sort" in request.args:
            element = None
            if request.args['sort'] == 'Entity_Last_Name':
                element = Entity.Entity_Last_Name
            elif request.args['sort'] == 'Entity_Type':
                element = Entity.Entity_Type
            elif request.args['sort'] == 'AID':
                element = Entity.AID
            if "order" in request.args and element is not None:
                if request.args['order'] == 'desc':
                    sort_by.append(element.desc())
                else:
                    sort_by.append(element.asc())
        else:
            sort_by.append(Entity.ID.asc())
        print("Test----------------------")
        #step 1 code
        '''entity_search = Entity.query.filter(or_(Entity.Entity_Last_Name.like('%' + search + '%'),\
                                               Entity.Entity_Type.like('%' + search + '%'),Entity.AID.like('%' + search + '%'))) \
                                       .order_by(*sort_by)
        count = entity_search.count()
    
    
        print("count of records"+str(count))
        #spthis code is replace by sqlite3 library because  above line is taking huge time to execute
    
    
        if 'limit' in request.args:
           entity_search = entity_search.limit(request.args['limit'])
        if 'offset' in request.args:
           entity_search = entity_search.offset(request.args['offset'])
        print("---------------------before getting all data-----------------------------------------------")
        final_entity = entity_search.all()
        for each_entity in final_entity:
            ## #######pdb.set_trace()
            ief each_entity:
                address_search = Address.query.filter(and_(Address.AID==each_entity.AID, \
                                                                        Address.State.like('%' + search + '%'))).first()
                State = ""
                if address_search:
                    State = address_search.State
                company_each = {'index':each_entity.ID,'Entity_Last_Name':each_entity.Entity_Last_Name, 'entity_type': each_entity.Entity_Type,\
                                'AID':each_entity.AID,'State':State}
                company_data['rows'].append(company_each)
        company_data['total'] = count'''


        #step 2

        if search != "":
            sql_query_count = "select count(*) from Entity as e where e.Entity_Type != 'Person' and (e.AID like '%" + search + "%' or  " + \
                              "e.Entity_Last_Name like '%" + search + "%' or e.Entity_Type like '%" + search + "%' or e.AID like '%" + search + "%') order by id asc   "
            sql_query = "select id, AID ,Entity_Last_Name, Entity_Type from Entity e where e.Entity_Type != 'Person' and  (e.AID like '%" + search + "%' or " + \
                              "e.Entity_Last_Name like '%" + search + "%' or e.Entity_Type like '%" + search + "%' or e.AID like '%" + search + "%')  order by id asc   limit " + \
                        request.args['limit'] + " offset " + request.args['offset']
        else:

            sql_query_count = "select count(*) from Entity where Entity_Type!='Person' order by id asc "
            sql_query = "select id, AID ,Entity_Last_Name, Entity_Type from Entity where Entity_Type!='Person' order by id asc   limit " + \
                        request.args['limit'] + " offset " + request.args['offset']

        sqlcountexec = text(sql_query_count)
        result2 = db.engine.execute(sqlcountexec)
        count = result2.fetchone()[0]
        sqlcountexec1 = text(sql_query)
        result1 = db.engine.execute(sqlcountexec1)

        dic = []
        for k in result1:
            dic.append({
                "index": k[0],
                "AID": k[1],
                "Entity_Last_Name": k[2],
                "Entity_Type": k[3]

            })
        def sort_list(list_of_dict,search):
            m = {}
            i = 0
            for k in list_of_dict:
                m[k['Entity_Last_Name']] = i
                i += 1
            c,d=[],[]
            j=0
            for u in list(m.keys()):
                if len(str(u)) > str(u).lower().find(search.lower()) + len(search) and not str(u)[str(u).lower().find(search.lower()) \
                                                                                              + len(search)].isalpha():
                    c.append(str(u).lower().find(search.lower()))
                else:
                    c.append(str(u).lower().find(search.lower()) + 0.1)
                d.append(j)
                j += 1
            n1,n2 = sort_by_key(c,d)
            b = []
            l=0
            for each in n2:
                b.append(list_of_dict[each])
                l += 1
            return b
        if search:
            dic = sort_list(dic,search)
        branchcode=""
        for each_entity in dic:

            if each_entity:
                if search != "":
                    sql_query_address = "select State,City from Address as a where   a.AID ='" + each_entity["AID"] + "'"
                else:
                    sql_query_address = "select State,City from Address as a where   a.AID ='" + each_entity["AID"]+"'"

                sqlcountexec3 = text(sql_query_address)
                result3 = db.engine.execute(sqlcountexec3)
                address_search = result3.fetchone()

                State,city = "",""
                if address_search:
                    State = address_search[0]
                    city = address_search[1]
                if search != "":
                    sql_query_address = "select Contact_Value from Contact as a where a.Contact_Type='Internet' and   a.AID ='" + \
                                        each_entity["AID"] + "'"
                else:
                    sql_query_address = "select Contact_Value from Contact as a where a.Contact_Type='Internet' and  a.AID ='" + \
                                        each_entity["AID"] + "'"
                sqlcountexec3 = text(sql_query_address)
                result3 = db.engine.execute(sqlcountexec3)
                website_search = result3.fetchone()
                if website_search:
                    website_search = website_search[0]
                if search != "":
                    sql_query_address = "select Branch_Code from Branch_Codes as a where  a.AID ='" + \
                                        each_entity["AID"] + "'"
                else:
                    sql_query_address = "select Branch_Code from Branch_Codes as a where a.AID ='" + \
                                        each_entity["AID"] + "'"
                sqlcountexec3 = text(sql_query_address)
                result3 = db.engine.execute(sqlcountexec3)
                branchcode_search = result3.fetchone()
                if branchcode_search:
                    branchcode = branchcode_search[0]
                company_each = {'index': replace_case_insensitive(search,each_entity["index"]), 'Entity_Last_Name': replace_case_insensitive(search,each_entity["Entity_Last_Name"]),
                                'Entity_Type': replace_case_insensitive(search,each_entity["Entity_Type"]), \
                                'AID': replace_case_insensitive(search,each_entity["AID"]), 'State': replace_case_insensitive(search,city+" , "+State),
                                'website':replace_case_insensitive(search,website_search),'Mainbranchcode':replace_case_insensitive(search,branchcode)}
                company_data['rows'].append(company_each)
        company_data['total'] = count

        # step 3 code for getting count
        # import sqlite3
        # conn=sqlite3.connect("E:\\database\\AVAD-2018-05.db")
        # c=conn.cursor()
        # cexecute=c.execute("select count(*) from Entity")
        # count= cexecute.fetchone()[0]


    return json.dumps(company_data)

@main.route('/branchwise_stat/', methods = ["GET","POST"])
@login_required
def branchwise_stat():
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("branchwise_stat.html",user=user.username)

@main.route('/branchwise_crawl_data/', methods = ["GET","POST"])
@login_required
def branchwise_crawl_data():
    branchwise_stat_data = {'total': 0, 'rows': []}
    sql_query = "select count(*),Branch_Level1 from Web_Branch_Data group by Branch_Level1"
    sql = text(sql_query)
    result1 = db.engine.execute(sql)
    i = 0
    for each_result in result1:
        branchwise_stat_data['rows'].append({'branch': each_result[1], 'Count': each_result[0]})
        i += 1
    branchwise_stat_data['total'] = i
    return json.dumps(branchwise_stat_data)

@main.route('/dub_filter/', methods = ["GET"])
@login_required
def dub_filter():
    search_extra = str(request.args.get('search_extra', '')).split(",")
    user = User.query.filter_by(email=current_user.email).first()
    if len(search_extra) < 3:
        search_extra = ['', '', '']
    branch_browse = dub_branch_data.query.filter_by().all()
    branch1 = []
    for each_branch in branch_browse:
        if each_branch.branch1 not in branch1:
            branch1.append(each_branch.branch1)
    btn_background, btn_text = set_button_color()
    return render_template("dub_filter.html",branch1=branch1,search_extra=search_extra,branch1data=search_extra[0],\
                           branch2data=search_extra[1],branch3data=search_extra[2],btn_background=btn_background,\
                           btn_text=btn_text,user=user.username)

@main.route('/set_dub_filter/', methods = ["POST"])
@login_required
def set_dub_filter():
    data_list = []
    if request.values.get('data','') == 'branch1':
        branch_browse = dub_branch_data.query.filter_by(branch1=request.values['branch1']).all()
    if request.values.get('data', '') == 'branch2':
        branch_browse = dub_branch_data.query.filter_by(branch2=request.values['branch2']).all()
    for each_branch in branch_browse:
        if request.values.get('data','') == 'branch1' and each_branch.branch2 and each_branch.branch2 not in data_list:
            data_list.append(each_branch.branch2)
        if request.values.get('data','') == 'branch2' and each_branch.branch3 and each_branch.branch3 not in data_list:
            data_list.append(each_branch.branch3)
    return json.dumps({'Data':list(set(data_list))})

@main.route('/dub_filter_data/', methods = ["GET"])
@login_required
def dub_filter_data():
    if 'search' not in request.args:
        search = ""
    else:
        search = request.args['search']
    search_extra = request.args.get('search_extra', '').split(",")
    is_search = True
    if search_extra[0] == 'select':
        is_search = False
    else:
        search_branch1, search_branch2, search_branch3 = search_extra[0], search_extra[1], search_extra[2]
    dub_browse = Portal_DUB_web_crawl.query.filter(or_(Portal_DUB_web_crawl.Id.like('%' + search + '%'),Portal_DUB_web_crawl.Description.like('%' + search + '%'),\
                                                   Portal_DUB_web_crawl.Branches.like('%' + search + '%'),Portal_DUB_web_crawl.Turnover.like('%' + search + '%'),\
                                                   Portal_DUB_web_crawl.Employees.like('%' + search + '%')))
    total_ids = []
    for each_dub in dub_browse:
        total_ids.append(each_dub.Id)
    if not is_search:
        if len(total_ids) == 1:
            final_total_ids = str(tuple(total_ids)).replace(",", "")
        else:
            final_total_ids = str(tuple(total_ids))
        sql_query = "select * from Portal_DUB_web_crawl where Id in " + final_total_ids + " limit " + request.args[
            'limit'] + \
                    " offset " + request.args['offset']
        sql_query_count = "select count(*) from Portal_DUB_web_crawl where Id in " + final_total_ids
    else:
        if len(total_ids) == 1:
            total_ids = str(tuple(total_ids)).replace(",", "")
        else:
            total_ids = str(tuple(total_ids))
        if search_branch1 != 'select' and search_branch2 == 'select' and search_branch3 == 'select':
            sql_query = "select * from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                 ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                        + "') limit " + request.args['limit'] + " offset " + request.args['offset']
            sql_query_count = "select count(*) from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                             ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                              + "') limit " + request.args['limit'] + " offset " + request.args['offset']
        if search_branch1 != 'select' and search_branch2 != 'select' and search_branch3 == 'select':
            sql_query = "select * from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                 ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                        + "' and branch2='" + str(search_branch2) + "') limit " + request.args['limit'] + " offset " + \
                        request.args['offset']
            sql_query_count = "select count(*) from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                             ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                              + "' and branch2='" + str(search_branch2) + "') limit " + request.args['limit'] + " offset " + \
                              request.args['offset']
        if search_branch1 != 'select' and search_branch2 != 'select' and search_branch3 != 'select':
            sql_query = "select * from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                             ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                        + "' and branch2='" + str(search_branch2) + "' and branch3='" + str(search_branch3) + "')  \
                            limit " + request.args['limit'] + " offset " + \
                        request.args['offset']
            sql_query_count = "select count(*) from Portal_DUB_web_crawl where Id in " + total_ids + " and Id in\
                                         ( select dub_id from dub_branch_data where branch1='" + str(search_branch1) \
                              + "' and branch2='" + str(search_branch2) + "' and branch3='" + str(
                search_branch3) + "') limit " + \
                              request.args['limit'] + " offset " + request.args['offset']
    sqlexec = text(sql_query)
    sqlcountexec = text(sql_query_count)
    result1 = db.engine.execute(sqlexec)
    result2 = db.engine.execute(sqlcountexec)
    count_data = result2.fetchone()[0]
    dub_data = {'total': 0, 'rows': []}
    for each in result1:
        dub_data['rows'].append({"Id": each.Id,'Description': each.Description,'Branches':each.Branches,\
                              'Turnover':each.Turnover,'Employees':each.Employees})
    dub_data['total'] = count_data
    return json.dumps(dub_data)

@main.route('/biztrade_filter/', methods = ["GET"])
@login_required
def biztrade_filter():
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("biztrade_filter.html",user=user.username)

@main.route('/biztrade_filter_data/', methods = ["GET"])
@login_required
def biztrade_filter_data():
    if 'search' not in request.args:
        search = ""
    else:
        search = request.args['search']
    biz_trade_browse = biz_trade_suche_crawl.query.filter(or_(biz_trade_suche_crawl.Id.like('%' + search + '%'),\
                                                              biz_trade_suche_crawl.Description.like('%' + search + '%'),\
                                                              biz_trade_suche_crawl.Branches.like('%' + search + '%'),\
                                                              biz_trade_suche_crawl.Turnover.like('%' + search + '%')))
    final_biz_trade_count = biz_trade_browse.count()
    biz_trade_data = {'total': 0, 'rows': []}
    if 'limit' in request.args:
        biz_trade_browse = biz_trade_browse.limit(request.args['limit'])
    if 'offset' in request.args:
        biz_trade_browse = biz_trade_browse.offset(request.args['offset'])
    final_biz_trade_browse = biz_trade_browse.all()
    for each in final_biz_trade_browse:
        biz_trade_data['rows'].append({"Id": each.Id,'Description': each.Description,'Branches':each.Branches,\
                              'Turnover':each.Turnover})
    biz_trade_data['total'] = final_biz_trade_count
    return json.dumps(biz_trade_data)

@main.route('/export_logs/', methods = ["GET"])
@login_required
def export_logs():
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("export_log_data.html",user=user.username)

@main.route('/export_log_data/', methods = ["GET"])
@login_required
def export_log_data():
    if 'search' not in request.args:
        search = ""
    else:
        search = request.args['search']
    export_log_brw = export_log.query.filter(or_(export_log.Id.like('%' + search + '%'), \
                                                 export_log.type.like('%' + search + '%'), \
                                                 export_log.user_email.like('%' + search + '%'), \
                                                 export_log.AID.like('%' + search + '%'), \
                                                 export_log.keywords.like('%' + search + '%'), \
                                                 export_log.date.like('%' + search + '%'), \
                                                 export_log.time.like('%' + search + '%')))
    export_log_brw_count = export_log_brw.count()
    export_data = {'total': 0, 'rows': []}
    if 'limit' in request.args:
        export_log_brw = export_log_brw.limit(request.args['limit'])
    if 'offset' in request.args:
        export_log_brw = export_log_brw.offset(request.args['offset'])
    final_export_log_brw = export_log_brw.all()
    for each in final_export_log_brw:
        if each:
            export_data['rows'].append({"Id": each.Id,'type': each.type,'user_email':each.user_email,\
                              'AID':each.AID,'keywords':each.keywords,'date':each.date,'time':each.time})
    export_data['total'] = export_log_brw_count
    return json.dumps(export_data)

from .avad_data_upload_functions import *
@main.route('/avad_data_upload/<optionselected>', methods = ["GET"])
@login_required
def avad_data_upload(optionselected=None):
    contentdisplay=optionselected
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("avad_data_upload.html",show_entity=contentdisplay,user=user.username)

def avad_upload_logs_update(action):
    avadlogs = avad_upload_logs(action=action,time=time.strftime("%Y-%m-%d %H-%M-%S"),user=current_user.email)
    db.session.add(avadlogs)
    db.session.commit()

@main.route('/single_file_upload', methods = ["POST"])
@login_required
def single_file_upload():
    user = User.query.filter_by(email=current_user.email).first()
    try:

        err = ""
        if sys.platform == 'linux':
            backslash = "/"
        else:
            backslash = "\\"
        filedir = os.getcwd()
        print(filedir)
        UPLOAD_FOLDER = filedir + backslash + "uploads" + backslash
        file = request.files['onefile']
        type(file)
        filename = file.filename
        try:
          file.save(os.path.join(UPLOAD_FOLDER, filename))
        except :
            if not os.path.isdir("uploads"):
                   os.mkdir("uploads")
            file.save(os.path.join(UPLOAD_FOLDER, filename))
        file_name=UPLOAD_FOLDER+filename
        Textdatei_konvertieren("Database",file_name)
        return render_template("avad_data_upload.html", user=user.username)
    except Exception as e:
        err = str(e)
        msg = "<b>Error : </b>" + str(e) + "<br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
    finally:
        if err:
            pass
            #send_email(current_app.config["EMAIL_ADDRESS"], "Single File Avad Data Upload Error", msg)
        else:
            msg = "<b>Successful single file upload </b><br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
        #    send_email(current_app.config["EMAIL_ADDRESS"], "Single File Avad Data Upload Success", msg)
            avad_upload_logs_update("Single File Upload")

@main.route('/files_upload', methods = ["POST"])
@login_required
def files_upload():
    try:
        err = ""
        backslash = "\\"
        filedir = os.getcwd()
        print(filedir)
        UPLOAD_FOLDER = filedir + backslash + "uploads" + backslash
        allfiles=request.files.to_dict(flat=False)
        for eachfile in allfiles['multiplefiles']:
            print(eachfile)
            print(type(eachfile))

            try:
                filename = eachfile.filename.split("/")[1]
            except:
                filename = eachfile.filename.split("/")[0]
            try:
              #  pdb.set_trace()
                eachfile.save(os.path.join(UPLOAD_FOLDER, filename))
            except :
                if not os.path.isdir("uploads"):
                   os.mkdir("uploads")
                eachfile.save(os.path.join(UPLOAD_FOLDER, filename))
            file_name=UPLOAD_FOLDER+filename
            print(file_name)
            Textdatei_konvertieren("Database",file_name)
        return render_template("avad_data_upload.html")
    except Exception as e:
        err = str(e)
        msg = "<b>Error : </b>" + str(e) + "<br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
    finally:
        if err:
          pass#  send_email(current_app.config["EMAIL_ADDRESS"], "Folder Avad Data Upload Error", msg)
        else:
            msg = "<b>Successful Folder Upload </b><br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
         #   send_email(current_app.config["EMAIL_ADDRESS"], "Folder Avad Data Upload Success", msg)
            avad_upload_logs_update("Folder Upload")

@main.route('/Datenbank_Update', methods = ["POST","GET"])
@login_required
def Datenbank_Update():
    user = User.query.filter_by(email=current_user.email).first()
    try:
        err=""
        db_file = current_app.config["DBDIR"]
        Upsert_Upload(db_file)
        return render_template("avad_data_upload.html",user=user.username)
    except Exception as e:
        err = str(e)
        msg = "<b>Error : </b>" + str(e) + "<br><b> Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
    finally:
        if err:
           pass# send_email(current_app.config["EMAIL_ADDRESS"], "Datenbank Update Avad Data Upload Error", msg)
        else:
            msg = "<b>Successful Datenbank Update upload </b><br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
           # send_email(current_app.config["EMAIL_ADDRESS"], "Datenbank Update Avad Data Upload Success", msg)
            avad_upload_logs_update("Datenbank Update")

def update_word_table(clean_word,raw_word,AID,wr_me):
    temp = Word_Mesh.query.filter_by(word_stem=clean_word).first()
    if temp:
        aid=temp.AID+", "+AID
        temp.AID=aid
        if temp.word.find(raw_word)==-1:
            temp.word=temp.word+", "+raw_word
    else:
        aid=AID
        mesh_detail = Word_Mesh(word_stem=clean_word,word=raw_word,AID=aid)
        db.session.add(mesh_detail)
    if wr_me:
        wr_me.Count=wr_me.Count+1
    else:
        wrd_cnt_data=Wordcount(word=raw_word,Count=1,Stem=clean_word)
        db.session.add(wrd_cnt_data)


@main.route('/Update_TFIDF', methods = ["POST","GET"])
@login_required
def Update_TFIDF():
        try:
            err=0
            date = str(datetime.datetime.now())
            today = date[:10]
            # database = current_app.config["DBDIR"]
            # connection = sqlite3.connect(database)
            # tfidf_cursor = connection.cursor()
            # support_cursor = connection.cursor()
            tfidf.query.delete()
            tfidf_cursor=Business_Activity.query.filter().all()
            # sql = "DELETE  FROM TFIDF"
            # tfidf_cursor.execute(sql)
            # sql = "SELECT * FROM Business_Activity"
            # tfidf_cursor.execute(sql)
            i = 0

            for res in tfidf_cursor:
                if i >= 13166:
                    print(logging.warning("00000000000000000000000000000000000000000000000000"+str(i)))
                    AID, taetigkeit = res.AID,res.Activity_Description_ROC
                    splited_descrip = taetigkeit.split(" ")
                    for each_word in splited_descrip:
                        print("222222222222222222222222222222")
                        raw_word=each_word.replace(".","").replace(":","")
                        clean_word=Stemmer(raw_word).stem
                        search_1="%"+AID+"%"
                        search_2 = "%" + AID
                        search_3 = AID + "%"
                        search_4 = AID
                        wr_mesh = Word_Mesh.query.filter(and_(Word_Mesh.word_stem==clean_word,or_(Word_Mesh.AID.like(search_1),Word_Mesh.AID.like(search_2),\
                                                             Word_Mesh.AID.like(search_3),Word_Mesh.AID==search_4))).first()
                        wr_me = Wordcount.query.filter(and_(Wordcount.word == raw_word, Wordcount.Stem == clean_word)).first()
                        if not wr_mesh:
                            update_word_table(clean_word, raw_word, AID,wr_me)
                        elif wr_me:
                            wr_me.Count=wr_me.Count-1
                            wr_mesh_aid=wr_mesh.AID
                            if wr_mesh.AID.find(AID+", ")!= -1:
                                wr_mesh_aid=wr_mesh.AID.replace(AID+", ","")
                            elif wr_mesh.AID.find(", "+AID)!= -1:
                                wr_mesh_aid=wr_mesh.AID.replace(", "+AID,"")
                            wr_mesh.AID=wr_mesh_aid
                            # Word_Mesh.query.filter_by(AID=AID).delete()
                            # sql="delete from Word_Mesh where AID='"+AID+"'"
                            # db.engine.execute(sql)
                            update_word_table(clean_word, raw_word, AID,wr_me)
                    dict1 = {}
                    if AID == "Crefo":
                        continue
                    dict1.setdefault("AID", AID)
                    dict1.setdefault("Business_Activity", taetigkeit)
                    entityy=Entity.query.filter_by(AID=AID).first()
                    # sql = "SELECT Entity_Last_Name FROM Entity WHERE AID = '" + AID + "'"
                    # support_cursor.execute(sql)
                    # Entity = support_cursor.fetchone()
                    if entityy:
                        dict1.setdefault("Name", entityy.Entity_Last_Name)
                    else:
                        continue
                    # sql = "SELECT Branch_Code FROM Branch_Codes WHERE AID = '" + AID + "' AND Branch_Rank_Main_Sec = 'WZ08H'"
                    # support_cursor.execute(sql)
                    bcodeH = Branch_Codes.query.filter(and_(Branch_Codes.AID==AID,Branch_Codes.Branch_Rank_Main_Sec=="WZ08H")).first()
                    if bcodeH:
                        dict1.setdefault("Branch_Code_Main", bcodeH.Branch_Code)
                    else:
                        continue
                    # sql = "SELECT Branch_Code FROM Branch_Codes WHERE AID = '" + AID + "' AND (Branch_Rank_Main_Sec = 'WZ08H' OR Branch_Rank_Main_Sec = 'WZ08N')"
                    # support_cursor.execute(sql)
                    bcodeN = Branch_Codes.query.filter(and_(Branch_Codes.AID==AID,\
                                or_(Branch_Codes.Branch_Rank_Main_Sec=="WZ08H",Branch_Codes.Branch_Rank_Main_Sec=="WZ08N"))).all()
                    if bcodeN:
                        bcN = ""
                        for code in bcodeN:
                            bcN = bcN + " " + code.Branch_Code
                        dict1.setdefault("Branch_Codes_Secondaries", bcN)
                    else:
                        continue
                    dict1.setdefault("Internet", "")
                    dict1.setdefault("Last_Change", today)
                    tfidf_data = tfidf(AID=dict1["AID"],Name=dict1["Name"],Business_Activity=dict1["Business_Activity"],\
                                Branch_Code_Main=dict1["Branch_Code_Main"],Branch_Codes_Secondaries=dict1["Branch_Codes_Secondaries"], \
                                Internet=dict1["Internet"], Last_Change=dict1["Last_Change"]  )
                    db.session.add(tfidf_data)
                    db.session.commit()
                    print(logging.warning("coomits sucessfully---------------------!"))
                    # sql = "INSERT INTO TFIDF VALUES (:AID, :Name, :Business_Activity, :Branch_Code_Main, :Branch_Codes_Secondaries, :Internet, :Last_Change)"
                    # support_cursor.execute(sql, dict1)
                i += 1
                # print(logging.warning("Erzeuge... ", format(i, ",d").replace(",", ".")))

            # connection.commit()
            # support_cursor.execute("SELECT COUNT(*) FROM TFIDF")
            count=tfidf.query.filter_by().count()

            size = format(count, ",d").replace(",", ".")
            Text = size + " Datensätze in Datenbank aufgenommen"


            # connection.close()
        except Exception as e:
            err=str(e)
            print(err)
            msg = "<b>Error : </b>" + str(e) + "<br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
        finally:
            if err!=0:
                 pass
            #    send_email(current_app.config["EMAIL_ADDRESS"], "Tfidf Update Avad Data Upload Error", msg)
            else:
                msg = "<b>Successful Tfidf Update upload </b><br><b>Time : </b>" + time.strftime("%Y-%m-%d %H-%M-%S")
                #send_email(current_app.config["EMAIL_ADDRESS"], "Tfidf Update Avad Data Upload Success", msg)
                avad_upload_logs_update("Tfidf Update")
            print(logging.warning("Executed Sucessfully......................................................."))

@main.route('/komm', methods = ["POST","/GET"])
@login_required
def komm():
   print("Test----------------------------")
   all_komm = Contact.query.filter_by().all()
   list_pnf, list_404 = [], []
   for each_komm in all_komm:
       try:
           URL = "http://" + str(each_komm.Contact_Value)
           req = urlopen(URL).read()
           soup = BeautifulSoup(req, "lxml")
           if str(soup.text).lower().find("page not found") != -1:
               #####pdb.set_trace()
               list_pnf.append(str(each_komm.Contact_Value))
           if str(soup.text).lower().find("404 error") != -1:
               #####pdb.set_trace()
               list_pnf.append(str(each_komm.Contact_Value))
       except Exception as e:
           pass

@main.route('/KI_Suche_data_change/', methods = ["POST","GET"])
@login_required
def KI_Suche_data_change():
    print(request.args)
    totalwords=int(request.args.get("totalrecords",0))
    startvalue = int(request.args.get("startvalue", 0))
    endvalue = int(request.args.get("endvalue", 0))
    pagecount=(totalwords//250)+1
    btntxt = request.args.get("btn","")
    if btntxt=="nextbtn":
        if endvalue<totalwords:
            startvalue=startvalue+250
            endvalue = endvalue + 250
        else:
            startvalue = startvalue + 250
            endvalue = (totalwords-endvalue) + 250
    if btntxt=="previousbtn":
        if endvalue==totalwords:
            startvalue=startvalue-250
            endvalue = totalwords-(endvalue-startvalue)
        else:
            startvalue = startvalue - 250
            endvalue = endvalue - 250
    print(endvalue)
    return json.dumps({'startvalue': startvalue,'endvalue':endvalue})

@main.route('/chart_test1/', methods = ["POST","GET"])
def chart_test1():
    a = requests.session()
    b = "https://frontendapi-emonit.dev.wortmann-wember.de/login"
    headers = {'content-type': 'application/json'}
    payload = {"email": "test@email.com", "password": "notroot"}
    a.post(b,data=json.dumps(payload),headers=headers,verify=False,proxies=None)
    c = a.get("https://frontendapi-emonit.dev.wortmann-wember.de/energie")
    d = json.loads(str(c.text)[1:].replace("\r", ""))
    color_list = []
    i = 0
    for each in d['Endenergie']['Strombilanz']['series']:
        color_list.append(d['Endenergie']['Strombilanz']['series'][each]['color'])
        if i == 0:
            graph_type = d['Endenergie']['Strombilanz']['series'][each]['type']
            valueSuffix = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueSuffix']
            valueDecimals = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueDecimals']
            enabled = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['enabled']
            forced = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['forced']
            units = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['units']
        i += 1
    x_val_list = list(d['Endenergie']['Strombilanz']['series'].keys())
    x_axis_text = d['Endenergie']['Strombilanz']['axis']['temp']['text']
    y_axis_text = d['Endenergie']['Strombilanz']['axis']['kwh']['text']
    y_axis_opposite = d['Endenergie']['Strombilanz']['axis']['kwh']['opposite']
    y_axis_showempty = d['Endenergie']['Strombilanz']['axis']['kwh']['showEmpty']
    y_axis_visible = d['Endenergie']['Strombilanz']['axis']['kwh']['visible']
    return render_template("graph_file.html",color_list=color_list,x_val_list=x_val_list,x_axis_text=x_axis_text,\
                           y_axis_text=y_axis_text,graph_type=graph_type,valueSuffix=valueSuffix,valueDecimals=valueDecimals,\
                           enabled=enabled,forced=forced,units=units,y_axis_opposite=y_axis_opposite,\
                           y_axis_showempty=y_axis_showempty,y_axis_visible=y_axis_visible)

@main.route('/chart_data/', methods = ["POST","GET"])
def chart_data():
   try:
    # if session['logged_in'] == True:
            data_all = {}
            weekdata1 = {}
            allyeardata=request.args.get("allyeardata",{})
            allyeardata=allyeardata.replace("&#39;", "'")
            allyeardata=ast.literal_eval(allyeardata)
            currentyr = allyeardata[str(2019)]
            #week = [currentyr[0]]
            for xval in currentyr:
                weekdata1[xval] = {}
                week = [currentyr[xval][0]]
                for each in range(1, len(currentyr[xval])):
                    if each % 7 != 0:
                        week.append(currentyr[xval][each])
                    else:

                        weekdata1[xval][each // 7] = week
                        week = [currentyr[xval][each]]
            gtype = request.args.get("filter_by","year").lower()
            if gtype == "week":

                weekdata = {}
                #week = currentyr[0]
                for xval in currentyr:
                   weekdata[xval] = {}
                   week = currentyr[xval][0]
                   for each in range(1, len(currentyr[xval])):
                        print(each)
                        if each % 7 != 0:
                            print("______________________________________________________")
                            print(each)
                            week = week + currentyr[xval][each]
                            print("______________________________________________________")

                        else:
                            week=round(week/7,3)
                            weekdata[xval][each // 7] = week
                            week = currentyr[xval][each]

                data_all['xAxis'] =[*weekdata['Netzbezug']]
                yaxisvalue=[]
                for each in weekdata:
                    yaxiss=[]
                    print(each)
                    tempdict=dict()
                    for eachdata in weekdata[each].values():
                        yaxiss.append(eachdata)
                    tempdict["name"]=each
                    tempdict["data"] = yaxiss
                    yaxisvalue.append(tempdict)
                data_all['yAxis'] = yaxisvalue
                data_all['graphtitle'] = "Weekly Average Temperature"
                data_all['xtext'] = "Week Count of year 2019"
            if gtype == "month":
                monthdata = {}
                categories = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30,
                              'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
                count = 0
                monthval = 0
                mn = 0
                for each in [*currentyr]:
                       # #pdb.set_trace()
                        monthdata[each] = {}
                        for eachval in range(0, len(currentyr[each])):
                            monthcount = int(list(categories.values())[monthval])
                            if count < monthcount:
                                mn += currentyr[each][eachval]
                                count += 1
                                print(count)

                            else:
                                print(mn)
                                print(list(categories.keys())[monthval])
                                mn=round(mn/monthcount,2)
                                print(monthval)
                                monthdata[each][[*categories][monthval]] = mn
                                monthval += 1
                                count = 1
                                mn = currentyr[each][eachval]

                        mn = round(mn / monthcount,2)
                        monthdata[each][[*categories][monthval]] = mn
                        monthval = 0
                        mn = 0
                        count=0

                data_all['xAxis'] = [*monthdata['Netzbezug']]
                #data_all['yAxis'] = list(monthdata.values())

                yaxisvalue = []
                for each in monthdata:
                    yaxiss = []
                    print(each)
                    tempdict = dict()
                    for eachdata in monthdata[each].values():
                        yaxiss.append(eachdata)
                    tempdict["name"] = each
                    tempdict["data"] = yaxiss
                    yaxisvalue.append(tempdict)
                data_all['yAxis'] = yaxisvalue
                data_all['graphtitle'] = "Monthly Average Temperature"
                data_all['xtext'] = "Months of year 2019"
            if gtype == "day":
                count = 1
                daysdata = {}
                # for each in currentyr:
                #     daysdata[count] = each
                #     count += 1
                days=['Sunday','Monday','Tuesday','wednesday','Thursday','Friday','Saturday']
                for xval in [*weekdata1]:
                    daysdata[xval] = {}
                    for d in range(len(days)):
                       avgday=0
                       for each in weekdata1[xval].values():
                          avgday=avgday+each[d]
                       daysdata[xval][str(days[d])]=avgday/ len(list(weekdata1.keys()))

                data_all['xAxis'] =  [*daysdata['Netzbezug']]
                yaxisvalue = []
                for each in daysdata:
                    yaxiss = []
                    print(each)
                    tempdict = dict()
                    for eachdata in daysdata[each].values():
                        yaxiss.append(eachdata)
                    tempdict["name"] = each
                    tempdict["data"] = yaxiss
                    yaxisvalue.append(tempdict)
                data_all['yAxis'] = yaxisvalue
                data_all['graphtitle'] = "Average Day Temperature"
                data_all['xtext'] = "Days of year "
            if gtype=="year":
                xvalues = []
                for each in allyeardata.keys():
                    xvalues.append(each)
                yvalues = dict()
                for each in [*weekdata1]:
                    sum1=0
                    yvalues[each] = {}
                    for eachyr in allyeardata:
                       for x in allyeardata[str(eachyr)][each]:
                           sum1=sum1+x
                       print(sum1)
                       sum1=round(sum1/len([*allyeardata['2009']]),3)
                       yvalues[each][eachyr]=sum1
                yval = []
                for each in [*yvalues]:
                    yaxiss = []
                    tempdict = dict()
                    tempdict["name"] = each
                    for eachdata in yvalues[each].values():
                        yaxiss.append(eachdata)
                    tempdict["data"] = yaxiss
                    yval.append(tempdict)
                # for each in allyeardata.values():
                #     sum = 0
                #
                #     for y in each:
                #         sum = sum + y
                #
                #     sum = sum / len(each)
                #     yvalues.append(sum)
                data_all['xAxis'] = xvalues
                data_all['yAxis'] = yval
                data_all['graphtitle'] = "Previous 11 years Temperature"
            return json.dumps(data_all)
    # else:
    #     session['logged_in'] = False
    #     session['username'] = ""
    #     return redirect(url_for("main.graph_login"))
   except Exception as e:
       session['logged_in'] = False
       session['username'] = ""
       return redirect(url_for("main.graph_login"))

@main.route('/chart_test123/', methods = ["POST","GET"])
def chart_test123():
    a = requests.session()
    b = "https://frontendapi-emonit.dev.wortmann-wember.de/login"
    headers = {'content-type': 'application/json'}
    payload = {"email": "test@email.com", "password": "notroot"}
    a.post(b, data=json.dumps(payload), headers=headers, verify=False, proxies=None)
    c = a.get("https://frontendapi-emonit.dev.wortmann-wember.de/energie")
    d = json.loads(str(c.text)[1:].replace("\r", ""))
    color_list = []
    i = 0
    for each in d['Endenergie']['Strombilanz']['series']:
        color_list.append(d['Endenergie']['Strombilanz']['series'][each]['color'])
        if i == 0:
            graph_type = d['Endenergie']['Strombilanz']['series'][each]['type']
            valueSuffix = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueSuffix']
            valueDecimals = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueDecimals']
            enabled = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['enabled']
            forced = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['forced']
            units = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['units']
        i += 1
    x_val_list = list(d['Endenergie']['Strombilanz']['series'].keys())
    x_axis_text = d['Endenergie']['Strombilanz']['axis']['temp']['text']
    y_axis_text = d['Endenergie']['Strombilanz']['axis']['kwh']['text']
    y_axis_opposite = d['Endenergie']['Strombilanz']['axis']['kwh']['opposite']
    y_axis_showempty = d['Endenergie']['Strombilanz']['axis']['kwh']['showEmpty']
    y_axis_visible = d['Endenergie']['Strombilanz']['axis']['kwh']['visible']
    try:
        # if session['logged_in'] == True:
                data_all={}
                import random
                from datetime import datetime as dt
                cyear=dt.now().year
                allyeardata = {}
                for each in range(cyear - 10, cyear + 1):
                    allyeardata[str(each)] = dict()
                    for eachxval in x_val_list:
                        days = []
                        for x in range(365):
                            days.append(random.randint(10, 100))
                        allyeardata[str(each)][eachxval] = days
                xvalues = []
                yvalues = {}
                for each in allyeardata.keys():
                    xvalues.append(str(each))
                for eachxval in x_val_list:
                    yvalues.setdefault(str(eachxval), None)
                # for each in allyeardata.keys():
                #    # yvalues[each]={}
                #     #pdb.set_trace()
                #     for x in allyeardata[each].keys():
                #         #pdb.set_trace()
                #         sum = 0
                #         for y in allyeardata[each][x]:
                #             sum=sum+y
                #         sum=sum/len(allyeardata[each][x])
                #         yvalues[each][x]=sum
                for each in yvalues:
                    sum1=0
                    yvalues[each] = {}
                    for eachyr in allyeardata:
                       for x in allyeardata[str(eachyr)][each]:
                        sum1=sum1+x
                        print(sum1)
                        print("______________________________________________")
                        print(sum1)
                        sum1=round(sum1/len(x_val_list),3)

                        yvalues[each][eachyr]=sum1
                for each in allyeardata.keys():
                    xvalues.append(each)
                yvalues = dict()
                weekdata1= dict()
                currentyr = allyeardata[str(2019)]
                for xval in currentyr:
                    weekdata1[xval] = {}
                    week = [currentyr[xval][0]]
                    for each in range(1, len(currentyr[xval])):
                        if each % 7 != 0:
                            week.append(currentyr[xval][each])
                        else:

                            weekdata1[xval][each // 7] = week
                            week = [currentyr[xval][each]]
                for each in [*weekdata1]:
                    sum1=0
                    yvalues[each] = {}
                    for eachyr in allyeardata:
                       for x in allyeardata[str(eachyr)][each]:
                           sum1=sum1+x
                       print(sum1)
                       sum1=round(sum1/len([*allyeardata['2009']]),3)
                       yvalues[each][eachyr]=sum1
                yval = []
                for each in [*yvalues]:
                    yaxiss = []
                    tempdict = dict()
                    tempdict["name"] = each
                    for eachdata in yvalues[each].values():
                        yaxiss.append(eachdata)
                    tempdict["data"] = yaxiss
                    yval.append(tempdict)

                data_all['xAxis'] = xvalues
                data_all['yAxis'] = yval
                data_all['graphtitle'] = "previous 11 years data"

                #return render_template("graph_file1.html" ,data_all=data_all,allyeardata=allyeardata,user12=session['username'])
                return render_template("graph_file.html", color_list=color_list, x_val_list=xvalues, x_axis_text=x_axis_text, \
                           y_axis_text=y_axis_text, graph_type=graph_type, valueSuffix=valueSuffix, valueDecimals=valueDecimals, \
                           enabled=enabled, forced=forced, units=units, y_axis_opposite=y_axis_opposite, \
                           y_axis_showempty=y_axis_showempty, y_axis_visible=y_axis_visible, y_val_list=yvalues,\
                                        allyeardata=allyeardata)
        # else:
        #     session['logged_in'] = False
        #     session['username'] = ""
        #     return redirect(url_for("main.graph_login"))
    except Exception as e:
        session['logged_in'] = False
        session['username'] = ""
        return redirect(url_for("main.graph_login"))

@main.route('/graph_login', methods = ["POST","GET"])
def graph_login():
    return render_template("graph_login.html")
    # try:
    #   if session['username'] == "test@email.com":
    #     return redirect(url_for("main.chart_test1", user12="test@email.com"))
    # except Exception as e:
    #     return render_template("graph_login.html")

@main.route('/graphlogin', methods = ["POST","GET"])
def graphlogin():
    return redirect(url_for("main.chart_test1", user12="test@email.com"))
    # userid = request.values.get('username', '')
    # userpw = request.values.get('pass', '')
    # if userid.lower()=="test@email.com" and userpw.lower()=="notroot":
    #     session['logged_in'] = True
    #     session['username'] = "test@email.com"
    #     session['message'] = "You are logged in"
    #     now = datetime.datetime.now()
    #     session['starttime'] = now.strftime("%I:%M:%S %p")
    #     return redirect(url_for("main.chart_test1",user12="test@email.com"))
    # else:
    #     try:
    #         if session['username']=="test@email.com":
    #             return redirect(url_for("main.chart_test1", user12="test@email.com"))
    #         else:
    #             return redirect(url_for('main.graph_login'))
    #     except Exception as e:
    #         session['logged_in'] = False
    #         session['username'] = ""
    #         return redirect(url_for("main.graph_login"))


@main.route('/logout_graph' ,methods=['GET', 'POST'])
def logout():
       # #pdb.set_trace()
        from datetime import datetime as dt
        now = datetime.datetime.now()
        session.pop('username',None)
        session.pop('logged_in', None)
        session['username'] =""
        session['logouttime'] = now.strftime("%I:%M:%S %p")
        session['message'] = "You were logged out"
        session['previous login']=session['starttime']
        flash('You were logged out.')
       # currentsession=session['logouttime']-session['starttime']
        #datetime.datetime.strptime(session['logouttime'])-datetime.datetime.strptime(session['starttime'])

        logging.info('successfully logout at ' + str(now))
        return redirect(url_for('main.graph_login'))


@main.route('/chart_test/', methods = ["POST","GET"])
def chart_test():
    #pdb.set_trace()
    # print(logging.warning( request.remote_addr))
    # print(logging.warning(request.environ['REMOTE_ADDR']))
    # a = requests.session()
    # b = "https://frontendapi-emonit.dev.wortmann-wember.de/login"
    # headers = {'content-type': 'application/json'}
    # print(a)
    # payload = {"email": "test@email.com", "password": "notroot"}
    # a.post(b, data=json.dumps(payload), headers=headers, verify=False, proxies=None)
    # c = a.get("https://frontendapi-emonit.dev.wortmann-wember.de/energie")
    # d = json.loads(str(c.text)[1:].replace("\r", ""))
    # dataurls=dict()
    json_path = os.getcwd() + "\\Graph_1.json"
    f = open(json_path, "r")
    f = open(json_path, "r")
    d = json.loads(str(f.read()))
    '''i = 0
    for each in d['Endenergie']['Strombilanz']['series']:
        color_list.append(d['Endenergie']['Strombilanz']['series'][each]['color'])
        dataurls[each]=d['Endenergie']['Strombilanz']['series'][each]['data']
        graph_type.append(d['Endenergie']['Strombilanz']['series'][each]['type'])
        if i == 0:
            valueSuffix = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueSuffix']
            valueDecimals = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueDecimals']
            enabled = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['enabled']
            forced = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['forced']
            units = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['units']
        i += 1'''
    main_level = 0
    for each_first in d:#d['Endenergie']['Strombilanz']['series']:
        if main_level > 1:
            break
        if each_first != "settings":
            graph_list = []
            for each_second in d[each_first]:
                graph_one = []
                color_list, graph_type, dataurls = [], [], dict()
                for each_third in d[each_first][each_second]['series']:
                        #pdb.set_trace()
                        print(each_third)
                        color_list.append(d[each_first][each_second]['series'][each_third]['color'])
                        dataurls[each_third]=d[each_first][each_second]['series'][each_third]['data']
                        graph_type.append(d[each_first][each_second]['series'][each_third]['type'])
                        #if i == 0:
                        valueSuffix = d[each_first][each_second]['series'][each_third]['tooltip']['valueSuffix']
                        valueDecimals = d[each_first][each_second]['series'][each_third]['tooltip']['valueDecimals']
                        enabled = d[each_first][each_second]['series'][each_third]['dataGrouping']['enabled']
                        forced = d[each_first][each_second]['series'][each_third]['dataGrouping']['forced']
                        units = d[each_first][each_second]['series'][each_third]['dataGrouping']['units']
                        #i += 1
                x_val_list = list(d[each_first][each_second]['series'].keys())
                x_axis_text = d[each_first][each_second]['axis']['temp']['text']
                y_axis_text = d[each_first][each_second]['axis']['kwh']['text']
                y_axis_opposite = d[each_first][each_second]['axis']['kwh']['opposite']
                y_axis_showempty = d[each_first][each_second]['axis']['kwh']['showEmpty']
                y_axis_visible = d[each_first][each_second]['axis']['kwh']['visible']
                graph_one.append(color_list)
                graph_one.append(dataurls)
                graph_one.append(graph_type)
                graph_one.append(valueSuffix)
                graph_one.append(valueDecimals)
                graph_one.append(enabled)
                graph_one.append(forced)
                graph_one.append(units)
                graph_one.append(x_val_list)
                graph_one.append(x_axis_text)
                graph_one.append(y_axis_text)
                graph_one.append(y_axis_opposite)
                graph_one.append(y_axis_showempty)
                graph_one.append(y_axis_visible)
                graph_list.append(graph_one)
        main_level += 1
    #pdb.set_trace()
    Aussentemperatur = dataurls["Aussentemperatur"]#[each[1] for each in allurlsdata["Aussentemperatur"]]
    Einspeisung = dataurls["Einspeisung"]#[each[1] for each in allurlsdata["Einspeisung"]]
    Eigennutzung = dataurls["Eigennutzung"]#[each[1] for each in allurlsdata["Eigennutzung"]]
    Netzbezug = dataurls["Netzbezug"]#[each[1] for each in allurlsdata["Netzbezug"]]
    return render_template("testgraph.html", color_list=color_list, x_axis_text=x_axis_text, \
                           y_axis_text=y_axis_text, graph_type=graph_type, valueSuffix=valueSuffix,
                           valueDecimals=valueDecimals,enabled=enabled, forced=forced, units=units, y_axis_opposite=y_axis_opposite, \
                           y_axis_showempty=y_axis_showempty, y_axis_visible=y_axis_visible,Netzbezug=Netzbezug, \
                           Aussentemperatur=Aussentemperatur,Eigennutzung=Eigennutzung,\
                           Einspeisung=Einspeisung,x_val_list=x_val_list)

@main.route('/fiche_detail/', methods = ["GET","POST"])
@login_required
def fiche_detail():
    all_data = request.args.get("all_data","yes")
    form = FicheForm()
    issubmit = request.values.get('sub','no')
    btn_background, btn_text = set_button_color()
    user = User.query.filter_by(email=current_user.email).first()
    if all_data == "no" and issubmit != 'issubmit':
        current_data = request.args.get("current_data","no")
        fiche_browse = "no"
        if current_data == "yes":
            fiche_browse = Fiche_Details.query.filter_by(Id=request.args.get('Id',0)).first()
        return render_template("fiche.html", all_data=all_data, form=form, fiche_browse=fiche_browse,\
                               current_data=current_data,btn_background=btn_background,btn_text=btn_text,user=user.username)
    if form.validate_on_submit():
        fiche_browse = Fiche_Details.query.filter_by(Id=request.values.get('Id','')).first()
        if fiche_browse:
            fiche_browse.Fiche_Name = request.values.get('Fiche_Name','')
            fiche_browse.Fiche_Number = request.values.get('Fiche_Number','')
            fiche_browse.fiche_desc = request.values.get('fiche_desc', '')
            flash_msg = "Fiche updated successfully"
        else:
            fiche_detail = Fiche_Details(Fiche_Name=request.values.get('Fiche_Name',''), \
                                         Fiche_Number=request.values.get('Fiche_Number',''), \
                                         fiche_desc=request.values.get('fiche_desc',''))
            db.session.add(fiche_detail)
            flash_msg = "Fiche added successfully"
        db.session.commit()
        flash(flash_msg)
        all_data="yes"
        btn_background, btn_text = set_button_color()
    return render_template("fiche.html",all_data=all_data,form=form,btn_background=btn_background,\
                           btn_text=btn_text,user=user.username)

@main.route('/fiche_data/', methods = ["GET"])
@login_required
def fiche_data():
    fiche_browse = Fiche_Details.query.filter_by().all()
    fiche_data = {'total':0,'rows':[]}
    i = 0
    for each_fiche in fiche_browse:
        fiche_data['rows'].append({'Id':each_fiche.Id,'Fiche_Name':each_fiche.Fiche_Name,\
                                       'fiche_desc':each_fiche.fiche_desc,'Fiche_Number':each_fiche.Fiche_Number})
        i += 1
    fiche_data['total'] = i
    return json.dumps(fiche_data)

@main.route('/set_fiche_details/', methods = ["POST"])
@login_required
def set_fiche_details():
    data_list = []
    if request.values.get('fiche_num',''):
        fiche_browse = Fiche_Details.query.filter_by(Fiche_Number=request.values['fiche_num']).first()
        data_list.append(fiche_browse.fiche_desc)
        data_list.append(fiche_browse.Fiche_Name)
    return json.dumps({'Data':list(set(data_list))})

@main.route('/search_fiche/', methods = ["POST"])
@login_required
def search_fiche():
    print("Search fiche------------------------------")
    print(request.values)
    search_list = []
    fiche_nums = Fiche_Details.query.filter(or_(Fiche_Details.Fiche_Number.like(request.values['fiche_term']+"%"), \
                                            Fiche_Details.Fiche_Name.like(request.values['fiche_term']+"%"))).all()
    for each_num in fiche_nums:
        search_list.append(each_num.Fiche_Number)
        search_list.append(each_num.Fiche_Name)
    print(search_list)
    return json.dumps({'search_list': search_list})

@main.route('/nexxt_change_intimation_log', methods = ["GET"])
def nexxt_change_intimation_log():
    user = User.query.filter_by(email=current_user.email).first()
    return render_template("nexxt_change_intimation_logs.html",user=user.username)

@main.route('/nexxt_intimation_logs_data', methods = ["GET"])
def nexxt_intimation_logs_data():
    nexxt_intimation_log_browse = nexxt_change_intimation_logs.query.filter_by().all()
    log_data = {'total': 0, 'rows': []}
    i = 0
    for each_log in nexxt_intimation_log_browse:
        log_data['rows'].append({'Chiffre': each_log.Chiffre, 'Fiche_Number': each_log.Fiche_Number, 'date': each_log.date})
        i += 1
        log_data['total'] = i
    return json.dumps(log_data)

@main.route('/get_fichebiz_location/', methods = ["POST"])
def get_fichebiz_location():
    fiche_num = request.values.get('fiche_num')
    if fiche_num:
        fiche_brw = Fiche_Details.query.filter_by(Fiche_Number=fiche_num).first()
        bizpurpose = fiche_brw.Fiche_Desc_Biz_Purpose
        location = fiche_brw.Fiche_Desc_Location
    return json.dumps({'bizpurpose':bizpurpose,'location':location})

@main.route('/buyer_portal_search/', methods = ["GET"])
def buyer_portal_search():
    return render_template("buyer_portal.html")

@main.route('/manual_company_search/', methods = ["GET"])
def manual_company_search():
    return render_template("manual_company.html")

@main.route('/smart_buyer_matching/', methods = ["GET"])
def smart_buyer_matching():
    return render_template("smart_buyer_matching.html")

@main.route('/chart_test_trial/', methods = ["POST","GET"])
def chart_test_trial():
    # pdb.set_trace()
    # print(logging.warning( request.remote_addr))
    # print(logging.warning(request.environ['REMOTE_ADDR']))
    # a = requests.session()
    # b = "https://frontendapi-emonit.dev.wortmann-wember.de/login"
    # headers = {'content-type': 'application/json'}
    # print(a)
    # payload = {"email": "test@email.com", "password": "notroot"}
    # a.post(b, data=json.dumps(payload), headers=headers, verify=False, proxies=None)
    # c = a.get("https://frontendapi-emonit.dev.wortmann-wember.de/energie")
    # d = json.loads(str(c.text)[1:].replace("\r", ""))
    json_path=os.getcwd()+"\\Graph_1.json"
    f = open(json_path, "r")
    f = open(json_path, "r")
    d = json.loads(str(f.read()))
    dataurls=dict()
    '''i = 0
    for each in d['Endenergie']['Strombilanz']['series']:
        color_list.append(d['Endenergie']['Strombilanz']['series'][each]['color'])
        dataurls[each]=d['Endenergie']['Strombilanz']['series'][each]['data']
        graph_type.append(d['Endenergie']['Strombilanz']['series'][each]['type'])
        if i == 0:
            valueSuffix = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueSuffix']
            valueDecimals = d['Endenergie']['Strombilanz']['series'][each]['tooltip']['valueDecimals']
            enabled = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['enabled']
            forced = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['forced']
            units = d['Endenergie']['Strombilanz']['series'][each]['dataGrouping']['units']
        i += 1'''
    main_level = 0
    for each_first in d:#d['Endenergie']['Settings']:
        if main_level > 1:
            break
        if each_first != "settings":
            graph_list = []
            for each_second in d[each_first]:#d[each_first]=[Strombilanz]
                graph_one = []
                color_list, graph_type, dataurls = [], [], dict()
                for each_third in d[each_first][each_second]['series']:#[series,axis]
                        # print("##############"+)
                        #pdb.set_trace()
                        print(each_third)
                        color_list.append(d[each_first][each_second]['series'][each_third]['color'])
                        dataurls[each_third]=d[each_first][each_second]['series'][each_third]['data']
                        graph_type.append(d[each_first][each_second]['series'][each_third]['type'])
                        #if i == 0:
                        valueSuffix = d[each_first][each_second]['series'][each_third]['tooltip']['valueSuffix']
                        valueDecimals = d[each_first][each_second]['series'][each_third]['tooltip']['valueDecimals']
                        enabled = d[each_first][each_second]['series'][each_third]['dataGrouping']['enabled']
                        forced = d[each_first][each_second]['series'][each_third]['dataGrouping']['forced']
                        units = d[each_first][each_second]['series'][each_third]['dataGrouping']['units']
                        #i += 1
                x_val_list = list(d[each_first][each_second]['series'].keys())
                # pdb.set_trace()
                # print("##########"+x_val_list)
                x_axis_text = d[each_first][each_second]['axis']['temp']['text']
                y_axis_text = d[each_first][each_second]['axis']['kwh']['text']
                y_axis_opposite = d[each_first][each_second]['axis']['kwh']['opposite']
                y_axis_showempty = d[each_first][each_second]['axis']['kwh']['showEmpty']
                y_axis_visible = d[each_first][each_second]['axis']['kwh']['visible']
                graph_one.append(color_list)
                graph_one.append(dataurls)
                graph_one.append(graph_type)
                graph_one.append(valueSuffix)
                graph_one.append(valueDecimals)
                graph_one.append(enabled)
                graph_one.append(forced)
                graph_one.append(units)
                graph_one.append(x_val_list)
                graph_one.append(x_axis_text)
                graph_one.append(y_axis_text)
                graph_one.append(y_axis_opposite)
                graph_one.append(y_axis_showempty)
                graph_one.append(y_axis_visible)
                graph_list.append(graph_one)
        main_level += 1
    temp=[]
    for a in dataurls.values():
        temp.append(a)
    print(temp)
    url_length=len(dataurls)
    return render_template("testgraph_trial.html", color_list=color_list, x_axis_text=x_axis_text, \
                           y_axis_text=y_axis_text, graph_type=graph_type, valueSuffix=valueSuffix,
                           valueDecimals=valueDecimals,enabled=enabled, forced=forced, units=units, y_axis_opposite=y_axis_opposite, \
                           y_axis_showempty=y_axis_showempty, y_axis_visible=y_axis_visible,
                           x_val_list=x_val_list,url_list=temp,url_length=url_length)

@main.route('/chart_test_trial_2nd/', methods = ["POST","GET"])
def chart_test_trial_2nd():
    json_path = os.getcwd() + "\\Graph.json"
    f = open(json_path, "r")
    f = open(json_path, "r")
    d = json.loads(str(f.read()))
    graph_list = []
    main_level = 0
    for each_first in d:#d['Endenergie']['Settings']:
        if main_level > 1:
            break
        if each_first != "settings":
            for each_second in d[each_first]:#d[each_first]=[Strombilanz]
                graph_one = []
                color_list, graph_type, dataurls,urlresp = [], [], [],[]
                for each_third in d[each_first][each_second]['series']:#[series,axis]
                        color_list.append(d[each_first][each_second]['series'][each_third]['color'])
                        dataurls.append(d[each_first][each_second]['series'][each_third]['data'])
                        graph_type.append(d[each_first][each_second]['series'][each_third]['type'])
                        valueSuffix = d[each_first][each_second]['series'][each_third]['tooltip']['valueSuffix']
                        valueDecimals = d[each_first][each_second]['series'][each_third]['tooltip']['valueDecimals']
                        enabled = d[each_first][each_second]['series'][each_third]['dataGrouping']['enabled']
                        forced = d[each_first][each_second]['series'][each_third]['dataGrouping']['forced']
                        units = d[each_first][each_second]['series'][each_third]['dataGrouping']['units']
                x_val_list = list(d[each_first][each_second]['series'].keys())
                x_axis_text = d[each_first][each_second]['axis']['temp']['text']
                y_axis_text = d[each_first][each_second]['axis']['kwh']['text']
                y_axis_opposite = d[each_first][each_second]['axis']['kwh']['opposite']
                y_axis_showempty = d[each_first][each_second]['axis']['kwh']['showEmpty']
                y_axis_visible = d[each_first][each_second]['axis']['kwh']['visible']
                for URL in dataurls:
                    f_url=URL[:-1]+"k"
                    res=requests.get(f_url)
                    final_resp=ast.literal_eval(res.text[1:-1])
                    urlresp.append(final_resp)

                graph_one.append(color_list)
                graph_one.append(urlresp)
                graph_one.append(graph_type)
                graph_one.append(valueSuffix)
                graph_one.append(valueDecimals)
                graph_one.append(enabled)
                graph_one.append(forced)
                graph_one.append(units)
                graph_one.append(x_val_list)
                graph_one.append(x_axis_text)
                graph_one.append(y_axis_text)
                graph_one.append(y_axis_opposite)
                graph_one.append(y_axis_showempty)
                graph_one.append(y_axis_visible)
                graph_one.append(each_second)

                graph_list.append(graph_one)
        main_level += 1

    return render_template("testgraph_trial_2nd.html", graph_list=graph_list)
