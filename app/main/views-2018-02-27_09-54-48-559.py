﻿from flask import render_template, session, redirect, current_app, flash, request, send_file, Flask
from flask_login import current_user
from bs4 import BeautifulSoup
from .. import db
from ..models import tfidf, adresse, kommunikation, User, financials, websites, entity,\
    branchencodes, tatigkeit, role, web_search_result, web_branch_data, web_location_data, ate, man, export_log
from . import main
from .forms import AIDForm, EntityForm, StopwortForm
from .import user_interface as ui
from .crawler import Crawler
from flask_login import login_required, logout_user
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sqlalchemy import func, text, or_, and_, text, distinct
from werkzeug import secure_filename
#from openpyxl.utils import get_column_letter
from PyQt5 import QtCore, QtGui, QtWebKit, QtNetwork

import collections, time
import openpyxl
import os, tkinter
import pandas as pd
import pdb
import re, ast
import shelve
import sqlite3
import json

#Debug:  pdb.set_trace()

# Später evtl. wieder weg
#from tkinter import *
#from tkinter import filedialog

"""--------
Funktion: allowed_file
--------"""
def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['xlsx'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@main.before_app_first_request
def _run_on_start():
    pass
    #    dbfile = current_app.config["DATABASE"]
#    if not current_app.config["B_LAND"] :
#        if not dbfile :
#            print("Keine Datenbank definiert - Statistik kann nicht berechnet werden")
#        print("Datenbank - Statistik berechnen")
#        b_land = {
#                "Ohne" : "0", "Baden-Württemberg" :f "0", "Bayern" : "0", "Berlin" : "0", "Brandenburg" : "0", "Bremen" : "0", "Hamburg" : "0",
#                "Hessen" : "0", "Mecklenburg-Vorpommern" : "0", "Niedersachsen" : "0", "Nordrhein-Westfalen" : "0", "Rheinland-Pfalz" : "0",
#                "Saarland" : "0", "Sachsen" : "0", "Sachsen-Anhalt" : "0", "Schleswig-Holstein" : "0", "Thüringen" : "0"}
#        bundeslaender = db.session.query(adresse.Bundesland, func.count(tfidf.AID)).filter(tfidf.AID == adresse.AID).group_by(adresse.Bundesland)
#        anzahl_laender = bundeslaender.count()
#        rec_count = 0
#        for nr in range (0, anzahl_laender) :
#            land, firmen = bundeslaender[nr]
#            rec_count += firmen
#            anzahl = format(firmen,",").replace(",",".")
#            if not land :
#                b_land["Ohne"] = anzahl
#            else :
#                b_land[land] = anzahl
#        current_app.config["B_LAND"] = b_land
#        print("Datenbank - Statistik berechnet")

"""--------
Funktion: main.route
--------"""
@main.route('/') #, methods=['GET', 'POST'])
@login_required
def index():
#    pdb.set_trace()
#    dbfile = globals["dbfile"]
#    records = globals["records"]
    print("------------------------------------------------------")
    dbfile = current_app.config["DATABASE"]
    print(dbfile)
    user = User.query.filter_by(email=current_user.email).first()
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    language = session.get('lang','')
    return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language,user=user.username)

"""--------
Funktion: Datenbak Öffnen
--------"""
@main.route('/Datenbank_Oeffnen/')
@login_required
def datenbank_oeffnen():
    dbfile = ui.open_file()
    shelf_file_name = current_app.config["SHELF_FILE_NAME"]
    shelf_file = shelve.open(shelf_file_name)
#    shelf_file = current_app.config["SHELF_FILE"]
    shelf_file["DATABASE"] = dbfile
    shelf_file.sync()
    shelf_file.close()
    rec_count = ""
    return render_template('index.html', datenbank=dbfile, records=rec_count, is_admin=current_app.config["IS_ADMIN"])

"""--------
Funktion: Datenbank Zeigen
--------"""
@main.route('/Datenbank_Zeigen/')
@login_required
def datenbank_zeigen():
    liste = db.session.query(tfidf.AID, tfidf.Name, tfidf.Tätigkeit)
    language = session.get('lang','german')
    return render_template('show_database.html', items=liste, language=language)

"""--------
Funktion: Datenbank Summary
--------"""
@main.route('/Datenbank_Summary/')
@login_required
def datenbank_summary():
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    if current_app.config["B_LAND"] :
        b_land = current_app.config["B_LAND"]
    else :
        b_land = {
                "Ohne" : "0", "Baden-Württemberg" : "0", "Bayern" : "0", "Berlin" : "0", "Brandenburg" : "0", "Bremen" : "0", "Hamburg" : "0",
                "Hessen" : "0", "Mecklenburg-Vorpommern" : "0", "Niedersachsen" : "0", "Nordrhein-Westfalen" : "0", "Rheinland-Pfalz" : "0",
                "Saarland" : "0", "Sachsen" : "0", "Sachsen-Anhalt" : "0", "Schleswig-Holstein" : "0", "Thüringen" : "0"}
        bundeslaender = db.session.query(adresse.Bundesland, func.count(tfidf.AID)).filter(tfidf.AID == adresse.AID).group_by(adresse.Bundesland)
        anzahl_laender = bundeslaender.count()
        rec_count = 0
        for nr in range (0, anzahl_laender) :
            land, firmen = bundeslaender[nr]
            rec_count += firmen
            anzahl = format(firmen,",").replace(",",".")
            if not land :
                b_land["Ohne"] = anzahl
            else :
                b_land[land] = anzahl
        records = format(rec_count,",").replace(",",".")
    language = session.get('lang', '')
    return render_template('db_summary.html', datenbank=dbfile, records=records, land = b_land, language=language)

"""--------
Funktion: KI-Suche
--------"""
@main.route('/KI_Suche/', methods = ["GET", "POST"])
@login_required
def ki_suche():
    autofill_val = request.values.get('autofill_val','')
    current_year = time.strftime("%Y")
    last_5_years, i = [], 5
    while i != 0:
        last_5_years.append(current_year)
        current_year = str(int(current_year)-1)
        i -= 1
    dbfile = current_app.config["DATABASE"]
    records = current_app.config["RECORDS"]
    aid = None
    ALL = []
    form = AIDForm()
    user = User.query.filter_by(email=current_user.email).first()
    is_admin = 0
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    language = session.get('lang', '')
    if form.validate_on_submit():
        year_list = []
        all_any = request.values.get('all_any','all')
        aid = form.aid.data
        suchworte = form.suchworte.data
        form.aid.data = ""
        form.suchworte.data = ""
        print("test")
        if aid :
            current_app.config["AID"] = aid
        else :
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language,user=user.username)
        ki_ergebnis = ui.create_tfidf(aid,ALL,suchworte)
        if isinstance(ki_ergebnis,str):
            flash("No AID found. Please Enter Proper AID.")
            return render_template('aid.html', form=form, aid=aid, language=language, is_admin=is_admin,user=user.username)
        state_list,umsatz_range,mitarbeiter_range = [],[],[]
        for each in request.values:
            if str(each)[0:5] == 'years':
                year_list.append(request.values.get(each,''))
            if str(each)[-5:] == 'state':
                print("test----------")
                state_list.append(request.values[each])
            if str(each) == 'from_range_umsatz' and request.values[str(each)] != '':
                umsatz_range = [int(request.values['from_range_umsatz']),int(request.values['to_range_umsatz'])]

            if str(each) == 'from_range_mitarbeiter' and request.values[str(each)] != '':
                mitarbeiter_range = [int(request.values['from_range_mitarbeiter']), int(request.values['to_range_mitarbeiter'])]
        for each_aid in ki_ergebnis.Company:
            if state_list:
                get_addr = adresse.query.filter_by(AID=each_aid).first()
                if get_addr:
                    if get_addr.Bundesland not in state_list:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                else:
                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
            if umsatz_range:
                if all_any == "all" and year_list:
                    for each_year in year_list:
                        sale_data = financials.query.filter(and_(financials.FTyp=='Umsatz',f.AID==each_aid,financials.Jahr==each_year)).all()
                        if sale_data:
                            for row1 in sale_data:
                                if row2.FWert and (int(str(row1.FWert).split(',')[0]) < umsatz_range[0] or int(str(row1.FWert).split(',')[0]) > umsatz_range[1]):
                                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                                if not row2.FWert:
                                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                        else:
                            ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                if all_any == "any" and year_list:
                    i=0
                    for each_year in year_list:
                        sale_data = financials.query.filter(and_(financials.FTyp=='Umsatz',f.AID==each_aid,financials.Jahr==each_year)).all()
                        if sale_data:
                            for row1 in sale_data:
                                if row2.FWert and (int(str(row1.FWert).split(',')[0]) < umsatz_range[0] or int(str(row1.FWert).split(',')[0]) > umsatz_range[1]):
                                    pass
                                else:
                                    i+=1
                                if not row2.FWert:
                                    pass
                                else:
                                    i+=1
                        else:
                            ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                    if not i:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] == "nothing"]
            if mitarbeiter_range:
                if all_any == "all" and year_list:
                    for each_year in year_list:
                        emp_data = financials.query.filter(and_(financials.FTyp=='Mitarbeiter', financials.AID==each_aid, financials.Jahr==each_year)).all()
                        if emp_data:
                            for row2 in emp_data:
                                if row2.FWert and (int(str(row2.FWert).split(',')[0]) < mitarbeiter_range[0] or int(str(row2.FWert).split(',')[0]) > mitarbeiter_range[1]):
                                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                                if not row2.FWert:
                                    ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                        else:
                            ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                if all_any == "any" and year_list:
                    i=0
                    for each_year in year_list:
                        emp_data = financials.query.filter(and_(financials.FTyp=='Mitarbeiter', financials.AID==each_aid, financials.Jahr==each_year)).all()
                        if emp_data:
                            for row2 in emp_data:
                                if row2.FWert and (int(str(row2.FWert).split(',')[0]) < mitarbeiter_range[0] or int(str(row2.FWert).split(',')[0]) > mitarbeiter_range[1]):
                                    pass
                                else:
                                    i += 1
                                if not row2.FWert:
                                    pass
                                else:
                                    i += 1
                        else:
                            ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] != each_aid]
                    if not i:
                        ki_ergebnis = ki_ergebnis[ki_ergebnis['Company'] == "nothing"]
        flash("Erzeuge KI-Suche.... Bitte warten")
        current_app.config["KI_ERGEBNIS"] = ki_ergebnis
        return render_template('ki_ergebnis.html', items=ki_ergebnis, language=language,aid=aid,is_admin=is_admin,user=user.username,search_word=suchworte)
    return render_template('aid.html', form=form, aid=aid, language=language, is_admin=is_admin,last_5_years=last_5_years,user=user.username,autofill_val=autofill_val)
        
"""--------
Funktion: KI-Export
--------"""        
@main.route('/KI_Export/<aid>/<search_word>')
@login_required
def ki_export(aid,search_word):
    df2 = ""
    if search_word:
        search_word_list = str(search_word).split(" ")
        search_word_list = list(filter(lambda a: a != '', search_word_list))
        time_list = ['']*len(search_word_list)
        df2 = pd.DataFrame({"Searched Words":search_word_list,time.strftime('%Y-%m-%d'):time_list})
    backslash = "\\"
    filedir = os.path.abspath(".")
    filename = filedir + backslash +"files" + backslash + "KI-Export.xlsx"
    writer = pd.ExcelWriter(filename)
    df1 = current_app.config["KI_ERGEBNIS"]
    states, emp_prev, emp_prev_year, sale_prev, sale_prev_year, emp_2_prev, emp_2_prev_year, sale_2_prev, sale_2_prev_year\
        = [], [], [], [], [], [], [], [], []
    for each_aid in df1.Company:
        get_addr = adresse.query.filter_by(AID=each_aid).first()
        if get_addr:
            states.append(get_addr.Bundesland)
        else:
            states.append('')

        sql_query = "select Jahr,FWert from Financials where FTyp='Mitarbeiter' and AID='"+each_aid+"' order by Jahr desc limit 2"
        sql = text(sql_query)
        result1 = db.engine.execute(sql)
        i = 0
        for row1 in result1:
            if i == 0:
                emp_prev_year.append(str(row1[0]).split(",")[0])
                emp_prev.append(str(row1[1]).split(",")[0])
                i += 1
            else:
                emp_2_prev_year.append(str(row1[0]).split(",")[0])
                emp_2_prev.append(str(row1[1]).split(",")[0])
                i += 1
        if i == 0:
            emp_prev.append('')
            emp_prev_year.append('')
            i += 1
        if i == 1:
            emp_2_prev.append('')
            emp_2_prev_year.append('')

        sql_query = "select Jahr,FWert from Financials where FTyp='Umsatz' and AID='"+each_aid+"' order by Jahr desc limit 2"
        sql = text(sql_query)
        result2 = db.engine.execute(sql)
        i = 0
        for row2 in result2:
            if i == 0:
                sale_prev_year.append(str(row2[0]).split(",")[0])
                sale_prev.append(str(row2[1]).split(",")[0])
                i += 1
            else:
                sale_2_prev_year.append(str(row2[0]).split(",")[0])
                sale_2_prev.append(str(row2[1]).split(",")[0])
                i += 1
        if i == 0:
            sale_prev.append('')
            sale_prev_year.append('')
            i += 1
        if i == 1:
            sale_2_prev.append('')
            sale_2_prev_year.append('')
    df1.insert(4,'State', states)
    df1.insert(5,'Previous year(Employee)',emp_prev_year)
    df1.insert(6, 'Previous year(Number of Employee)', emp_prev)
    df1.insert(7, 'Previous 2 year(Employee)', emp_2_prev_year)
    df1.insert(8, 'Previous 2 year(Number of Employee)', emp_2_prev)
    df1.insert(9, 'Previous year(Sales)', sale_prev_year)
    df1.insert(10, 'Previous year(Number of Sales)', sale_prev)
    df1.insert(11, 'Previous 2 year(Sales)', sale_2_prev_year)
    df1.insert(12, 'Previous 2 year(Number of Sales)', sale_2_prev)
    language = session.get('lang','')
    try :
        df1.to_excel(writer, sheet_name='KI-Suche', index=False)
        if not isinstance(df2,str):
            df2.to_excel(writer, sheet_name='Searched Words', index=False)
        writer.save()
        print("Datei gespeichert: ", filename)
        exportlog = export_log(type="KI_Export", user_email=current_user.email, AID=aid, keywords=search_word,
                               date=time.strftime('%Y-%m-%d'), \
                               time=time.strftime('%H-%M-%S'))
        db.session.add(exportlog)
        db.session.commit()
        return send_file(filename,attachment_filename='KI-Export.xlsx')
        
    except :
        dbfile = current_app.config["DATABASE"]
        rec_count = tfidf.query.count()
        records = format(rec_count,",").replace(",",".")
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],language=language)

"""--------
Funktion: KI-Liste Prüfen - Wählt per Template die Excel-Datei aus
--------"""
@main.route('/KI_Check/', methods = ["GET", "POST"])
@login_required
def liste_pruefen() :
    ip_adresse = "http://" + current_app.config["IP"] + ":5000/uploader/"
#    ip_adresse = "http://192.168.2.113:5000/uploader/"
    print(ip_adresse)
    return render_template('upload.html', ip_adresse=ip_adresse)


"""--------
Funktion: uploader - Verarbeitet die hochgeladene KI-Liste
--------"""
@main.route('/uploader/', methods = ["GET", "POST"])
@login_required
def uploader() :
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    backslash = "\\"
    filedir = os.getcwd()
    UPLOAD_FOLDER = filedir + backslash +"files" + backslash

    ExcelFile = ""

    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))

    ExcelFile = UPLOAD_FOLDER + file.filename
    if not ExcelFile :
        flash("Datei nicht gefunden")
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    wb = openpyxl.load_workbook(ExcelFile)
    try:
        sheet = wb.get_sheet_by_name("KI-Suche")
    except :
        Text = 'Die Datei\n\n' + ExcelFile + '\n\nenthält kein Sheet mit dem Namen "KI-Suche" !\nVorgang wird abgebrochen.'
        flash(Text)
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    ueberschrift = sheet["A1"].value
    if ueberschrift != "AID" :
        Text = 'Das Excel-Sheet "KI-Suche" enthält keine Überschrift "AID"" !\nVorgang wird abgebrochen.'
        flash(Text)
        return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
    ki_liste = []
    anzahl = sheet.max_row + 1
    for row in range(2, anzahl):
        aid = sheet["A" + str(row)].value
        ki_liste.append(aid)

    ki_ergebnis = ui.create_tfidf(ki_liste[0],ki_liste,"")
    current_app.config["KI_ERGEBNIS"] = ki_ergebnis
    return render_template('ki_ergebnis.html', items=ki_ergebnis)

"""--------
Funktion: Einzelanzeige
--------"""
@main.route('/Einzelanzeige/',  methods = ["GET", "POST"])
@login_required
def einzelanzeige() :
    aid = "None"
    form = EntityForm()
    language = session.get('lang', '')
    user = User.query.filter_by(email=current_user.email).first()
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    if form.validate_on_submit():
        aid = form.aid.data
        form.aid.data = ""
        suche1 = db.session.query(tfidf.AID, tfidf.Name, tfidf.Tätigkeit, tfidf.BCH, tfidf.BCN, tfidf.Internet).filter_by(AID=aid)
        suche2 = db.session.query(adresse.Strasse_Nr, adresse.PLZ, adresse.Ort, adresse.Landkreis, adresse.Bundesland, 
                                  adresse.Land).filter_by(AID=aid)
        suche3 = db.session.query(kommunikation.AID,kommunikation.KTyp, kommunikation.KWert).filter_by(AID=aid,KTyp = "Internet")
        #pdb.set_trace()
        try:
            aid=suche1[0][0]
        except:
            flash("No AID found. Please Enter Proper AID.")
            return render_template('aid.html', form=form, aid=aid, language=language, is_admin=is_admin, screen='analysieren',user=user.username)
        name=suche1[0][1]
        ttg=suche1[0][2]
        bch=suche1[0][3]
        bcn=suche1[0][4]
        internet=suche1[0][5]
        strasse=suche2[0][0]
        plz=suche2[0][1]
        ort=suche2[0][2]
        landkreis=suche2[0][3]
        bundesland=suche2[0][4]
        land=suche2[0][5]
        webinhalt = ""
        keywords = 0
        try :
            website=suche3[0][2]
        except:
            website = ""

        if website :
            KiCrawl = Crawler()
            KiCrawl.stopwords = current_app.config["STOPWORDS"]
            KiCrawl.url = website
            ok = KiCrawl.crawl_site()
            if ok :
                webinhalt = KiCrawl.ngrams
                keywords = len(webinhalt)
        return render_template('show_entity.html', aid=aid, name=name, ttg=ttg, bch=bch, bcn=bcn,internet=internet, strasse=strasse, 
                               plz=plz, ort=ort, landkreis=landkreis, bundesland=bundesland, land=land, website=website, keywords=keywords, 
                               webinhalt=webinhalt,language=language,is_admin=is_admin,screen='analysieren',user=user.username)
    return render_template('aid.html', form=form, aid=aid,language=language,is_admin=is_admin, screen='analysieren',user=user.username)
        

"""--------
Funktion: Create_Stopwords
--------"""
@main.route('/Create_Stopwords/',  methods = ["GET", "POST"])
@login_required
def create_stopwords() :
    dbfile = current_app.config["DATABASE"]
    shelf_file_name = current_app.config["SHELF_FILE_NAME"]

    anzahl_stopwoerter = len(current_app.config["STOPWORDS"])
    form = StopwortForm()
#    form.anzahl.data = 500
    if form.validate_on_submit():
        anzahl_stopwoerter = form.anzahl.data
        print("Ermittle Stopwörter")
        d = collections.Counter()
        connection = sqlite3.connect(dbfile)
        cursor = connection.cursor()
        cursor.execute("SELECT Tätigkeit FROM TFIDF")
        row = cursor.fetchone
        for row in cursor:
            taetigkeit = row[0]
            wortliste = re.split(r"(\W)", taetigkeit)
            for wort in wortliste :
                if wort.isalnum() :
                    d[wort.lower()] +=1
                    print("Anzahl Wörter: ", len(d))
        connection.close()
        mcm = d.most_common(anzahl_stopwoerter)
        liste = []
        anzeige = []
        nr = 1
        for wort in mcm :
            stopwort = wort[0]
            vorkommen = format(wort[1],",").replace(",",".")
            liste.append(stopwort)
            wert = (nr, stopwort, vorkommen)
            anzeige.append(wert)
            nr +=1
        current_app.config["STOPWORDS"] = liste
        print("******* Liste der Stopwörter ********")
        print(liste)
        print("******** Ende der Liste ********")
        shelf_file = shelve.open(shelf_file_name)
        shelf_file["STOPWORDS"] = liste
        shelf_file.sync()
        shelf_file.close()
        return render_template('show_stopwords.html', items=anzeige)
    language = session.get('lang','german')
    return render_template('get_stopwords.html', form=form, anzahl=anzahl_stopwoerter, language=language)
        
"""--------
Funktion: Nexxt_Change
--------"""
@main.route('/Nexxt_Change/',  methods = ["GET", "POST"])
@login_required
def nexxt_change() :
    aid = "None"
    form = EntityForm()
    user = User.query.filter_by(email=current_user.email).first()
    if user.is_admin:
        current_app.config["IS_ADMIN"] = True
        is_admin = True
    if form.validate_on_submit():
        aid = form.aid.data
        form.aid.data = ""
        suche1 = db.session.query(tfidf.AID, tfidf.Name, tfidf.Tätigkeit).filter_by(AID=aid)
        suche2 = db.session.query(adresse.PLZ, adresse.Landkreis, adresse.Bundesland, adresse.Land).filter_by(AID=aid)
        aid=suche1[0][0]
        name=suche1[0][1]
        ttg=suche1[0][2]
        plz=suche2[0][0]
        landkreis=suche2[0][1]
        bundesland=suche2[0][2]

        file_name = os.path.abspath(".") + "\\files\\nexxt-change.xlsx"
        if not os.path.exists(file_name):
            dbfile = current_app.config["DATABASE"]
            rec_count = tfidf.query.count()
            records = format(rec_count,",").replace(",",".")
            return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"])
        nx_raw_data = pd.read_excel(open(file_name, "rb"), sheetname=0, encoding='latin1')
        nx_daten = nx_raw_data[nx_raw_data.Bundesland == bundesland]
        nx_daten.insert(len(nx_daten.columns), "Inseratstext" , "")
        nx_daten.reset_index(drop=True, inplace=True)
        nx_daten.is_copy = False
        for index, row in nx_daten.iterrows() :
            titel = row["Titel"]
            bl1 = row["Branchenlevel 1"]
            bl2 = row["Branchenlevel 2"]
            bl3 = row["Branchenlevel 3"]
            beschreibung = row["Beschreibung"]
            if not pd.isnull(titel) :
                text = titel
            if not pd.isnull(bl1) :
                text = text + " " + bl1
            if not pd.isnull(bl2) :
                text = text + " " + bl2
            if not pd.isnull(bl3) :
                text = text + " " + bl3
            if not pd.isnull(beschreibung) :
                text = text + " " + beschreibung
            inseratstext = ""
            wortliste = re.split(r"(\W)", text)
            for wort in wortliste :
                if wort.isalnum() :
                    inseratstext = inseratstext + wort.lower() + " "
#            print(index)
            nx_daten.loc[index, "Inseratstext"] = inseratstext
        inseratstext = ""
        wortliste =  re.split(r"(\W)", ttg)
        for wort in wortliste :
            if wort.isalnum() :
                inseratstext = inseratstext + wort.lower() + " "
        nummer = len(nx_daten)
        nx_daten.loc[nummer] = ["", name, bundesland, plz, landkreis, "", "", "", "" , "", "", "", "aid", "Mandat", ttg, inseratstext]
#        nx_daten.loc[nummer] = ["", name, bundesland, plz, landkreis, "", "", "", "" , "", "", "", aid, "Mandat", inseratstext]
        tt_liste = nx_daten["Inseratstext"]
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_nx = tfidf_vectorizer.fit_transform(tt_liste)
        ar = cosine_similarity(tfidf_nx[nummer:nummer+1], tfidf_nx)
        nx_daten.insert(1, "Wert" , "")
        ergebnisliste = ar[0]
        se = pd.Series(ergebnisliste)
        nx_daten["Wert"] = se.values
        nx_daten = nx_daten.drop("ID", 1)
        nx_daten = nx_daten.drop("Inseratstext", 1)
        result_frame = nx_daten.sort_values(by="Wert", ascending = False)
        result_frame.reset_index(drop=True, inplace=True)
        return render_template('nx_matching.html', items=result_frame)
    language = session.get('lang', '')
    return render_template('aid.html', form=form, aid=aid,language=language,is_admin=is_admin)
    
@main.route('/set_language/',  methods = ["GET"])
def set_language() :
    session['lang'] = request.args['lang']
    return json.dumps({})

@main.route('/auto_logout/',  methods = ["GET"])
def auto_logout() :
    logout_user()
    return json.dumps({})

@main.route('/set_database/',  methods = ["GET","POST"])
def set_database() :
    '''import pdb
    pdb.set_trace()
    #RESET CODE
    current_app.config["DATABASE"] = "C:\\Users\\Sandeep Agrawal\\Dropbox\\AVKIS-Web\\database\\Firmen2.db"
    import shelve
    SHELF_FILE = shelve.open(current_app.config["SHELF_FILE_NAME"])
    SHELF_FILE['DBDIR'] = current_app.config["DATABASE"]
    SHELF_FILE.sync()
    SHELF_FILE.close()'''



    print("Test-----------------------")
    file_path = request.form.get("database_path","")
    current_app.config["DATABASE"] = file_path
    import shelve
    SHELF_FILE = shelve.open(current_app.config["SHELF_FILE_NAME"])
    SHELF_FILE['DBDIR'] = current_app.config["DATABASE"]
    SHELF_FILE.sync()
    SHELF_FILE.close()
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    records = format(rec_count, ",").replace(",", ".")
    language = session.get('lang', '')
    return render_template('index.html', datenbank=dbfile, records=records, is_admin=current_app.config["IS_ADMIN"],
                           language=language)


@main.route('/crawl_web/',  methods = ["GET","POST"])
def crawl_web() :
    import requests
    #pdb.set_trace()
    url = "https://www.nexxt-change.org/SiteGlobals/Forms/Kaufgesuch_Suche/Kaufgesuche_Formular.html"
    req = requests.get(url)
    html = req.content.decode('latin1', 'ignore')
    soup = BeautifulSoup(html, "lxml")
    i,j=1,0
    while i:
        for link in soup.findAll("a"):
            if str(link.get('href'))[-6:].isdigit() and str(link.get('href')[:27]) == '/DE/Kaufgesuch/Detailseite/':
                #pdb.set_trace()
                description, no_of_emp, date, branch2, serial_no, seeker_type, sales, asking_price, activity = \
                    "''","''","''","''","''","''","''","''","''"
                new_url = "https://www.nexxt-change.org"+str(link.get('href'))
                req_each = requests.get(new_url)
                html_each = req_each.content.decode('latin1', 'ignore')
                soup_each = BeautifulSoup(html_each, "lxml")
                for desc in soup_each.findAll("h3"):
                    if str(desc.text) == 'Beschreibung':
                        #pdb.set_trace()
                        if desc.findNext('p').text:
                            description = "'"+str(desc.findNext('p').text)+"'"
                for each_dt in soup_each.findAll("dt"):
                    if str(each_dt.text) == 'Anzahl Mitarbeiter:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        no_of_emp = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text) == 'Datum:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        date = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text)[:9] == 'Branchen:':
                        #pdb.set_trace()
                        branch1 = each_dt.findNext('dd').text.replace('\n\n','*')
                        branch2 = branch1.replace('\n','*')
                    if str(each_dt.text) == 'Chiffre:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        serial_no = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text) == 'Inseratstyp:'and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        seeker_type = str(each_dt.findNext('dd').text)
                    if str(each_dt.text) == 'Letzter Jahresumsatz in TEUR:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        sales = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text) == 'Preisvorstellung:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        asking_price = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text) == 'Internationale Tätigkeit:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        activity = "'"+str(each_dt.findNext('dd').text)+"'"
                    if str(each_dt.text)[:10] == 'Standorte:' and each_dt.findNext('dd').text:
                        #pdb.set_trace()
                        a = str(each_dt.findNext('dd').text).replace('\n\n','')
                        a = a.split('\n')
                        for each_standorte in a:
                            k = each_standorte.split('>')
                            if len(k) == 1:
                                bundesland = "'"+str(k[0])+"'"
                                bundesland = bundesland.replace('ü', 'u')
                                sql_query = "insert into web_location_data (chiffre,bundesland) values ("+\
                                            serial_no + "," + bundesland+")"
                            if len(k) == 2:
                                bundesland, region = "'"+str(k[0])+"'", "'"+str(k[1])+"'"
                                bundesland = bundesland.replace('ü', 'u')
                                region = region.replace('ü', 'u')
                                sql_query = "insert into web_location_data (chiffre,bundesland,region) values (" + \
                                            serial_no + "," + bundesland+","+ region + ")"
                            if len(k) == 3:
                                bundesland, region, kreis = "'"+str(k[0])+"'", "'"+str(k[1])+"'", "'"+str(k[2])+"'"
                                bundesland = bundesland.replace('ü', 'u')
                                region = region.replace('ü', 'u')
                                kreis = kreis.replace('ü', 'u')
                                sql_query = "insert into web_location_data (chiffre,bundesland,region,kreis) values (" + \
                                            serial_no + "," + bundesland+","+ region+","+ kreis + ")"
                            sql = text(sql_query)
                            #pdb.set_trace()
                            db.engine.execute(sql)
                    if str(each_dt.text)[:9] == 'Branchen:' and each_dt.findNext('dd').text:
                        a = str(each_dt.findNext('dd').text).replace('\n\n','')
                        a = a.split('\n')
                        for each_standorte in a:
                            k = each_standorte.split('>')
                            if len(k) == 1:
                                sektor = "'"+str(k[0])+"'"
                                sql_query = "insert into web_branch_data (chiffre,sektor) values ("+serial_no + "," + sektor+")"
                            if len(k) == 2:
                                sektor, rubrik = "'"+str(k[0])+"'", "'"+str(k[1])+"'"
                                sql_query = "insert into web_branch_data (chiffre,sektor,rubrik) values (" + serial_no + \
                                            "," + sektor+","+ rubrik + ")"
                            if len(k) == 3:
                                sektor, rubrik, einzelbranche = "'"+str(k[0])+"'", "'"+str(k[1])+"'", "'"+str(k[2])+"'"
                                sql_query = "insert into web_branch_data (chiffre,sektor,rubrik,einzelbranche) values (" + \
                                            serial_no + "," + sektor+","+ rubrik+","+ einzelbranche + ")"
                            sql = text(sql_query)
                            db.engine.execute(sql)
                date_current = "'"+time.strftime('%Y-%m-%d')+"'"
                reference_url = "'"+new_url+"'"
                #pdb.set_trace()
                sql_query = "insert into web_search_result (daturn,chiffre,beschreibung,anzahl_mitarbeiter,letzter_jahresumsatz,preisvorstellung,tatigkeit,last_crawl_date,reference_url) values ("+date+","+serial_no+","+description+ ","+no_of_emp+","+sales+","+asking_price+","+activity+","+date_current+","+reference_url+")"
                sql = text(sql_query)
                result1 = db.engine.execute(sql)
                print(link.get('href'))
        i=0
        print("completed---------i------------------------------")
        j+=1
        print(j)
        #pdb.set_trace()
        for each_li in soup.findAll("li"):
            if each_li.get('class') and str(each_li.get('class')[0]) == 'forward':
                #pdb.set_trace()
                print("test")
                new_url = "https://www.nexxt-change.org"+str(each_li.a.get('href'))
                #pdb.set_trace()
                req = requests.get(new_url)
                html = req.content.decode('latin1', 'ignore')
                soup = BeautifulSoup(html, "lxml")
                i+=1

@main.route('/set_web_data/',  methods = ["GET","POST"])
def set_web_data() :
    pdb.set_trace()
    # name = QtGui.QFileDialog.getOpenFileName('Open file')
    # file = open(name)
    # root = tkinter.Tk()
    # root.withdraw()
    # currdir = os.getcwd()
    # tempdir = tkinter.filedialog.askopenfile(parent=root, initialdir=currdir, title='Please select a File')
    # print("")
    excel_file = pd.read_excel(open('C:\\Users\\Sandeep Agrawal\\Documents\\web_data.xlsx','rb'))
    headers = excel_file.columns
    settlement_data = excel_file.as_matrix(columns=headers)
    for each_record in settlement_data:
        print(each_record)
        branch_code = str(each_record[0]) + str(each_record[1]).replace(".","")
        branch_code = "'"+'{:<05s}'.format(branch_code)+"'"
        business = "'"+str(each_record[2])+"'"
        business_detail = "'"+str(each_record[3])+"'"
        business_exclude = "'"+str(each_record[4])+"'"
        sql_query = "insert into web_data_storage (branchcode,langtitel,einschlusse,ausschlusse) values (" + \
                    branch_code + "," + business + "," + business_detail + "," + business_exclude + ")"
        sql = text(sql_query)
        result1 = db.engine.execute(sql)
        #pdb.set_trace()

@main.route('/test_web_data/',  methods = ["GET","POST"])
def test_web_data() :
    print(current_app.config['SHELF_FILE'])

def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.max()
    return 'background-color: yellow'

@main.route('/KI_Export_salesforce/<aid_firma_list>/<aid>/<search_word>',  methods = ["GET","POST"])
def ki_export_salesforce(aid_firma_list,aid,search_word) :
    aid_firma_list = aid_firma_list.split(',')
    aid_firma_list = ['2050828681']
    data_dict = {'AID':[],'ATE':[]}
    for each_aid in aid_firma_list:
        aid_man, aid_ate = [], []
        man_data = man.query.filter_by(MAN_Funktion='Geschäftsführer',AID_Firma=each_aid).all()
        for man_aid in man_data:
            aid_man.append(man_aid.AID_MAN)
        ate_data = ate.query.filter_by(AID_Firma=each_aid).all()
        for ate_aid in ate_data:
            aid_ate.append(ate_aid.AID_ATE)
        for each_aid_man in aid_man:
            if each_aid_man in aid_ate:
                data_dict['AID'].append(each_aid)
                data_dict['ATE'].append(each_aid_man)
                break
    exportlog = export_log(type="Salesforce",user_email=current_user.email,AID=aid,keywords=search_word,\
                           date=time.strftime('%Y-%m-%d'),time=time.strftime('%H-%M-%S'))
    db.session.add(exportlog)
    db.session.commit()
    df = pd.DataFrame(data_dict)
    df.style.applymap(highlight_max)
    df.to_csv("Salesforce_Export.csv",index=False)
    filedir = os.path.abspath("")
    filename = filedir + "\Salesforce_Export.csv"
    return send_file(filename,as_attachment=True)

@main.route('/bootstrap_test/', methods = ["GET"])
def bootstrap_test():
    print("sddddddddddddddddddddddd")
    print("Test your ")
    a = export_log.query.filter(export_log.Id < 24)
    print(dir(a))
    print(dir(export_log.query.filter(export_log.Id < 24)))
    print("Testing the data----------------------")
    print("Printing the values------------------------------")
    print("Testing whether the data prints properly bcoz we need to test it")
    print("")
    print(a)
    return render_template("bootstrap_test.html",a="hello")

@main.route('/Nexxt_Change_Filter/', methods = ["GET"])
def nexxt_change_filter():
    print(request.args)
    search_extra = str(request.args.get('search_extra','')).split(",")
    if len(search_extra) < 3:
        search_extra = ['','','']
    branch_browse = web_branch_data.query.filter_by().all()
    sektor = []
    for each_branch in branch_browse:
        if each_branch.sektor not in sektor:
            sektor.append(each_branch.sektor)
    return render_template("next_change_filter.html",sektor=sektor,search_extra=search_extra)

@main.route('/set_next_change_filter/', methods = ["POST"])
def set_next_change_filter():
    print(request.args)
    data_list = []
    if request.values.get('data','') == 'sek':
        branch_browse = web_branch_data.query.filter_by(sektor=request.values['sektor']).all()
    if request.values.get('data', '') == 'rub':
        branch_browse = web_branch_data.query.filter_by(rubrik=request.values['rubrik']).all()
    for each_branch in branch_browse:
        if request.values.get('data','') == 'sek' and each_branch.rubrik and each_branch.rubrik not in data_list:
            data_list.append(each_branch.rubrik)
        if request.values.get('data','') == 'rub' and each_branch.einzelbranche and each_branch.einzelbranche not in data_list:
            data_list.append(each_branch.einzelbranche)
    return json.dumps({'Data':list(set(data_list))})

@main.route('/get_nextchange_data/', methods = ["GET"])
def get_nextchange_data():
    print(request.args)
    print(request.values)
    print(request.data)
    data_sort = []
    if "sort" in request.args:
        element = None
        if request.args['sort'] == 'Id':
            element = web_search_result.Id
        elif request.args['sort'] == 'daturn':
            element = web_search_result.daturn
        elif request.args['sort'] == 'chiffre':
            element = web_search_result.chiffre
        elif request.args['sort'] == 'beschreibung':
            element = web_search_result.beschreibung
        elif request.args['sort'] == 'standorte':
            element = web_search_result.standorte
        elif request.args['sort'] == 'anzahl_mitarbeiter':
            element = web_search_result.anzahl_mitarbeiter
        elif request.args['sort'] == 'letzter_jahresumsatz':
            element = web_search_result.letzter_jahresumsatz
        elif request.args['sort'] == 'Preisvorstellung':
            element = web_search_result.Preisvorstellung

        if "order" in request.args and element is not None:
            if request.args['order'] == 'desc':
                data_sort.append(element.desc())
            else:
                data_sort.append(element.asc())
    else:
        data_sort.append(web_search_result.Id.asc())
    search = request.args.get('search', '')
    search_extra = ast.literal_eval(request.args.get('search_extra', ''))
    count_data = web_search_result.query.filter(or_(web_search_result.Id.like('%' + search + '%'),
                                     web_search_result.daturn.like('%' + search + '%'),
                                     web_search_result.chiffre.like('%' + search + '%'),
                                     web_search_result.beschreibung.like('%' + search + '%'),
                                     web_search_result.standorte.like('%' + search + '%'),
                                     web_search_result.anzahl_mitarbeiter.like('%' + search + '%'),
                                     web_search_result.letzter_jahresumsatz.like('%' + search + '%'),
                                     web_search_result.Preisvorstellung.like('%' + search + '%'))).count()

    next_data = web_search_result.query.filter(or_(web_search_result.Id.like('%' + search + '%'),
                                             web_search_result.daturn.like('%' + search + '%'),
                                             web_search_result.chiffre.like('%' + search + '%'),
                                             web_search_result.beschreibung.like('%' + search + '%'),
                                             web_search_result.standorte.like('%' + search + '%'),
                                             web_search_result.anzahl_mitarbeiter.like('%' + search + '%'),
                                             web_search_result.letzter_jahresumsatz.like('%' + search + '%'),
                                             web_search_result.Preisvorstellung.like('%' + search + '%'),
                                             )).order_by(*data_sort)
    '''total_ids, remove_ids = [], []
    for each_next in next_data:
        total_ids.append(each_next.Id)
        search_branch = web_branch_data.query.filter_by(chiffre=each_next.chiffre).all()
        i = 0
        for each_branch in search_branch:
            if search_extra[0] and each_branch.sektor == search_extra[0]:
                i += 1
            if not search_extra[0]:
                i += 1
            if search_extra[1] and each_branch.rubrik == search_extra[1]:
                i += 1
            if not search_extra[1]:
                i += 1
            if search_extra[2] and each_branch.einzelbranche == search_extra[2]:
                i += 1
            if not search_extra[2]:
                i += 1

        if i < 3:
            remove_ids.append(each_next.Id)

        #print(each_next)
    final_ids = list(set(total_ids) - set(remove_ids))
    next_data = next_data.filter(web_search_result.Id.in_(final_ids))
    pdb.set_trace()'''

    if 'limit' in request.args:
        next_data = next_data.limit(request.args['limit'])
    if 'offset' in request.args:
        next_data = next_data.offset(request.args['offset'])
    output = []
    for i in next_data:
        dic = {
            "Id":i.Id,
            "daturn": i.daturn,
            "chiffre": i.chiffre,
            "beschreibung": i.beschreibung,
            "standorte": i.standorte,
            "anzahl_mitarbeiter": i.anzahl_mitarbeiter,
            "letzter_jahresumsatz": i.letzter_jahresumsatz,
            "Preisvorstellung": i.Preisvorstellung,
        }
        output.append(dic)
    print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
    print(count_data)
    final_data = {'total': count_data, 'rows': output}
    return json.dumps(final_data)

@main.route('/bootstrap_test_data/', methods = ["GET"])
def bootstrap_test_data():
    print(request.args)
    a = {'total': 23 , 'rows':[{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'tandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii','state':'sandstate'},{'name':'sand','nii':'sandnii1','state':'sandstate'}]}
    return json.dumps(a)