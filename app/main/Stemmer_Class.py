# -*- coding: utf-8 -*-
"""
Created on Sun Feb  4 16:16:08 2018

@author: holge
"""

class Stemmer():
    'determines the word stem for German words. Result to be found in self.stem variable \
    algorithm derived from Snoeball Stemmer'
    def __init__(self,word_init):
        vowels = ["a","e","i","o","u","ä","ö","ü","y"] # define all vowels in German laguage
        s_ending = ["bs","ds", "fs","gs","hs","ks","ls","ms","ns","rs","ts" ] # definde valid s-endings
        st_ending = ["bst","dst","fst","gst","hst","kst","lst","mst","nst","tst" ] # define valid st-endings

        single_ending_1 = ["e"]
        double_ending_1 = ["em","er","en", "es"]
        triple_ending_1 = ["ern"]

        double_ending_2 = ["er","en", "st"]
        triple_ending_2 = ["est"]

        self.r1 = ""
        self.r2 = ""
        word = word_init.lower()
        word = word_init.replace("ß","ss")
        
        for i, letter in enumerate(word):
            if i == 0 :
                continue
            if letter == "u" or letter == "y":
                if i+1 < len(word):
                    if word[i-1] in vowels and word[i+1] in vowels:
                        word = word.replace(word[i],letter.upper())
        self.get_regions(word, vowels) # define the regions R1 and R2 wihtin the word

        # Step 1 of the algorithm
        self.stem = word
        for i in range(3,0,-1):
            if len(self.r1) < i-1:
                continue
            if self.r1[-i:] in triple_ending_1:
                self.stem = word[:-3]
                break
            elif self.r1[-i:] in double_ending_1:
                self.stem = word[:-2]
                break
            elif self.r1[-i:] in single_ending_1:
                self.stem = word[:-1]
            elif self.r1[-i:] in s_ending:
                self.stem = word[:-1]
        if self.stem[-4:] == "niss":
                self.stem = self.stem[:-1]

        # Step 2 of the algorithm
        self.get_regions(self.stem, vowels)
        for i in range(3,0,-1):
            if len(self.r1) < i-1:
                continue
            if self.r1[-i:] in triple_ending_2:
                self.stem = word[:-3]
                break
            elif self.r1[-i:] in double_ending_2:
                self.stem = word[:-2]
                break
            elif self.r1[-i:] in st_ending:
                self.stem = word[:-2]
        
        # Step 3 of the algorithm
        self.get_regions(self.stem, vowels)
        if self.r2.endswith("end") or self.r2.endswith("ung"):
                self.stem = self.stem[:-3]
                if self.r2.endswith("igend") or self.r2.endswith("igung"):
                    if not self.r2.endswith("eigend") or self.r2.endswith("eigung"):
                        self.stem = self.stem[:-3]
        if self.r2.endswith("isch"):
            if not self.r2.endswith("eisch"):
                self.stem = self.stem[:-4]
        if self.r2.endswith("ig") or self.r2.endswith("ik"):
            if not self.r2.endswith("eig") or self.r2.endswith("eik"):
                self.stem = self.stem[:-2]
        if self.r2.endswith("lich") or self.r2.endswith("heit"):
            self.stem = self.stem[:-4]
            if self.r1.endswith("erlich") or self.r1.endswith("enheit"):
                self.stem = self.stem[:-2]
        if self.r2.endswith("keit"):
            self.stem = self.stem[:-4]
            if self.r2.endswith("lichkeit"):
                self.stem = self.stem[:-4]
            elif self.r2.endswith("igkeit"):
                self.stem = self.stem[:-2]
                
        # Final works
        self.stem = self.stem.lower()
        self.stem = self.stem.replace("ä", "a")
        self.stem = self.stem.replace("ö", "o")
        self.stem = self.stem.replace("ü", "u")
        
    def get_regions(self,word, vowels):
        for i,letter in enumerate(word):
            if letter not in vowels :
                if i == 0:
                    continue
                else:
                    if word[i-1] in vowels:
                        if self.r1 == "":
                            self.r1 = word[i+1:]
                        else:
                            self.r2 = word[i+1:]
                            break



# w = Stemmer("dienstleist")
# print("Stemmer: ", w.stem)
