# -*- coding: utf-8 -*-

from .. import db
from ..models import tfidf, tfidf_stemmer
from . import main

from flask import current_app
from pandas import Series, DataFrame, ExcelWriter
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from tkinter import *
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import messagebox


import numpy as np
import os,time,sqlite3
import pandas as pd
import pdb
import pickle


from sklearn.feature_extraction.text import HashingVectorizer

"""---------------------------------------------------
Funktion:   open_file
Zweck:      fildedialog-Implementation für Web-Anwendung
-----------------------------------------------------"""
def open_file():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.db'
    options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
    options['initialdir'] = current_app.config["DBDIR"]
    options['initialfile'] = 'Firmen.db'
    options['parent'] = root
    options['title'] = 'Datenbank wählen'
    filename = filedialog.askopenfilename(**options)
    root.destroy()
    return(filename)

"""---------------------------------------------------
Funktion:   save_file
Zweck:      fildedialog-Implementation für Web-Anwendung
-----------------------------------------------------"""
def save_file():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.xlsx'
    options['filetypes'] = [('Excel files', '.xlsx'), ('All files', '.*')]
    options['initialdir'] = current_app.config["DBDIR"]
    options['initialfile'] = 'AID.xlsx'
    options['parent'] = root
    options['title'] = 'Datei wählen'
    filename = filedialog.asksaveasfilename(**options)
    root.destroy()
    return(filename)
    
"""--------
Funktion: MyInput
--------"""
def MyInput(text, msg):
    root = Tk()
    root.withdraw()
    AID = simpledialog.askstring(text, msg)
    root.destroy()
    return AID

"""--------
Funktion: MyMsgBox
--------"""
def MyMsgBox(headline, msg):
    root = Tk()
    root.withdraw()
    messagebox.showinfo(headline, msg) 
    root.destroy()


def tfidf(firmen_db, aid_list):
    start = time.time()
    corpus_list = []
    cursor = firmen_db.cursor()
    templist=[]
    for AID in aid_list:
        record_dict = {'AID': '', 'Name': '', 'Description': '', 'Branch_Code_Main': '', 'Branch_Codes_Secondaries': ''}
        statement = 'SELECT * from TFIDF where AID = "' + str(AID) + '"'
        cursor.execute(statement)
        res = cursor.fetchone()
        if res:
          if res[0]  not in  templist:
            record_dict['AID'] = res[0]
            record_dict['Name'] = res[1]
            record_dict['Description'] = res[2]
            record_dict['Branch_Code_Main'] = res[3]
            record_dict['Branch_Codes_Secondaries'] = res[4]
            corpus_list.append(record_dict)
            templist.append(res[0])
        else:
            continue
    corpus=pd.DataFrame(corpus_list)
    end = time.time()
    duration = end - start
    print("Corpus preparation: %s seconds " % duration)

    start2 = time.time()

    companies = corpus["AID"]
    wz_weight = 2
    wz_corpus = corpus["Branch_Code_Main"]
    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix = tfidf_vectorizer.fit_transform(wz_corpus)
    ar = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)
    result_list = ar[0]
    data = {"Company" : companies, "Rank" : result_list}
    frame_wz = pd.DataFrame(data)
    frame_wz["Rank"] = frame_wz["Rank"] * wz_weight
    end = time.time()
    duration = end - start2
    print("WZ preparation: %s seconds " % duration)

    start2 = time.time()
    desc_corpus = corpus["Description"] + corpus["Name"]
    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix = tfidf_vectorizer.fit_transform(desc_corpus)
    ar = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)
    result_list = ar[0]
    data = {"Company" : companies, "Rank" : result_list}
    frame_desc = pd.DataFrame(data)
    frame_final = frame_wz

    frame_final["Value"] = frame_wz["Rank"] + frame_desc["Rank"]
    frame_final["Name"] = corpus["Name"]
    frame_final["Business_Activity"] = corpus["Description"]
    result_frame = frame_final.sort_values(by="Value", ascending = False)
    result_frame = result_frame[result_frame.Value > 0.1]
    result_frame = result_frame[result_frame.Value != 2.0]
    end = time.time()
    duration = end - start2
    print("TTG preparation: %s seconds " % duration)

    end = time.time()
    duration = end - start
    return result_frame, duration





class Stemmer():
    'determines the word stem for German words. Result to be found in self.stem variable \
    algorithm derived from Snoeball Stemmer'

    def __init__(self, word_init):
        vowels = ["a", "e", "i", "o", "u", "ä", "ö", "ü", "y"]  # define all vowels in German laguage
        s_ending = ["bs", "ds", "fs", "gs", "hs", "ks", "ls", "ms", "ns", "rs", "ts"]  # definde valid s-endings
        st_ending = ["bst", "dst", "fst", "gst", "hst", "kst", "lst", "mst", "nst", "tst"]  # define valid st-endings

        single_ending_1 = ["e"]
        double_ending_1 = ["em", "er", "en", "es"]
        triple_ending_1 = ["ern"]

        double_ending_2 = ["er", "en", "st"]
        triple_ending_2 = ["est"]

        self.r1 = ""
        self.r2 = ""
        word = word_init.lower()
        word = word_init.replace("ß", "ss")

        for i, letter in enumerate(word):
            if i == 0:
                continue
            if letter == "u" or letter == "y":
                if i + 1 < len(word):
                    if word[i - 1] in vowels and word[i + 1] in vowels:
                        word = word.replace(word[i], letter.upper())
        self.get_regions(word, vowels)  # define the regions R1 and R2 wihtin the word

        # Step 1 of the algorithm
        self.stem = word
        for i in range(3, 0, -1):
            if len(self.r1) < i - 1:
                continue
            if self.r1[-i:] in triple_ending_1:
                self.stem = word[:-3]
                break
            elif self.r1[-i:] in double_ending_1:
                self.stem = word[:-2]
                break
            elif self.r1[-i:] in single_ending_1:
                self.stem = word[:-1]
            elif self.r1[-i:] in s_ending:
                self.stem = word[:-1]
        if self.stem[-4:] == "niss":
            self.stem = self.stem[:-1]

        # Step 2 of the algorithm
        self.get_regions(self.stem, vowels)
        for i in range(3, 0, -1):
            if len(self.r1) < i - 1:
                continue
            if self.r1[-i:] in triple_ending_2:
                self.stem = word[:-3]
                break
            elif self.r1[-i:] in double_ending_2:
                self.stem = word[:-2]
                break
            elif self.r1[-i:] in st_ending:
                self.stem = word[:-2]

        # Step 3 of the algorithm
        self.get_regions(self.stem, vowels)
        if self.r2.endswith("end") or self.r2.endswith("ung"):
            self.stem = self.stem[:-3]
            if self.r2.endswith("igend") or self.r2.endswith("igung"):
                if not self.r2.endswith("eigend") or self.r2.endswith("eigung"):
                    self.stem = self.stem[:-3]
        if self.r2.endswith("isch"):
            if not self.r2.endswith("eisch"):
                self.stem = self.stem[:-4]
        if self.r2.endswith("ig") or self.r2.endswith("ik"):
            if not self.r2.endswith("eig") or self.r2.endswith("eik"):
                self.stem = self.stem[:-2]
        if self.r2.endswith("lich") or self.r2.endswith("heit"):
            self.stem = self.stem[:-4]
            if self.r1.endswith("erlich") or self.r1.endswith("enheit"):
                self.stem = self.stem[:-2]
        if self.r2.endswith("keit"):
            self.stem = self.stem[:-4]
            if self.r2.endswith("lichkeit"):
                self.stem = self.stem[:-4]
            elif self.r2.endswith("igkeit"):
                self.stem = self.stem[:-2]

        # Final works
        self.stem = self.stem.lower()
        self.stem = self.stem.replace("ä", "a")
        self.stem = self.stem.replace("ö", "o")
        self.stem = self.stem.replace("ü", "u")

    def get_regions(self, word, vowels):
        for i, letter in enumerate(word):
            if letter not in vowels:
                if i == 0:
                    continue
                else:
                    if word[i - 1] in vowels:
                        if self.r1 == "":
                            self.r1 = word[i + 1:]
                        else:
                            self.r2 = word[i + 1:]
                            break
"""--------
Funktion: tfidf
--------"""
def create_tfidf(Company, selektion=[], suchworte="", test="no"):
   #  stopwords = ['und', 'von', 'die', 'der', 'sowie', 'mit', 'im', 'in', 'des', 'für', 'an', 'insbesondere',
   #               'ist', 'art', 'aller', 'oder', 'betrieb', 'vertrieb','zu', 'den', 'gesellschaft', 'als', 'das', 'erwerb', 'dem', 'dienstleistungen',
   #               'durchführung', 'gmbh', 'gegenstand', 'übernahme', 'damit', 'verkauf', 'einer', 'entwicklung',
   #               'beteiligung', 'vermittlung', 'beratung', 'eigenen', 'auf', 'co', 'tätigkeiten', 'zur', 'vermietung',
   #               'kg', 'immobilien', 'alle', 'bereich', 'bei', 'eines', 'beteiligungen', 'anderen', 'sind',
   #               'geschäfte', 'erbringung', 'deren', 'durch', 'geschäftsführung', 'zusammenhang', 'förderung',
   #               'sich', 'auch', 'nicht', 'unternehmens', 'zum', 'persönlich', 'vermögens', 'werden',
   #               'gesellschafterin', '1', 'haftung', 'grundstücken', 'planung', 'nach', 'haftende', 'halten', 'aus',
   #               'unter', 'persönlichen',  'einschließlich', 'waren', 'stehenden', 'wird',
   #               'zweck', 'anlagen', 'gesellschaften', 'firma',  'eine', 'sonstigen','über',  'es']
   #  file_name = current_app.config['RAWDATDF']
   # #  pdb.set_trace()
   # # # pdb.set_trace()
   #  # if current_app.config["RAW_DATA_TFIDF"] is not None:
   #  #     raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
   #  # elif os.path.exists(file_name):
   #  #     current_app.config["RAW_DATA_TFIDF"] = pd.read_pickle(file_name)
   #  #     raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
   #  # else:
   #  tfidf_factor = "Business_Activity"
   #  if test == "yes":
   #      tfidf_factor = "all_data"
   #  current_app.config["RAW_DATA_TFIDF"] = pd.read_sql(tfidf.query.statement, db.session.bind)#pd.read_pickle(file_name)#
   #  raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
   #   #   raw_data.to_pickle(file_name)
   #
   #  # Standard: Einfache Suche oder allererster Durchlauf -> erzeugt tfidf
   #
   #  tfidf_vectorizer = TfidfVectorizer()
   #  #tfidf_vectorizer = HashingVectorizer()
   #  file_name = current_app.config['WZPKL']
   #  WZ_Liste = raw_data["Branch_Code_Main"]
   #  if current_app.config["TFIDF_WZ"] is not None:
   #      tfidf_wz = current_app.config["TFIDF_WZ"]
   #  # elif os.path.exists(file_name) :
   #  #     current_app.config["TFIDF_WZ"] = pickle.load(open(file_name, "rb"))
   #  #     tfidf_wztfidf_wz = current_app.config["TFIDF_WZ"]
   #  else :
   #      current_app.config["TFIDF_WZ"] = tfidf_vectorizer.fit_transform(WZ_Liste)
   #      tfidf_wz = current_app.config["TFIDF_WZ"]
   #      pickle.dump(tfidf_wz, open(file_name, "wb"))
   #  file_name = current_app.config['TTPKL']
   #  TT_Liste = raw_data[tfidf_factor] + raw_data["Name"]
   #  if current_app.config["TFIDF_TT"] is not None:
   #      tfidf_tt = current_app.config["TFIDF_TT"]
   #  # elif os.path.exists(file_name) :
   #  #     current_app.config["TFIDF_TT"] = pickle.load(open(file_name, "rb"))
   #  #     tfidf_tt = current_app.config["TFIDF_TT"]
   #  else :
   #      current_app.config["TFIDF_TT"] = tfidf_vectorizer.fit_transform(TT_Liste)
   #      tfidf_tt = current_app.config["TFIDF_TT"]
   #
   #      #pickle.dump(tfidf_tt, open(file_name, "wb"))
   #  oldtatigkeit=raw_data["Business_Activity"]
   #  if Company != "0000000000" :
   #      try:
   #          Nummer = raw_data.loc[raw_data["AID"] == Company].index[0]
   #      except:
   #          return "no_aid"
   #  else :
   #      Nummer = len(raw_data)
   #
   #  # suchworte vorhanden -> müssen in die Activity_Description_ROC hinzugefügt werden
   #  if suchworte.find("undefined")>-1:
   #      suchworte=""
   #
   #  if suchworte != "" :
   #      if Nummer == len(raw_data) :
   #          taetigkeit = suchworte
   #          oldtaetigkeit = taetigkeit
   #          sample = re.findall(r"[\w']+", taetigkeit.lower())
   #          for each in sample:
   #              if str(each).lower() in stopwords:
   #                  sample.remove(each)
   #          taetigkeit = " ".join(str(e) for e in sample)
   #          raw_data.loc[Nummer] = [Company, "", taetigkeit, "XX", "XX", "XX"]
   #      else :
   #          taetigkeit = suchworte +" "+ raw_data.loc[Nummer, tfidf_factor]
   #          oldtaetigkeit = taetigkeit
   #          sample = re.findall(r"[\w']+", taetigkeit.lower())
   #          for each in sample:
   #              if str(each).lower() in stopwords:
   #                  sample.remove(each)
   #          taetigkeit = " ".join(str(e) for e in sample)
   #          raw_data.loc[Nummer, tfidf_factor] = taetigkeit
   #      WZ_Liste = raw_data["Branch_Code_Main"]
   #      tfidf_wz = tfidf_vectorizer.fit_transform(WZ_Liste)
   #      TT_Liste = raw_data[tfidf_factor] + raw_data["Name"]
   #      tfidf_tt = tfidf_vectorizer.fit_transform(TT_Liste)
   #  else:
   #      if Nummer == len(raw_data) :
   #          taetigkeit =  raw_data.loc[Nummer, tfidf_factor]
   #          oldtaetigkeit=taetigkeit
   #          sample=re.findall(r"[\w']+", taetigkeit.lower())
   #          for each in sample:
   #              if str(each).lower() in stopwords:
   #                  sample.remove(each)
   #          taetigkeit=" ".join(str(e) for e in sample)
   #          # for each in stopwords:
   #          #     taetigkeit=taetigkeit.replace(each,"")
   #          raw_data.loc[Nummer] = [Company, "", taetigkeit, "XX", "XX", "XX"]
   #      else :
   #          taetigkeit = raw_data.loc[Nummer, tfidf_factor]
   #          oldtaetigkeit = taetigkeit
   #          sample = re.findall(r"[\w']+", taetigkeit.lower())
   #          for each in sample:
   #              if str(each).lower() in stopwords:
   #                  sample.remove(each)
   #          taetigkeit = " ".join(str(e) for e in sample)
   #          # for each in stopwords:
   #          #     taetigkeit=taetigkeit.replace(each,"")
   #          raw_data.loc[Nummer, tfidf_factor] = taetigkeit
   #      WZ_Liste = raw_data["Branch_Code_Main"]
   #      tfidf_wz = tfidf_vectorizer.fit_transform(WZ_Liste)
   #      TT_Liste = raw_data[tfidf_factor] + raw_data["Name"]
   #      tfidf_tt = tfidf_vectorizer.fit_transform(TT_Liste)
   #  print(taetigkeit)
   #
   #  # Bewertung der WZ
   #  WZ_Gewichtung = 2
   #  companies = raw_data["AID"]
   #  ar = cosine_similarity(tfidf_wz[Nummer:Nummer+1], tfidf_wz)
   #  Ergebnisliste = ar[0]
   #  raw_data.loc[Nummer, tfidf_factor] = oldtaetigkeit
   #  data = {"Company" : companies, "Value" : Ergebnisliste}
   #  frame_WZ = DataFrame(data)
   #  frame_WZ["Value"] = frame_WZ["Value"] * WZ_Gewichtung
   #
   #  # Bewertung der Activity_Description_ROC
   #  ar = cosine_similarity(tfidf_tt[Nummer:Nummer+1], tfidf_tt)
   #  raw_data.loc[Nummer, tfidf_factor] = oldtaetigkeit
   #  Ergebnisliste = ar[0]
   #  data = {"Company" : companies, "Value" : Ergebnisliste}
   #  frame_Ttg = DataFrame(data)
   #
   #  frame_final = frame_WZ
   #  frame_final["Value"] = round(frame_WZ["Value"] + frame_Ttg["Value"], 3)
   #  frame_final["Name"] = raw_data["Name"]
   #  frame_final["Business_Activity"] = oldtatigkeit
   #  #set_trace()
   #
   #  result_frame = frame_final.sort_values(by="Value", ascending = False)
   #  if not selektion :
   #      result_frame = result_frame[result_frame.Value > 0.1]
   #      result_frame = result_frame[result_frame.Value != 2.0]
   #  else :
   #      result_frame = result_frame[result_frame["Company"].isin(selektion)]
   #
   #  result_frame.reset_index(drop=True, inplace=True)
   #  return(result_frame)

   start = time.time()
   print("Starting process....")
   dbfile = current_app.config["DBDIR"]
   firmen_db = sqlite3.connect(dbfile)
   cursor = firmen_db.cursor()

   # select stopwords
   stopper = 2000
   stopwords = []
   if stopper != 0:
       statement = 'SELECT * FROM Wordcount'
       cursor.execute(statement)
       i = 1
       for row in cursor:
           if i > stopper:
               break
           stem = row[3]
           stopwords.append(stem)
           i += 1
   statement = 'SELECT * FROM TFIDF where AID = "' + str(Company) + '"'
   cursor.execute(statement)
   res = cursor.fetchone()
   if res:
       if suchworte.find("undefined") != -1:
           records = (suchworte + " " + res[1] + " " + res[2]).split()
       else:
           records = (res[1] + " " + res[2]).split()

   words = set()
   for word in records:
       words.add(word.lower())


   word_stems = set()
   for word in words:

       clean_word = re.sub('[^A-Za-zßäÄöÖüÜ]+', ' ', word)
       clean_word = clean_word.replace(' ', '')
       stem = Stemmer(clean_word).stem
       if stem=="" or stem.find("undefined") != -1 or stem=='' or stem==' ':
           continue
       if stem in stopwords:
           continue
       word_stems.add(stem)
   print(word_stems)
   # select all the companies which have at least one word stem in common with selected AID
   aid_list = []
   for word in word_stems:
       statement = 'SELECT AID FROM Word_Mesh where word_stem = "' + str(word) + '"'
       cursor.execute(statement)
       for res in cursor:
          #aid_list.append(int(res[0]))
          #pdb.set_trace()
          aid_list.extend([int (i)for i in str(res[0]).split(",")])
   aid_set = set(aid_list)
   aid_set.discard(Company)

   aid_list = list(aid_set)
   aid_list.insert(0, Company)
   end = time.time()
   duration = end - start
   print("Company selection: %s seconds " % duration)

   result, duration2 = tfidf(firmen_db, aid_list)
   end = time.time()
   duration = end - start
   result.reset_index(drop=True, inplace=True)

   return (result)


"""--------
Funktion: tfidf stemmer
--------"""


def create_tfidf_stemmer(Company, selektion=[], suchworte=""):
    # file_name = current_app.config['RAWDATDF']
    # if current_app.config["RAW_DATA_TFIDF"] is not None:
    #     raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
    # elif os.path.exists(file_name):
    #     current_app.config["RAW_DATA_TFIDF"] = pd.read_pickle(file_name)
    #     raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
    # else:
    current_app.config["RAW_DATA_TFIDF"] = pd.read_sql(tfidf_stemmer.query.statement, db.session.bind)
    raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
       # raw_data.to_pickle(file_name)
    # Standard: Einfache Suche oder allererster Durchlauf -> erzeugt tfidf

    tfidf_vectorizer = TfidfVectorizer()
    file_name = current_app.config['WZPKL']
    WZ_Liste = raw_data["Branch_Code_Main"]
    if current_app.config["TFIDF_WZ"] is not None:
        tfidf_wz = current_app.config["TFIDF_WZ"]
    # elif os.path.exists(file_name):
    #     current_app.config["TFIDF_WZ"] = pickle.load(open(file_name, "rb"))
    #     tfidf_wztfidf_wz = current_app.config["TFIDF_WZ"]
    else:
        current_app.config["TFIDF_WZ"] = tfidf_vectorizer.fit_transform(WZ_Liste)
        tfidf_wz = current_app.config["TFIDF_WZ"]
       # pickle.dump(tfidf_wz, open(file_name, "wb"))
    file_name = current_app.config['TTPKL']
    TT_Liste = raw_data["Business_Activity"] + raw_data["Name"]
    if current_app.config["TFIDF_TT"] is not None:
        tfidf_tt = current_app.config["TFIDF_TT"]
    # elif os.path.exists(file_name):
    #     current_app.config["TFIDF_TT"] = pickle.load(open(file_name, "rb"))
    #     tfidf_tt = current_app.config["TFIDF_TT"]
    else:
        current_app.config["TFIDF_TT"] = tfidf_vectorizer.fit_transform(TT_Liste)
        tfidf_tt = current_app.config["TFIDF_TT"]
        #pickle.dump(tfidf_tt, open(file_name, "wb"))
    if Company != "0000000000":
        try:
            Nummer = raw_data.loc[raw_data["AID"] == Company].index[0]
        except:
            return "no_aid"
    else:
        Nummer = len(raw_data)
    # suchworte vorhanden -> müssen in die Activity_Description_ROC hinzugefügt werden
    if suchworte != "":
        if Nummer == len(raw_data):
            taetigkeit = suchworte
            raw_data.loc[Nummer] = [Company, "", taetigkeit, "XX", "XX", "XX"]
        else:
            taetigkeit = suchworte + raw_data.loc[Nummer, "Business_Activity"]
            raw_data.loc[Nummer, "Business_Activity"] = taetigkeit
        WZ_Liste = raw_data["Branch_Code_Main"]
        tfidf_wz = tfidf_vectorizer.fit_transform(WZ_Liste)
        TT_Liste = raw_data["Business_Activity"] + raw_data["Name"]
        tfidf_tt = tfidf_vectorizer.fit_transform(TT_Liste)
    # Bewertung der WZ
    WZ_Gewichtung = 2
    companies = raw_data["AID"]
    ar = cosine_similarity(tfidf_wz[Nummer:Nummer + 1], tfidf_wz)
    Ergebnisliste = ar[0]
    data = {"Company": companies, "Value": Ergebnisliste}
    frame_WZ = DataFrame(data)
    frame_WZ["Value"] = frame_WZ["Value"] * WZ_Gewichtung

    # Bewertung der Activity_Description_ROC
    ar = cosine_similarity(tfidf_tt[Nummer:Nummer + 1], tfidf_tt)
    Ergebnisliste = ar[0]
    data = {"Company": companies, "Value": Ergebnisliste}
    frame_Ttg = DataFrame(data)
    frame_final = frame_WZ
    frame_final["Value"] = round(frame_WZ["Value"] + frame_Ttg["Value"], 3)
    frame_final["Name"] = raw_data["Name"]
    frame_final["Business_Activity"] = raw_data["Business_Activity"]

    result_frame = frame_final.sort_values(by="Value", ascending=False)
    if not selektion:
        result_frame = result_frame[result_frame.Value > 0.1]
        result_frame = result_frame[result_frame.Value != 2.0]
    else:
        result_frame = result_frame[result_frame["Company"].isin(selektion)]

    result_frame.reset_index(drop=True, inplace=True)
    return (result_frame)
