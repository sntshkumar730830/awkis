import pdb,os,re,sys
import sqlite3
from flask import Flask, current_app, flash
from flask import render_template, session, redirect, current_app, flash, request, send_file, Flask

# def Seite_parsen(text, Inhalt):
#     Adressliste_dirty = Inhalt.splitlines()
#     #    text.insert(INSERT, "Länge Adressliste: " + str(len(Adressliste_dirty)) + " Zeilen\n\n")
#     Suchtext = "BvD ID Nummer"
#     Vorspann_Zeilen = Vorspann(Suchtext, Adressliste_dirty)  # Identifizieren des Beginns der Feldnamen
#     Such
#     text = "Spalten der Liste"
#     Abspann_Zeilen = Vorspann(Suchtext, Adressliste_dirty)  # Identifizieren des Beginns der Feldnamen
#     Adressliste = []
#     for i in range(Vorspann_Zeilen, Abspann_Zeilen):  # Entfernen der Vorspannzeilen
#         Adressliste.append(Adressliste_dirty[i])
#     Feldliste = Create_Fieldnames(Adressliste,
#                                   text)  # Erzeugen der Liste mit den Feldnamen und Löschen aller Feldüberschriften aus der Adressliste
#     Lösche_Leersätze(Adressliste, text)  # Danach sollten in der Adressliste bloß noch die reinen Daten vorhanden sein
#
#     Namensliste = []
#     Datenbank = []
#     for i in range(len(Adressliste)):
#         if len(Adressliste[i][0]) != 0 and Adressliste[i][0][0].isnumeric():
#             Namensliste.append(Adressliste[i][len(Adressliste[i]) - 1])
#         else:
#             Datenbank.append(Adressliste[i])
#     for i in range(len(Datenbank)):  # Einfügen eines Feldes zur Aufnahme des Namens des Unternehmens)
#         Datenbank[i].insert(0, "")
#     k = 0
#     for i in range(len(Namensliste)):
#         Datenbank[k][0] = Namensliste[i]
#         k += 1
#         if k >= len(Datenbank):
#             break
#         elif len(Datenbank[k][1]) == 0:
#             while len(Datenbank[k][1]) == 0:
#                 Datenbank[k][0] = Namensliste[i]
#                 k += 1
#                 if k >= len(Datenbank):
#                     break
#     Datenbank.insert(0, Feldliste)
#     return Datenbank
#
# """----------------------------------
# Funktion: Lösche_Leersätze
# Löscht alle leeren Datensätze (= Leerzeilen)
# ---------------------------------"""
#
#
# def Lösche_Leersätze(Adressliste, text_window):
#     Leersätze = []  # Dient zum Aufnehmen der leeren Zeilen, die später gelöscht werden
#     for i in range(len(Adressliste)):
#         Adressliste[i] = Adressliste[i].replace(";", " ")
#         Adressliste[i] = Adressliste[i].replace("\t\t", ";")
#         Adressliste[i] = Adressliste[i].split(";")
#         for k in range(len(Adressliste[i])):
#             Adressliste[i][k] = Adressliste[i][k].replace("\t\n", "")
#         Elemente = 0
#         for k in range(len(Adressliste[i])):
#             Adressliste[i][k] = Adressliste[i][k].strip()
#             Elemente = Elemente + len(Adressliste[i][k])
#         if Elemente == 0:
#             Leersätze.append(i)
#         elif len(Adressliste[i]) == 1:
#             Leersätze.append(i)
#     for i in range(len(Leersätze)):
#         Element = Leersätze[i]
#         Adressliste.pop(Element - i)
#     return
#
# """---------------------------------------
# Funktion: Create_Fieldnames
# Erzeugt die Liste der Feldnamen aus dem Datei-Header
# -------------------------------------"""
#
#
# def Create_Fieldnames(Adressliste, text_window):
#     #    Suchtext = "1.\t"
#     Suchtext = re.compile("\d+\.")
#
#     for i in range(len(Adressliste)):
#         lfd_nr = Suchtext.search(Adressliste[i])
#         if lfd_nr:
#             break
#     # Parsen der Struktur der übergebenen Datensätze
#     Headers = i
#     Feldzeilen = []
#     VollString = ""
#     for i in range(0, Headers):
#         VollString = VollString + str(Adressliste[i])
#         Feldzeilen.append(Adressliste[i])
#     VollString = VollString.replace("\n", " ")  # Entfernen der Zeilenvorschübe in den Namen
#     VollString = VollString.replace("\t", "\n")  # Alle Tabstopps werden mit Zeilenvorschub ersetzt
#     VollString = VollString.splitlines()  # Jetzt werden alle Zeilen wieder getrennt. Man erhält neben den Datensatznamen auch viele Leerzeilen
#     Feldliste = ["Company"]
#     for i in range(len(VollString)):  # Entfernen führender und schließender Leerzeichen
#         text = VollString[i].strip()
#         if text:
#             Feldliste.append(text)
#     for i in range(len(Feldzeilen)):
#         while (Feldzeilen[i] in Adressliste):
#             Adressliste.remove(Feldzeilen[i])
#     return (Feldliste)
#
# """------------------------------------
# Funktion: Vorpsann
# Bereinigt die nicht benötigten Zeilen aus dem Dateivorspann
# ------------------------------------"""
#
#
# def Vorspann(Suchtext, Adressliste):
#     for i in range(len(Adressliste)):
#         if (Suchtext in Adressliste[i]):
#             break
#     return i



def Datei_einlesen(Datei, Pfad):
    FileName = Pfad + Datei + ".txt"
    Adressdatei = open(FileName, encoding="Latin-1")
    Adressliste = Adressdatei.readlines()
    if "Seite \n" in Adressliste:
        position = Adressliste.index("Seite \n")
        pos = format(position, ",d").replace(",", ".")
        message = "Datei: " + FileName + "\n" + "Position: " + pos + "\n Datei wird übersprungen"
       # messagebox.showwarning("DATEIFEHLER", message)
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="DATEIFEHLER    "+message
        Adressliste = "Error"
    Adressdatei.close()
    return Adressliste

def DBTables():
    DBTables = {}

    sql_command = """CREATE TABLE Shareholder (AID_Company TEXT NOT NULL, AID_SHH TEXT NOT NULL, SHH_Function TEXT, Anteile REAL, \
                    SHH_Position TEXT, SHH_Function_Mailings TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("ATE", sql_command)

    sql_command = """CREATE TABLE Shareholder_Upload (Company TEXT, BvDId TEXT, CrefoID TEXT, \
                    SHH_CrefoID TEXT, SHH_Type TEXT, SHH_Title TEXT, SHH_Salutation TEXT,\
                    SHH_Full_Name TEXT, SHH_Last_Name TEXT, SHH_First_Name TEXT, SHH_Name_Part_3 TEXT, \
                    SHH_Gender TEXT, SHH_Function TEXT, SHH_Birth_Year TEXT, SHH_Function_Mailings TEXT, \
                    SHH_Street_Nr TEXT, SHH_City TEXT, SHH_District TEXT, SHH_ZIP TEXT, SHH_Country TEXT,\
                    SHH_Shares_Owned TEXT)"""
    DBTables.setdefault("Shareholder_Upload", sql_command)

    sql_command = """CREATE TABLE Address (AID TEXT NOT NULL , 'AD_Address_ID', 'Address_Addendum', `Street_Nr` , `ZIP` , `City` , \
                `County` , `State` , `Country` , `GKZ_X` , `GKZ_Y` , `GKZ_Street` , `Last_Change`)"""
    DBTables.setdefault("Address", sql_command)

    sql_command = """CREATE TABLE BDMaster (Branchenode TEXT, Branchencode_Beschreibung TEXT, Einschluss TEXT,  \
                    Ausschluss TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("BDMaster", sql_command)

    sql_command = """CREATE TABLE Branch_Upload (Company TEXT, BvDId TEXT, CrefoID TEXT, \
                    WZ08_Main_Code TEXT, WZ08_Secondary_Code TEXT, \
                    BD_Main_Code TEXT, BD_Secondary_Code TEXT)"""
    DBTables.setdefault("Branch_Upload", sql_command)

    sql_command = """CREATE TABLE Branch_Codes (AID TEXT, Branch_Code TEXT, Branch_Rank_Main_Sec TEXT, Source TEXT, \
                    Erfassungsatum TEXT)"""
    DBTables.setdefault("Branch_Codes", sql_command)

    sql_command = """CREATE TABLE Entity (`AID` TEXT, `Entity_Last_Name` TEXT, `Entity_First_Name` TEXT, `Address_Ownership` TEXT, \
                `Entity_Type` TEXT, `Legal_Form` TEXT, `Salutation` TEXT, `Titel` TEXT, `Education` TEXT, `Gender` TEXT, \
                `Birth_Year` TEXT, `Comparison_Group_Code` TEXT, `Comparison_Group_Size` TEXT, `Count_Shareholders` TEXT,\
                `Count_Group_Companies` TEXT, `Count_Subsidiaries` TEXT, `UID` TEXT,\
                `Reserved` TEXT, `Expiry_Date` TEXT, `Last_Change` TEXT )"""
    DBTables.setdefault("Entity", sql_command)

    sql_command = """CREATE TABLE Financials_Upload (CrefoID TEXT, Year INT, Type TEXT, Value REAL)"""
    DBTables.setdefault("Financials_Upload", sql_command)

    sql_command = """CREATE TABLE Financials (AID TEXT NOT NULL, Year INTEGER NOT NULL, Fin_Value_Type TEXT, Fin_Value REAL, \
                    Erfassungsatum TEXT)"""
    DBTables.setdefault("Financials", sql_command)

    sql_command = """CREATE TABLE Contact (CrefoID TEXT, Contact_Type TEXT, Contact_Value TEXT, Datum TEXT)"""
    DBTables.setdefault("Contact", sql_command)

    sql_command = """CREATE TABLE Manager (AID_Company TEXT NOT NULL, AID_MAN TEXT NOT NULL, MAN_Function TEXT, \
                    MAN_Position TEXT, MAN_Function_Mailings TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("Manager", sql_command)

    sql_command = """CREATE TABLE Manager_Upload (Company TEXT, BvDId TEXT, CrefoID TEXT, \
                    MAN_CrefoID TEXT, MAN_Type TEXT, MAN_Titel TEXT, MAN_Salutation TEXT,\
                    MAN_Last_Name TEXT, MAN_First_Name TEXT, MAN_Name_Part_3 TEXT, \
                    MAN_Gender TEXT, MAN_Function TEXT, MAN_Birth_Year TEXT, MAN_Function_Mailings TEXT, \
                    MAN_Street_Nr TEXT, MAN_City TEXT, MAN_ZIP TEXT, MAN_Country TEXT)"""
    DBTables.setdefault("Manager_Upload", sql_command)

    sql_command = """CREATE TABLE Role (`id` INTEGER UNIQUE, `name` TEXT UNIQUE, PRIMARY KEY(id) )"""
    DBTables.setdefault("Role", sql_command)

    sql_command = """CREATE TABLE TFIDF (`AID` TEXT, `Name` TEXT, `Business_Activity` TEXT, `Branch_Code_Main` TEXT, `Branch_Codes_Secondaries` TEXT, `Internet` TEXT,\
                PRIMARY KEY(AID))"""
    DBTables.setdefault("TFIDF", sql_command)

    sql_command = """CREATE TABLE Business_Activity_Upload (CrefoID TEXT, Business_Activity TEXT)"""
    DBTables.setdefault("Business_Activity_Upload", sql_command)

    sql_command = """CREATE TABLE Business_Activity (AID TEXT NOT NULL, Activity_Description_ROC TEXT, Last_Change TEXT)"""
    DBTables.setdefault("Business_Activity", sql_command)

    sql_command = """CREATE TABLE Company_Upload (Company TEXT NOT NULL, BvDId , CrefoID TEXT NOT NULL, \
                Street_Nr TEXT, ZIP TEXT, City TEXT, State TEXT, Country TEXT, Count_Shareholders INTEGER,  \
                Phone TEXT, URL TEXT, Email TEXT, WZ08_Main_Code TEXT,  BD_Main_Code TEXT, \
                Date_Incorporation TEXT, Legal_Form TEXT, Last_Turnover_K_Euro INTEGER, Last_Turnover_Year INTEGER,\
                Last_Count_Employees INTEGER, Last_Count_Employees_Year INTEGER, \
                ROC_number TEXT, UID TEXT, Count_Subsidiaries INTEGER, Count_Group_Companies\
                INTEGER, Comparison_Group_Name TEXT, Comparison_Group_Description TEXT, Comparison_Group_Size INTEGER, \
                GKZ_Street INTEGER, GKZ_X INTEGER, GKZ_Y INTEGER) """
    DBTables.setdefault("Company_Upload", sql_command)

    sql_command = """CREATE TABLE User (`id` INTEGER UNIQUE, `email` TEXT UNIQUE, `username` TEXT UNIQUE,\
                `password_hash` TEXT, `is_admin` INTEGER, PRIMARY KEY(id))"""
    DBTables.setdefault("User", sql_command)

    sql_command = """CREATE TABLE Vergleichsgruppe (Vergleichsgruppe TEXT NOT NULL, V_Beschreibung TEXT, Datum DATETIME )"""
    DBTables.setdefault("Vergleichsgruppe", sql_command)

    sql_command = """CREATE TABLE Websites (`AID` TEXT, `Content` TEXT, `Weblink` TEXT)"""
    DBTables.setdefault("Websites", sql_command)

    sql_command = """CREATE TABLE Salesforce_Kontakte (`ID` TEXT, `AID__c` TEXT, `LastName` TEXT, 'AccountId' TEXT, \
                'OwnerId' TEXT, 'ATE_Funktion_Mailing__c' TEXT, 'ATE_Funktion__c' TEXT, 'Anteile__c' TEXT, \
                'Ausbildung__c' TEXT, 'Email' TEXT, 'Fax' TEXT, 'FirstName' TEXT, 'Geburtsjahr__c' TEXT, \
                'Geschlecht__c' TEXT, 'Kommunikationssperre__c' TEXT, 'MAN_Funktion_Mailing__c' TEXT, \
                'MailingAdress_city' TEXT, 'MailingAdress_country' TEXT, 'MailingAdress_countryCode' TEXT, \
                'MailingAddress_postalCode' TEXT, 'MailingAddress_state' TEXT, 'MailingAddress_street' TEXT, \
                'Mailingsperre__c' TEXT, 'Manager_Funktion__c' TEXT, 'MobilePhone' TEXT, 'Name' TEXT, \
                'OtherAddress_city' TEXT, 'OtherAdress_country' TEXT, 'OtherAddress_countryCode' TEXT, \
                'OtherAddress_postalCode' TEXT, 'OtherAddress_state' TEXT, 'OtherAddress_street' TEXT, \
                'Phone' TEXT, 'Position__c' TEXT, 'Salutation' TEXT, 'Sperrgrund_Kommunikationssperre__c' TEXT, \
                'Title' TEXT, 'Vertrauliche_E_Mail__c' TEXT, 'Vertrauliche_Privatnummer__c' TEXT, 'AccountTyp__c' TEXT)"""
    DBTables.setdefault("Salesforce_Kontakte", sql_command)

    sql_command = """CREATE TABLE Salesforce_Accounts (`ID` TEXT, `AID__c` TEXT, `Name` TEXT, 'OwnerId' TEXT, \
                'Ablaufdatum_der_Reservierung__c' TEXT, 'Accounttyp__c' TEXT, 'Adressprovider__c' TEXT, 'Adresszusatz__c' TEXT, \
                'Aktiv__c' TEXT, 'Anzahl_der_Gesellschaften__c' TEXT, 'Anzahl_der_Gruppenunternehmen__c' TEXT, \
                'Anzahl_der_Tochtergesellschaften__c' TEXT, 'Avandil_Branche__c' TEXT, 'BD_Hauptcode__c' TEXT, \
                'BD_Nebencode__c' TEXT, 'BillingAddress_city' TEXT, 'BillingAddress_country' TEXT, \
                'BillingAddress_countryCode' TEXT, 'BillingAddress_postalCode' TEXT, 'BillingAddress_state' TEXT, \
                'BillingAddress_street' TEXT, 'Description' TEXT, 'E_Mail__c' TEXT, 'Eigentumsadresse__c' TEXT, \
                'Erfassungsdatum__c' TEXT, 'Fax' TEXT, 'Financials_Provider__c' TEXT, \
                'GKZ_Strasse__c' TEXT, 'GKZ_X__c' TEXT, 'GKZ_Y__c' TEXT, 'Gruendungsjahr__c' TEXT, \
                'Landkreis__c' TEXT, 'Mitarbeiter_laut_Provider_2012__c' TEXT, \
                'Mitarbeiter_laut_Provider_2013__c' TEXT, 'Mitarbeiter_laut_Provider_2014__c' TEXT, \
                'Mitarbeiter_laut_Provider_2015__c' TEXT, 'Mitarbeiter_laut_Provider_2016__c' TEXT, \
                'ParentId' TEXT, 'Phone' TEXT, 'Rechtsform__c' TEXT, 'Reserviert__c' TEXT, \
                'Reserviert_von_PLU__c' TEXT, 'ShippingAddress_city' TEXT, \
                'ShippingAddress_country' TEXT, 'ShippingAddress_countryCode' TEXT, 'ShippingAddress.postalCode' TEXT, \
                'ShippingAddress_state' TEXT, 'ShippingAddress_street' TEXT, 'Sperrgrund__c' TEXT, \
                'Taetigkeitsbeschreibung__c' TEXT, 'Telefon_2__c' TEXT, 'UID__c' TEXT, 'Umsatz_laut_Provider_2012__c' TEXT, \
                'Umsatz_laut_Provider_2013__c' TEXT, 'Umsatz_laut_Provider_2014__c' TEXT, 'Umsatz_laut_Provider_2015__c' TEXT, \
                'Umsatz_laut_Provider_2016__c' TEXT, 'Vergleichsgruppe_Code__c' TEXT, 'Vergleichsgruppe_Size__c' TEXT, \
                'WZ_08_Hauptcode__c' TEXT, 'WZ_08_Nebencode__c' TEXT, 'Website' TEXT, 'letzte_anzahl_mitarbeiter_laut_avandil__c' TEXT, \
                'letzte_anzahl_mitarbeiter_laut_provider__c' TEXT, 'letzter_umsatz_laut_avandil__c' TEXT, \
                'letzter_umsatz_laut_provider__c' TEXT)"""
    DBTables.setdefault("Salesforce_Accounts", sql_command)

    return DBTables

def DBIndex():
    DBIndex = {}

    sql_command = "CREATE INDEX idx_ATE_FIRMA ON ATE (`AID_Company` ASC)"
    DBIndex.setdefault("idx_ATE_Firma", sql_command)

    sql_command = "CREATE INDEX idx_ATE_PERSON ON ATE (`AID_SHH` ASC)"
    DBIndex.setdefault("idx_ATE_Person", sql_command)

    sql_command = "CREATE INDEX `idx_ATE_DATUM` ON `ATE` ( `Last_Change` )"
    DBIndex.setdefault("idx_ATE_DATUM", sql_command)

    sql_command = "CREATE UNIQUE INDEX idx_ATE_unique ON ATE (`AID_Company` ASC, `AID_SHH` ASC)"
    DBIndex.setdefault("idx_ATE_FIRMA_UNIQUE", sql_command)

    sql_command = "CREATE INDEX idx_Adressen ON Address (`AID` ASC)"
    DBIndex.setdefault("idx_Adressen", sql_command)

    sql_command = "CREATE INDEX idx_Financials ON Financials (`AID` ASC)"
    DBIndex.setdefault("idx_Financials", sql_command)

    sql_command = "CREATE UNIQUE INDEX `idx_Financials_unique` ON `Financials` (`AID` ASC, `Year` ASC, `Fin_Value_Type` ASC)"
    DBIndex.setdefault("idx_Financials_unique", sql_command)

    sql_command = "CREATE INDEX idx_Websites ON Websites (`AID` ASC)"
    DBIndex.setdefault("idx_Websites", sql_command)

    sql_command = "CREATE INDEX idx_MAN_FIRMA ON Manager (`AID_Company` ASC)"
    DBIndex.setdefault("idx_MAN_Firma", sql_command)

    sql_command = "CREATE INDEX idx_MAN_PERSON ON Manager (`AID_MAN` ASC)"
    DBIndex.setdefault("idx_MAN_Person", sql_command)

    sql_command = "CREATE INDEX idx_Branchencodes ON Branch_Codes (`AID` ASC)"
    DBIndex.setdefault("idx_Branchencodes", sql_command)

    sql_command = "CREATE INDEX idx_Entity ON Entity (`AID` ASC)"
    DBIndex.setdefault("idx_Entity", sql_command)

    sql_command = "CREATE INDEX idx_Taetigkeit ON Business_Activity (`AID` ASC)"
    DBIndex.setdefault("idx_Taetigkeit", sql_command)

    sql_command = "CREATE INDEX idx_TFIDF ON TFIDF (`AID` ASC)"
    DBIndex.setdefault("idx_TFIDF", sql_command)

    sql_command = "CREATE INDEX `idx_Salesforce_Accounts` ON `Salesforce_Accounts` ( `AID__c` ASC, `AID__c` ASC )"
    DBIndex.setdefault("idx_Salesforce_Accounts", sql_command)

    sql_command = "CREATE INDEX `idx_Salesforce_Kontakte` ON `Salesforce_Kontakte` ( `AccountId` ASC, `AID__c` ASC )"
    DBIndex.setdefault("idx_Salesforce_Kontakte", sql_command)

    return DBIndex

"""----------------------------
Funktion: Record_Dict
Gibt ein Dict mit den Feldnamen zurück,
Diese Feldnamen werden verwendet um Datensätze auszuwählen die in die SQL-Datenbank geschrieben zu werden
----------------------------"""

"""
"Contact_Type" : "", "BDPS" : "", "BD_Beschreibung" : "", "Einschluss" : "", "Ausschluss" : "", \
"Quelle" : "", "U_Datum" : "", \
"""

import datetime
def Record_Dict(db_table="Alles"):
    RDict = {}
    date = str(datetime.datetime.now())
    today = date[:10]
    #    today = date[8:10] + "." + date[5:7] + "." + date[:4]
    #    today = "31.12.2016"
    if db_table == "Entity":
        RDict.setdefault("AID", "")
        RDict.setdefault("Entity_Last_Name", "")
        RDict.setdefault("Entity_First_Name", "")
        RDict.setdefault("Address_Ownership", "1")
        RDict.setdefault("Entity_Type", "Firma")
        RDict.setdefault("Legal_Form", "")
        RDict.setdefault("Salutation", "")
        RDict.setdefault("Titel", "")
        RDict.setdefault("Education", "")
        RDict.setdefault("Gender", "")
        RDict.setdefault("Birth_Year", "")
        RDict.setdefault("Comparison_Group_Code", "")
        RDict.setdefault("Comparison_Group_Size", "")
        RDict.setdefault("Count_Shareholders", "")
        RDict.setdefault("Count_Group_Companies", "")
        RDict.setdefault("Count_Subsidiaries", "")
        RDict.setdefault("UID", "")
        RDict.setdefault("Reserved", "0")
        RDict.setdefault("Expiry_Date", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Branch_Code":
        RDict.setdefault("AID", "")
        RDict.setdefault("Branch_Code", "")
        RDict.setdefault("Branch_Rank_Main_Sec", "")
        RDict.setdefault("Source", "BvD")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Address":
        RDict.setdefault("AID", "")
        RDict.setdefault("AD_Address_ID", "99")
        RDict.setdefault("Address_Addendum", "")
        RDict.setdefault("Street_Nr", "")
        RDict.setdefault("ZIP", "")
        RDict.setdefault("City", "")
        RDict.setdefault("County", "")
        RDict.setdefault("State", "")
        RDict.setdefault("Country", "")
        RDict.setdefault("GKZ_X", "")
        RDict.setdefault("GKZ_Y", "")
        RDict.setdefault("GKZ_Street", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Contact":
        RDict.setdefault("AID", "")
        RDict.setdefault("Contact_Type", "")
        RDict.setdefault("Contact_Value", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "ATE":
        RDict.setdefault("AID_Company", "")
        RDict.setdefault("AID_SHH", "")
        RDict.setdefault("SHH_Function", "")
        RDict.setdefault("SHH_Shares_Owned", 0)
        RDict.setdefault("SHH_Position", "")
        RDict.setdefault("SHH_Function_Mailings", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Manager":
        RDict.setdefault("AID_Company", "")
        RDict.setdefault("AID_MAN", "")
        RDict.setdefault("MAN_Function", "")
        RDict.setdefault("MAN_Position", "")
        RDict.setdefault("MAN_Function_Mailings", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Taetigkeit":
        RDict.setdefault("AID", "")
        RDict.setdefault("Activity_Description_ROC", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Financials_Upload":
        RDict.setdefault("CrefoID", "")
        RDict.setdefault("Year", "")
        RDict.setdefault("Type", "")
        RDict.setdefault("Value", "")
    elif db_table == "Financials":
        RDict.setdefault("AID", "")
        RDict.setdefault("Year", "")
        RDict.setdefault("Fin_Value_Type", "")
        RDict.setdefault("Last_Change", today)
        RDict.setdefault("Fin_Value", "")
    elif db_table == "Salesforce_Kontakte":
        RDict.setdefault("ID", "")
        RDict.setdefault("AID__c", "")
        RDict.setdefault("LastName", "")
        RDict.setdefault("AccountId", "")
        RDict.setdefault("OwnerId", "")
        RDict.setdefault("ATE_Funktion_Mailing__c", "")
        RDict.setdefault("ATE_Funktion__c", "")
        RDict.setdefault("Anteile__c", "")
        RDict.setdefault("Ausbildung__c", "")
        RDict.setdefault("Email", "")
        RDict.setdefault("Fax", "")
        RDict.setdefault("FirstName", "")
        RDict.setdefault("Geburtsjahr__c", "")
        RDict.setdefault("Geschlecht__c", "")
        RDict.setdefault("Kommunikationssperre__c", "")
        RDict.setdefault("MAN_Funktion_Mailing__c", "")
        RDict.setdefault("MailingAdress_city", "")
        RDict.setdefault("MailingAdress_country", "")
        RDict.setdefault("MailingAdress_countryCode", "")
        RDict.setdefault("MailingAdress_postalCode", "")
        RDict.setdefault("MailingAdress_state", "")
        RDict.setdefault("MailingAdress_street", "")
        RDict.setdefault("Mailingsperre__c", "")
        RDict.setdefault("Manager_Funktion__c", "")
        RDict.setdefault("MobilePhone", "")
        RDict.setdefault("Name", "")
        RDict.setdefault("MAN_Funktion_Mailing__c", "")
        RDict.setdefault("OtherAdress_city", "")
        RDict.setdefault("OtherAdress_country", "")
        RDict.setdefault("OtherAdress_countryCode", "")
        RDict.setdefault("OtherAdress_postalCode", "")
        RDict.setdefault("OtherAdress_state", "")
        RDict.setdefault("OtherAdress_street", "")
        RDict.setdefault("Phone", "")
        RDict.setdefault("Position__c", "")
        RDict.setdefault("Salutation", "")
        RDict.setdefault("Sperrgrund_Kommunikationssperre__c", "")
        RDict.setdefault("Title", "")
        RDict.setdefault("Vertrauliche_E_Mail__c", "")
        RDict.setdefault("Vertrauliche_Privatnummer__c", "")
        RDict.setdefault("AccountTyp__c", "")
    elif db_table == "Salesforce_Accounts":
        RDict.setdefault("Id", "")
        RDict.setdefault("AID__c", "")
        RDict.setdefault("Name", "")
        RDict.setdefault("OwnerId", "")
        RDict.setdefault("Ablaufdatum_der_Reservierung__c", "")
        RDict.setdefault("Accounttyp__c", "")
        RDict.setdefault("Adressprovider__c", "")
        RDict.setdefault("Adresszusatz__c", "")
        RDict.setdefault("Aktiv__c", "")
        RDict.setdefault("Anzahl_der_Gesellschaften__c", "")
        RDict.setdefault("Anzahl_der_Gruppenunternehmen__c", "")
        RDict.setdefault("Anzahl_der_Tochtergesellschaften__c", "")
        RDict.setdefault("Avandil_Branche__c", "")
        RDict.setdefault("BD_Hauptcode__c", "")
        RDict.setdefault("BD_Nebencode__c", "")
        RDict.setdefault("BillingAddress_city", "")
        RDict.setdefault("BillingAddress_country", "")
        RDict.setdefault("BillingAddress_countryCode", "")
        RDict.setdefault("BillingAddress_postalCode", "")
        RDict.setdefault("BillingAddress_state", "")
        RDict.setdefault("BillingAddress_street", "")
        RDict.setdefault("Description", "")
        RDict.setdefault("E_Mail__c", "")
        RDict.setdefault("Eigentumsadresse__c", "")
        RDict.setdefault("Erfassungsdatum__c", "")
        RDict.setdefault("Fax", "")
        RDict.setdefault("Financials_Provider__c", "")
        RDict.setdefault("GKZ_Strasse__c", "")
        RDict.setdefault("GKZ_X__c", "")
        RDict.setdefault("GKZ_Y__c", "")
        RDict.setdefault("Gruendungsjahr__c", "")
        RDict.setdefault("Landkreis__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2012__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2013__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2014__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2015__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2016__c", "")
        RDict.setdefault("ParentId", "")
        RDict.setdefault("Phone", "")
        RDict.setdefault("Rechtsform__c", "")
        RDict.setdefault("Reserviert__c", "")
        RDict.setdefault("Reserviert_von_PLU__c", "")
        RDict.setdefault("ShippingAddress_city", "")
        RDict.setdefault("ShippingAddress_country", "")
        RDict.setdefault("ShippingAddress_countryCode", "")
        RDict.setdefault("ShippingAddress_postalCode", "")
        RDict.setdefault("ShippingAddress_state", "")
        RDict.setdefault("ShippingAddress_street", "")
        RDict.setdefault("Sperrgrund__c", "")
        RDict.setdefault("Taetigkeitsbeschreibung__c", "")
        RDict.setdefault("Telefon_2__c", "")
        RDict.setdefault("UID__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2012__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2013__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2014__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2015__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2016__c", "")
        RDict.setdefault("Vergleichsgruppe_Code__c", "")
        RDict.setdefault("Vergleichsgruppe_Size__c", "")
        RDict.setdefault("WZ_08_Hauptcode__c", "")
        RDict.setdefault("WZ_08_Nebencode__c", "")
        RDict.setdefault("Website", "")
        RDict.setdefault("letzte_anzahl_mitarbeiter_laut_avandil__c", "")
        RDict.setdefault("letzte_anzahl_mitarbeiter_laut_provider__c", "")
        RDict.setdefault("letzter_umsatz_laut_avandil__c", "")
        RDict.setdefault("letzter_umsatz_laut_provider__c", "")
    return RDict




def Textdatei_konvertieren(target, filename="", db_file=""):
    if target == "Database":
        # Get database file name to insert data
        if not db_file:
            db_file=current_app.config["DBDIR"]
        datenbank = sqlite3.connect(db_file)
        cursor = datenbank.cursor()

    Pfad = os.path.dirname(filename)  # Zerlegen des File names in Pfad, Dateiname und Endung
    Datei = os.path.basename(filename)
    Datei = os.path.splitext(Datei)
    Datei = Datei[0]
    tabelle = ""
    if "Liste 1" in Datei:
        tabelle = "Company_Upload"
        workbook = "Liste 1"
    elif "Liste 2" in Datei:
        tabelle = "Branch_Upload"
        workbook = "Liste 2"
    elif "Liste 3" in Datei:
        tabelle = "Shareholder_Upload"
        workbook = "Liste 3"
    elif "Liste 4" in Datei:
        tabelle = "Manager_Upload"
        workbook = "Liste 4"
    elif "Liste 6" in Datei:
        tabelle = "Business_Activity_Upload"
        workbook = "Liste 6"
    elif "Liste 7" in Datei:
        tabelle = "Financials_Upload"
        workbook = "Liste 7"
    elif target == "from_screen":
        workbook = "Manual upload"
    else:
        Ausgabe = "\nName der Liste nicht im Dateinamen enhalten\nBitte Namensgebung prüfen\nName muss 'Liste x' enthalten ! "
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] =Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="Datenübernahme : "+ Ausgabe
        # text.insert(END, Ausgabe)
        # messagebox.showinfo("Datenübernahme", Ausgabe)
        # return
    if sys.platform == 'linux':
        backslash = "/"
    else:
        backslash = "\\"
    if target == "from_screen":
        # Seitentext = text.get("1.0", "end")
        # Adressliste_dirty = Seite_parsen(text, Seitentext)
    #        Adressliste_dirty = text.get("1.0","end").splitlines()
        pass
    else:

        # text.insert(INSERT, "Lese Datei: " + filename + "\n\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="Lese Datei: " + filename + "\n\n"
        Adressliste_dirty = Datei_einlesen(Datei, Pfad + backslash)  # Einlesen der Datei
        if Adressliste_dirty == "Error":
            return
    # text.insert(INSERT, "Länge Adressliste: " + str(len(Adressliste_dirty)) + " Zeilen\n\n")
    # text.update_idletasks()
    current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="Länge Adressliste: " + str(len(Adressliste_dirty)) + " Zeilen\n\n"
    if workbook == "Liste 6":
        Datenbank = []
        Namensliste = []
        for row in Adressliste_dirty:
            values = row.split(";", 1)
            values[0] = values[0].strip('"')
            values[1] = values[1].strip('"')
            Datenbank.append(values)
            Namensliste.append(
                values[0])  # nur für die Ausgabe des Zählers am Ende der Routine - ansonsten keine Bedeutung
    else:
        record_header = Adressliste_dirty[0]
        Datenbank = []
        for record_dirty in Adressliste_dirty:
            if target == "from_screen":
                record = record_dirty
            else:
                record = record_dirty.split(";")
            if record_dirty == record_header:
                Feldliste = record
                continue
            elif record[0] == "\n":
                continue
            elif record[0][0:5] == "Seite":
                continue
            Datenbank.append(record)

    if target == "Excel" or target == "from_screen":

        '''     Spreadsheet = Pfad + "/" + Datei + ".xlsx"
        text.insert("end", "Erzeuge Excel-Tabelle\n\n")
        text.update_idletasks()
        Success = Create_Spreadsheet(Datenbank, Spreadsheet, Feldliste, workbook)
        if Success == 1:
            ausgabe = "Datensätze: " + str(len(Datenbank)) + "\n" + "\n\nExcel Datei erzeugt: " + Spreadsheet
            text.insert(INSERT, "========== Excel-Tabelle erzeugt ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "\n\n====================" + "\n")
        elif Success == 13:
            ausgabe = "Die Excel-Tabelle konnte nicht erzeugt werden\n\nEvtl. ist die Datei bereits geöffnet"
            text.insert(INSERT, "========== Exycel-Tabelle fehlgeschlagen ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "\n\n====================" + "\n")
        else:
            ausgabe = "Die Excel-Tabelle konnte nicht erzeugt werden\n\nGrund ist nicht bekannt"
            text.insert(INSERT, "========== Exycel-Tabelle fehlgeschlagen ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "====================" + "\n")
        messagebox.showinfo("Datenübernahme", ausgabe)'''
        pass

    elif target == "Database":
        tables = DBTables()
        sql_create = tables[tabelle]
        try:
            cursor.execute(sql_create)
        except:
            pass
        Index = DBIndex()
        if tabelle in Index:
            try:
                sql_command = Index[tabelle]
                cursor.execute(sql_command)
               # text.insert("end", "\nIndex in Datenbank \n" + db_file + "\n\nerzeugt. \n")
                current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nIndex in Datenbank \n" + db_file + "\n\nerzeugt. \n"
            except:
                current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="INDEX ERZEUGEN: "+ "Index in Tabelle " + tabelle + " konnte nicht erzeugt werden."
                pass
                #messagebox.showinfo("INDEX ERZEUGEN", "Index in Tabelle " + tabelle + " konnte nicht erzeugt werden.")
                #text.insert("end", (traceback.format_exc() + "\n"))
        if tabelle == "Company_Upload":
            keys = ['Company', 'BvDId', 'CrefoID', 'Street_Nr', 'ZIP', 'City', 'State', 'Country', \
                    'Count_Shareholders', 'Phone', 'URL', 'Email', 'WZ08_Main_Code', \
                    'BD_Main_Code', 'Date_Incorporation', 'Legal_Form', 'Last_Turnover_K_Euro',
                    'Last_Turnover_Year', \
                    'Last_Count_Employees', 'Last_Count_Employees_Year', 'ROC','ROC_number', \
                    'UID', 'Count_Subsidiaries', 'Count_Group_Companies',
                    'Comparison_Group_Name', \
                    'Comparison_Group_Description', 'Comparison_Group_Size', 'GKZ_Street', 'GKZ_X', \
                    'GKZ_Y']
            sql_insert = "INSERT INTO Company_Upload VALUES (:Company, :BvDId, :CrefoID, :Street_Nr, :ZIP, :City, \
                       :State, :Country, :Count_Shareholders, :Phone, :URL, :Email, :WZ08_Main_Code, \
                       :BD_Main_Code, :Date_Incorporation, :Legal_Form, :Last_Turnover_K_Euro, :Last_Turnover_Year,\
                       :Last_Count_Employees, :Last_Count_Employees_Year, :ROC, :ROC_number, \
                       :UID, :Count_Subsidiaries, :Count_Group_Companies, :Comparison_Group_Name, \
                       :Comparison_Group_Description, :Comparison_Group_Size, :GKZ_Street, :GKZ_X, \
                       :GKZ_Y)"
        elif tabelle == "Branch_Upload":
            keys = ['Company', 'BvDId', 'CrefoID', 'WZ08_Main_Code', 'WZ08_Secondary_Code', \
                    'BD_Main_Code', 'BD_Secondary_Code']
            sql_insert = "INSERT INTO Branch_Upload VALUES (:Company, :BvDId, :CrefoID, :WZ08_Main_Code,\
                           :WZ08_Secondary_Code, :BD_Main_Code, :BD_Secondary_Code)"
        elif tabelle == "Shareholder_Upload":
            keys = ['Company', 'BvDId', 'CrefoID', 'ATE_Crefo', 'SHH_Type', 'SHH_Title', 'SHH_Salutation', \
                    'SHH_Full_Name', 'ATE_Nachname', 'ATE_Vorname', 'ATE_Mittelname', 'SHH_Gender', 'SHH_Function', \
                    'SHH_Birth_Year', 'SHH_Function_Mailings', 'SHH_Street_Nr', 'SHH_City', 'ATE_Adresszusatz', 'SSH_ZIP', \
                    'SHH_Country', 'SHH_Shares_Owned']
            sql_insert = "INSERT INTO Shareholder_Upload VALUES (:Company, :BvDId, :CrefoID, :ATE_Crefo, :SHH_Type, \
                       :SHH_Title, :SHH_Salutation, :SHH_Full_Name, :ATE_Nachname, :ATE_Vorname, :ATE_Mittelname, :SHH_Gender, \
                       :SHH_Function, :SHH_Birth_Year, :SHH_Function_Mailings, :SHH_Street_Nr, :SHH_City, :ATE_Adresszusatz, \
                       :SSH_ZIP, :SHH_Country, :SHH_Shares_Owned)"
        elif tabelle == "Manager_Upload":
            keys = ['Company', 'BvDId', 'CrefoID', 'MAN_Function_Mailings', 'MAN_Salutation', 'MAN_Titel',
                    'MAN_Nachname', \
                    'MAN_Vorname', 'MAN_Mittelname', 'MAN_Gender', 'MAN_Crefo', 'MAN_Type', \
                    'MAN_Birth_Year', 'MAN_Function', 'MAN_Street_Nr', 'MAN_City', 'MAN_ZIP', 'MAN_Country']
            sql_insert = "INSERT INTO Manager_Upload VALUES (:Company, :BvDId, :CrefoID, :MAN_Crefo, :MAN_Type, \
                       :MAN_Titel, :MAN_Salutation, :MAN_Nachname, :MAN_Vorname, :MAN_Mittelname, :MAN_Gender, \
                       :MAN_Function, :MAN_Birth_Year, :MAN_Function_Mailings, :MAN_Street_Nr, :MAN_City, :MAN_ZIP, :MAN_Country)"
        elif tabelle == "Business_Activity_Upload":
            keys = ['CrefoID', 'Business_Activity']
            sql_insert = "INSERT INTO Business_Activity_Upload VALUES (:CrefoID, :Business_Activity)"
        elif tabelle == "Financials_Upload":
            sql_insert = "INSERT INTO Financials_Upload VALUES (:CrefoID, :Year, :Type, :Value)"

        last_crefo = ""
        for i in range(len(Datenbank)):
            if tabelle == "Financials_Upload":
                values = Datenbank[i]
                Feldliste[Feldliste.index('Crefonummer')]='CrefoID'
                record = dict(zip(Feldliste, values))
                AID = record["CrefoID"]
                for typ, wert in record.items():
                    if typ == "Company" or typ == "BvD ID Nummer" or typ == "CrefoID" or wert == "n.v.":
                        continue
                    if "nv" in wert.replace('.', ''):
                        continue
                    fin_dict = Record_Dict("Financials_Upload")
                    fin_typ = "Mitarbeiter"
                    if "Turnover" in typ:
                        fin_typ = "Turnover"
                    fin_dict["CrefoID"] = AID
                    this_year = datetime.datetime.now().year
                    jahr = 0
                    jahr = ''.join(x for x in typ if x.isdigit())
                    if len(jahr) == 0 or int(jahr) < 2000 or int(jahr) > this_year:
                        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="Dateifehler : "+"Ungültige Jahreszahl in\n" + filename + " \nDatei wird nicht verarbeitet"
                        # messagebox.showinfo("Dateifehler",
                        #                     "Ungültige Jahreszahl in\n" + filename + " \nDatei wird nicht verarbeitet")
                        sql = "Delete from Financials_Upload"
                        cursor.execute(sql)
                        continue
                    fin_dict["Year"] = jahr
                    fin_dict["Type"] = fin_typ
                    try:
                        fin_dict["Value"] = int(wert.replace('.', ''))
                    except:
                        #                        print(i, wert)
                        pass

                    cursor.execute(sql_insert, fin_dict)
            else:
                values = Datenbank[i]
                record = dict(zip(keys, values))
                crefo_id = record["CrefoID"]
                if not crefo_id:
                    record["CrefoID"] = last_crefo
                    crefo_id = last_crefo
                    if not last_crefo:
                        continue
                record['ROC']=''
                cursor.execute(sql_insert, record)
                last_crefo = crefo_id
            print("Zeile ", format(i + 1, ",d").replace(",", "."), " von ",
                  format(len(Datenbank), ",d").replace(",", "."), " eingefügt")
        datenbank.commit()
        unternehmen_count = format(len(Datenbank), ",").replace(",", ".")
        record_count = format(len(Datenbank), ",").replace(",", ".")
        Ausgabe = "Anzahl Company: " + unternehmen_count + "\n\nAnzahl Datensätze: " + record_count
        Ausgabe = Ausgabe + "\n" + workbook + " wurde in die Datenbank eingefügt"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] =Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        # if showbox:
        #     messagebox.showinfo("Datenübernahme", Ausgabe)
    return




"""---------------------------------------------------
Function:   dict_factory
Purpose:    format sqlite rows to dictionary
Paramter:   Text-Objekt
-----------------------------------------------------"""


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


"""---------------------------------------------------
Function:   Upsert_Upload
Purpose:    Does an upsert of the xxx_Upload tables
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Upsert_Upload(db_file):
    datenbank = sqlite3.connect(db_file)
    datenbank.row_factory = dict_factory
    cursor_from = datenbank.cursor()
    cursor_to = datenbank.cursor()

    # Upload Table Entity
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Company_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
       # text.insert("end", "\nUpload der Entity-Daten ist gestartet....\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nUpload der Entity-Daten ist gestartet....\n"
        sql = "SELECT * from Company_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            if not row["BvDId"]:
                continue
            # Insert Entity data
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["CrefoID"]
            entity_dict["Entity_Last_Name"] = row["Company"]
            entity_dict["Legal_Form"] = row["Legal_Form"]
            entity_dict["Birth_Year"] = row["Date_Incorporation"][6:]
            entity_dict["Comparison_Group_Code"] = row["Comparison_Group_Name"]
            entity_dict["Comparison_Group_Size"] = row["Comparison_Group_Size"]
            entity_dict["Count_Shareholders"] = row["Count_Shareholders"]
            entity_dict["Count_Group_Companies"] = row["Count_Group_Companies"]
            entity_dict["Count_Subsidiaries"] = row["Count_Subsidiaries"]
            entity_dict["UID"] = row["UID"]

            sql_delete = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Entity (AID, Entity_Last_Name, Entity_First_Name, Address_Ownership,\
                        Entity_Type, Legal_Form, Salutation, Titel, Education, Gender, Birth_Year, \
                        Comparison_Group_Code, Comparison_Group_Size, Count_Shareholders, Count_Group_Companies, \
                        Count_Subsidiaries, UID, Reserved, Expiry_Date, Last_Change)" \
                         " VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, :Expiry_Date, :Last_Change)"
            cursor_to.execute(sql_insert, entity_dict)

            # Insert Adress data
            entity_dict = Record_Dict("Address")
            entity_dict["AID"] = row["CrefoID"]
            entity_dict["Street_Nr"] = row["Street_Nr"]
            entity_dict["ZIP"] = row["ZIP"]
            entity_dict["City"] = row["City"]
            entity_dict["State"] = row["State"]
            entity_dict["Country"] = row["Country"]
            entity_dict["GKZ_X"] = row["GKZ_X"]
            entity_dict["GKZ_Y"] = row["GKZ_Y"]
            entity_dict["GKZ_Street"] = row["GKZ_Street"]
            sql_delete = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Address (AID, AD_Address_ID, Address_Addendum, Street_Nr,\
                        ZIP, City, County, State, Country, GKZ_X, GKZ_Y, \
                        GKZ_Street, Last_Change)  VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            cursor_to.execute(sql_insert, entity_dict)

            # Insert Contact data
            field_list = ["Phone", "URL", "Email"]
            code_dict = {}
            code_dict.setdefault("Phone", "Phone")
            code_dict.setdefault("URL", "Internet")
            code_dict.setdefault("Email", "eMail")
            record_list = []
            for key in field_list:
                if row[key]:
                    entity_dict = Record_Dict("Contact")
                    entity_dict["AID"] = row["CrefoID"]
                    entity_dict["Contact_Type"] = code_dict[key]
                    entity_dict["Contact_Value"] = row[key]
                    record_list.append(entity_dict)
            sql_delete = "DELETE FROM Contact WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            #sql_insert = "INSERT INTO Contact (AID, Contact_Type, Contact_Value, Last_Change) VALUES (:AID, :Contact_Type, :Contact_Value, :Last_Change)"
            sql_insert = "INSERT INTO Contact (AID, Contact_Type, Contact_Value) VALUES (:AID, :Contact_Type, :Contact_Value)"
            for record in record_list:
                cursor_to.execute(sql_insert, record)

            i += 1
            print(i, ": AID: " + entity_dict["AID"] + " updated. \n")
        sql_delete = "DELETE from Company_Upload"
        cursor_from.execute(sql_delete)

        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Company wurden eingefügt."
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] =Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\n" + "====================" + "\n"

    # Upload Table Branch_Codes
    field_list = ["WZ08_Main_Code", "WZ08_Secondary_Code", "BD_Main_Code", "BD_Secondary_Code"]
    code_dict = {}
    code_dict.setdefault("WZ08_Main_Code", "WZ08H")
    code_dict.setdefault("WZ08_Secondary_Code", "WZ08N")
    code_dict.setdefault("BD_Main_Code", "BDH")
    code_dict.setdefault("BD_Secondary_Code", "BDN")
    last_aid = ""

    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Branch_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        #text.insert("end", "\nUpload der Branch_Codes ist gestartet....\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nUpload der Branch_Codes ist gestartet....\n"
        sql = "SELECT * from Branch_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            record_list = []
            for code in field_list:
                if row[code]:
                    entity_dict = Record_Dict("Branch_Code")
                    entity_dict["AID"] = row["CrefoID"]
                    entity_dict["Branch_Code"] = row[code]
                    entity_dict["Branch_Rank_Main_Sec"] = code_dict[code]
                    record_list.append(entity_dict)

            if entity_dict["AID"] != last_aid:
                sql_delete = "DELETE FROM Branch_Codes WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_delete)
            for record in record_list:
                sql_insert = "INSERT INTO Branch_Codes (AID, Branch_Code, Branch_Rank_Main_Sec, Source, Last_Change) VALUES (:AID, :Branch_Code, :Branch_Rank_Main_Sec, :Source, :Last_Change)"
                cursor_to.execute(sql_insert, record)
                last_aid = record["AID"]
                i += 1
                print(format(i, ",d").replace(",", "."), ": AID: " + entity_dict["AID"] + " updated. \n")
        sql_delete = "DELETE from Branch_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Branchendefinitionen wurden eingefügt."
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"

    # Upload data from ATE: Entity and ATE need to be updated
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Shareholder_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        #text.insert("end", "\nUpload der ATE-Daten ist gestartet....\n")
        # Delete records from ATE table which will be updated through Shareholder_Upload
        sql = "SELECT DISTINCT CrefoID from Shareholder_Upload"
        cursor_from.execute(sql)
        for row in cursor_from:
            sql_delete_ATE = "DELETE FROM Shareholder WHERE AID_Company = '" + row["CrefoID"] + "'"
            cursor_to.execute(sql_delete_ATE)

        sql = "SELECT * from Shareholder_Upload"
        cursor_from.execute(sql)
        i = 0

        for row in cursor_from:
            # Read the Entity data from the Shareholder_Upload Record
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["SHH_CrefoID"]
            entity_dict["Entity_Last_Name"] = row["SHH_Last_Name"]
            entity_dict["Entity_First_Name"] = row["SHH_First_Name"]
            entity_dict["Entity_Type"] = row["SHH_Type"]
            entity_dict["Titel"] = row["SHH_Title"]
            entity_dict["Salutation"] = row["SHH_Salutation"]
            entity_dict["Gender"] = row["SHH_Gender"]
            entity_dict["Birth_Year"] = row["SHH_Birth_Year"]

            # Read the Address data from the Shareholder_Upload Record
            adresse_dict = Record_Dict("Address")
            adresse_dict["AID"] = row["SHH_CrefoID"]
            adresse_dict["Street_Nr"] = row["SHH_Street_Nr"]
            adresse_dict["ZIP"] = row["SHH_ZIP"]
            adresse_dict["City"] = row["SHH_City"]
            adresse_dict["Country"] = row["SHH_Country"]

            # Read the ATE data from the Shareholder_Upload Record
            ate_dict = Record_Dict("ATE")
            ate_dict["AID_Company"] = row["CrefoID"]
            ate_dict["AID_SHH"] = row["SHH_CrefoID"]
            ate_dict["SHH_Function"] = row["SHH_Function"]
            ate_dict["SHH_Shares_Owned"] = row["SHH_Shares_Owned"]
            if ate_dict["AID_SHH"] == "":
                ate_dict["AID_SHH"] = "0000000000"
                # if ATE has only BvDId but no Crefo, the data is not extractable (see i.e. 9370299738 in Markus)

            # If Entity_Type = "Company" neither Entity nor Address will be changed
            # Changes for Entity will only be through Liste 1 !

            sql_delete_entity = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_entity = "INSERT INTO Entity (AID, Entity_Last_Name, Entity_First_Name, Address_Ownership,\
                        Entity_Type, Legal_Form, Salutation, Titel, Education, Gender, Birth_Year, \
                        Comparison_Group_Code, Comparison_Group_Size, Count_Shareholders, Count_Group_Companies, \
                        Count_Subsidiaries, UID, Reserved, Expiry_Date, Last_Change)" \
                         "VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, \
                        :Expiry_Date, :Last_Change)"
            sql_delete_adresse = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_adresse = "INSERT INTO Address (AID, AD_Address_ID, Address_Addendum, Street_Nr,\
                        ZIP, City, County, State, Country, GKZ_X, GKZ_Y, \
                        GKZ_Street, Last_Change) VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            sql_delete_ate = "DELETE FROM Shareholder WHERE AID_Company = '" + ate_dict["AID_Company"] + "' AND AID_SHH = '" + \
                             ate_dict["AID_SHH"] + "'"
            sql_insert_ate = "INSERT INTO Shareholder (AID_Company, AID_SHH, SHH_Function, SHH_Shares_Owned,\
                        SHH_Function_Mailings, SHH_Position, Last_Change) VALUES (:AID_Company, :AID_SHH, :SHH_Function, :SHH_Shares_Owned,\
                        :SHH_Function_Mailings, :SHH_Position, :Last_Change)"
            cursor_to.execute(sql_delete_ate)
            cursor_to.execute(sql_insert_ate, ate_dict)
            if row["SHH_Type"] == "Person":
                cursor_to.execute(sql_delete_entity)
                cursor_to.execute(sql_insert_entity, entity_dict)
                cursor_to.execute(sql_delete_adresse)
                cursor_to.execute(sql_insert_adresse, adresse_dict)
            elif row["SHH_Type"] == "Company":
                # Wenn der Datensatz bereits existiert -> nichts Company
                # Wenn der Datensatz in Entity noch nicht existiert -> aufnehmen
                sql_exists_entity = "SELECT * FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_exists_entity)
                entity_exists = cursor_to.fetchone()
                if not entity_exists:
                    cursor_to.execute(sql_insert_entity, entity_dict)
                    cursor_to.execute(sql_insert_adresse, adresse_dict)
            print("ATE: ", format(i, ",d").replace(",", "."), "Datensätze verarbeitet. \n")
            i += 1

        sql_delete = "DELETE from Shareholder_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " ATE's wurden eingefügt."
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "========== DATENÜBERHNAHME ERFOLGT ===========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"

    # Upload data from MAN: Entity and ATE need to be updated
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Manager_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        #text.insert("end", "\nUpload der MAN-Daten ist gestartet....\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nUpload der MAN-Daten ist gestartet....\n"
        # Delete records from ATE table which will be updated through Shareholder_Upload
        sql = "SELECT DISTINCT CrefoID from Manager_Upload"
        cursor_from.execute(sql)
        for row in cursor_from:
            sql_delete_MAN = "DELETE FROM Manager WHERE AID_Company = '" + row["CrefoID"] + "'"
            cursor_to.execute(sql_delete_MAN)
        sql = "SELECT * from Manager_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            # If no MAN-CrefoID skip to next record
            if not row["MAN_CrefoID"]:
                continue
            # Read the Entity data from the Manager_Upload Record
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["MAN_CrefoID"]
            entity_dict["Entity_Last_Name"] = row["MAN_Last_Name"]
            entity_dict["Entity_First_Name"] = row["MAN_First_Name"]
            entity_dict["Entity_Type"] = row["MAN_Type"]
            entity_dict["Titel"] = row["MAN_Titel"]
            entity_dict["Salutation"] = row["MAN_Salutation"]
            entity_dict["Gender"] = row["MAN_Gender"]
            entity_dict["Birth_Year"] = row["MAN_Birth_Year"]

            # Read the Address data from the Manager_Upload Record
            adresse_dict = Record_Dict("Address")
            adresse_dict["AID"] = row["MAN_CrefoID"]
            adresse_dict["Street_Nr"] = row["MAN_Street_Nr"]
            adresse_dict["ZIP"] = row["MAN_ZIP"]
            adresse_dict["City"] = row["MAN_City"]
            adresse_dict["Country"] = row["MAN_Country"]

            # Read the MAN data from the Manager_Upload Record
            man_dict = Record_Dict("Manager")
            man_dict["AID_Company"] = row["CrefoID"]
            man_dict["AID_MAN"] = row["MAN_CrefoID"]
            man_dict["MAN_Function"] = row["MAN_Function"]
            man_dict["MAN_Function_Mailings"] = row["MAN_Function_Mailings"]
            if man_dict["AID_MAN"] == "":
                man_dict["MAN_ATE"] = "0000000000"

            # If Entity_Type = "Company" neither Entity nor Address will be changed
            # Changes for Entity will only be through Liste 1 !

            sql_delete_entity = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_entity = "INSERT INTO Entity (AID, Entity_Last_Name, Entity_First_Name, Address_Ownership,\
                        Entity_Type, Legal_Form, Salutation, Titel, Education, Gender, Birth_Year, \
                        Comparison_Group_Code, Comparison_Group_Size, Count_Shareholders, Count_Group_Companies, \
                        Count_Subsidiaries, UID, Reserved, \
                        Expiry_Date, Last_Change) VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, \
                        :Expiry_Date, :Last_Change)"
            sql_delete_adresse = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_adresse = "INSERT INTO Address (AID, AD_Address_ID, Address_Addendum, Street_Nr,\
                        ZIP, City, County, State, Country, GKZ_X, GKZ_Y, \
                        GKZ_Street, Last_Change) VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            sql_delete_man = "DELETE FROM Manager WHERE AID_Company = '" + man_dict["AID_Company"] + "' AND AID_MAN = '" + \
                             man_dict["AID_MAN"] + "'"
            sql_insert_man = "INSERT INTO Manager (AID_Company, AID_MAN, MAN_Function, \
                        MAN_Function_Mailings,MAN_Position,Last_Change) VALUES (:AID_Company, :AID_MAN, :MAN_Function, \
                        :MAN_Function_Mailings, :MAN_Position, :Last_Change)"
            cursor_to.execute(sql_delete_man)
            cursor_to.execute(sql_insert_man, man_dict)
            if row["MAN_Type"] == "Person":
                cursor_to.execute(sql_delete_entity)
                cursor_to.execute(sql_insert_entity, entity_dict)
                cursor_to.execute(sql_delete_adresse)
                cursor_to.execute(sql_insert_adresse, adresse_dict)
            elif row["MAN_Type"] == "Company":
                # Wenn der Datensatz bereits existiert -> nichts Company
                # Wenn der Datensatz in Entity noch nicht existiert -> aufnehmen
                sql_exists_entity = "SELECT * FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_exists_entity)
                entity_exists = cursor_to.fetchone()
                if not entity_exists:
                    cursor_to.execute(sql_insert_entity, entity_dict)
                    cursor_to.execute(sql_insert_adresse, adresse_dict)
            print("MAN: ", format(i, ",d").replace(",", "."), "Datensätze verarbeitet. \n")
            i += 1
        sql_delete = "DELETE from Manager_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " MAN's wurden eingefügt."
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"
############################################################
    # Upload data from TTG
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Business_Activity_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        #text.insert("end", "\nUpload der TTG-Daten ist gestartet....\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nUpload der TTG-Daten ist gestartet....\n"
        # Delete records from Business_Activity table which will be updated through Business_Activity_Upload
        sql = "SELECT DISTINCT CrefoID from Business_Activity_Upload"
        cursor_from.execute(sql)
        sql = "SELECT * from Business_Activity_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            ttg_dict = Record_Dict("Taetigkeit")
            ttg_dict["AID"] = row["CrefoID"]
            ttg_dict["Activity_Description_ROC"] = row["Business_Activity"]

            sql_delete = "DELETE FROM Business_Activity WHERE AID = '" + ttg_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Business_Activity (AID, Activity_Description_ROC, Last_Change) VALUES (:AID, :Activity_Description_ROC, :Last_Change)"
            cursor_to.execute(sql_insert, ttg_dict)

            i += 1
            print(format(i, ",d").replace(",", "."), ": AID: " + ttg_dict["AID"] + " updated. \n")

        sql_delete = "DELETE from Business_Activity_Upload"
        cursor_from.execute(sql_delete)

        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Tätigkeitsbeschreibungen wurden eingefügt.\n"
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"

    # Upload data from FIN
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Financials_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        #text.insert("end", "\nUpload der FIN-Daten ist gestartet....\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] ="\nUpload der FIN-Daten ist gestartet....\n"
        sql = "SELECT * from Financials_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            fin_dict = Record_Dict("Financials")
            fin_dict["AID"] = row["CrefoID"]
            fin_dict["Year"] = row["Year"]
            fin_dict["Fin_Value_Type"] = row["Type"]
            fin_dict["Fin_Value"] = row["Value"]

            sql_delete = "DELETE FROM Financials WHERE AID = '" + fin_dict["AID"] + "' AND Year = " + str(
                fin_dict["Year"]) + " AND Fin_Value_Type = '" + fin_dict["Fin_Value_Type"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Financials (AID, Year, Fin_Value_Type, Fin_Value, Last_Change) VALUES (:AID, :Year, :Fin_Value_Type, :Fin_Value, :Last_Change)"
            cursor_to.execute(sql_insert, fin_dict)

            i += 1
            print(format(i, ",d").replace(",", "."), ": AID: " + fin_dict["AID"] + " updated. \n")

        sql_delete = "DELETE from Financials_Upload"
        cursor_from.execute(sql_delete)

        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Finanzdaten wurden eingefügt.\n"
        # text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        # text.insert(INSERT, Ausgabe)
        # text.insert(INSERT, "\n" + "====================" + "\n")
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n"
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = Ausgabe
        current_app.config["AVAD_DATA_UPLOAD_OUTPUT"] = "\n" + "====================" + "\n"

    return True