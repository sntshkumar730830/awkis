# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 14:13:07 2015

@author: fw93
In der Version 0.98 wird die Markus-Übernahme der Activity_Description_ROC eingebunden - so daß nur noch eine einzige Programmdatei für AVAD existiert

In der Version 0.97 wird versucht, nach einem Marlus-Copy die Seite sofort zu parsen und bereits vorformatiert in eine txt-Datei zu schreiben
Dabei wird dann auch die Upsert-Funktion entsprechend angepasst
"""

from openpyxl.utils import get_column_letter  # , column_index_from_string
from tkinter import filedialog
from tkinter import messagebox
from tkinter import END, INSERT, Text, Scrollbar, RIGHT, Y

import csv, pdb
import datetime
# import numpy as np
import openpyxl
import os
# import pandas as pd
import pyautogui
import pyperclip
import re
import shelve
import sqlite3
# import sys
import time
import tkinter
import traceback

# import pdb
# import codecs

Version = "0.983"
XLSX_OFFSET = 1

"""---------------------------------------------------
Funktion:   Markus_Übernahme
Zweck:      Übernimmt Daten direkt aus einer geöffneten Markus-Abfrage
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Markus_Übernahme(text, markus_format="Standard"):
    global NxtScr, PageLoc, EmptyField, HomeDir
    text.delete(1.0, END)
    list_counter = re.compile("\\d*von \\d*\\.?\\d*")
    #    list_counter = re.compile("\\d*von \\d*")
    options = {}
    options['defaultextension'] = '.txt'
    options['filetypes'] = [('text files', '.txt'), ('All files', '.*')]
    options['initialdir'] = HomeDir
    options['initialfile'] = 'avad.txt'
    options['parent'] = root
    options['title'] = 'Daten speichern'
    FileName = filedialog.asksaveasfilename(**options)
    HomeDir = os.path.dirname(FileName)
    markusfile = open(FileName, "w")
    Inhalt = []
    Seite = ""
    pyperclip.copy("")
    pyautogui.hotkey("altleft", "tab")
    time.sleep(1)
    Seite = Get_Markus_Page()
    time.sleep(1)

    if markus_format == "Standard":
        Inhalt, First, Last = Copy_Markus_Page()
        if not Inhalt:
            text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
            pyautogui.moveTo(EmptyField)
            pyautogui.click(EmptyField)
            time.sleep(1)
            pyautogui.hotkey("altleft", "tab")
            return
        listeninhalt = list_counter.search(Inhalt)
        if not listeninhalt:
            text.insert("end", "Seitenzahlen Nicht gefunden\n")
            return
        else:
            _, Total = listeninhalt.group(0).split()
            text.insert("end", "Aktuelle Seite: " + str(Seite) + "\nSeiten insgesamt: " + str(Total) + "\n")
        inhalt_geparst = Seite_parsen(text, Inhalt)
        inhalt_to_datei(markusfile, inhalt_geparst)
        markusfile = open(FileName, "a")
        markusfile.write("\n")
        Neue_Seite = Seite
        for i in range(int(Seite), int(Total)):
            pyautogui.moveTo(EmptyField)
            pyautogui.click(EmptyField)  # Click in leeres Feld
            pyautogui.moveTo(NxtScr)
            pyautogui.click(NxtScr)
            time.sleep(1)
            for k in range(0, 25):
                Seite = Get_Markus_Page()
                time.sleep(1)
                if Seite != Neue_Seite:
                    break
            if Seite == Neue_Seite:
                text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
                pyautogui.moveTo(EmptyField)
                pyautogui.click(EmptyField)
                time.sleep(1)
                pyautogui.hotkey("altleft", "tab")
                return
            if not Seite:
                text.insert("end", "Fehler beim Einlesen von Seite: " + Seite + "\n")
                pyautogui.hotkey("altleft", "tab")
                return
            Neue_Seite = Seite
            Erster = First
            Letzter = Last
            for k in range(0, 25):
                Inhalt, First, Last = Copy_Markus_Page()
                if not Inhalt:
                    text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
                    text.update_idletasks
                    pyautogui.moveTo(EmptyField)
                    pyautogui.click(EmptyField)
                    time.sleep(1)
                    pyautogui.hotkey("altleft", "tab")
                    return
                if First == Letzter + 1:
                    break
                elif Erster == First:
                    time.sleep(1)
                else:
                    text.insert("end",
                                "MARKUS hat Seitenzahlen beim Einlesen übersprungen !!" + "\n" + "Einlesen gestoppt !!")
                    text.update_idletasks
                    pyautogui.moveTo(EmptyField)
                    pyautogui.click(EmptyField)
                    time.sleep(1)
                    pyautogui.hotkey("altleft", "tab")
                    return
            markusfile = open(FileName, "a")
            markusfile.write("Seite " + Seite + "\n")
            inhalt_geparst = Seite_parsen(text, Inhalt)
            inhalt_to_datei(markusfile, inhalt_geparst)
            markusfile = open(FileName, "a")
            markusfile.write("\n")
        text.insert("end", "Seite: " + Seite + " bearbeitet\n")
        markusfile.close()
        pyautogui.moveTo(EmptyField)
        pyautogui.click(EmptyField)
        time.sleep(1)
        pyautogui.hotkey("altleft", "tab")
        text.insert("end", "\n ===== Download beendet =====\n")
    elif markus_format == "Business_Activity":
        Inhalt = Copy_Markus_Page_TTG()
        if not Inhalt:
            pyautogui.moveTo(EmptyField)
            pyautogui.click(EmptyField)
            time.sleep(90)
            Inhalt = Copy_Markus_Page_TTG()
            if not Inhalt:
                text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
                text.update_idletasks
                pyautogui.moveTo(EmptyField)
                pyautogui.click(EmptyField)
                time.sleep(1)
                pyautogui.hotkey("altleft", "tab")
                return
        listeninhalt = list_counter.search(Inhalt)
        if not listeninhalt:
            text.insert("end", "Seitenzahlen Nicht gefunden\n")
            text.update_idletasks
            return
        else:
            _, Total = listeninhalt.group(0).split()
            print("Anzahl Seiten unformatiert: ", Total)
            Total = Total.replace(".", "")
            print("Anzahl Seiten: ", Total)
            text.insert("end", "Aktuelle Seite: " + str(Seite) + "\nSeiten insgesamt: " + str(Total) + "\n")
        Crefo, Business_Activity = Formatiere_Tätigkeitsbeschreibung(Inhalt)
        Speichertext = '"' + Crefo + '"' + ";" + '"' + Business_Activity + '"'
        markusfile.write(Speichertext)
        markusfile.write("\n")
        Seite = Seite.replace(".", "")
        Neue_Seite = Seite
        for i in range(int(Seite), int(Total) + 1):
            pyautogui.moveTo(EmptyField)
            pyautogui.click(EmptyField)  # Click in leeres Feld
            pyautogui.moveTo(NxtScr)
            pyautogui.click(NxtScr)
            time.sleep(1)
            for k in range(0, 25):
                Seite = Get_Markus_Page()
                time.sleep(2)
                if Seite != Neue_Seite:
                    break
            if Seite == Neue_Seite:
                text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
                text.update_idletasks
                pyautogui.moveTo(EmptyField)
                pyautogui.click(EmptyField)
                time.sleep(1)
                pyautogui.hotkey("altleft", "tab")
                return
            if not Seite:
                text.insert("end", "Fehler beim Einlesen von Seite: " + Seite + "\n")
                pyautogui.hotkey("altleft", "tab")
                return
            Neue_Seite = Seite
            for k in range(0, 25):
                Inhalt = Copy_Markus_Page_TTG()
                if not Inhalt:
                    text.insert("end", "Timeout beim Einlesen der Markus-Seiten")
                    text.update_idletasks
                    pyautogui.moveTo(EmptyField)
                    pyautogui.click(EmptyField)
                    time.sleep(1)
                    pyautogui.hotkey("altleft", "tab")
                    print("\nTimeout-Schleife: ", k, "\n")
                    return
                else:
                    break
            Crefo, Business_Activity = Formatiere_Tätigkeitsbeschreibung(Inhalt)
            Speichertext = '"' + Crefo + '"' + ";" + '"' + Business_Activity + '"'
            markusfile.write(Speichertext)
            markusfile.write("\n")
            markusfile.flush()
        text.insert("end", "Seite: " + Seite + " bearbeitet\n")
        markusfile.close()
        pyautogui.moveTo(EmptyField)
        pyautogui.click(EmptyField)
        time.sleep(1)
        pyautogui.hotkey("altleft", "tab")
    text.insert("end", "\n ===== Download beendet =====\n")
    return


"""---------------------------------------------------
Funktion:   Get_Markus_Page
Zweck:      Holt sich die aktuelle Seitenzahl der Markus-Seite
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Get_Markus_Page():
    global PageLoc, EmptyField

    for i in range(0, 25):
        pyautogui.moveTo(EmptyField)
        for k in range(0, 5):
            try:
                pyautogui.click(EmptyField)
                break
            except:
                time.sleep(1)
                continue
        pyautogui.moveTo(PageLoc)
        for k in range(0, 5):
            try:
                pyautogui.click(PageLoc)
                break
            except:
                time.sleep(1)
                continue
        pyautogui.hotkey("ctrl", "a")
        pyautogui.hotkey("ctrl", "c")
        Seite = pyperclip.paste()
        if Seite:
            if len(Seite) < 9:
                break
    pyperclip.copy("")
    #    print("Ausgelesene Seite: ", format(int(Seite), ",d").replace(",","."), "\n")
    return Seite


"""---------------------------------------------------
Funktion:   Copy_Markus_Page
Zweck:      Kopiert die aktuelle Markus-Seite
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Copy_Markus_Page():
    global PageLoc, EmptyField
    numbers = re.compile("\\n\\d*\\.\\t")
    for i in range(0, 25):
        pyautogui.moveTo(EmptyField)
        time.sleep(1)
        pyautogui.click(EmptyField)
        time.sleep(1)
        pyautogui.hotkey("ctrl", "a")
        pyautogui.hotkey("ctrl", "c")
        time.sleep(1)
        Seite = pyperclip.paste()
        nummern = []
        if Seite:
            time.sleep(1)
            nummern = numbers.findall(Seite)
            if len(nummern) == 0:
                continue
            else:
                break
    try:
        First = int(nummern[0].split()[0][:-1])
    except:
        print("===== FEHLERPROTOKOLL =====")
        print("Durchlauf i: ", i, "\n")
        print("Seite: \n", Seite)
        print("==========")
        print("Länge nummern: ", len(nummern))
        print("===== ENDE FEHLERPROTOKOLL =====")
        if len(nummern) == 0:
            return ("", "", "")
    Last = int(nummern[-1].split()[0][:-1])
    pyperclip.copy("")
    return Seite, First, Last


"""---------------------------------------------------
Funktion:   Copy_Markus_Page_TTG
Zweck:      Kopiert die aktuelle Markus-Seite
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Copy_Markus_Page_TTG():
    global PageLoc, EmptyField
    for i in range(0, 25):
        pyautogui.moveTo(EmptyField)
        time.sleep(1)
        pyautogui.click(EmptyField)
        time.sleep(1)
        pyautogui.hotkey("ctrl", "a")
        pyautogui.hotkey("ctrl", "c")
        time.sleep(1)
        Seite = pyperclip.paste()
        if Seite:
            break
        else:
            continue
    pyperclip.copy("")
    return Seite


"""---------------------------------------------------
Funktion:   Formatiere_Tätigkeitsbeschreibung
Zweck:      Kopiert die aktuelle Markus-Seite
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Formatiere_Tätigkeitsbeschreibung(Textstring):
    Suchtext = "CrefoID"
    Inhalt = Textstring.splitlines()
    Crefotext = ""
    CrefoID = ""
    Business_Activity = ""
    for Entry in Inhalt:
        if Suchtext in Entry:
            Crefotext = Entry
            break
    if Crefotext:
        _, CrefoID = Crefotext.split()
        Business_Activity = ""
        Suchtext = "Activity_Description_ROC"
        for Position, Entry in enumerate(Inhalt):
            if Suchtext in Entry and len(Entry) == len(Suchtext):
                if Inhalt[Position + 1] != "":
                    Business_Activity = Inhalt[Position + 1].strip()
                else:
                    Business_Activity = Inhalt[Position + 2].strip()
                break
    return CrefoID, Business_Activity


"""---------------------------------------------------
Funktion:   Datei_speichern
Zweck:      Kann Daten aus dem Clipboard übernehmen um diese dann als Textfile zum Einlesen abzuspeichern
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Datei_speichern(text, formatangabe):
    global HomeDir
    Seitentext = text.get("1.0", "end")
    if formatangabe == "Rohdaten":
        Inhalt = Seitentext
    else:
        Inhalt = Seite_parsen(text, Seitentext)
    options = {}
    options['defaultextension'] = '.txt'
    options['filetypes'] = [('text files', '.txt'), ('All files', '.*')]
    options['initialdir'] = HomeDir
    options['initialfile'] = 'avad.txt'
    options['parent'] = root
    options['title'] = 'Daten speichern'
    FileName = filedialog.asksaveasfilename(**options)
    if FileName:
        HomeDir = os.path.dirname(FileName)
        datei = open(FileName, "w")
        if formatangabe == "Rohdaten":
            datei.writelines(Inhalt)
        else:
            inhalt_to_datei(datei, Inhalt)
        datei.close()
    else:
        messagebox.showinfo("Dateifehler", "Keine Datei ausgewählt")
        return

    #    with datei:
    #        wr = csv.writer(datei, delimiter=";", lineterminator="\n")
    #        wr.writerows(Inhalt)
    return


def inhalt_to_datei(datei, inhalt):
    with datei:
        wr = csv.writer(datei, delimiter=";", lineterminator="\n")
        wr.writerows(inhalt)


"""---------------------------------------------------
Funktion:   Seite_parsen
Zweck:      Kann Daten aus dem Clipboard übernehmen um diese dann als Textfile zum Einlesen abzuspeichern
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Seite_parsen(text, Inhalt):
    Adressliste_dirty = Inhalt.splitlines()
    #    text.insert(INSERT, "Länge Adressliste: " + str(len(Adressliste_dirty)) + " Zeilen\n\n")
    Suchtext = "BvD ID Nummer"
    Vorspann_Zeilen = Vorspann(Suchtext, Adressliste_dirty)  # Identifizieren des Beginns der Feldnamen
    Suchtext = "Spalten der Liste"
    Abspann_Zeilen = Vorspann(Suchtext, Adressliste_dirty)  # Identifizieren des Beginns der Feldnamen
    Adressliste = []
    for i in range(Vorspann_Zeilen, Abspann_Zeilen):  # Entfernen der Vorspannzeilen
        Adressliste.append(Adressliste_dirty[i])
    Feldliste = Create_Fieldnames(Adressliste,
                                  text)  # Erzeugen der Liste mit den Feldnamen und Löschen aller Feldüberschriften aus der Adressliste
    Lösche_Leersätze(Adressliste, text)  # Danach sollten in der Adressliste bloß noch die reinen Daten vorhanden sein

    Namensliste = []
    Datenbank = []
    for i in range(len(Adressliste)):
        if len(Adressliste[i][0]) != 0 and Adressliste[i][0][0].isnumeric():
            Namensliste.append(Adressliste[i][len(Adressliste[i]) - 1])
        else:
            Datenbank.append(Adressliste[i])
    for i in range(len(Datenbank)):  # Einfügen eines Feldes zur Aufnahme des Namens des Unternehmens)
        Datenbank[i].insert(0, "")
    k = 0
    for i in range(len(Namensliste)):
        Datenbank[k][0] = Namensliste[i]
        k += 1
        if k >= len(Datenbank):
            break
        elif len(Datenbank[k][1]) == 0:
            while len(Datenbank[k][1]) == 0:
                Datenbank[k][0] = Namensliste[i]
                k += 1
                if k >= len(Datenbank):
                    break
    Datenbank.insert(0, Feldliste)
    return Datenbank


"""---------------------------------------------------
Funktion:   Textdatei_konvertieren
Zweck:      Liest die Textdatei ein, bereinigt diese un erzeugt eine Excel-Datei
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Textdatei_konvertieren(text, target, filename="", db_file=""):
    showbox = False
    if not filename:
        options = {}
        options['defaultextension'] = '.txt'
        options['filetypes'] = [('Text files', '.txt'), ('All files', '.*')]
        options['initialfile'] = 'avad.txt'
        options['parent'] = root
        options['title'] = 'Datei auswählen'
        text.delete(1.0, END)
        filename = filedialog.askopenfilename(**options)
        showbox = True

    if target == "Database":
        # Get database file name to insert data
        if not db_file:
            options = {}
            options['defaultextension'] = '.db'
            options['filetypes'] = [('Database files', '.db'), ('All files', '.*')]
            options['initialfile'] = 'Firmen.db'
            options['parent'] = root
            options['title'] = 'Datenbank auswählen'
            db_file = filedialog.askopenfilename(**options)
        datenbank = sqlite3.connect(db_file)
        cursor = datenbank.cursor()

    Pfad = os.path.dirname(filename)  # Zerlegen des File names in Pfad, Dateiname und Endung----
    Datei = os.path.basename(filename)
    Datei = os.path.splitext(Datei)
    Datei = Datei[0]
    tabelle = ""
    if "Liste 1" in Datei:
        tabelle = "Unternehmen_Upload"
        workbook = "Liste 1"
    elif "Liste 2" in Datei:
        tabelle = "Branchen_Upload"
        workbook = "Liste 2"
    elif "Liste 3" in Datei:
        tabelle = "ATE_Upload"
        workbook = "Liste 3"
    elif "Liste 4" in Datei:
        tabelle = "MAN_Upload"
        workbook = "Liste 4"
    elif "Liste 6" in Datei:
        tabelle = "TTG_Upload"
        workbook = "Liste 6"
    elif "Liste 7" in Datei:
        tabelle = "FIN_Upload"
        workbook = "Liste 7"
    elif target == "from_screen":
        workbook = "Manual upload"
    else:
        Ausgabe = "\nName der Liste nicht im Dateinamen enhalten\nBitte Namensgebung prüfen\nName muss 'Liste x' enthalten ! "
        text.insert(END, Ausgabe)
        messagebox.showinfo("Datenübernahme", Ausgabe)
        return

    if target == "from_screen":
        Seitentext = text.get("1.0", "end")
        Adressliste_dirty = Seite_parsen(text, Seitentext)
    #        Adressliste_dirty = text.get("1.0","end").splitlines()
    else:
        text.insert(INSERT, "Lese Datei: " + filename + "\n\n")
        Adressliste_dirty = Datei_einlesen(Datei, Pfad + "\\")  # Einlesen der Datei
        if Adressliste_dirty == "Error":
            return
    text.insert(INSERT, "Länge Adressliste: " + str(len(Adressliste_dirty)) + " Zeilen\n\n")
    text.update_idletasks()
    if workbook == "Liste 6":
        Datenbank = []
        Namensliste = []
        for row in Adressliste_dirty:
            values = row.split(";", 1)
            values[0] = values[0].strip('"')
            values[1] = values[1].strip('"')
            Datenbank.append(values)
            Namensliste.append(
                values[0])  # nur für die Ausgabe des Zählers am Ende der Routine - ansonsten keine Bedeutung
    else:
        record_header = Adressliste_dirty[0]
        Datenbank = []
        for record_dirty in Adressliste_dirty:
            if target == "from_screen":
                record = record_dirty
            else:
                record = record_dirty.split(";")
            if record_dirty == record_header:
                Feldliste = record
                continue
            elif record[0] == "\n":
                continue
            elif record[0][0:5] == "Seite":
                continue
            Datenbank.append(record)

    if target == "Excel" or target == "from_screen":
        Spreadsheet = Pfad + "/" + Datei + ".xlsx"
        text.insert("end", "Erzeuge Excel-Tabelle\n\n")
        text.update_idletasks()
        Success = Create_Spreadsheet(Datenbank, Spreadsheet, Feldliste, workbook)
        if Success == 1:
            ausgabe = "Datensätze: " + str(len(Datenbank)) + "\n" + "\n\nExcel Datei erzeugt: " + Spreadsheet
            text.insert(INSERT, "========== Excel-Tabelle erzeugt ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "\n\n====================" + "\n")
        elif Success == 13:
            ausgabe = "Die Excel-Tabelle konnte nicht erzeugt werden\n\nEvtl. ist die Datei bereits geöffnet"
            text.insert(INSERT, "========== Exycel-Tabelle fehlgeschlagen ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "\n\n====================" + "\n")
        else:
            ausgabe = "Die Excel-Tabelle konnte nicht erzeugt werden\n\nGrund ist nicht bekannt"
            text.insert(INSERT, "========== Exycel-Tabelle fehlgeschlagen ==========" + "\n")
            text.insert(INSERT, ausgabe)
            text.insert(INSERT, "====================" + "\n")
        messagebox.showinfo("Datenübernahme", ausgabe)

    elif target == "Database":
        tables = DBTables()
        sql_create = tables[tabelle]
        try:
            cursor.execute(sql_create)
        except:
            pass
        Index = DBIndex()
        if tabelle in Index:
            try:
                sql_command = Index[tabelle]
                cursor.execute(sql_command)
                text.insert("end", "\nIndex in Datenbank \n" + db_file + "\n\nerzeugt. \n")
            except:
                messagebox.showinfo("INDEX ERZEUGEN", "Index in Tabelle " + tabelle + " konnte nicht erzeugt werden.")
                text.insert("end", (traceback.format_exc() + "\n"))
        if tabelle == "Unternehmen_Upload":
            keys = ['Company', 'BvDID', 'CrefoID', 'Strasse', 'Postleitzahl', 'City', 'State', 'Country', \
                    'Count_Shareholders', 'Phone', 'URL', 'Email', 'WZ2008_Haupt_Code', \
                    'BD_Main_Code', 'Date_Incorporation', 'Legal_Form', 'Letzter_Umsatz_tsd_EUR',
                    'Last_Turnover_Year', \
                    'Last_Count_Employees', 'Last_Count_Employees_Year', 'ROC_number', \
                    'UID', 'Count_Subsidiaries', 'Count_Group_Companies',
                    'Comparison_Group_Name', \
                    'Comparison_Group_Description', 'Comparison_Group_Size', 'GKZ_Street', 'GKZ_X', \
                    'GKZ_Y']
            sql_insert = "INSERT INTO Unternehmen_Upload VALUES (:Company, :BvDID, :CrefoID, :Strasse, :Postleitzahl, :City, \
                    :State, :Country, :Count_Shareholders, :Phone, :URL, :Email, :WZ2008_Haupt_Code, \
                    :BD_Main_Code, :Date_Incorporation, :Legal_Form, :Letzter_Umsatz_tsd_EUR, :Last_Turnover_Year,\
                    :Last_Count_Employees, :Last_Count_Employees_Year, :ROC_number, \
                    :UID, :Count_Subsidiaries, :Count_Group_Companies, :Comparison_Group_Name, \
                    :Comparison_Group_Description, :Comparison_Group_Size, :GKZ_Street, :GKZ_X, \
                    :GKZ_Y)"
        elif tabelle == "Branchen_Upload":
            keys = ['Company', 'BvDID', 'CrefoID', 'WZ2008_Haupt_Code', 'WZ2008_Nebentätigkeit_Code', \
                    'BD_Main_Code', 'BD_Secondary_Code']
            sql_insert = "INSERT INTO Branchen_Upload VALUES (:Company, :BvDID, :CrefoID, :WZ2008_Haupt_Code,\
                        :WZ2008_Nebentätigkeit_Code, :BD_Main_Code, :BD_Secondary_Code)"
        elif tabelle == "ATE_Upload":
            keys = ['Company', 'BvDID', 'CrefoID', 'ATE_Crefo', 'SHH_Type', 'SHH_Title', 'SHH_Salutation', \
                    'SHH_Full_Name', 'ATE_Nachname', 'ATE_Vorname', 'ATE_Mittelname', 'SHH_Gender', 'SHH_Function', \
                    'SHH_Birth_Year', 'ATE_Funktion_Mailing', 'SHH_Street_Nr', 'SHH_City', 'ATE_Adresszusatz', 'ATE_PLZ', \
                    'SHH_Country', 'SHH_Shares_Owned']
            sql_insert = "INSERT INTO ATE_Upload VALUES (:Company, :BvDID, :CrefoID, :ATE_Crefo, :SHH_Type, \
                    :SHH_Title, :SHH_Salutation, :SHH_Full_Name, :ATE_Nachname, :ATE_Vorname, :ATE_Mittelname, :SHH_Gender, \
                    :SHH_Function, :SHH_Birth_Year, :ATE_Funktion_Mailing, :SHH_Street_Nr, :SHH_City, :ATE_Adresszusatz, \
                    :ATE_PLZ, :SHH_Country, :SHH_Shares_Owned)"
        elif tabelle == "MAN_Upload":
            keys = ['Company', 'BvDID', 'CrefoID', 'MAN_Function_Mailings', 'MAN_Salutation', 'MAN_Titel',
                    'MAN_Nachname', \
                    'MAN_Vorname', 'MAN_Mittelname', 'MAN_Gender', 'MAN_Crefo', 'MAN_Type', \
                    'MAN_Birth_Year', 'MAN_Function', 'MAN_Street_Nr', 'MAN_City', 'MAN_PLZ', 'MAN_Country']
            sql_insert = "INSERT INTO MAN_Upload VALUES (:Company, :BvDID, :CrefoID, :MAN_Crefo, :MAN_Type, \
                    :MAN_Titel, :MAN_Salutation, :MAN_Nachname, :MAN_Vorname, :MAN_Mittelname, :MAN_Gender, \
                    :MAN_Function, :MAN_Birth_Year, :MAN_Function_Mailings, :MAN_Street_Nr, :MAN_City, :MAN_PLZ, :MAN_Country)"
        elif tabelle == "TTG_Upload":
            keys = ['CrefoID', 'Business_Activity']
            sql_insert = "INSERT INTO TTG_Upload VALUES (:CrefoID, :Business_Activity)"
        elif tabelle == "FIN_Upload":
            sql_insert = "INSERT INTO FIN_Upload VALUES (:CrefoID, :Year, :Type, :Value)"

        last_crefo = ""
        for i in range(len(Datenbank)):
            if tabelle == "FIN_Upload":
                values = Datenbank[i]
                record = dict(zip(Feldliste, values))
                AID = record["CrefoID"]
                for typ, wert in record.items():
                    if typ == "Company" or typ == "BvD ID Nummer" or typ == "CrefoID" or wert == "n.v.":
                        continue
                    if "nv" in wert.replace('.', ''):
                        continue
                    fin_dict = Record_Dict("FIN_Upload")
                    fin_typ = "Mitarbeiter"
                    if "Turnover" in typ:
                        fin_typ = "Turnover"
                    fin_dict["CrefoID"] = AID
                    this_year = datetime.datetime.now().year
                    jahr = 0
                    jahr = ''.join(x for x in typ if x.isdigit())
                    if len(jahr) == 0 or int(jahr) < 2000 or int(jahr) > this_year:
                        messagebox.showinfo("Dateifehler",
                                            "Ungültige Jahreszahl in\n" + filename + " \nDatei wird nicht verarbeitet")
                        sql = "Delete from FIN_Upload"
                        cursor.execute(sql)
                        return
                    fin_dict["Year"] = jahr
                    fin_dict["Type"] = fin_typ
                    try:
                        fin_dict["Value"] = int(wert.replace('.', ''))
                    except:
                        #                        print(i, wert)
                        pass
                    cursor.execute(sql_insert, fin_dict)
            else:
                values = Datenbank[i]
                record = dict(zip(keys, values))
                crefo_id = record["CrefoID"]
                if not crefo_id:
                    record["CrefoID"] = last_crefo
                    crefo_id = last_crefo
                    if not last_crefo:
                        continue
                cursor.execute(sql_insert, record)
                last_crefo = crefo_id
            print("Zeile ", format(i + 1, ",d").replace(",", "."), " von ",
                  format(len(Datenbank), ",d").replace(",", "."), " eingefügt")
        datenbank.commit()
        unternehmen_count = format(len(Datenbank), ",").replace(",", ".")
        record_count = format(len(Datenbank), ",").replace(",", ".")
        Ausgabe = "Anzahl Company: " + unternehmen_count + "\n\nAnzahl Datensätze: " + record_count
        Ausgabe = Ausgabe + "\n" + workbook + " wurde in die Datenbank eingefügt"
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")
        if showbox:
            messagebox.showinfo("Datenübernahme", Ausgabe)
    return


"""---------------------------------------------------
Function:   Upsert_Upload
Purpose:    Does an upsert of the xxx_Upload tables
Paramter:   Text-Objekt
-----------------------------------------------------"""


def Upsert_Upload(text):
    options = {}
    options['defaultextension'] = '.db'
    options['filetypes'] = [('Database files', '.db'), ('All files', '.*')]
    options['initialfile'] = 'Firmen.db'
    options['parent'] = root
    options['title'] = 'Datenbank auswählen'
    db_file = filedialog.askopenfilename(**options)
    datenbank = sqlite3.connect(db_file)
    datenbank.row_factory = dict_factory
    cursor_from = datenbank.cursor()
    cursor_to = datenbank.cursor()

    # Upload Table Entity
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Unternehmen_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der Entity-Daten ist gestartet....\n")
        sql = "SELECT * from Unternehmen_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            if not row["BvDID"]:
                continue
            # Insert Entity data
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["CrefoID"]
            entity_dict["Entity_Last_Name"] = row["Company"]
            entity_dict["Legal_Form"] = row["Legal_Form"]
            entity_dict["Birth_Year"] = row["Date_Incorporation"][6:]
            entity_dict["Comparison_Group_Code"] = row["Comparison_Group_Name"]
            entity_dict["Comparison_Group_Size"] = row["Comparison_Group_Size"]
            entity_dict["Count_Shareholders"] = row["Count_Shareholders"]
            entity_dict["Count_Group_Companies"] = row["Count_Group_Companies"]
            entity_dict["Count_Subsidiaries"] = row["Count_Subsidiaries"]
            entity_dict["UID"] = row["UID"]

            sql_delete = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Entity VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, :Expiry_Date, :Last_Change)"
            cursor_to.execute(sql_insert, entity_dict)

            # Insert Adress data
            entity_dict = Record_Dict("Address")
            entity_dict["AID"] = row["CrefoID"]
            entity_dict["Street_Nr"] = row["Strasse"]
            entity_dict["ZIP"] = row["Postleitzahl"]
            entity_dict["City"] = row["City"]
            entity_dict["State"] = row["State"]
            entity_dict["Country"] = row["Country"]
            entity_dict["GKZ_X"] = row["GKZ_X"]
            entity_dict["GKZ_Y"] = row["GKZ_Y"]
            entity_dict["GKZ_Street"] = row["GKZ_Street"]
            sql_delete = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Address VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            cursor_to.execute(sql_insert, entity_dict)

            # Insert Contact data
            field_list = ["Phone", "URL", "Email"]
            code_dict = {}
            code_dict.setdefault("Phone", "Phone")
            code_dict.setdefault("URL", "Internet")
            code_dict.setdefault("Email", "eMail")
            record_list = []
            for key in field_list:
                if row[key]:
                    entity_dict = Record_Dict("Contact")
                    entity_dict["AID"] = row["CrefoID"]
                    entity_dict["Contact_Type"] = code_dict[key]
                    entity_dict["Contact_Value"] = row[key]
                    record_list.append(entity_dict)
            sql_delete = "DELETE FROM Contact WHERE AID = '" + entity_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Contact VALUES (:AID, :Contact_Type, :Contact_Value, :Last_Change)"
            for record in record_list:
                cursor_to.execute(sql_insert, record)

            i += 1
            print(i, ": AID: " + entity_dict["AID"] + " updated. \n")

        sql_delete = "DELETE from Unternehmen_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Company wurden eingefügt."
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")

    # Upload Table Branch_Codes
    field_list = ["WZ2008_Haupt_Code", "WZ2008_Nebentätigkeit_Code", "BD_Main_Code", "BD_Secondary_Code"]
    code_dict = {}
    code_dict.setdefault("WZ2008_Haupt_Code", "WZ08H")
    code_dict.setdefault("WZ2008_Nebentätigkeit_Code", "WZ08N")
    code_dict.setdefault("BD_Main_Code", "BDH")
    code_dict.setdefault("BD_Secondary_Code", "BDN")
    last_aid = ""

    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='Branchen_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der Branch_Codes ist gestartet....\n")
        sql = "SELECT * from Branchen_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            record_list = []
            for code in field_list:
                if row[code]:
                    entity_dict = Record_Dict("Branch_Code")
                    entity_dict["AID"] = row["CrefoID"]
                    entity_dict["Branch_Code"] = row[code]
                    entity_dict["Branch_Rank_Main_Sec"] = code_dict[code]
                    record_list.append(entity_dict)

            if entity_dict["AID"] != last_aid:
                sql_delete = "DELETE FROM Branch_Codes WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_delete)
            for record in record_list:
                sql_insert = "INSERT INTO Branch_Codes VALUES (:AID, :Branch_Code, :Branch_Rank_Main_Sec, :Source, :Last_Change)"
                cursor_to.execute(sql_insert, record)
                last_aid = record["AID"]
                i += 1
                print(format(i, ",d").replace(",", "."), ": AID: " + entity_dict["AID"] + " updated. \n")
        sql_delete = "DELETE from Branchen_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Branchendefinitionen wurden eingefügt."
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")

    # Upload data from ATE: Entity and ATE need to be updated
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='ATE_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der ATE-Daten ist gestartet....\n")
        # Delete records from ATE table which will be updated through ATE_Upload
        sql = "SELECT DISTINCT CrefoID from ATE_Upload"
        cursor_from.execute(sql)
        for row in cursor_from:
            sql_delete_ATE = "DELETE FROM ATE WHERE AID_Company = '" + row["CrefoID"] + "'"
            cursor_to.execute(sql_delete_ATE)

        sql = "SELECT * from ATE_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            # Read the Entity data from the ATE_Upload Record
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["SHH_CrefoID"]
            entity_dict["Entity_Last_Name"] = row["SHH_Last_Name"]
            entity_dict["Entity_First_Name"] = row["SHH_First_Name"]
            entity_dict["Entity_Type"] = row["SHH_Type"]
            entity_dict["Titel"] = row["SHH_Title"]
            entity_dict["Salutation"] = row["SHH_Salutation"]
            entity_dict["Gender"] = row["SHH_Gender"]
            entity_dict["Birth_Year"] = row["SHH_Birth_Year"]

            # Read the Address data from the ATE_Upload Record
            adresse_dict = Record_Dict("Address")
            adresse_dict["AID"] = row["SHH_CrefoID"]
            adresse_dict["Street_Nr"] = row["SHH_Street_Nr"]
            adresse_dict["ZIP"] = row["SHH_ZIP"]
            adresse_dict["City"] = row["SHH_City"]
            adresse_dict["Country"] = row["SHH_Country"]

            # Read the ATE data from the ATE_Upload Record
            ate_dict = Record_Dict("ATE")
            ate_dict["AID_Company"] = row["CrefoID"]
            ate_dict["AID_SHH"] = row["SHH_CrefoID"]
            ate_dict["SHH_Function"] = row["SHH_Function"]
            ate_dict["SHH_Shares_Owned"] = row["SHH_Shares_Owned"]
            if ate_dict["AID_SHH"] == "":
                ate_dict["AID_SHH"] = "0000000000"
                # if ATE has only BvDID but no Crefo, the data is not extractable (see i.e. 9370299738 in Markus)

            # If Entity_Type = "Company" neither Entity nor Address will be changed
            # Changes for Entity will only be through Liste 1 !

            sql_delete_entity = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_entity = "INSERT INTO Entity VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, \
                        :Expiry_Date, :Last_Change)"
            sql_delete_adresse = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_adresse = "INSERT INTO Address VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            sql_delete_ate = "DELETE FROM ATE WHERE AID_Company = '" + ate_dict["AID_Company"] + "' AND AID_SHH = '" + \
                             ate_dict["AID_SHH"] + "'"
            sql_insert_ate = "INSERT INTO ATE VALUES (:AID_Company, :AID_SHH, :SHH_Function, :SHH_Shares_Owned,\
                        :ATE_Funktion_Mailing, :SHH_Position, :Last_Change)"
            cursor_to.execute(sql_delete_ate)
            cursor_to.execute(sql_insert_ate, ate_dict)
            if row["SHH_Type"] == "Person":
                cursor_to.execute(sql_delete_entity)
                cursor_to.execute(sql_insert_entity, entity_dict)
                cursor_to.execute(sql_delete_adresse)
                cursor_to.execute(sql_insert_adresse, adresse_dict)
            elif row["SHH_Type"] == "Company":
                # Wenn der Datensatz bereits existiert -> nichts unternehmen
                # Wenn der Datensatz in Entity noch nicht existiert -> aufnehmen
                sql_exists_entity = "SELECT * FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_exists_entity)
                entity_exists = cursor_to.fetchone()
                if not entity_exists:
                    cursor_to.execute(sql_insert_entity, entity_dict)
                    cursor_to.execute(sql_insert_adresse, adresse_dict)
            print("ATE: ", format(i, ",d").replace(",", "."), "Datensätze verarbeitet. \n")
            i += 1

        sql_delete = "DELETE from ATE_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " ATE's wurden eingefügt."
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")

    # Upload data from MAN: Entity and ATE need to be updated
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='MAN_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der MAN-Daten ist gestartet....\n")
        # Delete records from ATE table which will be updated through ATE_Upload
        sql = "SELECT DISTINCT CrefoID from MAN_Upload"
        cursor_from.execute(sql)
        for row in cursor_from:
            sql_delete_MAN = "DELETE FROM MAN WHERE AID_Company = '" + row["CrefoID"] + "'"
            cursor_to.execute(sql_delete_MAN)

        sql = "SELECT * from MAN_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            # If no MAN-CrefoID skip to next record
            if not row["MAN_CrefoID"]:
                continue
            # Read the Entity data from the MAN_Upload Record
            entity_dict = Record_Dict("Entity")
            entity_dict["AID"] = row["MAN_CrefoID"]
            entity_dict["Entity_Last_Name"] = row["MAN_Last_Name"]
            entity_dict["Entity_First_Name"] = row["MAN_First_Name"]
            entity_dict["Entity_Type"] = row["MAN_Type"]
            entity_dict["Titel"] = row["MAN_Titel"]
            entity_dict["Salutation"] = row["MAN_Salutation"]
            entity_dict["Gender"] = row["MAN_Gender"]
            entity_dict["Birth_Year"] = row["MAN_Birth_Year"]

            # Read the Address data from the MAN_Upload Record
            adresse_dict = Record_Dict("Address")
            adresse_dict["AID"] = row["MAN_CrefoID"]
            adresse_dict["Street_Nr"] = row["MAN_Street_Nr"]
            adresse_dict["ZIP"] = row["MAN_Postleitzahl"]
            adresse_dict["City"] = row["MAN_City"]
            adresse_dict["Country"] = row["MAN_Country"]

            # Read the MAN data from the MAN_Upload Record
            man_dict = Record_Dict("MAN")
            man_dict["AID_Company"] = row["CrefoID"]
            man_dict["AID_MAN"] = row["MAN_CrefoID"]
            man_dict["MAN_Function"] = row["MAN_Function"]
            man_dict["MAN_Function_Mailings"] = row["MAN_Function_Mailings"]
            if man_dict["AID_MAN"] == "":
                man_dict["MAN_ATE"] = "0000000000"

            # If Entity_Type = "Company" neither Entity nor Address will be changed
            # Changes for Entity will only be through Liste 1 !

            sql_delete_entity = "DELETE FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_entity = "INSERT INTO Entity VALUES (:AID, :Entity_Last_Name, :Entity_First_Name, :Address_Ownership,\
                        :Entity_Type, :Legal_Form, :Salutation, :Titel, :Education, :Gender, :Birth_Year, \
                        :Comparison_Group_Code, :Comparison_Group_Size, :Count_Shareholders, :Count_Group_Companies, \
                        :Count_Subsidiaries, :UID, :Reserved, \
                        :Expiry_Date, :Last_Change)"
            sql_delete_adresse = "DELETE FROM Address WHERE AID = '" + entity_dict["AID"] + "'"
            sql_insert_adresse = "INSERT INTO Address VALUES (:AID, :AD_Address_ID, :Address_Addendum, :Street_Nr,\
                        :ZIP, :City, :County, :State, :Country, :GKZ_X, :GKZ_Y, \
                        :GKZ_Street, :Last_Change)"
            sql_delete_man = "DELETE FROM MAN WHERE AID_Company = '" + man_dict["AID_Company"] + "' AND AID_MAN = '" + \
                             man_dict["AID_MAN"] + "'"
            sql_insert_man = "INSERT INTO MAN VALUES (:AID_Company, :AID_MAN, :MAN_Function, \
                        :MAN_Function_Mailings, :MAN_Position, :Last_Change)"
            cursor_to.execute(sql_delete_man)
            cursor_to.execute(sql_insert_man, man_dict)
            if row["MAN_Type"] == "Person":
                cursor_to.execute(sql_delete_entity)
                cursor_to.execute(sql_insert_entity, entity_dict)
                cursor_to.execute(sql_delete_adresse)
                cursor_to.execute(sql_insert_adresse, adresse_dict)
            elif row["MAN_Type"] == "Company":
                # Wenn der Datensatz bereits existiert -> nichts unternehmen
                # Wenn der Datensatz in Entity noch nicht existiert -> aufnehmen
                sql_exists_entity = "SELECT * FROM Entity WHERE AID = '" + entity_dict["AID"] + "'"
                cursor_to.execute(sql_exists_entity)
                entity_exists = cursor_to.fetchone()
                if not entity_exists:
                    cursor_to.execute(sql_insert_entity, entity_dict)
                    cursor_to.execute(sql_insert_adresse, adresse_dict)
            print("MAN: ", format(i, ",d").replace(",", "."), "Datensätze verarbeitet. \n")
            i += 1
        sql_delete = "DELETE from MAN_Upload"
        cursor_from.execute(sql_delete)
        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " MAN's wurden eingefügt."
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")

    # Upload data from TTG
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='TTG_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der TTG-Daten ist gestartet....\n")
        # Delete records from Business_Activity table which will be updated through TTG_Upload
        sql = "SELECT DISTINCT CrefoID from TTG_Upload"
        cursor_from.execute(sql)
        sql = "SELECT * from TTG_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            ttg_dict = Record_Dict("Taetigkeit")
            ttg_dict["AID"] = row["CrefoID"]
            ttg_dict["Activity_Description_ROC"] = row["Business_Activity"]

            sql_delete = "DELETE FROM Business_Activity WHERE AID = '" + ttg_dict["AID"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Business_Activity VALUES (:AID, :Activity_Description_ROC, :Last_Change)"
            cursor_to.execute(sql_insert, ttg_dict)

            i += 1
            print(format(i, ",d").replace(",", "."), ": AID: " + ttg_dict["AID"] + " updated. \n")

        sql_delete = "DELETE from TTG_Upload"
        cursor_from.execute(sql_delete)

        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Tätigkeitsbeschreibungen wurden eingefügt.\n"
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")

    # Upload data from FIN
    sql = "SELECT count(*) AS 'Exists' FROM sqlite_master WHERE type='table' AND name='FIN_Upload'"
    cursor_from.execute(sql)
    res = cursor_from.fetchone()
    if res["Exists"] > 0:
        text.insert("end", "\nUpload der FIN-Daten ist gestartet....\n")
        sql = "SELECT * from FIN_Upload"
        cursor_from.execute(sql)
        i = 0
        for row in cursor_from:
            fin_dict = Record_Dict("Financials")
            fin_dict["AID"] = row["CrefoID"]
            fin_dict["Year"] = row["Year"]
            fin_dict["Fin_Value_Type"] = row["Type"]
            fin_dict["Fin_Value"] = row["Value"]

            sql_delete = "DELETE FROM Financials WHERE AID = '" + fin_dict["AID"] + "' AND Year = " + str(
                fin_dict["Year"]) + " AND Fin_Value_Type = '" + fin_dict["Fin_Value_Type"] + "'"
            cursor_to.execute(sql_delete)
            sql_insert = "INSERT INTO Financials VALUES (:AID, :Year, :Fin_Value_Type, :Fin_Value, :Last_Change)"
            cursor_to.execute(sql_insert, fin_dict)

            i += 1
            print(format(i, ",d").replace(",", "."), ": AID: " + fin_dict["AID"] + " updated. \n")

        sql_delete = "DELETE from FIN_Upload"
        cursor_from.execute(sql_delete)

        datenbank.commit()
        record_count = format(i, ",").replace(",", ".")
        print("Upsert completed. ", format(i, ",d").replace(",", "."), "Records uploaded\n")
        Ausgabe = record_count + " Finanzdaten wurden eingefügt.\n"
        text.insert(INSERT, "========== DATENÜBERHNAHME ERFOLGT ==========" + "\n")
        text.insert(INSERT, Ausgabe)
        text.insert(INSERT, "\n" + "====================" + "\n")
    return


"""---------------------------------------------------
Function:   dict_factory
Purpose:    format sqlite rows to dictionary
Paramter:   Text-Objekt
-----------------------------------------------------"""


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


"""---------------------------------------------------
Funktion: Create_Spreadsheet
Erzeugt die Excel-Datei aus den eingelesenen Daten
-----------------------------------------------------"""


def Create_Spreadsheet(Datenbank, File, Feldliste, workbook):
    wb = openpyxl.Workbook()
    sheet = wb.get_active_sheet()
    sheet.title = workbook
    for i in range(len(Feldliste)):
        Spalte = get_column_letter(i + 1)
        Zelle = Spalte + str(1)
        sheet[Zelle] = Feldliste[i]
    for i in range(len(Datenbank)):
        Zeile = i + 2
        for k in range(len(Datenbank[i])):
            Spalte = get_column_letter(k + 1)
            Zelle = Spalte + str(Zeile)
            Value = Datenbank[i][k].replace(".", "").replace(",", "")
            if Value.isdigit():
                sheet[Zelle] = Value
            else:
                sheet[Zelle] = Datenbank[i][k]
    try:
        wb.save(File)
        return 1
    except PermissionError:
        return 13


"""-------------------------------------------------
Funktion: Datei_einlesen
Öffnet die txt-Datei und liest die Daten ein
------------------------------------------------"""


def Datei_einlesen(Datei, Pfad):
    FileName = Pfad + Datei + ".txt"
    Adressdatei = open(FileName, encoding="Latin-1")
    Adressliste = Adressdatei.readlines()
    if "Seite \n" in Adressliste:
        position = Adressliste.index("Seite \n")
        pos = format(position, ",d").replace(",", ".")
        message = "Datei: " + FileName + "\n" + "Position: " + pos + "\n Datei wird übersprungen"
        messagebox.showwarning("DATEIFEHLER", message)
        Adressliste = "Error"
    Adressdatei.close()
    return Adressliste


"""------------------------------------
Funktion: Vorpsann
Bereinigt die nicht benötigten Zeilen aus dem Dateivorspann
------------------------------------"""


def Vorspann(Suchtext, Adressliste):
    for i in range(len(Adressliste)):
        if (Suchtext in Adressliste[i]):
            break
    return i


"""---------------------------------------
Funktion: Create_Fieldnames
Erzeugt die Liste der Feldnamen aus dem Datei-Header
-------------------------------------"""


def Create_Fieldnames(Adressliste, text_window):
    #    Suchtext = "1.\t"
    Suchtext = re.compile("\d+\.")

    for i in range(len(Adressliste)):
        lfd_nr = Suchtext.search(Adressliste[i])
        if lfd_nr:
            break
    # Parsen der Struktur der übergebenen Datensätze
    Headers = i
    Feldzeilen = []
    VollString = ""
    for i in range(0, Headers):
        VollString = VollString + str(Adressliste[i])
        Feldzeilen.append(Adressliste[i])
    VollString = VollString.replace("\n", " ")  # Entfernen der Zeilenvorschübe in den Namen
    VollString = VollString.replace("\t", "\n")  # Alle Tabstopps werden mit Zeilenvorschub ersetzt
    VollString = VollString.splitlines()  # Jetzt werden alle Zeilen wieder getrennt. Man erhält neben den Datensatznamen auch viele Leerzeilen
    Feldliste = ["Company"]
    for i in range(len(VollString)):  # Entfernen führender und schließender Leerzeichen
        text = VollString[i].strip()
        if text:
            Feldliste.append(text)
    for i in range(len(Feldzeilen)):
        while (Feldzeilen[i] in Adressliste):
            Adressliste.remove(Feldzeilen[i])
    return (Feldliste)


"""----------------------------------
Funktion: Lösche_Leersätze
Löscht alle leeren Datensätze (= Leerzeilen) 
---------------------------------"""


def Lösche_Leersätze(Adressliste, text_window):
    Leersätze = []  # Dient zum Aufnehmen der leeren Zeilen, die später gelöscht werden
    for i in range(len(Adressliste)):
        Adressliste[i] = Adressliste[i].replace(";", " ")
        Adressliste[i] = Adressliste[i].replace("\t\t", ";")
        Adressliste[i] = Adressliste[i].split(";")
        for k in range(len(Adressliste[i])):
            Adressliste[i][k] = Adressliste[i][k].replace("\t\n", "")
        Elemente = 0
        for k in range(len(Adressliste[i])):
            Adressliste[i][k] = Adressliste[i][k].strip()
            Elemente = Elemente + len(Adressliste[i][k])
        if Elemente == 0:
            Leersätze.append(i)
        elif len(Adressliste[i]) == 1:
            Leersätze.append(i)
    for i in range(len(Leersätze)):
        Element = Leersätze[i]
        Adressliste.pop(Element - i)
    return


"""----------------------------
Funktion: Record_Dict
Gibt ein Dict mit den Feldnamen zurück,
Diese Feldnamen werden verwendet um Datensätze auszuwählen die in die SQL-Datenbank geschrieben zu werden
----------------------------"""

"""
"Contact_Type" : "", "BDPS" : "", "BD_Beschreibung" : "", "Einschluss" : "", "Ausschluss" : "", \
"Quelle" : "", "U_Datum" : "", \
"""


def Record_Dict(db_table="Alles"):
    RDict = {}
    date = str(datetime.datetime.now())
    today = date[:10]
    #    today = date[8:10] + "." + date[5:7] + "." + date[:4]
    #    today = "31.12.2016"
    if db_table == "Entity":
        RDict.setdefault("AID", "")
        RDict.setdefault("Entity_Last_Name", "")
        RDict.setdefault("Entity_First_Name", "")
        RDict.setdefault("Address_Ownership", "1")
        RDict.setdefault("Entity_Type", "Firma")
        RDict.setdefault("Legal_Form", "")
        RDict.setdefault("Salutation", "")
        RDict.setdefault("Titel", "")
        RDict.setdefault("Education", "")
        RDict.setdefault("Gender", "")
        RDict.setdefault("Birth_Year", "")
        RDict.setdefault("Comparison_Group_Code", "")
        RDict.setdefault("Comparison_Group_Size", "")
        RDict.setdefault("Count_Shareholders", "")
        RDict.setdefault("Count_Group_Companies", "")
        RDict.setdefault("Count_Subsidiaries", "")
        RDict.setdefault("UID", "")
        RDict.setdefault("Reserved", "0")
        RDict.setdefault("Expiry_Date", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Branch_Code":
        RDict.setdefault("AID", "")
        RDict.setdefault("Branch_Code", "")
        RDict.setdefault("Branch_Rank_Main_Sec", "")
        RDict.setdefault("Source", "BvD")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Address":
        RDict.setdefault("AID", "")
        RDict.setdefault("AD_Address_ID", "99")
        RDict.setdefault("Address_Addendum", "")
        RDict.setdefault("Street_Nr", "")
        RDict.setdefault("ZIP", "")
        RDict.setdefault("City", "")
        RDict.setdefault("County", "")
        RDict.setdefault("State", "")
        RDict.setdefault("Country", "")
        RDict.setdefault("GKZ_X", "")
        RDict.setdefault("GKZ_Y", "")
        RDict.setdefault("GKZ_Street", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Contact":
        RDict.setdefault("AID", "")
        RDict.setdefault("Contact_Type", "")
        RDict.setdefault("Contact_Value", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "ATE":
        RDict.setdefault("AID_Company", "")
        RDict.setdefault("AID_SHH", "")
        RDict.setdefault("SHH_Function", "")
        RDict.setdefault("SHH_Shares_Owned", 0)
        RDict.setdefault("SHH_Position", "")
        RDict.setdefault("ATE_Funktion_Mailing", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "MAN":
        RDict.setdefault("AID_Company", "")
        RDict.setdefault("AID_MAN", "")
        RDict.setdefault("MAN_Function", "")
        RDict.setdefault("MAN_Position", "")
        RDict.setdefault("MAN_Function_Mailings", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "Taetigkeit":
        RDict.setdefault("AID", "")
        RDict.setdefault("Activity_Description_ROC", "")
        RDict.setdefault("Last_Change", today)
    elif db_table == "FIN_Upload":
        RDict.setdefault("CrefoID", "")
        RDict.setdefault("Year", "")
        RDict.setdefault("Type", "")
        RDict.setdefault("Value", "")
    elif db_table == "Financials":
        RDict.setdefault("AID", "")
        RDict.setdefault("Year", "")
        RDict.setdefault("Fin_Value_Type", "")
        RDict.setdefault("Last_Change", today)
        RDict.setdefault("Fin_Value", "")
    elif db_table == "Salesforce_Kontakte":
        RDict.setdefault("ID", "")
        RDict.setdefault("AID__c", "")
        RDict.setdefault("LastName", "")
        RDict.setdefault("AccountId", "")
        RDict.setdefault("OwnerId", "")
        RDict.setdefault("ATE_Funktion_Mailing__c", "")
        RDict.setdefault("ATE_Funktion__c", "")
        RDict.setdefault("Anteile__c", "")
        RDict.setdefault("Ausbildung__c", "")
        RDict.setdefault("Email", "")
        RDict.setdefault("Fax", "")
        RDict.setdefault("FirstName", "")
        RDict.setdefault("Geburtsjahr__c", "")
        RDict.setdefault("Geschlecht__c", "")
        RDict.setdefault("Kommunikationssperre__c", "")
        RDict.setdefault("MAN_Funktion_Mailing__c", "")
        RDict.setdefault("MailingAdress_city", "")
        RDict.setdefault("MailingAdress_country", "")
        RDict.setdefault("MailingAdress_countryCode", "")
        RDict.setdefault("MailingAdress_postalCode", "")
        RDict.setdefault("MailingAdress_state", "")
        RDict.setdefault("MailingAdress_street", "")
        RDict.setdefault("Mailingsperre__c", "")
        RDict.setdefault("Manager_Funktion__c", "")
        RDict.setdefault("MobilePhone", "")
        RDict.setdefault("Name", "")
        RDict.setdefault("MAN_Funktion_Mailing__c", "")
        RDict.setdefault("OtherAdress_city", "")
        RDict.setdefault("OtherAdress_country", "")
        RDict.setdefault("OtherAdress_countryCode", "")
        RDict.setdefault("OtherAdress_postalCode", "")
        RDict.setdefault("OtherAdress_state", "")
        RDict.setdefault("OtherAdress_street", "")
        RDict.setdefault("Phone", "")
        RDict.setdefault("Position__c", "")
        RDict.setdefault("Salutation", "")
        RDict.setdefault("Sperrgrund_Kommunikationssperre__c", "")
        RDict.setdefault("Title", "")
        RDict.setdefault("Vertrauliche_E_Mail__c", "")
        RDict.setdefault("Vertrauliche_Privatnummer__c", "")
        RDict.setdefault("AccountTyp__c", "")
    elif db_table == "Salesforce_Accounts":
        RDict.setdefault("Id", "")
        RDict.setdefault("AID__c", "")
        RDict.setdefault("Name", "")
        RDict.setdefault("OwnerId", "")
        RDict.setdefault("Ablaufdatum_der_Reservierung__c", "")
        RDict.setdefault("Accounttyp__c", "")
        RDict.setdefault("Adressprovider__c", "")
        RDict.setdefault("Adresszusatz__c", "")
        RDict.setdefault("Aktiv__c", "")
        RDict.setdefault("Anzahl_der_Gesellschaften__c", "")
        RDict.setdefault("Anzahl_der_Gruppenunternehmen__c", "")
        RDict.setdefault("Anzahl_der_Tochtergesellschaften__c", "")
        RDict.setdefault("Avandil_Branche__c", "")
        RDict.setdefault("BD_Hauptcode__c", "")
        RDict.setdefault("BD_Nebencode__c", "")
        RDict.setdefault("BillingAddress_city", "")
        RDict.setdefault("BillingAddress_country", "")
        RDict.setdefault("BillingAddress_countryCode", "")
        RDict.setdefault("BillingAddress_postalCode", "")
        RDict.setdefault("BillingAddress_state", "")
        RDict.setdefault("BillingAddress_street", "")
        RDict.setdefault("Description", "")
        RDict.setdefault("E_Mail__c", "")
        RDict.setdefault("Eigentumsadresse__c", "")
        RDict.setdefault("Erfassungsdatum__c", "")
        RDict.setdefault("Fax", "")
        RDict.setdefault("Financials_Provider__c", "")
        RDict.setdefault("GKZ_Strasse__c", "")
        RDict.setdefault("GKZ_X__c", "")
        RDict.setdefault("GKZ_Y__c", "")
        RDict.setdefault("Gruendungsjahr__c", "")
        RDict.setdefault("Landkreis__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2012__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2013__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2014__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2015__c", "")
        RDict.setdefault("Mitarbeiter_laut_Provider_2016__c", "")
        RDict.setdefault("ParentId", "")
        RDict.setdefault("Phone", "")
        RDict.setdefault("Rechtsform__c", "")
        RDict.setdefault("Reserviert__c", "")
        RDict.setdefault("Reserviert_von_PLU__c", "")
        RDict.setdefault("ShippingAddress_city", "")
        RDict.setdefault("ShippingAddress_country", "")
        RDict.setdefault("ShippingAddress_countryCode", "")
        RDict.setdefault("ShippingAddress_postalCode", "")
        RDict.setdefault("ShippingAddress_state", "")
        RDict.setdefault("ShippingAddress_street", "")
        RDict.setdefault("Sperrgrund__c", "")
        RDict.setdefault("Taetigkeitsbeschreibung__c", "")
        RDict.setdefault("Telefon_2__c", "")
        RDict.setdefault("UID__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2012__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2013__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2014__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2015__c", "")
        RDict.setdefault("Umsatz_laut_Provider_2016__c", "")
        RDict.setdefault("Vergleichsgruppe_Code__c", "")
        RDict.setdefault("Vergleichsgruppe_Size__c", "")
        RDict.setdefault("WZ_08_Hauptcode__c", "")
        RDict.setdefault("WZ_08_Nebencode__c", "")
        RDict.setdefault("Website", "")
        RDict.setdefault("letzte_anzahl_mitarbeiter_laut_avandil__c", "")
        RDict.setdefault("letzte_anzahl_mitarbeiter_laut_provider__c", "")
        RDict.setdefault("letzter_umsatz_laut_avandil__c", "")
        RDict.setdefault("letzter_umsatz_laut_provider__c", "")
    return RDict


"""----------
Funktion: DBTables
Zweck: Gibt ein Dict mit den Datenbanktabellen sowie den SQL-Statements zum Erzeugen zurück
----------"""


def DBTables():
    DBTables = {}

    sql_command = """CREATE TABLE ATE (AID_Company TEXT NOT NULL, AID_SHH TEXT NOT NULL, SHH_Function TEXT, Anteile REAL, \
                    SHH_Position TEXT, ATE_Funktion_Mailing TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("ATE", sql_command)

    sql_command = """CREATE TABLE ATE_Upload (Company TEXT, BvDID TEXT, CrefoID TEXT, \
                    SHH_CrefoID TEXT, SHH_Type TEXT, SHH_Title TEXT, SHH_Salutation TEXT,\
                    SHH_Full_Name TEXT, SHH_Last_Name TEXT, SHH_First_Name TEXT, SHH_Name_Part_3 TEXT, \
                    SHH_Gender TEXT, SHH_Function TEXT, SHH_Birth_Year TEXT, SHH_Function_Mailings TEXT, \
                    SHH_Street_Nr TEXT, SHH_City TEXT, SHH_District TEXT, SHH_ZIP TEXT, SHH_Country TEXT,\
                    SHH_Shares_Owned TEXT)"""
    DBTables.setdefault("ATE_Upload", sql_command)

    sql_command = """CREATE TABLE Address (AID TEXT NOT NULL , 'AD_Address_ID', 'Address_Addendum', `Street_Nr` , `ZIP` , `City` , \
                `County` , `State` , `Country` , `GKZ_X` , `GKZ_Y` , `GKZ_Street` , `Last_Change`)"""
    DBTables.setdefault("Address", sql_command)

    sql_command = """CREATE TABLE BDMaster (Branchenode TEXT, Branchencode_Beschreibung TEXT, Einschluss TEXT,  \
                    Ausschluss TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("BDMaster", sql_command)

    sql_command = """CREATE TABLE Branchen_Upload (Company TEXT, BvDID TEXT, CrefoID TEXT, \
                    WZ2008_Haupt_Code TEXT, WZ2008_Nebentätigkeit_Code TEXT, \
                    BD_Main_Code TEXT, BD_Secondary_Code TEXT)"""
    DBTables.setdefault("Branchen_Upload", sql_command)

    sql_command = """CREATE TABLE Branch_Codes (AID TEXT, Branch_Code TEXT, Branch_Rank_Main_Sec TEXT, Source TEXT, \
                    Erfassungsatum TEXT)"""
    DBTables.setdefault("Branch_Codes", sql_command)

    sql_command = """CREATE TABLE Entity (`AID` TEXT, `Entity_Last_Name` TEXT, `Entity_First_Name` TEXT, `Address_Ownership` TEXT, \
                `Entity_Type` TEXT, `Legal_Form` TEXT, `Salutation` TEXT, `Titel` TEXT, `Education` TEXT, `Gender` TEXT, \
                `Birth_Year` TEXT, `Comparison_Group_Code` TEXT, `Comparison_Group_Size` TEXT, `Count_Shareholders` TEXT,\
                `Count_Group_Companies` TEXT, `Count_Subsidiaries` TEXT, `UID` TEXT,\
                `Reserved` TEXT, `Expiry_Date` TEXT, `Last_Change` TEXT )"""
    DBTables.setdefault("Entity", sql_command)

    sql_command = """CREATE TABLE FIN_Upload (CrefoID TEXT, Year INT, Type TEXT, Value REAL)"""
    DBTables.setdefault("FIN_Upload", sql_command)

    sql_command = """CREATE TABLE Financials (AID TEXT NOT NULL, Year INTEGER NOT NULL, Fin_Value_Type TEXT, Fin_Value REAL, \
                    Erfassungsatum TEXT)"""
    DBTables.setdefault("Financials", sql_command)

    sql_command = """CREATE TABLE Contact (CrefoID TEXT, Contact_Type TEXT, Contact_Value TEXT, Datum TEXT)"""
    DBTables.setdefault("Contact", sql_command)

    sql_command = """CREATE TABLE MAN (AID_Company TEXT NOT NULL, AID_MAN TEXT NOT NULL, MAN_Function TEXT, \
                    MAN_Position TEXT, MAN_Function_Mailings TEXT, Erfassungsatum TEXT)"""
    DBTables.setdefault("MAN", sql_command)

    sql_command = """CREATE TABLE MAN_Upload (Company TEXT, BvDID TEXT, CrefoID TEXT, \
                    MAN_CrefoID TEXT, MAN_Type TEXT, MAN_Titel TEXT, MAN_Salutation TEXT,\
                    MAN_Last_Name TEXT, MAN_First_Name TEXT, MAN_Name_Part_3 TEXT, \
                    MAN_Gender TEXT, MAN_Function TEXT, MAN_Birth_Year TEXT, MAN_Function_Mailings TEXT, \
                    MAN_Street_Nr TEXT, MAN_City TEXT, MAN_Postleitzahl TEXT, MAN_Country TEXT)"""
    DBTables.setdefault("MAN_Upload", sql_command)

    sql_command = """CREATE TABLE Role (`id` INTEGER UNIQUE, `name` TEXT UNIQUE, PRIMARY KEY(id) )"""
    DBTables.setdefault("Role", sql_command)

    sql_command = """CREATE TABLE TFIDF (`AID` TEXT, `Name` TEXT, `Business_Activity` TEXT, `Branch_Code_Main` TEXT, `Branch_Codes_Secondaries` TEXT, `Internet` TEXT,\
                PRIMARY KEY(AID))"""
    DBTables.setdefault("TFIDF", sql_command)

    sql_command = """CREATE TABLE TTG_Upload (CrefoID TEXT, Business_Activity TEXT)"""
    DBTables.setdefault("TTG_Upload", sql_command)

    sql_command = """CREATE TABLE Business_Activity (AID TEXT NOT NULL, Activity_Description_ROC TEXT, Last_Change TEXT)"""
    DBTables.setdefault("Business_Activity", sql_command)

    sql_command = """CREATE TABLE Unternehmen_Upload (Company TEXT NOT NULL, BvDID , CrefoID TEXT NOT NULL, \
                Strasse TEXT, Postleitzahl TEXT, City TEXT, State TEXT, Country TEXT, Count_Shareholders INTEGER,  \
                Phone TEXT, URL TEXT, Email TEXT, WZ2008_Haupt_Code TEXT,  BD_Main_Code TEXT, \
                Date_Incorporation TEXT, Legal_Form TEXT, Letzter_Umsatz_tsd_EUR INTEGER, Last_Turnover_Year INTEGER,\
                Last_Count_Employees INTEGER, Last_Count_Employees_Year INTEGER, \
                ROC_number TEXT, UID TEXT, Count_Subsidiaries INTEGER, Count_Group_Companies\
                INTEGER, Comparison_Group_Name TEXT, Comparison_Group_Description TEXT, Comparison_Group_Size INTEGER, \
                GKZ_Street INTEGER, GKZ_X INTEGER, GKZ_Y INTEGER) """
    DBTables.setdefault("Unternehmen_Upload", sql_command)

    sql_command = """CREATE TABLE User (`id` INTEGER UNIQUE, `email` TEXT UNIQUE, `username` TEXT UNIQUE,\
                `password_hash` TEXT, `is_admin` INTEGER, PRIMARY KEY(id))"""
    DBTables.setdefault("User", sql_command)

    sql_command = """CREATE TABLE Vergleichsgruppe (Vergleichsgruppe TEXT NOT NULL, V_Beschreibung TEXT, Datum DATETIME )"""
    DBTables.setdefault("Vergleichsgruppe", sql_command)

    sql_command = """CREATE TABLE Websites (`AID` TEXT, `Content` TEXT, `Weblink` TEXT)"""
    DBTables.setdefault("Websites", sql_command)

    sql_command = """CREATE TABLE Salesforce_Kontakte (`ID` TEXT, `AID__c` TEXT, `LastName` TEXT, 'AccountId' TEXT, \
                'OwnerId' TEXT, 'ATE_Funktion_Mailing__c' TEXT, 'ATE_Funktion__c' TEXT, 'Anteile__c' TEXT, \
                'Ausbildung__c' TEXT, 'Email' TEXT, 'Fax' TEXT, 'FirstName' TEXT, 'Geburtsjahr__c' TEXT, \
                'Geschlecht__c' TEXT, 'Kommunikationssperre__c' TEXT, 'MAN_Funktion_Mailing__c' TEXT, \
                'MailingAdress_city' TEXT, 'MailingAdress_country' TEXT, 'MailingAdress_countryCode' TEXT, \
                'MailingAddress_postalCode' TEXT, 'MailingAddress_state' TEXT, 'MailingAddress_street' TEXT, \
                'Mailingsperre__c' TEXT, 'Manager_Funktion__c' TEXT, 'MobilePhone' TEXT, 'Name' TEXT, \
                'OtherAddress_city' TEXT, 'OtherAdress_country' TEXT, 'OtherAddress_countryCode' TEXT, \
                'OtherAddress_postalCode' TEXT, 'OtherAddress_state' TEXT, 'OtherAddress_street' TEXT, \
                'Phone' TEXT, 'Position__c' TEXT, 'Salutation' TEXT, 'Sperrgrund_Kommunikationssperre__c' TEXT, \
                'Title' TEXT, 'Vertrauliche_E_Mail__c' TEXT, 'Vertrauliche_Privatnummer__c' TEXT, 'AccountTyp__c' TEXT)"""
    DBTables.setdefault("Salesforce_Kontakte", sql_command)

    sql_command = """CREATE TABLE Salesforce_Accounts (`ID` TEXT, `AID__c` TEXT, `Name` TEXT, 'OwnerId' TEXT, \
                'Ablaufdatum_der_Reservierung__c' TEXT, 'Accounttyp__c' TEXT, 'Adressprovider__c' TEXT, 'Adresszusatz__c' TEXT, \
                'Aktiv__c' TEXT, 'Anzahl_der_Gesellschaften__c' TEXT, 'Anzahl_der_Gruppenunternehmen__c' TEXT, \
                'Anzahl_der_Tochtergesellschaften__c' TEXT, 'Avandil_Branche__c' TEXT, 'BD_Hauptcode__c' TEXT, \
                'BD_Nebencode__c' TEXT, 'BillingAddress_city' TEXT, 'BillingAddress_country' TEXT, \
                'BillingAddress_countryCode' TEXT, 'BillingAddress_postalCode' TEXT, 'BillingAddress_state' TEXT, \
                'BillingAddress_street' TEXT, 'Description' TEXT, 'E_Mail__c' TEXT, 'Eigentumsadresse__c' TEXT, \
                'Erfassungsdatum__c' TEXT, 'Fax' TEXT, 'Financials_Provider__c' TEXT, \
                'GKZ_Strasse__c' TEXT, 'GKZ_X__c' TEXT, 'GKZ_Y__c' TEXT, 'Gruendungsjahr__c' TEXT, \
                'Landkreis__c' TEXT, 'Mitarbeiter_laut_Provider_2012__c' TEXT, \
                'Mitarbeiter_laut_Provider_2013__c' TEXT, 'Mitarbeiter_laut_Provider_2014__c' TEXT, \
                'Mitarbeiter_laut_Provider_2015__c' TEXT, 'Mitarbeiter_laut_Provider_2016__c' TEXT, \
                'ParentId' TEXT, 'Phone' TEXT, 'Rechtsform__c' TEXT, 'Reserviert__c' TEXT, \
                'Reserviert_von_PLU__c' TEXT, 'ShippingAddress_city' TEXT, \
                'ShippingAddress_country' TEXT, 'ShippingAddress_countryCode' TEXT, 'ShippingAddress.postalCode' TEXT, \
                'ShippingAddress_state' TEXT, 'ShippingAddress_street' TEXT, 'Sperrgrund__c' TEXT, \
                'Taetigkeitsbeschreibung__c' TEXT, 'Telefon_2__c' TEXT, 'UID__c' TEXT, 'Umsatz_laut_Provider_2012__c' TEXT, \
                'Umsatz_laut_Provider_2013__c' TEXT, 'Umsatz_laut_Provider_2014__c' TEXT, 'Umsatz_laut_Provider_2015__c' TEXT, \
                'Umsatz_laut_Provider_2016__c' TEXT, 'Vergleichsgruppe_Code__c' TEXT, 'Vergleichsgruppe_Size__c' TEXT, \
                'WZ_08_Hauptcode__c' TEXT, 'WZ_08_Nebencode__c' TEXT, 'Website' TEXT, 'letzte_anzahl_mitarbeiter_laut_avandil__c' TEXT, \
                'letzte_anzahl_mitarbeiter_laut_provider__c' TEXT, 'letzter_umsatz_laut_avandil__c' TEXT, \
                'letzter_umsatz_laut_provider__c' TEXT)"""
    DBTables.setdefault("Salesforce_Accounts", sql_command)

    return DBTables


"""----------
Funktion: DBIndex
Zweck: Gibt ein Dict mit den Datenbanktabellen sowie den SQL-Statements zum Erzeugen zurück
----------"""


def DBIndex():
    DBIndex = {}

    sql_command = "CREATE INDEX idx_ATE_FIRMA ON ATE (`AID_Company` ASC)"
    DBIndex.setdefault("idx_ATE_Firma", sql_command)

    sql_command = "CREATE INDEX idx_ATE_PERSON ON ATE (`AID_SHH` ASC)"
    DBIndex.setdefault("idx_ATE_Person", sql_command)

    sql_command = "CREATE INDEX `idx_ATE_DATUM` ON `ATE` ( `Last_Change` )"
    DBIndex.setdefault("idx_ATE_DATUM", sql_command)

    sql_command = "CREATE UNIQUE INDEX idx_ATE_unique ON ATE (`AID_Company` ASC, `AID_SHH` ASC)"
    DBIndex.setdefault("idx_ATE_FIRMA_UNIQUE", sql_command)

    sql_command = "CREATE INDEX idx_Adressen ON Address (`AID` ASC)"
    DBIndex.setdefault("idx_Adressen", sql_command)

    sql_command = "CREATE INDEX idx_Financials ON Financials (`AID` ASC)"
    DBIndex.setdefault("idx_Financials", sql_command)

    sql_command = "CREATE UNIQUE INDEX `idx_Financials_unique` ON `Financials` (`AID` ASC, `Year` ASC, `Fin_Value_Type` ASC)"
    DBIndex.setdefault("idx_Financials_unique", sql_command)

    sql_command = "CREATE INDEX idx_Websites ON Websites (`AID` ASC)"
    DBIndex.setdefault("idx_Websites", sql_command)

    sql_command = "CREATE INDEX idx_MAN_FIRMA ON MAN (`AID_Company` ASC)"
    DBIndex.setdefault("idx_MAN_Firma", sql_command)

    sql_command = "CREATE INDEX idx_MAN_PERSON ON MAN (`AID_MAN` ASC)"
    DBIndex.setdefault("idx_MAN_Person", sql_command)

    sql_command = "CREATE INDEX idx_Branchencodes ON Branch_Codes (`AID` ASC)"
    DBIndex.setdefault("idx_Branchencodes", sql_command)

    sql_command = "CREATE INDEX idx_Entity ON Entity (`AID` ASC)"
    DBIndex.setdefault("idx_Entity", sql_command)

    sql_command = "CREATE INDEX idx_Taetigkeit ON Business_Activity (`AID` ASC)"
    DBIndex.setdefault("idx_Taetigkeit", sql_command)

    sql_command = "CREATE INDEX idx_TFIDF ON TFIDF (`AID` ASC)"
    DBIndex.setdefault("idx_TFIDF", sql_command)

    sql_command = "CREATE INDEX `idx_Salesforce_Accounts` ON `Salesforce_Accounts` ( `AID__c` ASC, `AID__c` ASC )"
    DBIndex.setdefault("idx_Salesforce_Accounts", sql_command)

    sql_command = "CREATE INDEX `idx_Salesforce_Kontakte` ON `Salesforce_Kontakte` ( `AccountId` ASC, `AID__c` ASC )"
    DBIndex.setdefault("idx_Salesforce_Kontakte", sql_command)

    return DBIndex


"""----------
Funktion: Create_Table
Zweck: Erzeugt eine Tabelle in der AVAD-DB
----------"""


def Create_Table(tabelle, text, db_file):
    datenbank = sqlite3.connect(db_file)
    cursor = datenbank.cursor()
    text.insert(INSERT, "Datenbank " + db_file + " geöffnet" + "\n\n")
    sql_command = """SELECT * FROM sqlite_master WHERE type = 'table' AND name = """ + "'" + tabelle + "'"
    try:
        cursor.execute(sql_command)
        result = cursor.fetchone()
    except:
        result = None
    if result != None:
        sql_command = """DROP TABLE """ + "'" + tabelle + "'"
        cursor.execute(sql_command)
    Tables = DBTables()
    sql_command = Tables[tabelle]
    try:
        cursor.execute(sql_command)
        text.insert("end", "\nTabelle " + tabelle + " in Datenbank \n" + db_file + "\n\nerzeugt. \n")
    except:
        messagebox.showinfo("TABELLE ERZEUGEN", "Tabelle " + tabelle + " konnte nicht erzeugt werden.")
        text.insert("end", (traceback.format_exc() + "\n"))
    return


"""----------
Funktion: Create_Index
Zweck: Erzeugt eine Tabelle in der AVAD-DB
----------"""


def Create_Index(index, text, db_file):
    datenbank = sqlite3.connect(db_file)
    cursor = datenbank.cursor()
    text.insert(INSERT, "Datenbank " + db_file + " geöffnet" + "\n\n")
    sql_command = """DROP INDEX """ + "'" + index + "'"
    try:
        cursor.execute(sql_command)
    except:
        pass
    Indexes = DBIndex()
    sql_command = Indexes[index]
    try:
        cursor.execute(sql_command)
        text.insert("end", "\nIndex " + index + " in Datenbank \n" + db_file + "\n\nerzeugt. \n")
    except:
        messagebox.showinfo("INDEX ERZEUGEN", "Index " + index + " konnte nicht erzeugt werden.")
        text.insert("end", (traceback.format_exc() + "\n"))
    return


"""----------
Funktion: HilfeText
Zweck: Gibt den im fenster angezeigten Hilfetext zurück
----------"""


def HilfeText(text):
    text.insert(END, " 0. Datei \n", "u")
    text.insert(END, " Dateimenü mit generellen Inhalten.\n\n")
    text.insert(END, " 0.1. Bildschirm löschen\n", "u")
    text.insert(END, " Löscht den Inhalt des Textfensters\n\n")
    text.insert(END, " 0.2. Hilfe anzeigen\n", "u")
    text.insert(END, " Zeigt diesen Hilfetext an\n\n")
    text.insert(END, " 0.3 Bildschirminhalt Speichern - Rohdaten\n", "u")
    text.insert(END, " Speichert den Inhalt des Textfensters in einer Datei\n")
    text.insert(END, " ohne Formatierung - genaus so wie im Bildschirm gezeigt\n\n")
    text.insert(END, " 0.4 Bildschirminhalt Speichern - Formatiert\n", "u")
    text.insert(END, " Formatiert den Bildschirminhalt vor dem Speichern\n")
    text.insert(END, " zur weiteren bearbeitung mit AVAD\n\n")
    text.insert(END, " 0.5 Beenden\n", "u")
    text.insert(END, " Beendet das Programm\n\n")
    text.insert(END, " 1.1. Automatische Datenübernahme\n", "u")
    text.insert(END, " Startet die automatische Datenübernahme aus Markus (nicht für Activity_Description_ROC)\n\n")
    text.insert(END, " 1.2. Automatische Datenübernahme - Activity_Description_ROC\n", "u")
    text.insert(END, " Startet die automatische Datenübernahme aus Markus für die Activity_Description_ROC\n")
    text.insert(END, " Diese kann wegen eines anderen Seitenformats nicht mit der Standarübernahme erfolgen\n\n")
    text.insert(END, " 1.3. Markus Kalibrieren - Nächste Seite\n", "u")
    text.insert(END, " Kalibrieren des Mauszeigers auf den Button für die\n")
    text.insert(END, " nächste Seite. Vor jeder Datenübernahme durchführen !\n\n")
    text.insert(END, " 1.4. Markus Kalibrieren - Seitenzahl\n", "u")
    text.insert(END, " Kalibrieren des Mauszeigers zum Auslesen der\n")
    text.insert(END, " Seitenzahl. Vor jeder Datenübernahme durchführen !\n\n")
    text.insert(END, " 1.5. Markus Kalibrieren - Leeres Feld\n", "u")
    text.insert(END, " Kalibrieren des Mauszeigers auf leeren Inhalt\n")
    text.insert(END, " Vor jeder Datenübernahme durchführen !\n\n")
    text.insert(END, " 1.6. Markus Kalibrieren - Seitenzahl testen\n", "u")
    text.insert(END, " Liest die aktuelle Seitanzahl aus Markus aus\n")
    text.insert(END, " Vor jeder Datenübernahme durchführen !\n\n")
    text.insert(END, " 2. Externe Daten \n", "u")
    text.insert(END, " Verarbeiten von Daten aus dem Dowanload.\n\n")
    text.insert(END, " 2.1. Excel-Übernahme aus dem Textfenster\n", "u")
    text.insert(END, " Erstellt eine Excel-Datei aus dem Markus-Inhalt des Textfensters.\n")
    text.insert(END, " Dieser Inhalt muß vorher durch Copy / Paste aus einer Markus-Liste\n")
    text.insert(END, " eingefügt werden \n\n")
    text.insert(END, " 2.2 Excel-Übernahme aus Datei \n", "u")
    text.insert(END, " Erstellt eine Excel-Datei aus einer txt-Datei welche von Markus\n")
    text.insert(END, " heruntergeladen wurde \n\n")
    text.insert(END, " 2.3. Datenübernahme aus Datei \n", "u")
    text.insert(END, " Daten werden aus einer txt-Datei (Markus.Dowbload) in die\n")
    text.insert(END, " AVAD-Datenbank übernommen. \n")
    text.insert(END, " Diese Daten werden in eine temporäre Zwischentabelle kopiert,\n")
    text.insert(END, " um diese ggf. noch einmal ansehen zu können \n")
    text.insert(END, " Um die Daten endgültig in AVAD zu übernehmen, muß Schritt \n")
    text.insert(END, " 2.5 ausgeführt werden. \n\n")
    text.insert(END, " 2.4. Datenübernahme aus Verzeichnis \n", "u")
    text.insert(END, " Wie 2.3. - jedoch wird ein ganzes Verzeichnis übernommen\n\n")
    text.insert(END, " 2.5. Datenbank-Update \n", "u")
    text.insert(END, " Alle Daten aus temporären Zwischentabellen werden in AVAD übernommen")
    return


"""----------
Funktion: fill_sf_record
Zweck: Füllt ein Dict vom Type "Salesforce" mit den entsprechenden Daten
----------"""


def fill_sf_record(cursor_sf, sf_dict, errlog):
    sql = "SELECT * from Entity where AID = '" + sf_dict["AID__c"] + "'"
    cursor_sf.execute(sql)
    Entity = cursor_sf.fetchone()
    if Entity:
        sf_dict["LastName"] = Entity[1].strip()
        sf_dict["FirstName"] = Entity[2].strip()
        sf_dict["Name"] = (sf_dict["FirstName"] + " " + sf_dict["LastName"]).strip()
        sf_dict["AccountTyp__c"] = Entity[4].strip()
        sf_dict["Salutation"] = Entity[6].strip()
        sf_dict["Title"] = Entity[7].strip()
        sf_dict["Ausbildung__c"] = Entity[8].strip()
        sf_dict["Geschlecht__c"] = Entity[9].strip()
        sf_dict["Geburtsjahr__c"] = Entity[10].strip()
    else:
        missing_aid = sf_dict["AID__c"] + "\n"
        f = open(errlog, "a")
        f.write(missing_aid)
        f.close
        return  # war im Original "continue" um zum nächsten Record zu gehen

    sql = "SELECT * from Contact where AID = '" + sf_dict["AID__c"] + "' AND Contact_Type = 'Phone'"
    cursor_sf.execute(sql)
    telefon = cursor_sf.fetchone()
    if telefon:
        sf_dict["Phone"] = telefon[2].strip()
    sql = "SELECT * from Contact where AID = '" + sf_dict["AID__c"] + "' AND Contact_Type = 'EMail'"
    cursor_sf.execute(sql)
    email = cursor_sf.fetchone()
    if email:
        sf_dict["Email"] = email[2].strip()

    sql = "SELECT * from Address where AID = '" + sf_dict["AID__c"] + "' AND (AD_Address_ID = '0' OR AD_Address_ID = '99')"
    cursor_sf.execute(sql)
    adresse = cursor_sf.fetchone()
    if adresse:
        sf_dict["OtherAdress_city"] = adresse[5].strip()
        sf_dict["OtherAdress_country"] = adresse[8].strip()
        sf_dict["OtherAdress_postalCode"] = adresse[4].strip()
        sf_dict["OtherAdress_state"] = adresse[7].strip()
        sf_dict["OtherAdress_street"] = adresse[3].strip()

    sql = "SELECT * from Address where AID = '" + sf_dict["AccountId"] + "' AND (AD_Address_ID = '0' OR AD_Address_ID = '99')"
    cursor_sf.execute(sql)
    adresse = cursor_sf.fetchone()
    if adresse:
        sf_dict["MailingAdress_city"] = adresse[5].strip()
        sf_dict["MailingAdress_country"] = adresse[8].strip()
        sf_dict["MailingAdress_postalCode"] = adresse[4].strip()
        sf_dict["MailingAdress_state"] = adresse[7].strip()
        sf_dict["MailingAdress_street"] = adresse[3].strip()

    sql = "SELECT * from MAN where AID_Company = '" + sf_dict["AccountId"] + "' AND  AID_MAN = '" + sf_dict[
        "AID__c"] + "'"
    cursor_sf.execute(sql)
    man = cursor_sf.fetchone()
    if man:
        sf_dict["Manager_Funktion__c"] = man[2].strip()
        sf_dict["MAN_Funktion_Mailing__c"] = man[3].strip()
        sf_dict["Position__c"] = man[4].strip()

    sql = "INSERT INTO Salesforce_Kontakte VALUES (:ID, :AID__c, :LastName, :AccountId, :OwnerId, :ATE_Funktion_Mailing__c, \
        :ATE_Funktion__c, :Anteile__c, :Ausbildung__c, :Email, :Fax, :FirstName, :Geburtsjahr__c, :Geschlecht__c, \
        :Kommunikationssperre__c, :MAN_Funktion_Mailing__c, :MailingAdress_city, :MailingAdress_country, :MailingAdress_countryCode, \
        :MailingAdress_postalCode, :MailingAdress_state, :MailingAdress_street, :Mailingsperre__c, :Manager_Funktion__c, \
        :MobilePhone, :Name, :OtherAdress_city, :OtherAdress_country, :OtherAdress_countryCode, :OtherAdress_postalCode, \
        :OtherAdress_state, :OtherAdress_street, :Phone, :Position__c, :Salutation, :Sperrgrund_Kommunikationssperre__c, \
        :Title, :Vertrauliche_E_Mail__c, :Vertrauliche_Privatnummer__c, :AccountTyp__c)"
    cursor_sf.execute(sql, sf_dict)


"""----------------------------------
Hauptteil des Programms
--------------------------------"""


# Pfad = "C:\\Users\\Holger\\Documents\\Ablage\\Projekte\\Avandil\\Python\\Daten\\"
# Pfad = "C:\\Users\\Holger\\Documents\\Ablage\\Projekte\\Avandil\\Python\\"

class MyApp(tkinter.Frame):
    def __init__(self, master):
        super().__init__(master)
        master.minsize(width=700, height=500)
        self.text = Text(master, height=40, width=70)
        S = Scrollbar(master)
        S.pack(side=RIGHT, fill=Y)
        S.config(command=self.text.yview)
        self.text.config(yscrollcommand=S.set)
        self.text.pack()
        self.pack()

        self.text.tag_config("u", underline=True)

        HilfeText(self.text)

        self.menuBar = tkinter.Menu(master)
        master.config(menu=self.menuBar)

        self.fillMenuBar()

    def fillMenuBar(self):
        self.menuFile = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuMarkus = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuExtern = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuDBAdmin = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuEinlesen = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuEinstellungen = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuSFExport = tkinter.Menu(self.menuBar, tearoff=False)
        self.menuVorversion = tkinter.Menu(self.menuBar, tearoff=False)

        self.menuMarkus.add_command(label="1.1 Automatische Datenübernahme", command=self.Automatisch_übernehmen)
        self.menuMarkus.add_command(label="1.2 Automatische Datenübernahme - Activity_Description_ROC",
                                    command=self.Automatisch_übernehmen_TTG)
        self.menuMarkus.add_separator()
        self.menuMarkus.add_command(label="1.3 Markus Kalibrieren - Nächste Seite", command=self.Calibrate_NextPage)
        self.menuMarkus.add_command(label="1.4 Markus Kalibrieren - Seitenzahl", command=self.Calibrate_PageLoc)
        self.menuMarkus.add_command(label="1.5 Markus Kalibrieren - Leeres Feld", command=self.Calibrate_EmptyField)
        self.menuMarkus.add_separator()
        self.menuMarkus.add_command(label="1.6 Markus Kalibrieren - Seitenzahl Testen", command=self.Test_PageLoc)

        self.menuExtern.add_command(label="2.1. Excel-Übernahme aus Textfenster", command=self.Direkt_übernehmen)
        self.menuExtern.add_command(label="2.2. Excel-Übernahme aus Datei", command=self.Excel_konvertieren)
        self.menuExtern.add_separator()
        self.menuExtern.add_command(label="2.3. Datenübernahme aus Datei", command=self.Datei_konvertieren)
        self.menuExtern.add_command(label="2.4. Datenübernahme aus Verzeichnis", command=self.Verzeichnis_konvertieren)
        self.menuExtern.add_separator()
        self.menuExtern.add_command(label="2.5. Datenbank-Update (zwingend nach Datenübernahme !)",
                                    command=self.update_db)
        self.menuExtern.add_separator()
        self.menuExtern.add_command(label="2.6. Update TFIDF", command=self.update_tfidf)
        self.menuExtern.add_separator()
        self.menuExtern.add_command(label="Daten prüfen", command=self.Daten_Pruefen)

        self.menuDBAdmin.add_command(label="3.1 Neue Datenbank erzeugen", command=self.Neue_Datenbank)
        self.menuDBAdmin.add_command(label="3.2 Unlock Datenbank", command=self.Unlock_Database)
        self.menuDBAdmin.add_separator()
        self.menuDBAdmin.add_command(label="3.3a. Entity ohne WZ-Code", command=self.consistency_Check_WZ)
        self.menuDBAdmin.add_command(label="3.3b. Entity ohne ATE", command=self.consistency_Check_ATE)
        self.menuDBAdmin.add_command(label="3.3c. Entity ohne MAN", command=self.consistency_Check_MAN)
        self.menuDBAdmin.add_command(label="3.3d. Entity ohne Financials", command=self.consistency_Check_FIN)
        self.menuDBAdmin.add_command(label="3.3e. Entity ohne Activity_Description_ROC",
                                     command=self.consistency_Check_TTG)
        self.menuDBAdmin.add_command(label="3.3f. Entity ohne TFIDF", command=self.consistency_Check_TFIDF)
        self.menuDBAdmin.add_separator()
        self.menuDBAdmin.add_command(label="3.4a. WZ-Code ohne Entity", command=self.entity_Check_WZ)
        self.menuDBAdmin.add_command(label="3.4b. ATE ohne Entity", command=self.entity_Check_ATE)
        self.menuDBAdmin.add_command(label="3.4c. MAN ohne Entity", command=self.entity_Check_MAN)
        self.menuDBAdmin.add_command(label="3.4d. Financials ohne Entity", command=self.entity_Check_FIN)
        self.menuDBAdmin.add_command(label="3.4e. Activity_Description_ROC ohne Entity", command=self.entity_Check_TTG)
        self.menuDBAdmin.add_command(label="3.4f. TFIDF ohne Entity", command=self.entity_Check_TFIDF)
        self.menuDBAdmin.add_separator()
        self.menuDBAdmin.add_command(label="3.5. Demodatenbank erzeugen", command=self.Create_Demo_Database)

        self.menuSFExport.add_command(label="4.1 Account-Export vorbereiten",
                                      command=self.Salesforce_Accounts_Vorbereiten)
        self.menuSFExport.add_command(label="4.2 Kontakte-Export vorbereiten",
                                      command=self.Salesforce_Kontakte_Vorbereiten)
        self.menuSFExport.add_separator()
        self.menuSFExport.add_command(label="4.3 Accounts exportieren", command=self.Salesforce_Accounts_Exportieren)
        self.menuSFExport.add_command(label="4.4 Kontakte-Export vorbereiten",
                                      command=self.Salesforce_Kontakte_Exportieren)

        self.menuFile.add_command(label="0.1 Bildschirm löschen", command=self.ClearScreen)
        self.menuFile.add_separator()
        self.menuFile.add_command(label="0.2 Hilfe anzeigen", command=self.ShowHelp)
        self.menuFile.add_separator()
        self.menuFile.add_command(label="0.3 Bildschirminhalt Speichern - Rohdaten", command=self.SaveFile_raw)
        self.menuFile.add_command(label="0.4 Bildschirminhalt Speichern - Formatiert", command=self.SaveFile_formatted)
        self.menuFile.add_separator()
        self.menuFile.add_command(label="0.5 Beenden", command=self.quit)

        self.menuBar.add_cascade(label="0. Datei", menu=self.menuFile)
        self.menuBar.add_cascade(label="1. Markus-Übernahme", menu=self.menuMarkus)
        self.menuBar.add_separator()
        self.menuBar.add_cascade(label="2. Externe Daten", menu=self.menuExtern)
        self.menuBar.add_separator()
        self.menuBar.add_cascade(label="3. Datenbank-Manager", menu=self.menuDBAdmin)
        self.menuBar.add_separator()
        self.menuBar.add_cascade(label="4. Salesforce-Export", menu=self.menuSFExport)

    def Automatisch_übernehmen(self):
        self.text.delete(1.0, END)
        Markus_Übernahme(self.text, "Standard")

    def Automatisch_übernehmen_TTG(self):
        self.text.delete(1.0, END)
        Markus_Übernahme(self.text, "Business_Activity")

    def Calibrate_EmptyField(self):
        global EmptyField, shelfFile
        pyautogui.hotkey("altleft", "tab")
        pyautogui.moveTo(EmptyField)
        time.sleep(5)
        position = pyautogui.position()
        pyautogui.hotkey("altleft", "tab")
        x, y = position
        positionstr = "X: " + str(x).rjust(4) + "  Y: " + str(y).rjust(4)
        EmptyField = position
        messagebox.showinfo("Mausposition gelesen", positionstr)
        shelfFile["EmptyField"] = EmptyField
        shelfFile.sync()

    def Calibrate_NextPage(self):
        global NxtScr, shelfFile
        pyautogui.hotkey("altleft", "tab")
        pyautogui.moveTo(NxtScr)
        time.sleep(5)
        position = pyautogui.position()
        pyautogui.hotkey("altleft", "tab")
        x, y = position
        positionstr = "X: " + str(x).rjust(4) + "  Y: " + str(y).rjust(4)
        NxtScr = position
        messagebox.showinfo("Mausposition gelesen", positionstr)
        shelfFile["NxtScr"] = NxtScr
        shelfFile.sync()

    def Calibrate_PageLoc(self):
        global PageLoc, shelfFile
        pyautogui.hotkey("altleft", "tab")
        pyautogui.moveTo(PageLoc)
        time.sleep(5)
        position = pyautogui.position()
        pyautogui.hotkey("altleft", "tab")
        x, y = position
        positionstr = "X: " + str(x).rjust(4) + "  Y: " + str(y).rjust(4)
        PageLoc = position
        messagebox.showinfo("Mausposition gelesen", positionstr)
        shelfFile["PageLoc"] = PageLoc
        shelfFile.sync()

    def ClearScreen(self):
        self.text.delete(1.0, END)

    def Close_Accounts_Export_Window(self):
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        self.text.delete(1.0, END)
        db_file = filedialog.askopenfilename(**options)
        print(db_file)
        datum = self.ex_params.get()
        if datum:
            print(datum)
        self.ex_window.destroy()
        tabelle = "Salesforce_Accounts"
        idx = "idx_Salesforce_Accounts"
        datenbank = sqlite3.connect(db_file)
        datenbank.row_factory = dict_factory
        cursor = datenbank.cursor()
        cursor_sf = datenbank.cursor()
        tables = DBTables()
        sql_create = tables[tabelle]
        try:
            cursor.execute(sql_create)
        except:
            result = messagebox.askquestion("Daten löschen ?", "Salesforce - Accounts enthält Daten\nLöschen ?",
                                            icon="warning")
            if result:
                sql = "DELETE from Salesforce_Accounts"
                cursor.execute(sql)
        index = DBIndex()
        sql_create = index[idx]
        try:
            cursor.execute(sql_create)
        except:
            pass
        datenbank.commit()
        sql = "SELECT COUNT() from Entity where Last_Change >= '" + datum + "' and Entity_Type != 'Person'"
        cursor.execute(sql)
        num = cursor.fetchone()["COUNT()"]
        anzahl = format(num, ",d").replace(",", ".")
        sql = "SELECT * from Entity where Last_Change >= '" + datum + "'"
        cursor.execute(sql)

        max_records = 0  # limit records for testing purposes - set to 0 if not required

        i = 0
        for row in cursor:
            i += 1
            if i > max_records and max_records > 0:
                break
            i_string = format(i, ",d").replace(",", ".")
            #            print(i_string, "von", anzahl, " : ", row)
            print(i_string, "von", anzahl)
            sf_dict = Record_Dict("Salesforce_Accounts")
            sf_dict["AID__c"] = row["AID"].strip()
            sf_dict["Name"] = row["Entity_Last_Name"].strip()
            sf_dict["Ablaufdatum_der_Reservierung__c"] = row["Expiry_Date"].strip()
            sf_dict["Accounttyp__c"] = row["Entity_Type"].strip()
            sf_dict["Adressprovider__c"] = "AVAD"
            sf_dict["Anzahl_der_Gesellschaften__c"] = row["Count_Shareholders"].strip()
            sf_dict["Anzahl_der_Gruppenunternehmen__c"] = row["Count_Group_Companies"].strip()
            sf_dict["Anzahl_der_Tochtergesellschaften__c"] = row["Count_Subsidiaries"].strip()
            sf_dict["Eigentumsadresse__c"] = row["Address_Ownership"].strip()
            sf_dict["Erfassungsdatum__c"] = row["Last_Change"].strip()
            sf_dict["Gruendungsjahr__c"] = row["Birth_Year"].strip()
            sf_dict["Rechtsform__c"] = row["Legal_Form"].strip()
            sf_dict["Vergleichsgruppe_Code__c"] = row["Comparison_Group_Code"].strip()
            sf_dict["Vergleichsgruppe_Size__c"] = row["Comparison_Group_Size"].strip()
            sf_dict["UID__c"] = row["UID"].strip()

            sql = "SELECT * from Contact where AID = '" + sf_dict["AID__c"] + "' AND Contact_Type = 'Phone'"
            cursor_sf.execute(sql)
            telefon = cursor_sf.fetchone()
            if telefon:
                sf_dict["Phone"] = telefon["Contact_Value"].strip()
            sql = "SELECT * from Contact where AID = '" + sf_dict["AID__c"] + "' AND Contact_Type = 'EMail'"
            cursor_sf.execute(sql)
            email = cursor_sf.fetchone()
            if email:
                sf_dict["E_Mail__c"] = email["Contact_Value"].strip()
            sql = "SELECT * from Contact where AID = '" + sf_dict["AID__c"] + "' AND Contact_Type = 'Internet'"
            cursor_sf.execute(sql)
            website = cursor_sf.fetchone()
            if website:
                sf_dict["Website"] = website["Contact_Value"].strip()

            sql = "SELECT * from Branch_Codes where AID = '" + sf_dict["AID__c"] + "' AND Branch_Rank_Main_Sec = 'WZ08H'"
            cursor_sf.execute(sql)
            branchencode = cursor_sf.fetchone()
            if branchencode:
                sf_dict["WZ_08_Hauptcode__c"] = branchencode["Branch_Code"].strip()
            sql = "SELECT * from Branch_Codes where AID = '" + sf_dict["AID__c"] + "' AND Branch_Rank_Main_Sec = 'WZ08N'"
            cursor_sf.execute(sql)
            branchencode_n = ""
            branchencode = cursor_sf.fetchall()
            for record in branchencode:
                branchencode_n = branchencode_n + record["Branch_Code"] + "; "
            sf_dict["WZ_08_Nebencode__c"] = branchencode_n
            sql = "SELECT * from Branch_Codes where AID = '" + sf_dict["AID__c"] + "' AND Branch_Rank_Main_Sec = 'BDH'"
            cursor_sf.execute(sql)
            branchencode = cursor_sf.fetchone()
            if branchencode:
                sf_dict["BD_Hauptcode__c"] = branchencode["Branch_Code"].strip()
            sql = "SELECT * from Branch_Codes where AID = '" + sf_dict["AID__c"] + "' AND Branch_Rank_Main_Sec = 'BDN'"
            cursor_sf.execute(sql)
            branchencode_n = ""
            branchencode = cursor_sf.fetchall()
            for record in branchencode:
                try:
                    to_add = record["Branch_Code"].split()[0]  # some records contain "\n"
                except:
                    continue
                branchencode_n = branchencode_n + to_add + "; "
            sf_dict["BD_Nebencode__c"] = branchencode_n

            sql = "SELECT * from Address where AID = '" + sf_dict["AID__c"] + "' AND AD_Address_ID = '99'"
            cursor_sf.execute(sql)
            adresse = cursor_sf.fetchone()
            if adresse:
                sf_dict["Adresszusatz__c"] = adresse["Address_Addendum"].strip()
                sf_dict["BillingAddress_city"] = adresse["City"].strip()
                sf_dict["BillingAddress_country"] = adresse["Country"].strip()
                sf_dict["BillingAddress_postalCode"] = adresse["ZIP"].strip()
                sf_dict["BillingAddress_state"] = adresse["State"].strip()
                sf_dict["BillingAddress_street"] = adresse["Street_Nr"].strip()
                sf_dict["GKZ_Strasse__c"] = adresse["GKZ_Street"].strip()
                sf_dict["GKZ_X__c"] = adresse["GKZ_X"].strip()
                sf_dict["GKZ_Y__c"] = adresse["GKZ_Y"].strip()
                sf_dict["Landkreis__c"] = adresse["County"].strip()

            years = ["2012", "2013", "2014", "2015", "2016"]
            sql = "SELECT * from Financials where AID = '" + sf_dict["AID__c"] + "'"
            cursor_sf.execute(sql)
            financials = cursor_sf.fetchall()
            for record in financials:
                if record["Year"] in years:
                    year = record["Year"]
                    sf_ma = "Mitarbeiter_laut_Provider_" + year + "__c"
                    sf_ums = "Umsatz_laut_Provider_" + year + "__c"
                    if record["Fin_Value_Type"] == "Mitarbeiter":
                        sf_dict[sf_ma] = record["Fin_Value"]
                    if record["Fin_Value_Type"] == "Turnover":
                        sf_dict[sf_ums] = record["Fin_Value"]

            sql = "SELECT * from Business_Activity where AID = '" + sf_dict["AID__c"] + "'"
            cursor_sf.execute(sql)
            taetigkeit = cursor_sf.fetchone()
            if taetigkeit:
                sf_dict["Taetigkeitsbeschreibung__c"] = taetigkeit["Activity_Description_ROC"].replace("\n", " ")

            sql = "SELECT * from ATE where AID_Company = '" + sf_dict["AID__c"] + "'"
            cursor_sf.execute(sql)
            ate_list = cursor_sf.fetchall()
            try:
                ate = ate_list[0]["AID_SHH"]
            except:
                ate = ""
            ate_anteile = 0.0

            if i == 12205:
                print("Erreicht")

            if len(ate_list) > 1:
                for ate_item in ate_list:
                    ate_anteile_raw = ate_item["SHH_Shares_Owned"]
                    if not ate_anteile_raw:
                        ate_anteile_raw = "0,0"
                    print("ate_anteile_raw: ", ate_anteile_raw)
                    var_type = type(ate_anteile_raw).__name__
                    if var_type == "float":
                        ate_anteile = ate_anteile_raw
                    else:
                        ate_anteile_string = ate_anteile_raw.replace(",", ".")
                        ate_anteile_new = float(ate_anteile_string)
                    if ate_anteile_new > ate_anteile:
                        ate_anteile = ate_anteile_new
                        ate = ate_item["AID_SHH"]
            else:
                if len(ate_list) > 0:
                    ate_anteile_raw = ate_list[0]["SHH_Shares_Owned"]
                    var_type = type(ate_anteile_raw).__name__
                    if var_type == "float":
                        ate_anteile = ate_anteile_raw
                    else:
                        ate_anteile_string = ate_list[0]["SHH_Shares_Owned"].strip().replace(",", ".")
                        if len(ate_anteile_string) > 0:
                            ate_anteile = float(ate_anteile_string)
                        else:
                            ate_anteile = 0

            if ate:
                sql = "SELECT * from Entity where  AID = '" + ate + "'"
                cursor_sf.execute(sql)
                Entity = cursor.fetchone()
                entity_typ = Entity["Entity_Type"]
                if entity_typ != "Person":
                    if ate_anteile > 50:
                        sf_dict["ParentId"] = ate  # ParentID only populated when ATE > 50% AND "Firma"

            sql = "INSERT INTO Salesforce_Accounts VALUES (:Id, :AID__c, :Name, :OwnerId, :Ablaufdatum_der_Reservierung__c, \
                :Accounttyp__c, :Adressprovider__c, :Adresszusatz__c, :Aktiv__c, :Anzahl_der_Gesellschaften__c, :Anzahl_der_Gruppenunternehmen__c, \
                :Anzahl_der_Tochtergesellschaften__c, :Avandil_Branche__c, :BD_Hauptcode__c, :BD_Nebencode__c, :BillingAddress_city, \
                :BillingAddress_country, :BillingAddress_countryCode, :BillingAddress_postalCode, :BillingAddress_state, :BillingAddress_street, \
                :Description, :E_Mail__c, :Eigentumsadresse__c, :Erfassungsdatum__c, :Fax, :Financials_Provider__c, :GKZ_Strasse__c, \
                :GKZ_X__c, :GKZ_Y__c, :Gruendungsjahr__c, :Landkreis__c, :Mitarbeiter_laut_Provider_2012__c, \
                :Mitarbeiter_laut_Provider_2013__c, :Mitarbeiter_laut_Provider_2014__c, :Mitarbeiter_laut_Provider_2015__c, :Mitarbeiter_laut_Provider_2016__c, \
                :ParentId, :Phone, :Rechtsform__c, :Reserviert__c, :Reserviert_von_PLU__c, :ShippingAddress_city, :ShippingAddress_country, \
                :ShippingAddress_countryCode, :ShippingAddress_postalCode, :ShippingAddress_state, :ShippingAddress_street, :Sperrgrund__c, \
                :Taetigkeitsbeschreibung__c, :Telefon_2__c, :UID__c, :Umsatz_laut_Provider_2012__c, :Umsatz_laut_Provider_2013__c, \
                :Umsatz_laut_Provider_2014__c, :Umsatz_laut_Provider_2015__c, :Umsatz_laut_Provider_2016__c, :Vergleichsgruppe_Code__c, \
                :Vergleichsgruppe_Size__c, :WZ_08_Hauptcode__c, :WZ_08_Nebencode__c, :Website, :letzte_anzahl_mitarbeiter_laut_avandil__c, \
                :letzte_anzahl_mitarbeiter_laut_provider__c, :letzter_umsatz_laut_avandil__c, :letzter_umsatz_laut_provider__c)"

            cursor_sf.execute(sql, sf_dict)
            datenbank.commit()
        print("Accounts eingefügt")
        messagebox.showinfo("Tabelle erzeugt", "Accounts wurden in die Tabelle eingefügt")

    def Close_Kontakte_Export_Window(self):
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        self.text.delete(1.0, END)
        db_file = filedialog.askopenfilename(**options)
        print(db_file)
        datum = self.ex_params.get()
        if datum:
            print(datum)
        self.ex_window.destroy()
        tabelle = "Salesforce_Kontakte"
        idx = "idx_Salesforce_Kontakte"
        datenbank = sqlite3.connect(db_file)
        cursor = datenbank.cursor()
        cursor_sf = datenbank.cursor()
        tables = DBTables()
        sql_create = tables[tabelle]
        try:
            cursor.execute(sql_create)
        except:
            result = messagebox.askquestion("Daten löschen ?", "Salesforce - Kontakte enthält Daten\nLöschen ?",
                                            icon="warning")
            if result:
                sql = "DELETE from Salesforce_Kontakte"
                cursor.execute(sql)
        index = DBIndex()
        sql_create = index[idx]
        try:
            cursor.execute(sql_create)
        except:
            pass
        datenbank.commit()

        max_records = 0  # limit records for testing purposes - set to 0 if not required

        # Generate contacts from ATE data

        sql = "SELECT COUNT() from ATE where Last_Change >= '" + datum + "' AND AID_SHH != '0000000000'"  # '0000000000' is a contact with no Crefo, see i.e. '9370299738' in Markus
        cursor.execute(sql)
        anzahl = format(cursor.fetchone()[0], ",d").replace(",", ".")
        sql = "SELECT * from ATE where Last_Change >= '" + datum + "' AND AID_SHH != '0000000000'"
        cursor.execute(sql)
        i = 0
        for row in cursor:
            i += 1
            if i > max_records and max_records > 0:
                break
            i_string = format(i, ",d").replace(",", ".")
            print(i_string, "von", anzahl, " : ", row)
            sf_dict = Record_Dict("Salesforce_Kontakte")
            sf_dict["AID__c"] = row[1].strip()
            sf_dict["AccountId"] = row[0].strip()
            sf_dict["ATE_Funktion__c"] = row[2].strip()
            sf_dict["Anteile__c"] = row[3]
            sf_dict["ATE_Funktion_Mailing__c"] = row[5].strip()

            errlog = os.path.split(db_file)[0] + "//errlog.txt"
            fill_sf_record(cursor_sf, sf_dict, errlog)

        print("Alle ATE-Kontakte eingefügt")
        datenbank.commit()

        # Generate or update contacts from MAN data

        sql = "SELECT COUNT() from MAN where Last_Change >= '" + datum + "' AND AID_MAN != '0000000000'"  # '0000000000' is a contact with no Crefo, see i.e. '9370299738' in Markus
        cursor.execute(sql)
        anzahl = format(cursor.fetchone()[0], ",d").replace(",", ".")
        sql = "SELECT * from MAN where Last_Change >= '" + datum + "' AND AID_MAN != '0000000000'"
        cursor.execute(sql)
        i = 0
        for row in cursor:
            i += 1
            if i > max_records and max_records > 0:
                break
            i_string = format(i, ",d").replace(",", ".")
            print(i_string, "von", anzahl, " : ", row)
            sf_dict = Record_Dict("Salesforce_Kontakte")
            sf_dict["AID__c"] = row[1].strip()
            sf_dict["AccountId"] = row[0].strip()

            sql = "Select * from Salesforce_Kontakte where AccountId = '" + sf_dict["AccountId"] + "' and AID__c = '" + \
                  sf_dict["AID__c"] + "'"
            cursor_sf.execute(sql)
            man = cursor_sf.fetchone()
            if man:
                continue  # keine Aktion notwendig, da wenn der Kontakt bereits erfasst ist, alle Daten eigetragen wurden
            else:
                # print("AID_Company: ",sf_dict["AccountId"], "AID_MAN: ", sf_dict["AID__c"], "nicht vorhanden" )
                fill_sf_record(cursor_sf, sf_dict, errlog)

        print("Manager fertig")
        datenbank.commit()
        messagebox.showinfo("Tabelle erzeugt", "Kontakte wurden in die Tabelle eingefügt")

    def Create_Demo_Database(self):
        self.text.delete(1.0, END)
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialdir'] = HomeDir
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Basis-Datenbank auswählen'
        database = filedialog.askopenfilename(**options)
        if not database:
            return

        sample_database = self.Neue_Datenbank()
        msg_text = "Mask Entity names after database is created ?"
        mask_records = messagebox.askyesno("Question", msg_text)
        db = sqlite3.connect(database)
        cursor = db.cursor()
        copy_cursor = db.cursor()
        sql_command = "ATTACH DATABASE '" + sample_database + "' AS sample_db"
        cursor.execute(sql_command)
        sql_command = "SELECT COUNT(*) FROM Entity"
        cursor.execute(sql_command)
        res = cursor.fetchone()
        total_entities = res[0]
        entity_count = format(total_entities, ",").replace(",", ".")
        print("Entities: ", entity_count)
        sample_size = 100
        #        sample_size = int(total_entities * 0.05)
        sample_count = format(sample_size, ",").replace(",", ".")
        print("Samples: ", sample_count)
        sql_command = "select AID from Entity where Entity_Type = 'Firma' order by random() limit " + str(sample_size)
        cursor.execute(sql_command)
        #       Insert Entities into sample database
        i = 0
        for AID in cursor:
            i += 1
            sql_copy = "INSERT INTO  sample_db.ATE SELECT * FROM ATE WHERE AID_Company = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Address SELECT * FROM Address WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Branch_Codes SELECT * FROM Branch_Codes WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Entity SELECT * FROM Entity WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Financials SELECT * FROM Financials WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Contact SELECT * FROM Contact WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.MAN SELECT * FROM MAN WHERE AID_Company = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Business_Activity SELECT * FROM Business_Activity WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.TFIDF SELECT * FROM TFIDF WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Websites SELECT * FROM Websites WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            print("Entity: ", format(i, ",").replace(",", "."))
        #       Insert ATE from sample Entities into sample database
        sql = "SELECT DISTINCT AID_SHH from sample_db.ATE"
        cursor.execute(sql)
        i = 0
        for AID in cursor:
            i += 1
            sql_copy = "INSERT INTO  sample_db.Address SELECT * FROM Address WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Entity SELECT * FROM Entity WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Contact SELECT * FROM Contact WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            print("ATE: ", format(i, ",").replace(",", "."))
        #       Insert MAN from sample Entities into sample database
        sql = "SELECT DISTINCT AID_MAN from sample_db.MAN"
        cursor.execute(sql)
        i = 0
        for AID in cursor:
            i += 1
            sql_copy = "INSERT INTO  sample_db.Address SELECT * FROM Address WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Entity SELECT * FROM Entity WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            sql_copy = "INSERT INTO  sample_db.Contact SELECT * FROM Contact WHERE AID = '" + AID[0] + "'"
            copy_cursor.execute(sql_copy)
            print("MAN: ", format(i, ",").replace(",", "."))

        sql_copy = "INSERT INTO  sample_db.Role SELECT * FROM Role"
        copy_cursor.execute(sql_copy)
        sql_copy = "INSERT INTO  sample_db.User SELECT * FROM User"
        copy_cursor.execute(sql_copy)
        db.commit()
        print("Sample database created")
        if mask_records == True:
            sql = "SELECT * FROM sample_db.Entity"
            cursor.execute(sql)
            count = 0
            for record in cursor:
                count += 1
                name = record[1]
                AID = record[0]
                name = name.replace('"', '')
                length = len(name)
                fill = ""
                if length > 15:
                    left = name[:5]
                    right = name[-5:]
                    for i in range(0, length - 10):
                        fill += "*"
                else:
                    left = name[:int(length / 2)]
                    right = name[-int(len(left) / 2):]
                    for i in range(0, length - len(left) - len(right)):
                        fill += "*"
                name_new = left + fill + right
                sql_update = 'Update sample_db.Entity set Entity_Last_Name = "' + name_new + '" where AID = "' + AID + '"'
                print(count, ": ", AID, " - ", name_new)
                copy_cursor.execute(sql_update)
            db.commit()
            mask_count = format(count, ",").replace(",", ".")
            msg_text = "Sample database created\n" + sample_count + " Entities copied"
            msg_text = msg_text + "\n\nMasking completed\n" + mask_count + " Entities masked"
            messagebox.showinfo("Database created", msg_text)
        else:
            msg_text = "Sample database created\n" + sample_count + " Entities copied"
            messagebox.showinfo("Database created", msg_text)

    def consistency_check(self, check_type):
        self.text.delete(1.0, END)
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialdir'] = HomeDir
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank öffnen'
        database = filedialog.askopenfilename(**options)
        if not database:
            return
        dbdir = os.path.dirname(database)
        if check_type == "WZ-Code":
            start_text = "Starting Consistency check WZ-Code\n"
            result_text = "Entities ohne WZ-Code: "
            export_text = "Consistency Check WZ-Code"
            sql_command = "select AID from Entity where not exists (select * from Branch_Codes where Branch_Codes.AID = Entity.AID) \
                            and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne WZ.csv"
        elif check_type == "ATE":
            start_text = "Starting Consistency check ATE\n"
            result_text = "Entities ohne ATE: "
            export_text = "Consistency Check ATE"
            sql_command = "select AID from Entity where not exists (select * from ATE where ATE.AID_Company = Entity.AID) \
                            and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne ATE.csv"
        elif check_type == "MAN":
            start_text = "Starting Consistency check MAN\n"
            result_text = "Entities ohne MAN: "
            export_text = "Consistency Check MAN"
            sql_command = "select AID from Entity where not exists (select * from MAN where MAN.AID_Company = Entity.AID) \
                        and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne MAN.csv"
        elif check_type == "FIN":
            start_text = "Starting Consistency check FINANCIALS\n"
            result_text = "Entities ohne FINANCIALS: "
            export_text = "Consistency Check FIANCIALS"
            sql_command = "select AID from Entity where not exists (select * from Financials where Financials.AID = Entity.AID) \
                        and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne FIN.csv"
        elif check_type == "TTG":
            start_text = "Starting Consistency check TTG\n"
            result_text = "Entities ohne Description: "
            export_text = "Consistency Check Description"
            sql_command = "select AID from Entity where not exists (select * from Business_Activity where Business_Activity.AID = Entity.AID) \
                        and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne TTG.csv"
        elif check_type == "TFIDF":
            start_text = "Starting Consistency check TFIDF\n"
            result_text = "Entities ohne TFIDF: "
            export_text = "Consistency Check TFIDF"
            sql_command = "select AID from Entity where not exists (select AID from TFIDF where TFIDF.AID = Entity.AID) \
                        and Entity.Entity_Type = 'Firma'"
            export_file = dbdir + "//Entity ohne TTG.csv"
        elif check_type == "Entity-WZ":
            start_text = "Starting Consistency check WZ-Code ohne Entity\n"
            result_text = "WZ-Codes ohne Entity: "
            export_text = "Consistency Check WZ-Code ohne Entity"
            sql_command = "select distinct AID from Branch_Codes where not exists (select * from Entity where Entity.AID = Branch_Codes.AID)"
            export_file = dbdir + "//WZ ohne Entity.csv"
        elif check_type == "Entity-ATE":
            start_text = "Starting Consistency check ATE ohne Entity\n"
            result_text = "ATE ohne Entity: "
            export_text = "Consistency Check ATE ohne Entity"
            sql_command = "select distinct AID_Company from ATE where not exists (select * from Entity where Entity.AID = ATE.AID_Company)"
            export_file = dbdir + "//ATE ohne Entity.csv"
        elif check_type == "Entity-MAN":
            start_text = "Starting Consistency check MAN ohne Entity\n"
            result_text = "MAN ohne Entity: "
            export_text = "Consistency Check MAN ohne Entity"
            sql_command = "select distinct AID_Company from MAN where not exists (select * from Entity where Entity.AID = MAN.AID_Company)"
            export_file = dbdir + "//MAN ohne Entity.csv"
        elif check_type == "Entity-FIN":
            start_text = "Starting Consistency check Financials ohne Entity\n"
            result_text = "Financials ohne Entity: "
            export_text = "Consistency Check Financials ohne Entity"
            sql_command = "select distinct AID from Financials where not exists (select * from Entity where Entity.AID = Financials.AID)"
            export_file = dbdir + "//FIN ohne Entity.csv"
        elif check_type == "Entity-TTG":
            start_text = "Starting Consistency check Description ohne Entity\n"
            result_text = "Description ohne Entity: "
            export_text = "Consistency Check Description ohne Entity"
            sql_command = "select distinct AID from Business_Activity where not exists (select * from Entity where Entity.AID = Business_Activity.AID)"
            export_file = dbdir + "//TTG ohne Entity.csv"
        elif check_type == "Entity-TFIDF":
            start_text = "Starting Consistency check TFIDF ohne Entity\n"
            result_text = "TFIDF ohne Entity: "
            export_text = "Consistency Check TFIDF ohne Entity"
            sql_command = "select distinct AID from TFIDF where not exists (select * from Entity where Entity.AID = TFIDF.AID)"
            export_file = dbdir + "//TFIDF ohne Entity.csv"
        else:
            messagebox.showinfo("Nicht gefunden", "Menüauswahl nicht gefunden")
            return
        self.text.insert("end", start_text)
        db = sqlite3.connect(database)
        cursor = db.cursor()
        try:
            cursor.execute(sql_command)
            res = cursor.fetchall()
            consistency_count = format(len(res), ",").replace(",", ".")
            print(consistency_count)
        except:
            pass

        self.text.insert("end", result_text + str(consistency_count) + "\n\n")
        export_data = messagebox.askyesno(export_text, str(
            consistency_count) + " inkonsistente Datensätze gefunden !\nErgebnis exportieren ? ?")
        if export_data:
            self.export_aid(res, export_file)
            msg_text = "File " + export_file + " created"
            messagebox.showinfo(check_type + "-Check", msg_text)
            print("Exported")

    def consistency_Check_ATE(self):
        self.text.delete(1.0, END)
        self.consistency_check("ATE")

    def consistency_Check_FIN(self):
        self.text.delete(1.0, END)
        self.consistency_check("FIN")

    def consistency_Check_MAN(self):
        self.text.delete(1.0, END)
        self.consistency_check("MAN")

    def consistency_Check_TFIDF(self):
        self.text.delete(1.0, END)
        self.consistency_check("TFIDF")

    def consistency_Check_TTG(self):
        self.text.delete(1.0, END)
        self.consistency_check("TTG")

    def consistency_Check_WZ(self):
        self.text.delete(1.0, END)
        self.consistency_check("WZ-Code")

    def Datei_konvertieren(self):
        self.text.delete(1.0, END)
        Textdatei_konvertieren(self.text, "Database")

    def Daten_Pruefen(self):
        self.text.delete(1.0, END)
        numbers = re.compile("\\d*\\.\\t")
        Nummernliste = []
        options = {}
        options['defaultextension'] = '.txt'
        options['filetypes'] = [('text files', '.txt'), ('All files', '.*')]
        options['initialdir'] = HomeDir
        options['initialfile'] = 'avad.txt'
        options['parent'] = root
        options['title'] = 'Datei öffnen'
        self.text.delete(1.0, END)
        FileName = filedialog.askopenfilename(**options)
        markusfile = open(FileName, encoding="Latin-1")
        Inhalt = markusfile.readlines()
        markusfile.close()
        for Zeile in Inhalt:
            if not Zeile[0].isnumeric():
                continue
            Zahl = numbers.search(Zeile)
            if Zahl:
                Nummer = int(Zahl.group().split()[0][:-1])
                if Nummer:
                    Nummernliste.append(Nummer)
        if not Nummernliste:
            messagebox.showinfo("Daten prüfen", "Company konnten nicht identifiziert werden")
            return
        for i, wert in enumerate(Nummernliste):
            if i == 0:
                continue
            if wert - Nummernliste[i - 1] > 1:
                messagebox.showinfo("Daten prüfen", "Fehler bei: " + str(wert))
                return
        messagebox.showinfo("Daten prüfen", "Company sind ok")

    def Direkt_übernehmen(self):
        #        self.text.delete(1.0, END)
        global HomeDir
        options = {}
        #        options['defaultextension'] = '.xlsx'
        options['filetypes'] = [('All files', '.*'), ('Database files', '.db')]
        options['initialfile'] = 'AVAD.xlsx'
        options['initialdir'] = HomeDir
        options['parent'] = root
        options['title'] = 'Dateiname auswählen'

        excel_file = filedialog.asksaveasfilename(**options)
        if not excel_file:
            return
        Textdatei_konvertieren(self.text, "from_screen", excel_file)

    def entity_Check_ATE(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-ATE")

    def entity_Check_FIN(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-FIN")

    def entity_Check_MAN(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-MAN")

    def entity_Check_TFIDF(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-TFIDF")

    def entity_Check_TTG(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-TTG")

    def entity_Check_WZ(self):
        self.text.delete(1.0, END)
        self.consistency_check("Entity-WZ")

    def Excel_konvertieren(self):
        self.text.delete(1.0, END)
        Textdatei_konvertieren(self.text, "Excel")

    def export_aid(self, aid_list, export_file):
        with open(export_file, "w") as filehandler:
            for item in aid_list:
                filehandler.write(item[0] + "\n")

    def Neue_Datenbank(self):
        self.text.delete(1.0, END)
        global HomeDir
        options = {}
        #        options['defaultextension'] = '.db'
        #        options['filetypes'] = [('All files', '.*'), ('Database files', '.db')]
        options['initialfile'] = 'avad.db'
        options['initialdir'] = HomeDir
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        db_file = filedialog.asksaveasfilename(**options)
        if not db_file:
            return
        Tables = DBTables()
        for table in Tables:
            Create_Table(table, self.text, db_file)
        return db_file

        Indexes = DBIndex()
        for index in Indexes:
            Create_Index(index, self.text, db_file)

    def Salesforce_Accounts_Vorbereiten(self):
        self.ex_window = tkinter.Toplevel(root)
        tkinter.Label(self.ex_window, text="Export ab Datum (JJJJ-MM-TT): ").pack()
        self.ex_params = tkinter.Entry(self.ex_window)
        self.ex_params.pack()
        tkinter.Button(self.ex_window, text="Fertig", command=self.Close_Accounts_Export_Window).pack()

    def Salesforce_Accounts_Exportieren(self):
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        db_file = filedialog.askopenfilename(**options)

        options['defaultextension'] = '.csv'
        options['filetypes'] = [('csv files', '.csv'), ('All files', '.*')]
        options['initialfile'] = 'salesforce.csv'
        options['parent'] = root
        options['title'] = 'Daten speichern'
        export_file = filedialog.asksaveasfilename(**options)

        f_name = os.path.splitext(export_file)[0]
        f_ext = os.path.splitext(export_file)[1]

        csv_delimiter = ","
        max_lines = 300000

        datenbank = sqlite3.connect(db_file)
        datenbank.row_factory = sqlite3.Row
        cursor = datenbank.cursor()

        crsr = datenbank.execute("Select * from Salesforce_Accounts")
        row = crsr.fetchone()
        titles = row.keys()

        datenbank.close()
        datenbank = sqlite3.connect(db_file)
        cursor = datenbank.cursor()

        f = open(export_file, "w", newline="")
        writer = csv.writer(f, delimiter=csv_delimiter, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(titles)

        sql = "Select * from Salesforce_Accounts"
        cursor.execute(sql)
        data = cursor.fetchall()
        file_counter = 0
        i = 0
        for row in data:
            if i == max_lines:
                f.close()
                file_counter += 1
                new_export_file = f_name + "_" + str(file_counter) + f_ext
                f = open(new_export_file, "w", newline="\n")
                writer = csv.writer(f, delimiter=csv_delimiter, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(titles)
                i = 1
            writer.writerow(row)
            i += 1

        f.close()
        print("Daten gespeichert")
        messagebox.showinfo("Daten gespeichert", "Salesforce - Accounts wurden erfolgreich exportiert")

    def Salesforce_Kontakte_Exportieren(self):
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        db_file = filedialog.askopenfilename(**options)

        options['defaultextension'] = '.csv'
        options['filetypes'] = [('csv files', '.csv'), ('All files', '.*')]
        options['initialfile'] = 'salesforce.csv'
        options['parent'] = root
        options['title'] = 'Daten speichern'
        export_file = filedialog.asksaveasfilename(**options)

        f_name = os.path.splitext(export_file)[0]
        f_ext = os.path.splitext(export_file)[1]

        csv_delimiter = ","
        max_lines = 300000

        datenbank = sqlite3.connect(db_file)
        datenbank.row_factory = sqlite3.Row
        cursor = datenbank.cursor()

        crsr = datenbank.execute("Select * from Salesforce_Kontakte")
        row = crsr.fetchone()
        titles = row.keys()

        datenbank.close()
        datenbank = sqlite3.connect(db_file)
        cursor = datenbank.cursor()

        f = open(export_file, "w", newline="")
        writer = csv.writer(f, delimiter=csv_delimiter, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(titles)

        sql = "Select * from Salesforce_Kontakte"
        cursor.execute(sql)
        data = cursor.fetchall()
        file_counter = 0
        i = 0
        for row in data:
            if i == max_lines:
                f.close()
                file_counter += 1
                new_export_file = f_name + "_" + str(file_counter) + f_ext
                f = open(new_export_file, "w", newline="\n")
                writer = csv.writer(f, delimiter=csv_delimiter, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(titles)
                i = 1
            writer.writerow(row)
            i += 1

        f.close()
        print("Daten gespeichert")
        messagebox.showinfo("Daten gespeichert", "Salesforce - Kontakte wurden erfolgreich exportiert")

    def Salesforce_Kontakte_Vorbereiten(self):
        self.ex_window = tkinter.Toplevel(root)
        tkinter.Label(self.ex_window, text="Export ab Datum (JJJJ-MM-TT): ").pack()
        self.ex_params = tkinter.Entry(self.ex_window)
        self.ex_params.pack()
        tkinter.Button(self.ex_window, text="Fertig", command=self.Close_Kontakte_Export_Window).pack()

    def SaveFile_raw(self):
        Datei_speichern(self.text, "Rohdaten")

    def SaveFile_formatted(self):
        Datei_speichern(self.text, "Formatiert")

    def ShowHelp(self):
        HilfeText(self.text)

    def Test_PageLoc(self):
        Seite = Get_Markus_Page()
        messagebox.showinfo("Seitenzahl testen", "Ausgelesene Seitenzahl: " + Seite)

    def Unlock_Database(self):
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('SQLite files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        self.text.delete(1.0, END)
        database = filedialog.askopenfilename(**options)
        connection = sqlite3.connect(database)
        connection.commit()
        connection.close()
        Text = " Datebank unlocked"
        messagebox.showinfo("Unlock", Text)

    def update_db(self):
        self.text.delete(1.0, END)
        Upsert_Upload(self.text)

    def update_tfidf(self):
        self.text.delete(1.0, END)
        date = str(datetime.datetime.now())
        today = date[:10]
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('SQLite files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'AVAD.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        self.text.delete(1.0, END)
        database = filedialog.askopenfilename(**options)
        connection = sqlite3.connect(database)
        tfidf_cursor = connection.cursor()
        support_cursor = connection.cursor()
        sql = "DELETE  FROM TFIDF"
        tfidf_cursor.execute(sql)
        sql = "SELECT * FROM Business_Activity"
        tfidf_cursor.execute(sql)
        i = 0
        for res in tfidf_cursor:
            AID, taetigkeit, _ = res
            dict1 = {}
            if AID == "Crefo":
                continue
            dict1.setdefault("AID", AID)
            dict1.setdefault("Business_Activity", taetigkeit)
            sql = "SELECT Entity_Last_Name FROM Entity WHERE AID = '" + AID + "'"
            support_cursor.execute(sql)
            Entity = support_cursor.fetchone()
            if Entity:
                dict1.setdefault("Name", Entity[0])
            else:
                continue
            sql = "SELECT Branch_Code FROM Branch_Codes WHERE AID = '" + AID + "' AND Branch_Rank_Main_Sec = 'WZ08H'"
            support_cursor.execute(sql)
            bcodeH = support_cursor.fetchone()
            if bcodeH:
                dict1.setdefault("Branch_Code_Main", bcodeH[0])
            else:
                continue
            sql = "SELECT Branch_Code FROM Branch_Codes WHERE AID = '" + AID + "' AND (Branch_Rank_Main_Sec = 'WZ08H' OR Branch_Rank_Main_Sec = 'WZ08N')"
            support_cursor.execute(sql)
            bcodeN = support_cursor.fetchall()
            if bcodeN:
                bcN = ""
                for code in bcodeN:
                    bcN = bcN + " " + code[0]
                dict1.setdefault("Branch_Codes_Secondaries", bcN)
            else:
                continue
            dict1.setdefault("Internet", "")
            dict1.setdefault("Last_Change", today)
            sql = "INSERT INTO TFIDF VALUES (:AID, :Name, :Business_Activity, :Branch_Code_Main, :Branch_Codes_Secondaries, :Internet, :Last_Change)"
            support_cursor.execute(sql, dict1)
            i += 1
            print("Erzeuge... ", format(i, ",d").replace(",", "."))
        connection.commit()
        support_cursor.execute("SELECT COUNT(*) FROM TFIDF")
        size = format(support_cursor.fetchone()[0], ",d").replace(",", ".")
        Text = size + " Datensätze in Datenbank aufgenommen"
        messagebox.showinfo("Datenbank Update", Text)
        connection.close()

    def Verzeichnis_konvertieren(self):
        self.text.delete(1.0, END)
        self.text.delete(1.0, END)
        directory = filedialog.askdirectory()
        options = {}
        options['defaultextension'] = '.db'
        options['filetypes'] = [('Database files', '.db'), ('All files', '.*')]
        options['initialfile'] = 'Firmen.db'
        options['parent'] = root
        options['title'] = 'Datenbank auswählen'
        db_file = filedialog.askopenfilename(**options)
        #        for root, dirs, files in os.walk(root, topdown=False):
        for _, _, files in os.walk(directory):
            # print(os.path.join(root, dirs))
            for filename in files:
                datei = directory + "//" + filename
                Textdatei_konvertieren(self.text, "Database", datei, db_file)
        Ausgabe = str(len(files)) + " Dateien verarbeitet."
        messagebox.showinfo("Datenübernahme", Ausgabe)


"""----------------------------------
Main-Funktion des Programms
--------------------------------"""
global NxtScr, PageLoc, EmptyField, HomeDir, DBDir, shelfFile
shelfFile = shelve.open("avad")
if "NxtScr" in shelfFile:
    NxtScr = shelfFile["NxtScr"]
else:
    NxtScr = (100, 100)
if "PageLoc" in shelfFile:
    PageLoc = shelfFile["PageLoc"]
else:
    PageLoc = (100, 100)
if "EmptyField" in shelfFile:
    EmptyField = shelfFile["EmptyField"]
else:
    EmptyField = (100, 100)
if "HomeDir" in shelfFile:
    HomeDir = shelfFile["HomeDir"]
else:
    HomeDir = os.getcwd()
if "DBDir" in shelfFile:
    DBDir = shelfFile["DBDir"]
else:
    DBDir = os.getcwd()

root = tkinter.Tk()
root.wm_title("AVAD - Avandil Adressen Research Ver. " + Version)
app = MyApp(root)
app.mainloop()
shelfFile["NxtScr"] = NxtScr
shelfFile["PageLoc"] = PageLoc
shelfFile["EmptyField"] = EmptyField
shelfFile["HomeDir"] = HomeDir
shelfFile["DBDir"] = DBDir
shelfFile.close()
