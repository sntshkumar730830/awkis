from selenium import webdriver
import pyautogui, sqlite3, sys
from urllib.request import urlopen
import pdb
from bs4 import BeautifulSoup
import  time
platform = sys.platform
if str(platform) == "win32":
    path = 'C:\\Users\\Acer\\Dropbox\\database\\Firmen.db'
if str(platform) == "linux":
    path = '/opt/database/AVAD-2018-05.db'
def crawl_dub_link(URL,main_branch,middle_branch_name,sub_branch_name):
    #pdb.set_trace()
    conn = sqlite3.connect(path)
    cr=conn.cursor()
    sql_query_exist = "select Id from Portal_DUB_web_crawl where URL='" + URL + "'"
    print(sql_query_exist)
    is_url_exist = cr.execute(sql_query_exist)
    row_count = is_url_exist.fetchall()
    #pdb.set_trace()
    if row_count and row_count[0] and row_count[0][0]:
        #pdb.set_trace()
        print("URL already browser---------------")
        print(URL)
        # if sub_branch:
        #     branch_hierarchy = str(row_count[0][0]) + " , " + str(sub_branch)
        # else:
        #     #pdb.set_trace()
        #     branch_hierarchy = str(row_count[0][0]) + " , " + str(main_branch)
        #pdb.set_trace()
        dub_data_insert = "insert into dub_branch_data (dub_id,branch1,branch2,branch3) values (" + \
                          str(row_count[0][0]) + ",'" + main_branch + "','" + middle_branch_name + "','" + sub_branch_name + "')"
        cr.execute(dub_data_insert)
        conn.commit()
        conn.close()
    else:
        print("now browsing URL----------------")
        req = urlopen(URL).read()
        soup = BeautifulSoup(req, "lxml")
        description, branch, country, ziprange, salesvolume, employee, estateinclude, investmentvolume, methodofinvestment, fraction,\
                reasonofsale, kurvorstellung,branch_hierarchy = "","","","","","","","","","","","",""
        try:
            for link in soup.findAll("tr"):
                if not link.get("class", "") and len(link.findChildren("td")) >= 2 and \
                    str(link.findChildren("td")[1].get("class")[0]) == "description" and not description and len(link.findChildren("td")) >= 2:
                    description = str(link.findChildren("td")[1].text).replace("\t", "").replace("\n", "").replace("\r", "").\
                        replace("  ","").replace("'","")
                if link.get("class", "") and isinstance(link.get("class"),list):
                    if  link.get("class")[0] == "branches" and not branch:
                        branch = ""
                        for each_branch in link.findChildren("li"):
                            branch += str(each_branch.text).replace("\n"," ").replace("\t"," ").replace("\r"," ") + " , "
                        #pdb.set_trace()
                        branch = str(branch).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "country" and not country and len(link.findChildren("td")) >= 2:
                        country = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "ziprange" and not ziprange and len(link.findChildren("td")) >= 2:
                        ziprange = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "salesvolume" and not salesvolume and len(link.findChildren("td")) >= 2:
                        salesvolume = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "employees" and not employee and len(link.findChildren("td")) >= 2:
                        employee = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "estateIncluded" and not estateinclude and len(link.findChildren("td")) >= 2:
                        estateinclude = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "investmenvolume" and not investmentvolume and len(link.findChildren("td")) >= 2:
                        investmentvolume = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "methodofinvestment" and not methodofinvestment and len(link.findChildren("td")) >= 2:
                        methodofinvestment = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "fraction" and not fraction and len(link.findChildren("td")) >= 2:
                        fraction = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                    if link.get("class")[0] == "reasonsOfSale" and not reasonofsale and len(link.findChildren("td")) >= 2:
                        reasonofsale = str(link.findChildren("td")[1].text).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                        kurvorstellung = ""
                        for eachkurvor in link.findNext("tr").findChildren("td"):
                            if eachkurvor.get("class")[0] == "shortPresentation":
                                kurvorstellung += str(eachkurvor.text).replace("\n"," ").replace("\t"," ").replace("\r"," ") + " , "
                        kurvorstellung = str(kurvorstellung).replace("\n"," ").replace("\t"," ").replace("\r"," ").replace("  ","")
                #pdb.set_trace()
                print("fdks gjd jf")
            # pdb.set_trace()
            #pdb.set_trace()
            URL = str(URL)
            # if sub_branch:
            #     branch_hierarchy = main_branch + "," + sub_branch
            # else:
            #     branch_hierarchy = main_branch
            #pdb.set_trace()
            sql_query = "insert into Portal_DUB_web_crawl ('Description','Branches','Country','PLZ','Turnover','Employees','Investment_volume',\
                           'Kind_of_participation','Share_of_participation','Reason_for_sale','Real_Estate','Brief_introduction','URL') values ('"+\
                            description + "','" + branch + "','" + country + "','" + ziprange + "','" + salesvolume + "','" +employee + "','" + \
                            investmentvolume + "','" + methodofinvestment + "','" + fraction + "','" + reasonofsale + "','" + estateinclude + "','" +\
                            kurvorstellung + "','" + URL + "')"
            print("____________________________TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT________________________")
            print(sql_query)
            # pdb.set_trace()
            cr.execute(sql_query)
            conn.commit()
            max_id_query = "select max(id) from Portal_DUB_web_crawl"
            cr.execute(max_id_query)
            max_id = cr.fetchall()[0][0]
            dub_data_insert = "insert into dub_branch_data (dub_id,branch1,branch2,branch3) values ("+\
                str(max_id) + ",'" + main_branch + "','" + middle_branch_name + "','" + sub_branch_name + "')"
            cr.execute(dub_data_insert)
            conn.commit()
            conn.close()
            print("Successfull execution---------------------------------------")
        except Exception as e:
            print("Unsuccessfull execution-----------------------------")
            print(str(e))

def links_crawl():
     i = 0
     links=[]
     time.sleep(10)
     new_htmlsource = wd.page_source
     new_soup = BeautifulSoup(new_htmlsource, "lxml")
     for each_link in new_soup.findAll("span"):
        if each_link.get('class') and (len(each_link.get('class')) > 1):
            if each_link.get('class') and str(each_link.get('class')[1]) == 'hyphenate':
                i = i + 1
                print(i)
                print(str(i)+"th link title: "+str(each_link.get('title')))
                children = each_link.findChildren()
                for child in children:
                    if child.get('href'):
                        link_for_crowling="https://www.dub.de"+str(child.get('href'))
                        print("final link for crawling : "+ link_for_crowling )
                        links.append(link_for_crowling)
     return links
#wd = webdriver.Chrome(executable_path="F:\\chromedriver\\chromedriver")
wd = webdriver.Chrome(executable_path="C:\\Users\\Acer\Dropbox\\AVKIS-Web\\files\\chromedriver")
wd.get('https://www.dub.de/unternehmensboerse/unternehmen-verkaufen/kaeufer-suchen/?myPersonalSearch=1&branche=0&PLZ=null')

pyautogui.moveTo(579,524)
pyautogui.click()
pyautogui.click()
htmlsource=wd.page_source
soup = BeautifulSoup(htmlsource, "lxml")
i=0
middle_branch_name = ""
sub_branch_name = ""
for each_ul in soup.findAll("ul"):
    if  str(each_ul.get('id'))!=None  and  each_ul.get('id') and str(each_ul.get('id')) == 'branchSelector':
          all_branch = each_ul.findChildren("li")
          for main_branch_data in all_branch:
                if len(str(main_branch_data.get('id')).split("_"))==4:# and str(main_branch_data.get('id')) == "li_item_Tree_5":
                    #pdb.set_trace()
                    main_branch_element_id=str(main_branch_data.get('id'))
                    main_branch_name_id = str(main_branch_element_id[3:])
                    main_branch_id_click = str(main_branch_element_id[8:]).lower()
                    #pdb.set_trace()
                    main_element = wd.find_element_by_id(main_branch_id_click)
                    time.sleep(2)
                    wd.execute_script("arguments[0].click();", main_element)
                    main_branch_name_id = "item_Tree_"+main_branch_element_id.split("_")[3]
                    main_branch_name_s = wd.find_element_by_id(main_branch_name_id).text
                    links_for_crowling = links_crawl()
                    print(main_branch_name_s)
                    print("Count of links 5555555555555555555555555555555555")
                    print(len(links_for_crowling))
                    link_count = 1
                    for each in links_for_crowling:
                        print("88888888888888888888888888888888888888888888888888final link for crawling : " +each)
                        print(link_count)
                        link_count += 1
                        crawl_dub_link(each,main_branch_name_s,"","")
                    #pdb.set_trace()
                    wd.execute_script("arguments[0].click();", main_element)
                    main_branch_elements = main_branch_data.findChildren()
                    sub_branch_elements = main_branch_data.findChildren("li")
                    for each_main_branch in main_branch_elements:
                        if each_main_branch.get('id') and str(each_main_branch.get('id')) == main_branch_name_id:
                           main_branch_name= str(each_main_branch.text).replace("\n"," ").replace("\t"," ").replace("\r"," ")
                           print("main_branch_name:" + main_branch_name)
                           element = wd.find_element_by_id(main_branch_name_id)
                           time.sleep(2)
                           wd.execute_script("arguments[0].click();", element)
                           j=-1
                           k = 0
                    for each_sub_branch in sub_branch_elements:
                            if len(str(each_sub_branch.get('id')).split("_")) == 5:
                              middle_branch_name = str(each_sub_branch.text).split("\n\n\n\n")[0].replace("\n", " ").replace("\t"," ").replace("\r", " ")
                            if len(str(each_sub_branch.get('id')).split("_")) == 6:
                              sub_branch_name = str(each_sub_branch.text).replace("\n", " ").replace("\t"," ").replace("\r", " ")
                            else:
                              sub_branch_name = ""
                            # if sub_branch_name:
                            #     final_sub_branch = sub_branch_name
                            # else:
                            #     final_sub_branch = middle_branch_name
                            sub_li_id = str(each_sub_branch.get('id')[8:]).lower()
                            if j==-1:
                             prev_branch=""
                            else :
                                prev_branch=str(sub_branch_elements[j].get('id')[8:]).lower()
                            #sub_branch_name= str(each_sub_branch.text).replace("\n"," ").replace("\t"," ").replace("\r"," ")
                            print("&&&&&&&&&&&&&&&&&&&&&&&&&   middle branch name:"+middle_branch_name)
                            print("&&&&&&&&&&&&&&&&&&&&&&&&&   sub_branch_name:"+sub_branch_name)
                            for each_sub_branch_element in each_sub_branch:
                             if each_sub_branch_element!="\n":
                                  if str(each_sub_branch_element.get('id'))!=None  and each_sub_branch_element.get('id') \
                                          and str(each_sub_branch_element.get('id')) == sub_li_id:
                                        #sub_branch_name = str(each_sub_branch.text)
                                        element_click = wd.find_element_by_id(sub_li_id)
                                        time.sleep(2)
                                        wd.execute_script("arguments[0].click();", element_click)
                                        if k != 0:
                                            time.sleep(2)
                                            wd.execute_script("arguments[0].click();", element_click)
                                        if j>-1:
                                             prev_branch_click = wd.find_element_by_id(prev_branch)
                                             time.sleep(2)
                                             wd.execute_script("arguments[0].click();", prev_branch_click)
                                        j+=1
                                        k+=1
                                        filter_btn = wd.find_elements_by_class_name("done")
                                        time.sleep(2)
                                        wd.execute_script("arguments[0].click();", filter_btn[0])
                                        #for clicking filter button
                                        links_for_crowling = links_crawl()
                                        for each in links_for_crowling:
                                            print("final link for crawling : " + each)
                                            crawl_dub_link(each,main_branch_name_s,middle_branch_name,sub_branch_name)
                                            #crawl_dub_link(link_for_crowling)
                    print("total sub branch count:" + str(j))
                    wd.execute_script("arguments[0].click();", element)
                    wd.execute_script("arguments[0].click();", element_click)
                    filter_btn = wd.find_elements_by_class_name("done")
                    wd.execute_script("arguments[0].click();", filter_btn[0])