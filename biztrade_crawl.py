from bs4 import BeautifulSoup
import sqlite3, pdb, sys
platform = sys.platform
from urllib.request import urlopen
if str(platform) == "win32":
    path = 'C:\\Users\\Acer\\Dropbox\\database\\Firmen.db'
if str(platform) == "linux":
    path = '/opt/database/AVAD-2018-11_new_structure1.db'
conn = sqlite3.connect(path)
cr=conn.cursor()
URL = "https://www.biz-trade.de/marktplatz.htm"
while URL:
    req = urlopen(URL).read()
    URL = ""
    soup = BeautifulSoup(req, "lxml")
    try:
        for link in soup.findAll("a"):
            get_href = str(link.get("href",""))
            if get_href and (get_href[3:8].isdigit() or get_href[3:4].isdigit()) and get_href.find("-") != -1 and len(get_href) in (12,11)\
                    and get_href[-4:] == ".htm":
                all_link = "https://www.biz-trade.de/" + get_href
                print(all_link)
                req_each = urlopen(all_link).read()
                soup_each = BeautifulSoup(req_each, "lxml")
                branch, Region, Entry, Employees, Turnover, Real_Estate_Property, Asking_Price, State, umgebung, description, Business_Type, category = \
                    "", "", "", "", "", "", "", "", "", "", "", ""
                for each_link in soup_each.findAll("h2"):
                    if str(each_link.get("itemprop", "")) == "category":
                        category = each_link.text
                for each_link in soup_each.findAll("h1"):
                    if str(each_link.get("itemprop", "")) == "name":
                        Business_Type = each_link.text
                for each_link in soup_each.findAll("div"):
                    if each_link.get("itemprop", ""):
                        if str(each_link.get("itemprop", "")) == "description":
                            description = str(each_link.text).replace("'","")
                    if each_link.get("class","") and len(each_link.get("class")) >= 1 and each_link.get("class")[0] == "sectionlabel":
                        if str(each_link.text) == "Branche" and not branch:
                            branch = each_link.findNext("div").text
                        if str(each_link.text) == "Region" and not Region:
                            Region = each_link.findNext("div").text
                        if str(each_link.text) == "Entry" and not Entry:
                            Entry = each_link.findNext("div").text
                        if str(each_link.text) == "Employees" and not Employees:
                            Employees = each_link.findNext("div").text
                        if str(each_link.text) == "Umsatzklasse" and not Turnover:
                            Turnover = each_link.findNext("div").text
                        if str(each_link.text) == "Real_Estate_Property / Immobilien" and not Real_Estate_Property:
                            Real_Estate_Property = each_link.findNext("div").text
                        if str(each_link.text)[0:16] == "Asking_Price" and not Asking_Price:
                            Asking_Price = str(each_link.findNext("div").text).replace("'","*")
                        if str(each_link.text) == "State > Kreis" and not State:
                            State = each_link.findNext("div").text
                        if str(each_link.text) == "Umgebung" and not umgebung:
                            umgebung = each_link.findNext("div").text
                        #print("got the link")
                #pdb.set_trace()
                if str(category)[0:5] == "Suche":
                    is_url_exist = "select count(*) from biz_trade_suche_crawl where URL='" + str(all_link) + "'"
                    cr.execute(is_url_exist)
                    get_count = cr.fetchall()
                    if get_count and get_count[0] and get_count[0][0] > 0:
                        pdb.set_trace()
                    else:
                        sql_query = "insert into biz_trade_suche_crawl (Business_Type,Description,branch,Region,Entry,Employees,Turnover,Real_Estate_Property,"+\
                        "Asking_Price,State,Surrounding_Area,URL) values ('" + Business_Type + "','" +description + "','" + branch + "','" + Region + "','" + Entry + \
                            "','" + Employees + "','" + Turnover + "','" + Real_Estate_Property + "','" + Asking_Price + "','" + State + "','" + \
                                    umgebung + "','" + all_link + "')"
                        cr.execute(sql_query)
                        conn.commit()
                if str(category)[0:5] == "Biete":
                    #pdb.set_trace()
                    is_url_exist = "select count(*) from biz_trade_Sell_Offer_crawl where URL='" + str(all_link) + "'"
                    cr.execute(is_url_exist)
                    get_count = cr.fetchall()
                    if get_count and get_count[0] and get_count[0][0] > 0:
                        pdb.set_trace()
                    else:
                        sql_query = "insert into biz_trade_Sell_Offer_crawl (Business_Type,Description,branch,Region,Entry,Employees,Turnover,Real_Estate_Property," + \
                                    "Asking_Price,State,Surrounding_Area,URL) values ('" + Business_Type + "','" + description + "','" + branch + "','" + Region + "','" + Entry + \
                                    "','" + Employees + "','" + Turnover + "','" + Real_Estate_Property + "','" + Asking_Price + "','" + State + "','" + \
                                    umgebung + "','" + all_link + "')"
                        cr.execute(sql_query)
                        conn.commit()
            if str(link.text).find("Seite weiter ") != -1 and not URL:
                URL = "https://www.biz-trade.de/" + get_href
                print(URL)
                #pdb.set_trace()
    except Exception as e:
        print("Unsuccessfull execution ============================================")
        print(e)
        print(sql_query)
conn.close()