import urllib.request
from bs4 import BeautifulSoup
from urllib.error import HTTPError
from urllib.parse import urlparse
import pandas as pd
import re,os,platform
import requests
global download_link_url
skip_download_link_url=[]
from celery import Celery
import smtplib,datetime,sqlite3
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from celery.contrib import rdb
app = Celery('celery_task', broker='pyamqp://guest@localhost//')


@app.task
def crawl_again(aid, website,earlier_data,email,stopwords=[]):
    print("Recrawling website")
    KiCrawl = Crawler()
    KiCrawl.stopwords =stopwords
    KiCrawl.url = website
    ok = KiCrawl.crawl_site()
    if ok:
        webinhalt = KiCrawl.ngrams
    if not set(webinhalt)==set(earlier_data):
        to_email=[]
        print("Website has been changed so current data is updated")
        today2 = datetime.datetime.now()
        rdb.set_trace()
        sql_query='UPDATE keyword_Analysis SET analysis_result ="'+str(webinhalt).replace("]", "").replace("[", "")+ '" , date_of_data_insert = "'+str(today2.strftime("%A %d %B %y %I:%M:%S %p")) +'"  WHERE aid = "'+ aid +'";'
        dbfilepath = ""
        currentpath = os.path.abspath(os.curdir)
        if platform.system() == 'Windows':
            print("System on which project is running = " + platform.system())
            currentpath = os.path.abspath(os.path.join(os.getcwd(), '..', 'database'))
            print("Project path = " + currentpath)
            currentpath = currentpath.replace('\\', '\\\\')
            dbfilepath = currentpath + "\\\\Firmen.db"
            print("database path = " + dbfilepath)

        if platform.system() == 'Linux':
            print("System on which project is running = " + platform.system())
            currentpath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'database1'))
            print("Project path = " + currentpath)
            currentpath = currentpath.replace('\\', '/')
            dbfilepath = currentpath + "/AVAD-2018-05.db"
            print("Database path = " + dbfilepath)
        conn = sqlite3.connect(dbfilepath)
        cr = conn.cursor()
        cr.execute(sql_query)
        conn.commit()
        to_email.append(email)
        subject="Keyword-Analyse Firma Result Update for "+str(aid)
        msg=website +" has been changed so Keyword-Analyse Firma Result data is updated"
        send_email(to_email,subject,msg)


def send_email(to,subject,body,path_list=[]):
    for each_to in to:
        to = each_to
        froms = "santosh.erpincloud@gmail.com"
        smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = str(subject)
        msg['From'] = froms
        msg['To'] = to
        html = body
        for f in path_list:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=os.path.basename(f)
                )
            part['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(f)
            msg.attach(part)
        part2 = MIMEText(html, 'html')
        gmail_pwd = "$0lutions"
        msg.attach(part2)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login(froms, gmail_pwd)
        smtpserver.sendmail(froms, to, msg.as_string())
        smtpserver.close()

class Crawler:
    def __init__(self, url = "http://avandil.com"):
        self.url = url
        self.pages = []
        self.log_daten = []
        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
                    , "?pdf=1", ".exe", "bat"]
        self.no_search_sites = ["sitemap", "impressum", "kontakt", "download", "standort", "agb", "datenschutz", "login", "site-map",
                           "contact", "youtube", "google", "anfahrt", "karriere", "location", "printpdf", "anfrage", "job", "jobs"]
        self.seiteninhalt = ""
        self.encode = "latin1"
        self.page_content = []
        self.page_text = []
        self.ngrams = pd.DataFrame
        self.stopwords =  []
        self.url_logs = dict()
        self.perpagelinks = []
        self.all_crawl_urls = []

    def crawl_site(self):
        url = self.url
        scheme, start_url = self.verify_main_url(url)
        url, content = self.get_object(start_url)
        self.pages = [url]

        self.page_content = [(url, content)]
        for page in self.pages : # self.pages wird rekursiv über get_links und verify_sub_link gefüllt
            newpages, page_content = self.get_links(scheme, page)
            pc_dict = dict(page_content)
            for new_page in newpages :
#                if page == "http://www.meierguss.de/mobile/index.html":
#                    print ("Waiting")
#                 print("Page: ", new_page, "iiiiiiii-", i)
#                 print(len(self.pages))
#                 print(len(list(set(self.pages))))
                if new_page not in self.pages :
                    try :
                      #  print(new_page)
                        self.pages.append(new_page)
                        content = pc_dict[new_page]
                        self.page_content.append((new_page, content))

                    except:
                        pass
        self.page_text=self.get_page_text()
        self.all_crawl_urls.extend(self.pages)
        self.ngrams = self.suchwort_extractor()
        return True

    def get_object(self, url):
        for filetype in self.no_search_filetypes :
            if url and url.lower().endswith(filetype) :
                return None, None
        try :
            try :
                #req = requests.get(url)
                req = urllib.request.urlopen(url)
                if req.getheader('content-type').find("html")==-1:
                #if req.headers['content-Type'].find("html")==-1:
                    return None, None
                html = req.read()
            except Exception as e:
                print(e)
                try:
                      try:
                        req = requests.get(url)
                        if req.headers['content-Type'].find("html")==-1:
                            return None, None

                      except requests.exceptions.Timeout:
                          self.log_daten.append("Timeout")
                          return None, None
                      except:
                          ReadError = None
                          return ReadError, None
                except HTTPError as ReadError:
                    ReadError = None
                    return ReadError, None
                html = req.content.decode(self.encode, 'ignore')
                soup = BeautifulSoup(html, "lxml")

            except requests.exceptions.Timeout:
                self.log_daten.append("Timeout")
                return None, None
            except :
                ReadError = None
                return ReadError, None
        except HTTPError as ReadError:
            ReadError = None
            return ReadError, None
        #html = req.content.decode(self.encode, 'ignore')
        try :
            url = html.geturl() #Für den Fall redirecting der url
        except:
            pass
        try :
            soup = BeautifulSoup(html, "lxml")
            if str(soup) == '':
                try:
                      try:
                        req = requests.get(url)
                        if req.headers['content-Type'].find("html")==-1:
                            return None, None
                      except requests.exceptions.Timeout:
                          self.log_daten.append("Timeout")
                          return None, None
                      except:
                          ReadError = None
                          return ReadError, None
                except HTTPError as ReadError:
                    ReadError = None
                    return ReadError, None
                html = req.content.decode(self.encode, 'ignore')
                soup = BeautifulSoup(html, "lxml")
            req.close()

        except AttributeError as ReadError:
            ReadError = None
            return ReadError, None
        return url, soup

    def get_links(self, scheme, url):
        pages = []
        page_content = []
        url, soup_object = self.get_object(url)
        if not url :
            return pages, page_content
       # print("current url for url searching: "+ url)
        domain = urlparse(url).netloc
        log_text ="Website crawlen domian: " + domain + "\n"+ "Website crawlen: " + url + "\n"
        self.log_daten.append(log_text)
        if len(self.pages) == 0 and len(soup_object.findAll("a")) == 0:
            self.log.append("Keine weiterführenden Links gefunden")
            return False
        old_base_url=""
        for link in soup_object.findAll("a") :
            link_ref = link.get('href')
            ref_text = self.text_splitter(self.clean_text(link.text))
            if link_ref not in self.perpagelinks:
              self.perpagelinks.append(link_ref)
            else:
                 continue
            if not link_ref or link_ref.find("produkt")!=-1 or link_ref=="http://"+domain or link_ref=="https://"+domain or link_ref=="http://"+domain+"/" or link_ref=="https://"+domain+"/":
                continue
            parse_not = False
            linktext = link_ref.split(".") + ref_text
            for wort in linktext :
                if wort.lower() in self.no_search_sites :
                    parse_not = True
                    break
            if parse_not :
                continue
            if link_ref.find(domain)>-1:
                temptext=linktext[len(linktext)-1]
                if temptext.find("/")==-1:
                    temptext=linktext[len(linktext)-2]+linktext[len(linktext)-1]
                link_ref=temptext[temptext.find("/"):]
            if link_ref.find("#") > -1:
                   continue
            temp1=link_ref.split("/")
            temp=[]
            for each in temp1:
                if each=="" or each==" " or each=="..":
                    pass
                else:
                    temp.append(each)
            if temp:
              baseurl="/"+temp[0]+"/"
            else:
              baseurl="/"
            newpage = self.verify_sub_url(scheme, domain, link_ref)
            if not newpage:
                continue
            if newpage.rfind("../") != -1:
                newpage = newpage.replace("../", "")
            if len(baseurl)<4 or baseurl.find("?")!=-1:
                baseurl="http://"+domain
            try:
                    if newpage and (newpage not in pages):
                        if baseurl and  baseurl in self.url_logs:
                            pos = newpage.find(domain) + len(domain)
                            for each in range(1, 4):
                                finalpos = newpage.find("/", pos)
                                if each < 3:
                                    pos = newpage.find("/", pos + 1)
                                    if pos == -1 :
                                        pos=finalpos
                            current_url = newpage[newpage.find(domain) + len(domain):finalpos + 1]
                            if newpage.find(domain) + len(domain)==finalpos:
                                current_url = newpage[newpage.find(domain) + len(domain):]
                            if newpage.find(domain) + len(domain) + len(current_url)<len(newpage):
                                if current_url not in self.url_logs and old_base_url != current_url:
                                    urlcount = 1
                                    self.url_logs[current_url] = {}
                                    self.url_logs[current_url][str(urlcount)] = newpage
                                    pages.append(newpage)
                                    old_base_url = current_url
                                    continue
                                else:
                                    od1 = list(self.url_logs[current_url].keys())
                                    urlcount = int(od1[-1])
                                    if urlcount >= 20:
                                        continue
                                    else:
                                        pages.append(newpage)
                                        page_content.append((newpage, soup_object))
                                        urlcount = int(urlcount) + 1
                                        if newpage not in list(self.url_logs[current_url].values()):
                                         self.url_logs[current_url][urlcount] = newpage
                                        continue
                            else:
                                od = list(self.url_logs[baseurl].keys())
                                urlcount = int(od[-1])
                                if urlcount >= 20:
                                    continue
                                else:
                                    pages.append(newpage)
                                    page_content.append((newpage, soup_object))
                                    urlcount = int(urlcount) + 1
                                    if newpage not in list(self.url_logs[baseurl].values()):
                                        self.url_logs[baseurl][urlcount] = newpage
                        else:
                            urlcount = 1
                            self.url_logs[baseurl] = {}
                            self.url_logs[baseurl][str(urlcount)] = newpage
                            pages.append(newpage)
                            page_content.append((newpage, soup_object))
            except Exception as e:
                 print(e)
        if len(pages) == 0 :
            self.log_daten.append("Keine weiterführenden Links gefunden")
            return [], []
        else :
          #  print("__________________________________________________________________________________________")
            return pages, page_content

    def verify_sub_url(self, scheme, domain, url):
        urlpage = urlparse(url)
        if urlpage.scheme and not urlpage.scheme.startswith("http") :
            return None
        if urlpage.netloc and urlpage.netloc != domain :
            nl = urlpage.netloc.replace("www.","",1)
            dm = domain.replace("www.","",1)
            if nl != dm :
                return None
        path = urlpage.path
        parse_not = False
        for filetype in self.no_search_filetypes :
            if path and path.lower().endswith(filetype) :
                parse_not = True
                break
        if parse_not :
            return None
        if path and path.startswith("/") :
            path = path.replace("/","",1)
        if path and path.startswith("../") :
            path = path.replace("../","",1)
        if path and path.startswith("./") : #wenn relativer Pfad angegeben wird
            urlpath = urlparse(url).path
            urlpathlist = urlpath.split("/")
            path_location = urlpathlist[:len(urlpathlist)-1]
            pathlist = path.split("/")
            location = pathlist[1:]
            newpath = ""
            for part in path_location :
                if part :
                    if newpath :
                        newpath = newpath + "/" + part
                    else :
                        newpath = part
            newpath = newpath + "/" + location[0]
            path = newpath
        query = urlpage.query
        if query :
            query = "?" + query

        newpage = scheme + "//" + domain + "/" + path + query
        self.log_daten.append("Gefunden: " + newpage)
        return newpage

    def verify_main_url(self, url):
        test = urlparse(url)
        if not test.scheme :
            scheme = "http:"
        else :
            scheme = test.scheme
            if not scheme.endswith(":") :
              scheme = scheme + ":"
        if not test.netloc :
             netloc = test.path
             path = ""
        else :
             netloc = test.netloc
        if test.path != netloc:
             path = test.path
        url = scheme + "//" + netloc + "/" + path
        return scheme, url


    def get_page_text(self) :
        page_refs = ["title", "h1", "h2", "h3", "h4", "h5", "p", "li", "div", "br"]
        title_refs = ["title", "h1", "h2", "h3", "h4", "h5"]
        text_set = set()
        textliste = []
        for page in self.page_content :
            url, soup = page
            for ref in page_refs :
                if soup is not None:
                    seite = soup.findAll(ref)
                    for zeile in seite :
                        ref_text = zeile.getText(separator=" ")
                        if ref in title_refs :
                            wortliste = self.text_splitter(self.clean_text(ref_text))
                            for wort in wortliste :
                                if wort.lower() in self.no_search_sites :
                                    ref_text = ""
                        if ref_text :
                            clean_text = self.clean_text(ref_text)
                            text_set.add(clean_text)
        for seite in text_set :
            clean_seite = self.clean_text(seite)
            textliste.append(clean_seite)
        return textliste

    def text_splitter(self, text) :
        textliste = []
        wortliste = re.split(r"(\W)", text)
        for wort in wortliste :
            if len(wort)>20:
                continue
            if not self.stopwords:
                self.stopwords = ['und', 'von', 'die', 'der', 'sowie', 'mit', 'im', 'in', 'des', 'für', 'an','insbesondere',                                  'verwaltung', 'ist', 'art', 'aller', 'handel', 'oder', 'betrieb', 'vertrieb',
                                  'unternehmen', 'zu','den', 'gesellschaft', 'als', 'das', 'erwerb', 'dem', 'dienstleistungen',
                                  'durchführung', 'gmbh','gegenstand', 'übernahme', 'damit', 'verkauf', 'einer', 'entwicklung', 'herstellung',
                                  'beteiligung','vermittlung', 'beratung', 'eigenen', 'auf', 'co', 'tätigkeiten', 'zur', 'vermietung',
                                  'kg','immobilien', 'alle', 'bereich', 'bei', 'eines', 'beteiligungen', 'anderen', 'sind',
                                  'geschäfte','erbringung', 'deren', 'durch', 'geschäftsführung', 'zusammenhang', 'förderung',
                                  'sich', 'auch','nicht', 'unternehmens', 'zum', 'persönlich', 'vermögens', 'werden',
                                  'gesellschafterin', '1','haftung', 'grundstücken', 'planung', 'nach', 'haftende', 'halten', 'aus', 'unter',
                                  'verein','persönlichen', 'einzelhandel', 'einschließlich', 'waren', 'stehenden', 'wird',
                                  'zweck', 'anlagen','gesellschaften', 'errichtung', 'firma', 'produkten', 'eine', 'sonstigen', 'software',
                                  'über','veräußerung', 'es']
            if wort.isalnum() and wort.lower() not in self.stopwords:
                if wort.lower() == "getelementbyid" or wort.lower() == "document" or wort.lower() == "innerhtml" or wort.lower() == "var" or wort.lower() == "path":
                    continue
                else:
                   textliste.append(wort.lower())
        return textliste

    def suchwort_extractor(self):
        pages = []
        for page in self.page_text :
            text = self.text_splitter(page)
            if text :
                pages = pages + text
        # fd = FreqDist(pages)
        # anzahl = len(fd)
        # unigramme = fd.most_common(anzahl)
        # df1 = pd.DataFrame(unigramme, columns=["Wort", "Anzahl "])
        # pairs = nltk.bigrams(pages)
        # fdist = FreqDist(pairs)
        # bigramme = fdist.most_common(anzahl)
        # df2 = pd.DataFrame(bigramme, columns=["Bigramme", "Anzahl  "])
        # triples = nltk.trigrams(pages)
        # fdist = FreqDist(triples)
        # trigramme = fdist.most_common(anzahl)
        # df3 = pd.DataFrame(trigramme, columns=["Trigramme", " Anzahl  "])
        # df = pd.concat([df1, df2, df3], axis=1)
        return pages

    def clean_text(self, text):
        text = text.replace("Ã¼", "ü")
        text = text.replace("Ã¶", "ö")
        text = text.replace(" Ã", " Ü")
        text = text.replace("Ãb", "Üb")
        text = text.replace("Ã¤", "ä")
        text = text.replace("ß¤", "ä")
        text = text.replace("Ã", "ß")
        return text


"""
print("Crawler gestartet")
KiCrawl = Crawler("www.meierguss.de")
#KiCrawl = Crawler("www.bus-taxi-sieben.de")
#KiCrawl = Crawler("http://www.aundwtiefbau.de")
ok = KiCrawl.crawl_site()
print("Ende des Programms")
"""
#########################################################################################
