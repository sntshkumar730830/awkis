num = "-1234"
k = ""
if not num:
    num = 0
a = str(num)
if a.find(".") != -1:
    k = a[a.find(".") + 1:a.find(".") + 4]
    a = a[0:a.find(".")]
i = len(a)
b = ""
c = 0
while i:
    i -= 1
    if c % 3 == 0 and c != 0:
        b = b + "."
    c = float(c) + 1;
    b = b + a[i]
final_ans = b[::-1].replace("-.","-")
print(final_ans)
